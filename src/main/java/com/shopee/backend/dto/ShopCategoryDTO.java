package com.shopee.backend.dto;

import lombok.Data;

@Data
public class ShopCategoryDTO {
	private Long id;
	private Long idShop;
	private String name;

}
