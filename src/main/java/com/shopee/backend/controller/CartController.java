package com.shopee.backend.controller;

import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.model.pk.CartDetailPK;
import com.shopee.backend.payload.request.cart.CartDetailRequest;
import com.shopee.backend.payload.response.DataResponse;
import com.shopee.backend.payload.response.ListResponse;
import com.shopee.backend.payload.response.cart.GroupCartDetailResponse;
import com.shopee.backend.service.ICartDetailService;
import com.shopee.backend.service.auth.BuyerDetails;

@RestController	@Validated
@RequestMapping("/api/buyer/cart")
public class CartController {

	@Autowired
	ICartDetailService cartDetailService;

	@Value("${app.success}")
	private String success;

	@PutMapping
	public ResponseEntity<?> updateCart(@AuthenticationPrincipal BuyerDetails buyer,
			@RequestParam("id_product_classification") @NotNull Long idProductClassification,
			@RequestParam("quantity") @NotNull Long quantity) {
		CartDetailPK pk = new CartDetailPK(buyer.getId(), idProductClassification);
		return ResponseEntity.ok(cartDetailService.updateCart(pk, quantity));
	}

	@GetMapping
	public ResponseEntity<?> getAll(@AuthenticationPrincipal BuyerDetails buyer) {
		ListResponse<GroupCartDetailResponse> response = new ListResponse<>();
		response.setData(cartDetailService.findByBuyer(buyer.getId()));
		return ResponseEntity.ok(response);
	}

	@PostMapping
	public ResponseEntity<?> addToCart(@RequestBody @Valid CartDetailRequest body,
			@AuthenticationPrincipal BuyerDetails buyer) {
		cartDetailService.addToCart(body, buyer.getId());
		return ResponseEntity.ok(success);
	}

	@DeleteMapping
	public ResponseEntity<?> removeFromCart(@RequestBody @NotEmpty Set<Long> idProducts,
			@AuthenticationPrincipal BuyerDetails buyer) {
		cartDetailService.deleteAllByIds(idProducts, buyer.getId());
		return ResponseEntity.ok(success);
	}

	@DeleteMapping("/all")
	public ResponseEntity<?> removeAllFromCart(@AuthenticationPrincipal BuyerDetails buyer) {
		cartDetailService.deleteAllByBuyer(buyer.getId());
		return ResponseEntity.ok(success);
	}

	@GetMapping("/count")
	public ResponseEntity<?> count(@AuthenticationPrincipal BuyerDetails buyer) {
		DataResponse<Long> response = new DataResponse<>();
		Long count = cartDetailService.countByBuyer(buyer.getId());
		response.setData(count != null ? count : 0);
		return ResponseEntity.ok(response);
	}
}