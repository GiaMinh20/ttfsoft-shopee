package com.shopee.backend.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.payload.request.order.CreateOrderRequest;
import com.shopee.backend.payload.response.CreateOrderResponse;
import com.shopee.backend.payload.response.DataResponse;
import com.shopee.backend.payload.response.OrderManageResponse;
import com.shopee.backend.service.IOrderService;
import com.shopee.backend.service.auth.BuyerDetails;
import com.shopee.backend.service.impl.PaymentService;
import com.shopee.backend.utility.datatype.EPaymentMethod;

@RestController @Validated
@RequestMapping("/api/buyer/order")
public class OrderController {

    @Autowired
    IOrderService orderService;

    @Autowired
    PaymentService paymentService;

    @Value("${data.emailConfirmRequired}")
    private String emailConfirmRequired;
    @Value("${data.emptyPayUrl}")
    private String emptyPayUrl;
    @Value("${data.emptyReturnUrl}")
    private String emptyReturnUrl;

    @PostMapping
    public ResponseEntity<?> createOrder(@AuthenticationPrincipal BuyerDetails buyer,
            HttpServletRequest req, @RequestBody @Valid CreateOrderRequest request) {
        if (buyer.getEmailConfirmed() == null || !buyer.getEmailConfirmed())
            throw new CommonRuntimeException(emailConfirmRequired);
        if (request.getPaymentMethod().equals(EPaymentMethod.ONLINE_PAYMENT_MOMO) 
            && request.getReturnUrl().isEmpty())
            throw new CommonRuntimeException(emptyReturnUrl);
        
        CreateOrderResponse data = orderService.createOrder(request, buyer.getId());

        String payUrl = paymentService.orderConfirm(buyer.getId(), data.getIdOrder(), request.getReturnUrl(), req);

        if (data.getPaymentMethod() == EPaymentMethod.ONLINE_PAYMENT_MOMO && payUrl.isEmpty())
            throw new CommonRuntimeException(emptyPayUrl);
        data.setPayUrl(payUrl);

        return ResponseEntity.ok(new DataResponse<>(data));
    }

    @GetMapping
    public ResponseEntity<List<OrderManageResponse>> getOrdersByBuyer(@AuthenticationPrincipal BuyerDetails buyer) {
        return ResponseEntity.ok(orderService.getOrdersByBuyer(buyer.getId()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<OrderManageResponse> getOrderByShopOrder(@PathVariable("id") Long id) {
        return ResponseEntity.ok(orderService.orderDetail(id));
    }

    @GetMapping("/status")
    public ResponseEntity<List<OrderManageResponse>> getOrderByStatus(@AuthenticationPrincipal BuyerDetails buyer,
            @RequestParam("status") Long status) {
        return ResponseEntity.ok(orderService.getOrderByStatus(buyer.getId(), status));
    }
    
    @GetMapping("/search")
    public ResponseEntity<List<OrderManageResponse>> search(@AuthenticationPrincipal BuyerDetails buyer,
            @RequestParam(value = "keyword", required = true) String keyword){
    	return ResponseEntity.ok(orderService.searchOrders(buyer.getId(), keyword));
    }
}
