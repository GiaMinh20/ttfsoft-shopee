package com.shopee.backend.service.impl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.model.Buyer;
import com.shopee.backend.model.Shop;
import com.shopee.backend.payload.request.auth.EmailConfirmRequest;
import com.shopee.backend.payload.response.BaseResponse;
import com.shopee.backend.repository.BuyerRepository;
import com.shopee.backend.repository.ShopRepository;
import com.shopee.backend.service.IEmailConfirmService;
import com.shopee.backend.utility.RandomString;
import com.shopee.backend.utility.RolePrefix;
import com.shopee.backend.utility.jwt.JwtUtils;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Service
public class EmailConfirmService implements IEmailConfirmService {
	@Autowired
	BuyerRepository buyerRepository;

	@Autowired
	ShopRepository shopRepository;

	@Autowired
	JavaMailSender javaMailSender;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	private JavaMailSender sender;

	@Autowired
	private Configuration config;

	@Value("${spring.mail.username}")
	private String shopeeEmail;

	@Value("${email.content.confirmemail}")
	private String content;

	@Override
	public BaseResponse sendVerifyCode(Long id, String role) {
		String email = null;
		String code = RandomString.get(10);
		String name = "";
		if (role.equals(RolePrefix.SHOP)) {
			Shop shop = shopRepository.getReferenceById(id);
			if (shop.getEmailConfirmed())
				throw new CommonRuntimeException("Email is already confirmed.");
			email = shop.getEmail();
			name = shop.getName();
			shop.setVerificationToken(String.format("%s@email", code));
			shopRepository.save(shop);
		} else if (role.equals(RolePrefix.BUYER)) {
			Buyer buyer = buyerRepository.getReferenceById(id);
			if (buyer.getEmailConfirmed())
				throw new RuntimeException("Email is already confirmed.");
			email = buyer.getEmail();
			name = buyer.getName();
			buyer.setVerificationToken(String.format("%s@email", code));
			buyerRepository.save(buyer);
		}

		if (email == null) {
			throw new CommonRuntimeException("No email found. You must update email address first!");
		}

		MimeMessage message = sender.createMimeMessage();
		try {
			// set mediaType
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
					StandardCharsets.UTF_8.name());
			// add attachment

			Template t = config.getTemplate("email-template.ftl");
			Map<String, Object> model = new HashMap<>();
			model.put("content", content);
			model.put("name", name);
			model.put("email", email);
			model.put("code", code);

			String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);

			helper.setTo(email);
			helper.setText(html, true);
			helper.setSubject("[TTFSOFT-SHOPEE] Email confirmation code");
			helper.setFrom(shopeeEmail);
			sender.send(message);

		} catch (MessagingException | IOException | TemplateException e) {
			throw new CommonRuntimeException(String.format("Send verify code failed (%s)", e.getMessage()));
		}
		return new BaseResponse();
	}

	@Override
	public BaseResponse confirmEmailConfirmToken(Long id, EmailConfirmRequest request, String role) {
		String code = String.format("%s@email", request.getConfirmCode());

		if (role.equals(RolePrefix.SHOP)) {
			Shop shop = shopRepository.getReferenceById(id);
			if (shop.getVerificationToken().equals(code)) {
				shop.setVerificationToken(null);
				shop.setEmailConfirmed(true);
				shopRepository.save(shop);
			} else
				return new BaseResponse(false, "Invalid email confirmation code");
		} else if (role.equals(RolePrefix.BUYER)) {
			Buyer buyer = buyerRepository.getReferenceById(id);
			if (buyer.getVerificationToken().equals(code)) {
				buyer.setVerificationToken(null);
				buyer.setEmailConfirmed(true);
				buyerRepository.save(buyer);
			} else
				return new BaseResponse(false, "Invalid email confirmation code");
		}

		return new BaseResponse();
	}
}
