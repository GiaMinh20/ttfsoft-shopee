package com.shopee.backend.payload.response.statistic;

import java.util.ArrayList;
import java.util.List;

import com.shopee.backend.dto.holder.AdminDataHolder;
import com.shopee.backend.dto.holder.DataHolder;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StatisticResponse {
	private Boolean success;
	private String messsage = "";
	private List<DataHolder> dataHolder = new ArrayList<>();
	private AdminDataHolder adminDataHolder;
	private Long workingVoucher = 0L;
	private Long saleAmount = 0L;
}
