package com.shopee.backend.payload.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class WorkPerformance {
	private Long bannedProduct;
	private Double canceledOrderRate;
	private Double rating;
	private Double avgTimePrepare;
	
}
