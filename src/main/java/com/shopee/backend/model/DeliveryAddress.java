package com.shopee.backend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "delivery_address")
public class DeliveryAddress {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_buyer")
	private Buyer buyer;

	@Column(length = 30)
	@NotBlank
	private String country;

	@NotBlank
	@Column(length = 30)
	private String state;

	@Column(length = 30)
	@NotBlank
	private String city;

	@Column(length = 120)
	@NotBlank
	private String address;

	@Column(length = 120)
	@NotBlank
	private String name;

	@Column(length = 10)
	private String phone;

	@Column(name = "is_default")
	@NotNull
	private Boolean isDefault;

	@Version
	@Column(columnDefinition = "bigint DEFAULT 0", nullable = false)
	private Long version = 0L;
}
