package com.shopee.backend.model;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import com.shopee.backend.utility.datatype.EPaymentMethod;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "orders")
public class Order {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@OneToMany(mappedBy = "order", fetch = FetchType.LAZY)
	private List<ProductReview> reviews = new ArrayList<>();

	@OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	private List<ShopOrder> shopOrders = new ArrayList<>();

	@ManyToOne
	@JoinColumn(name = "id_buyer")
	private Buyer buyer;

	@ManyToOne
	@JoinColumn(name = "id_address")
	private DeliveryAddress address;

	@Column(name = "create_time")
	private Date createTime;
	
	@Column(name = "price_off")
	private Long priceOff = 0L;

	@Column(name = "price_orginal")
	private Long priceOrginal = 0L;

	@Column(name = "platform_coupon")
	private String platformCoupon;

	@Column(name = "freeship_coupon")
	private String freeshipCoupon;

	@Column(name = "price_final")
	private Long priceFinal;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "payment_method")
	private EPaymentMethod paymentMethod;

	private Date complete;


	public void addPriceOriginal(Long price){
		priceOrginal += price;
	}

	public void addPriceOff(Long price){
		priceOff += price;
	}
	@PrePersist
    public void setPriceFinal() {
		priceFinal = priceOrginal - priceOff;
    }
}