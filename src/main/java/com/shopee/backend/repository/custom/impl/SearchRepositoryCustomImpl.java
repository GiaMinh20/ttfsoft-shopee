package com.shopee.backend.repository.custom.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.query.QueryUtils;
import org.springframework.stereotype.Repository;

import com.shopee.backend.model.Product;
import com.shopee.backend.model.ProductCategory_;
import com.shopee.backend.model.ProductType;
import com.shopee.backend.model.ProductType_;
import com.shopee.backend.model.Product_;
import com.shopee.backend.model.SellPlace_;
import com.shopee.backend.repository.custom.SearchRepositoryCustom;
import com.shopee.backend.utility.Page;
import com.shopee.backend.utility.datatype.EProductStatus;

@Repository
public class SearchRepositoryCustomImpl implements SearchRepositoryCustom {
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Product> getSearchProduct(String keyword, Page page, String locations, Long idCategory, Long idType,
			Long maxPrice, Long minPrice, Double rate, Boolean isNew) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Product> cq = cb.createQuery(Product.class);
		Root<Product> root = cq.from(Product.class);
		List<Predicate> filters = new ArrayList<>();

		if (locations != null) {
			filters.add(cb.equal(root.get(Product_.sellPlace).get(SellPlace_.CODE), locations));
		}
		if (idCategory != null) {
			filters.add(cb.equal(root.get(Product_.productCategory).get(ProductCategory_.id), idCategory));
		}
		if (maxPrice != null) {
			filters.add(cb.lessThanOrEqualTo(root.get(Product_.price), maxPrice));
		}
		if (minPrice != null) {
			filters.add(cb.greaterThanOrEqualTo(root.get(Product_.price), minPrice));
		}
		if (rate != null) {
			filters.add(cb.greaterThanOrEqualTo(root.get(Product_.averageRating), rate));
		}
		if (page != null) {
			cq.orderBy(QueryUtils.toOrders(page.getSort(), root, cb));
		}
		filters.add(cb.equal(root.get(Product_.status), EProductStatus.ACTIVE));
		filters.add(cb.equal(root.get(Product_.isNew), isNew));
		if (idType != null) {
			Long level = getLevel(idType);
			switch (level.intValue()) {
			case 3:
				filters.add(cb.equal(root.get(Product_.productType).get(ProductType_.id), idType));
				break;
			case 2:
				List<Long> arr = new ArrayList<>();
				arr.add(idType);
				List<Long> ids = getIds(arr);
				ids.add(idType);
				Expression<Long> lstIdType = root.get(Product_.productType).get(ProductType_.id);
				filters.add(lstIdType.in(ids));
				break;
			case 1:
				List<Long> arr1 = new ArrayList<>();
				arr1.add(idType);
				List<Long> ids1 = getIds(arr1);
				ids1.add(idType);
				// Ex: 4 -> #('86','87','88','89','90','4')
				ids1 = getIds(ids1);
				// -> 86,87,88,89,90,201,202,203,204,205,206,207,208,209
				Expression<Long> lstIdType1 = root.get(Product_.productType).get(ProductType_.id);
				filters.add(lstIdType1.in(ids1));
				break;
			default:
				break;
			}
		}

		Predicate filter = cb.and(filters.toArray(new Predicate[0]));

		filter = cb.and(filter, cb.or(cb.like(root.get(Product_.name), "%" + keyword + "%"),
				cb.like(root.get(Product_.description), "%" + keyword + "%")));
		cq.select(root).where(filter);
		TypedQuery<Product> query = em.createQuery(cq);
		if (page != null) {
			query.setFirstResult(page.getPageNumber() * page.getPageSize());
			query.setMaxResults(page.getPageSize());
		}
		return query.getResultList();
	}

	@Override
	public Long getCountSearchProduct(String keyword, Page page, String locations, Long idCategory, Long idType,
			Long maxPrice, Long minPrice, Double rate, Boolean isNew) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<Product> root = cq.from(Product.class);
		List<Predicate> filters = new ArrayList<>();

		if (locations != null) {
			filters.add(cb.equal(root.get(Product_.sellPlace).get(SellPlace_.CODE), locations));
		}
		if (idCategory != null) {
			filters.add(cb.equal(root.get(Product_.productCategory).get(ProductCategory_.id), idCategory));
		}
		if (maxPrice != null) {
			filters.add(cb.lessThanOrEqualTo(root.get(Product_.price), maxPrice));
		}
		if (minPrice != null) {
			filters.add(cb.greaterThanOrEqualTo(root.get(Product_.price), minPrice));
		}
		if (rate != null) {
			filters.add(cb.greaterThanOrEqualTo(root.get(Product_.averageRating), rate));
		}
//		if (keyword != null && !keyword.isBlank()) {
//			filters.add(cb.like(root.get(Product_.name), "%" + keyword + "%"));
//			filters.add(cb.like(root.get(Product_.description), "%" + keyword + "%"));
//		}
		filters.add(cb.equal(root.get(Product_.status), EProductStatus.ACTIVE));
		filters.add(cb.equal(root.get(Product_.isNew), isNew));
		if (idType != null) {
			Long level = getLevel(idType);
			switch (level.intValue()) {
			case 3:
				filters.add(cb.equal(root.get(Product_.productType).get(ProductType_.id), idType));
				break;
			case 2:
				List<Long> arr = new ArrayList<>();
				arr.add(idType);
				List<Long> ids = getIds(arr);
				ids.add(idType);
				Expression<Long> lstIdType = root.get(Product_.productType).get(ProductType_.id);
				filters.add(lstIdType.in(ids));
				break;
			case 1:
				List<Long> arr1 = new ArrayList<>();
				arr1.add(idType);
				List<Long> ids1 = getIds(arr1);
				ids1.add(idType);
				// Ex: 4 -> #('86','87','88','89','90','4')
				ids1 = getIds(ids1);
				// -> 86,87,88,89,90,201,202,203,204,205,206,207,208,209
				Expression<Long> lstIdType1 = root.get(Product_.productType).get(ProductType_.id);
				filters.add(lstIdType1.in(ids1));
				break;
			default:
				break;
			}
		}

		Predicate filter = cb.and(filters.toArray(new Predicate[0]));

		filter = cb.and(filter, cb.or(cb.like(root.get(Product_.name), "%" + keyword + "%"),
				cb.like(root.get(Product_.description), "%" + keyword + "%")));
		
		cq.select(cb.count(root)).where(filter);
		return em.createQuery(cq).getSingleResult();
	}

	@Transactional
	@Override
	public void refresh(Product product) {
		em.refresh(product);
	}

	public Long getLevel(Long id) {
		/*
		 * Get the level of product type: 1 or 2 or 3
		 */
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<ProductType> root = cq.from(ProductType.class);
		Predicate pd = cb.equal(root.get(ProductType_.id), id);
		cq.select(root.get(ProductType_.level)).where(pd);
		return em.createQuery(cq).getSingleResult();
	}

	public List<Long> getIds(List<Long> ids) {
		// Get the id of the product type that their parent id is in the specified
		// array.
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<ProductType> root = cq.from(ProductType.class);
		Expression<Long> parentIds = root.get(ProductType_.parent).get(ProductType_.id);

		Predicate pd = parentIds.in(ids);
		cq.select(root.get(ProductType_.id)).where(pd);
		return em.createQuery(cq).getResultList();
	}
}
