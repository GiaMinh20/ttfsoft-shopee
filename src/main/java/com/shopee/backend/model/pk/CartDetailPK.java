package com.shopee.backend.model.pk;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class CartDetailPK implements Serializable {
	private static final long serialVersionUID = -1368353305031820607L;
	
	@Column(name="id_buyer")
	private Long idBuyer;
	
	@Column(name="id_product_classification")
	private Long idProductClassification;
}
