package com.shopee.backend.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Data;

@Data
public class FavoriteDTO{
	@JsonProperty(access = Access.WRITE_ONLY)
	private Long idUser;
	private ProductDTO product;
}
