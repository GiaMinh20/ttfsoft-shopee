package com.shopee.backend.payload.response;

import java.util.ArrayList;
import java.util.List;

import com.shopee.backend.dto.ProductDTO;

import lombok.Data;

@Data
public class ProductResponse {
	private Boolean success;
	private int page;
	private int totalPage;
	private List<ProductDTO> products = new ArrayList<>();
}
