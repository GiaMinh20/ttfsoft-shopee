package com.shopee.backend.payload.request.shopcategory;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter

public class EditShopCategoryRequest {
	@NotBlank
	private String name;
}
