package com.shopee.backend.service.impl;

import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.dto.FollowDTO;
import com.shopee.backend.model.Follow;
import com.shopee.backend.model.pk.FollowPK;
import com.shopee.backend.payload.request.follow.FollowRequest;
import com.shopee.backend.payload.response.BaseResponse;
import com.shopee.backend.payload.response.ListWithPagingResponse;
import com.shopee.backend.repository.BuyerRepository;
import com.shopee.backend.repository.FollowRepository;
import com.shopee.backend.repository.ShopRepository;
import com.shopee.backend.utility.Page;

@Service
public class FollowService {
	@Autowired
	FollowRepository followRepository;

	@Autowired
	BuyerRepository buyerRepository;

	@Autowired
	ShopRepository shopRepository;

	@Autowired
	ModelMapper modelMapper;

	public ListWithPagingResponse<FollowDTO> get(Long idBuyer, Integer page, Integer size, Sort sort) {
		int count = followRepository.countBuyerFollowList(idBuyer).intValue();
		Page paging = new Page(page, size, count, sort);

		return listWithPagingResponseMakeup(followRepository.getBuyerFollowList(idBuyer, paging), paging);
	}

	public BaseResponse add(Long idBuyer, FollowRequest request) {
		if (!shopRepository.existsById(request.getIdShop())) {
			throw new CommonRuntimeException("No shop found with given id");
		}
		FollowPK id = new FollowPK(idBuyer, request.getIdShop());
		if (followRepository.findById(id).isPresent()) {
			throw new CommonRuntimeException("Already follow this shop");
		}

		followRepository.save(new Follow(id, buyerRepository.getReferenceById(idBuyer),
				shopRepository.getReferenceById(request.getIdShop()), new Date(new java.util.Date().getTime())));

		return new BaseResponse();
	}

	public BaseResponse remove(Long idBuyer, Long idShop) {
		if (!shopRepository.existsById(idShop)) {
			throw new CommonRuntimeException("No shop found with given id");
		}

		FollowPK id = new FollowPK(idBuyer, idShop);
		if (followRepository.findById(id).isEmpty()) {
			throw new CommonRuntimeException("You haven't follow this shop yet");
		}

		Follow fl = followRepository.getReferenceById(id);
		followRepository.delete(fl);
		return new BaseResponse();
	}

	private ListWithPagingResponse<FollowDTO> listWithPagingResponseMakeup(List<Follow> follows, Page page) {
		return new ListWithPagingResponse<>(page.getPageNumber(), page.getTotalPage(),
				follows.stream().map(p -> modelMapper.map(p, FollowDTO.class)).toList());
	}
}
