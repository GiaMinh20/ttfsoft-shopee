package com.shopee.backend.repository.custom;

public interface ShopRepositoryCustom {
	boolean setPassword(String email, String hashedPassword);
	Long getIdByEmail(String email);
	boolean setEmailConfirmed(Long id);
}
