package com.shopee.backend.service;

import java.util.Collection;
import java.util.List;

import com.shopee.backend.model.CartDetail;
import com.shopee.backend.model.pk.CartDetailPK;
import com.shopee.backend.payload.request.cart.CartDetailRequest;
import com.shopee.backend.payload.response.BaseResponse;
import com.shopee.backend.payload.response.cart.CartDetailResponse;
import com.shopee.backend.payload.response.cart.GroupCartDetailResponse;

public interface ICartDetailService {
    CartDetail findById(CartDetailPK id);

    List<CartDetail> findAllById(Collection<Long> idPs, Long idBuyer);

    CartDetailResponse addToCart(CartDetailRequest requestBody, Long idBuyer);

    void delete(CartDetail entity);

    void deleteAllByIds(Iterable<Long> idProducts, Long idBuyer);

    void deleteAllByBuyer(Long idBuyer);

    List<GroupCartDetailResponse> findByBuyer(Long idBuyer);

    Long countByBuyer(Long idBuyer);

	BaseResponse updateCart(CartDetailPK pk, Long quantity);
}
