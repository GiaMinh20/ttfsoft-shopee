package com.shopee.backend.dto;

import lombok.Data;

@Data
public class CartDetailDTO {
	private Long idBuyer;
	private Long idProductClassification;
	private Long quantity;
}
