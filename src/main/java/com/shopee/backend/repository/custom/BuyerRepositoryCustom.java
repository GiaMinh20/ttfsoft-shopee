package com.shopee.backend.repository.custom;

public interface BuyerRepositoryCustom {
	boolean setPassword(String email, String hashedPassword);
	Long getIdByEmail(String email);
	boolean setEmailConfirmed(Long id);
}
