package com.shopee.backend.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.shopee.backend.model.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "product_category")
public class ProductCategory extends BaseModel {
	@OneToMany(mappedBy = "productCategory")
	private List<Product> products = new ArrayList<>();

	@OneToOne
	@JoinColumn(name = "id_image")
	private MediaResource image;

	@Column(length = 20)
	@NotBlank
	private String name;

	private Date dod;

	public ProductCategory() {

	}

	public ProductCategory(MediaResource image, @NotBlank String name, Date dod) {
		this.image = image;
		this.name = name;
		this.dod = dod;
	}

	@PreRemove
	private void preRemove() {
		for (Product p : products) {
			p.setProductCategory(null);
		}
	}
}
