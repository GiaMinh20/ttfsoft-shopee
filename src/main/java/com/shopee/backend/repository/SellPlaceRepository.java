package com.shopee.backend.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shopee.backend.model.SellPlace;

@Repository
public interface SellPlaceRepository extends JpaRepository<SellPlace, Long>{
	Optional<SellPlace> findByCode(String code);
}
