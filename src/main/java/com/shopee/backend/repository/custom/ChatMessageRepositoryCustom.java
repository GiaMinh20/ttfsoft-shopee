package com.shopee.backend.repository.custom;

import java.util.List;

import com.shopee.backend.model.ChatMessage;
import com.shopee.backend.utility.Page;

public interface ChatMessageRepositoryCustom {

	List<ChatMessage> get(Long idBuyer, Long idShop, Page page);
	
	Long count(Long idBuyer, Long idShop);

}