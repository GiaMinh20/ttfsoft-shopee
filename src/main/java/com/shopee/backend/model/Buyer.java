package com.shopee.backend.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.shopee.backend.model.base.BaseModel;
import com.shopee.backend.utility.datatype.EGender;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "buyer")
public class Buyer extends BaseModel {
	@OneToMany(mappedBy = "buyer", fetch = FetchType.LAZY)
	private List<FavoriteBuyer> favoriteBuyers;

	@OneToMany(mappedBy = "buyer", fetch = FetchType.LAZY)
	private List<CartDetail> cartDetails;

	@OneToMany(mappedBy = "buyer", fetch = FetchType.LAZY)
	private List<Order> orders;

	@OneToMany(mappedBy = "buyer", fetch = FetchType.LAZY)
	private List<DeliveryAddress> addresses;

	@OneToMany(mappedBy = "buyer", fetch = FetchType.LAZY)
	private List<Notification> notifications;

	@OneToMany(mappedBy = "buyer", fetch = FetchType.LAZY)
	private List<BuyerCoupon> buyerCoupons;

	@OneToMany(mappedBy = "buyer", fetch = FetchType.LAZY)
	private List<Follow> follows;

	@OneToMany(mappedBy = "buyer", fetch = FetchType.LAZY)
	private List<Message> messages;

	@OneToOne
	@JoinColumn(name = "id_avatar")
	private MediaResource avatar;

	@NotEmpty
	@Column(length = 50)
	private String username;

	@Column(length = 120)
	private String password;

	@NotEmpty
	@Column(length = 50, unique = true)
	private String email;

	@NotEmpty
	@Column(length = 50)
	private String name;

	@Column(length = 10, unique = true)
	private String phone;

	@NotNull
	@Column(name = "phone_confirmed")
	private Boolean phoneConfirmed;

	@Column(name = "email_confirmed")
	@NotNull
	private Boolean emailConfirmed;

	@Enumerated(EnumType.STRING)
	private EGender gender;

	private Date dob; // date of birth

	@Column(name = "is_active")
	@NotNull
	private Boolean isActive;

	private String verificationToken;

	@Column(name = "deliver_addresses", columnDefinition = "bigint default 0")
	@Max(value = 5)
	@Min(value = 0)
	private Long deliveryAddresses;

	@Version
	@Column(columnDefinition = "bigint DEFAULT 0", nullable = false)
	private Long version = 0L;

	public Buyer(@NotBlank String username, @NotBlank String password, String phone, @NotNull Boolean phoneConfirmed,
			@NotBlank String email, @NotNull Boolean emailConfirmed, @NotBlank String name, EGender gender, Date dob,
			@NotNull Boolean isActive, Long deliveryAddresses) {
		super();
		this.username = username;
		this.password = password;
		this.phone = phone;
		this.phoneConfirmed = phoneConfirmed;
		this.email = email;
		this.emailConfirmed = emailConfirmed;
		this.name = name;
		this.gender = gender;
		this.dob = dob;
		this.isActive = isActive;
		this.deliveryAddresses = deliveryAddresses;
	}
}
