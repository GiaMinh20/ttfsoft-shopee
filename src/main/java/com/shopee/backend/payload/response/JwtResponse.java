package com.shopee.backend.payload.response;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class JwtResponse<T> extends BaseResponse {
	private String token;
	private final String type = "Bearer";
	private T userInfo;
	
	public JwtResponse(String token, T userInfo) {
		super(true, "");
		this.token = token;
		this.userInfo = userInfo;
	}
}