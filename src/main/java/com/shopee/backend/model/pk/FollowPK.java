package com.shopee.backend.model.pk;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@Embeddable
public class FollowPK implements Serializable {
	private static final long serialVersionUID = -6409088180192382973L;

	@Column(name="id_buyer")
	private Long idBuyer;

	@Column(name="id_shop")
	private Long idShop;

	public FollowPK(Long idBuyer, Long idShop) {
		super();
		this.idBuyer = idBuyer;
		this.idShop = idShop;
	}
}
