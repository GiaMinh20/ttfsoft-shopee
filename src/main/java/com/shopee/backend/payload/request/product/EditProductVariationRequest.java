package com.shopee.backend.payload.request.product;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

import com.shopee.backend.utility.datatype.EProductClassificationStatus;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EditProductVariationRequest {
	@NotNull
	private Long idVariation;
	
	private String variationName;
	
	@Range(min = 1)
	private Long price;
	
	private Long availableQuantity;
	
	private Double discount;
	
	private EProductClassificationStatus status;
}