package com.shopee.backend.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.shopee.backend.model.listener.ProductReviewListener;
import com.shopee.backend.model.pk.ProductReviewPK;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(ProductReviewListener.class)
@Entity
@Table(name = "product_review")
public class ProductReview { // only one review for one product per order
	@EmbeddedId
	private ProductReviewPK id;

	@OneToMany(cascade = CascadeType.ALL)
	private List<MediaResource> medias;

	@MapsId("id_order")
	@ManyToOne
	@JoinColumn(name = "id_order", insertable = false, updatable = false)
	private Order order;

	@MapsId("id_shop_order")
	@ManyToOne
	@JoinColumn(name = "id_shop_order")
	private ShopOrder shopOrder;

	@MapsId("idProductClassification")
	@ManyToOne
	@JoinColumn(name = "id_product_classification", insertable = false, updatable = false)
	private ProductClassification productClassification;

	@Lob
	@Column(length = 65535)
	@NotBlank
	private String content;

	@NotNull
	private Integer rating;

}
