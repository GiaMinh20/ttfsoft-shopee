<div>


    <table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="m_6145247629272310561backgroundTable">
        <tbody>
        <tr>
            <td>
                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">
                    <tbody>
                    <tr>
                        <td width="100%">
                            <table bgcolor="#ffffff" width="600" cellpadding="0" cellspacing="0" border="0" align="center">
                                <tbody>
                                <tr>
                                    <td>
                                        <table width="560" align="center" cellpadding="0" cellspacing="0" border="0">
                                            <tbody>
                                            
                                            <tr>
                                                <td align="center"><img src="https://ci6.googleusercontent.com/proxy/pYFHITRPqvhJTw_S-07ijzWoaEBQAzRP2pEx0td9IIihdWyPRk-YTRHGwEkm3CIgZtGxN3yi13JhXCixF_4piF9xAjvOdFZqQX0jD1E=s0-d-e1-ft#https://cf.shopee.sg/file/0cd023d64f04491f3dc8076d6932dfdc" width="140" height="auto" style="width:25%;height:auto" class="CToWUd" data-bit="iit"></td>
                                            </tr>
                                            
                                            
                                            
                                            <tr>
                                                <td height="10" style="font-size:1px;line-height:1px">&nbsp;</td>
                                            </tr>
                                            
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    
    
        
    <table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="m_6145247629272310561backgroundTable">
        <tbody>
        <tr>
            <td>
                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">
                    <tbody>
                    <tr>
                        <td width="100%">
                            <table bgcolor="#ffffff" width="600" cellpadding="0" cellspacing="0" border="0" align="center">
                                <tbody>
                                
                                <tr>
                                    <td height="10" style="font-size:1px;line-height:1px">&nbsp;</td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        <table width="560" align="center" cellpadding="0" cellspacing="0" border="0">
                                            <tbody>
                                            
            
        <tr>
            <td style="font-family:Helvetica,arial,sans-serif;font-size:13px;color:#000000;text-align:left;line-height:18px">
                Dear ${name},
            </td>
        </tr>
    
            
        <tr>
            <td width="100%" height="10" style="font-size:1px;line-height:1px">&nbsp;</td>
        </tr>
    
            <tr>
                <td style="font-family:Helvetica,arial,sans-serif;font-size:13px;color:#000000;text-align:left;line-height:18px">
					${content}
                 <br><br>
                    Your confirmation code is ${code}
                    
                </td>
            </tr>
            
    <tr><td><br>
    </td></tr><tr>
        <td style="font-family:Helvetica,arial,sans-serif;font-size:13px;color:#000000;text-align:left;line-height:18px">
            <br>
            Best regards,<br>
            Shopee team
        </td>
    </tr>
    
            
        <tr>
            <td style="font-family:Helvetica,arial,sans-serif;font-size:13px;color:#000000;text-align:left;line-height:18px">
                <br>
                If you have any question, please contact us <a href="https://help.shopee.vn/vn/s/contactusform" style="text-decoration:underline!important;color:#ff5722" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://help.shopee.vn/vn/s/contactusform&amp;source=gmail&amp;ust=1660299954794000&amp;usg=AOvVaw3-Lsy6-3-bmxXYI7IIvpbJ">here</a>.
            </td>
        </tr>
    
        
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" height="1" bgcolor="#ffffff" style="font-size:1px;line-height:1px">&nbsp;</td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    
    
        
    
    
    
    
    <table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="m_6145247629272310561backgroundTable">
        <tbody>
        <tr>
            <td>
                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">
                    <tbody>
                    <tr>
                        <td width="100%">
                            <table bgcolor="#ffffff" width="600" cellpadding="0" cellspacing="0" border="0" align="center">
                                <tbody>
                                
                                <tr>
                                    <td height="20" style="font-size:1px;line-height:1px">&nbsp;</td>
                                </tr>
                                
    
                                <tr>
                                    <td>
                                        <table width="560" align="center" cellpadding="0" cellspacing="0" border="0">
                                            <tbody>
                                            
                                            <tr>
                                                <td width="100%" height="10" style="font-size:1px;line-height:1px">&nbsp;</td>
                                            </tr>
                                            
                                            <tr>
                                                <td width="100%" height="10" style="font-size:1px;line-height:1px">&nbsp;</td>
                                            </tr>
                                            
                                            <tr><td style="font-family:Helvetica,arial,sans-serif;font-size:13px;color:#747474;text-align:center;line-height:18px">
                                                Let's shop with shopee
                                                </td></tr>
                                            <tr>
                                                <td width="100%" height="5" style="font-size:1px;line-height:1px">&nbsp;</td>
                                            </tr>
                                            <tr><td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody><tr>
                                                    <td width="160"></td>
                                                    <td width="130"><a href="https://play.google.com/store/apps/details?id=com.shopee.vn" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://play.google.com/store/apps/details?id%3Dcom.shopee.vn&amp;source=gmail&amp;ust=1660299954794000&amp;usg=AOvVaw0Sg6_4obubzRB20trkZDb1"><img src="https://ci3.googleusercontent.com/proxy/wMyUGP_9zlO1kmTJ1wI6w5tG3QYq6dXydCJg0ePOV7p6DUBeZlw99BuZZlU0LOW8jD20PqkxMfCK8ZAGJ7m0OnXAWokK0I08RWyEqio=s0-d-e1-ft#https://cf.shopee.sg/file/cacc3e27277d02501b0989fdcbaf18e9" width="130" style="width:130px" class="CToWUd" data-bit="iit"></a></td>
                                                    <td width="5"></td>
                                                    <td width="130"><a href="https://apps.apple.com/vn/app/id959841449" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://apps.apple.com/vn/app/id959841449&amp;source=gmail&amp;ust=1660299954794000&amp;usg=AOvVaw1Qivp8pwehjYpVR35hn3mM"><img src="https://ci5.googleusercontent.com/proxy/QL-v7elCQEI_UfK0bI8s2Ao8TVj7pEBorNit381WqoEd5N9eGa4_wJTlep4n0Qex9vDeO83sinIwTPk1Ke5WyhSWXaEd9vGIfYuYSE8=s0-d-e1-ft#https://cf.shopee.sg/file/5b4dcec6c9c60950954b465bafee9cff" width="130" style="width:130px" class="CToWUd" data-bit="iit"></a></td>
                                                    <td width="160"></td>
                                                </tr>
                                            </tbody></table>
                                            
                                            </td></tr><tr>
                                                <td width="100%" height="5" style="font-size:1px;line-height:1px">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    
    <div style="width:100%;height:5px;display:block" align="center">
        <div style="width:100%;max-width:600px;height:1px;border-top:1px solid #e0e0e0">
        </div>
    </div>
    
    
    <table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="m_6145247629272310561backgroundTable">
        <tbody>
        <tr>
            <td>
                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">
                    <tbody>
                    <tr>
                        <td width="100%">
                            <table bgcolor="#ffffff" width="600" cellpadding="0" cellspacing="0" border="0" align="center">
                                <tbody>
                                <tr>
                                    <td>
                                        <table width="560" align="center" cellpadding="0" cellspacing="0" border="0">
                                            <tbody>
                                            <tr>
                                                <td colspan="5" height="10" style="font-size:1px;line-height:1px;text-overflow:ellipsis;color:#ffffff">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="112"></td>
                                                <td width="112" align="center">
                                                    <a href="https://vi-vn.facebook.com/ShopeeVN/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://vi-vn.facebook.com/ShopeeVN/&amp;source=gmail&amp;ust=1660299954794000&amp;usg=AOvVaw2-kaJeXJPgo1W94SHJrj2H"><img src="https://ci3.googleusercontent.com/proxy/ZAMWMy2h8H1T9U-n9cKrywcZMowU7HGBgsW60ppcLZhTm0Et6x006D__Eg5MU_nymArZ4P-KahqPBkozNOUBAwPh49rUi9xtRZ1q7A=s0-d-e1-ft#https://f.shopee.sg/file/3814109aa6a3721b577919d5c8a36cfe" width="40" style="height:40px;width:40px" class="CToWUd" data-bit="iit"></a>
                                                </td>
                                                <td width="112" align="center">
                                                    <a href="https://www.instagram.com/shopee_vn/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.instagram.com/shopee_vn/&amp;source=gmail&amp;ust=1660299954794000&amp;usg=AOvVaw05N7qJypmYKiava_mN_Keh"><img src="https://ci6.googleusercontent.com/proxy/pX1avZ3xttx5eI3Pzufh9tapusOVMGXuZGyH7jhY6P8LJGfZLRMafC8MS51E_4XAkXJdTvi1LtS67-gXjtkEkNoRY3LV15SK6VxH5A=s0-d-e1-ft#https://f.shopee.sg/file/3b08ded58cbbb9beb74ec4d5ad4c7d53" width="40" style="height:40px;width:40px" class="CToWUd" data-bit="iit"></a>
                                                </td>
                                                <td width="112" align="center">
                                                    <a href="https://www.youtube.com/channel/UCKcxoGpxOJx3nt83MCG-nuQ" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.youtube.com/channel/UCKcxoGpxOJx3nt83MCG-nuQ&amp;source=gmail&amp;ust=1660299954794000&amp;usg=AOvVaw2d82FDIPMIg4U8H0jhv71y"><img src="https://ci3.googleusercontent.com/proxy/qFJz0coveqY6pC_KqKpGuOsGvEE5mHrUVO35lmGLPmoJWCp66WvIRk8Tu5J-RpRbygqXSOTkj645s34Ticzxs_OPbMhf8e079bRTfQ=s0-d-e1-ft#https://f.shopee.sg/file/e7ab71dae1223ca9eb4b9f120353a31e" width="40" style="height:40px;width:40px" class="CToWUd" data-bit="iit"></a>
                                                </td>
                                                <td width="112"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" height="10" style="font-size:1px;line-height:1px">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    
    <div style="width:100%;height:15px;display:block" align="center">
        <div style="width:100%;max-width:600px;height:1px;border-top:1px solid #e0e0e0">
            &nbsp;
        </div>
    </div>
    
    
    
    <table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="m_6145247629272310561backgroundTable">
        <tbody>
        <tr>
            <td>
                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">
                    <tbody>
                    <tr>
                        <td width="100%">
                            <table bgcolor="#ffffff" width="600" cellpadding="0" cellspacing="0" border="0" align="center">
                                <tbody>
                                <tr>
                                    <td>
                                        <table width="560" align="center" cellpadding="0" cellspacing="0" border="0">
                                            <tbody>
                                            <tr>
                                                <td style="font-family:Helvetica,arial,sans-serif;font-size:11px;color:#747474;text-align:center;line-height:100%"><a href="https://shopee.vn/docs/3603" style="text-decoration:none;color:#ff5722" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://shopee.vn/docs/3603&amp;source=gmail&amp;ust=1660299954794000&amp;usg=AOvVaw1o6Qf644F1K-bel3zNyvX0">Privacy Policy</a> | <a href="https://shopee.vn/legaldoc/policies/" style="text-decoration:none;color:#ff5722" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://shopee.vn/legaldoc/policies/&amp;source=gmail&amp;ust=1660299954794000&amp;usg=AOvVaw3ILpubY8U0nPnD4Gl7WCvq">Shopee Terms</a></td>
                                            </tr>
    
                                            
                                            <tr>
                                                <td width="100%" height="10" style="font-size:1px;line-height:1px">&nbsp;</td>
                                            </tr>
                                            
    
                                            <tr>
                                                <td style="font-family:Helvetica,arial,sans-serif;font-size:11px;color:#747474;text-align:center;line-height:100%">
                                                    This is an automated email. Please do not reply to this email. Add <a href="mailto:info@shopee.vn" style="text-decoration:none;color:#ff5722" target="_blank">info@shopee.vn</a> to your email contacts to ensure you always receive emails from us.
                                            </tr>
    
                                            
                                            <tr>
                                                <td width="100%" height="10" style="font-size:1px;line-height:1px">&nbsp;</td>
                                            </tr>
                                            
    
                                            <tr>
                                                <td style="font-family:Helvetica,arial,sans-serif;font-size:11px;color:#747474;text-align:center;line-height:100%">Ho Chi Minh City University of Technology and Education.</td>
                                            </tr>
    
                                            
                                            <tr>
                                                <td width="100%" height="40" style="font-size:1px;line-height:1px">&nbsp;</td>
                                            </tr>
                                            
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    
    
    <div style="white-space:nowrap!important;line-height:0;color:#ffffff">
        - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    </div>
    <img width="1px" height="1px" alt="" src="https://ci4.googleusercontent.com/proxy/rdv19fafVtlslL_W7AU_EI11OEVJVO4WbhQID77GXNlSQydcKe_7AvD3ZHWx65Mer587Qv1_-yTA-c6R5FFQA-7YEk66SMctRrxiqoFU2ocDTlWVirudA8uJNVT5zyJVPDTBmsHPRfXCGt-6bxWxbEJyhftGjle0MZzXBerPh0oFeZZL_Xuv2Vcv_WLzN5dGxYMErMxcGx8YyZZ-y9iaJANZFWC8Ij69JP-k3eMHdErDObSgD3JWZdPxTVNtKlXE4GxdYoEId3u1FeFddovthX5cnNc4UTu08nV3GgGtazt4RsRKDqgcofHk3saEvamVoxmySLWFMKno639vjnbq3EkPt_PLi6i_8tii-5S9LvZDlqAzu7Iclvhn1yrVzLTmyZnj6dJWdyiVDN5v7rzU3ii5DXRKfDXm7DROPZniipTBnmL_mY8bOWuGxGm3BBYkWplk_IJG4xdErY7XPO6xhn6D1iWDQnu0KUR04sZP7r_myeGeOBUyMQGS5Qms4ApBCOpJWNG8sNzBBmGZDGzXFeOTjg=s0-d-e1-ft#https://email.mg.shopee.vn/o/eJxNkM2OwyAMhJ-m3IgI_zlw7XGPe40ocRIUAhXQtH37pe1Kuxfb0ow_jf3AEA8T_AEITC-F1lpyrdEDZ1h8iub7C_k_gQ-cDUIJNnSUdYxqJhTRlHM5SKYY7U-cbLr0-J7yBhmXBb_YmPVdWdMVYE8XH6BzaUcV9muwFcZodzDWuXSL9UTPv9N4QPbzsyUpECfIxsc5NfyH0x2xKUtbv9snfhN268Nyi-PHML4NNVsHxl2WwQdV7DYUJdehlJWgamKqvrVr9in7-kSrkYJSNlnC1czF7BTpgUhqxXxxRAmtGtGlWG47TLj6_d_HOMrGhlimLdi11TW0qMsr0vvWyTjlZgE_HEN4Kg" class="CToWUd" data-bit="iit" jslog="138226; u014N:xr6bB; 53:W2ZhbHNlLDJd"></div>