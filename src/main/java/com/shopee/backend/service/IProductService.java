package com.shopee.backend.service;

import java.io.IOException;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.dto.ProductDTO;
import com.shopee.backend.dto.ProductGeneralDTO;
import com.shopee.backend.model.ProductClassification;
import com.shopee.backend.payload.request.product.CreateProductRequest;
import com.shopee.backend.payload.request.product.EditProductRequest;
import com.shopee.backend.payload.response.DataResponse;
import com.shopee.backend.payload.response.ListWithPagingResponse;
import com.shopee.backend.utility.datatype.EProductStatus;
import com.shopee.backend.utility.datatype.EUserRole;

public interface IProductService {

	DataResponse<ProductDTO> create(Long idShop, CreateProductRequest request, MultipartFile avatar,
			List<MultipartFile> media) throws IOException;

	/**
	 * Get product by id
	 */
	DataResponse<ProductDTO> getById(Long id, EUserRole role, Long idShop);

	int countProductImage(Long id);

	ListWithPagingResponse<ProductGeneralDTO> getWithFilter(Long idShop, Long idCategory, Long idType, Long idShopCategory,
			Long idSellPlace, String searchName, EProductStatus status, Boolean isNew, Integer page, Integer size,
			Sort sort);

	Long countWithFilter(Long idShop, Long idCategory, Long idType, Long idShopCategory, Long idSellPlace,
			String searchName, EProductStatus status, Boolean isNew);

	/**
	 * Delete product by hide it
	 */
	void delete(Long id, Long idShop);

	DataResponse<ProductDTO> update(Long idShop, EditProductRequest request, MultipartFile avatar)  throws IOException;

	ListWithPagingResponse<ProductGeneralDTO> getTopBuy();
	
	void updatePriceWhenVariationCreated(ProductClassification pc);
	
	void updatePriceWhenVariationChange(ProductClassification pc);

}