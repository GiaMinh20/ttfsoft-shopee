package com.shopee.backend.repository.custom.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.shopee.backend.model.Buyer;
import com.shopee.backend.model.Buyer_;
import com.shopee.backend.repository.custom.BuyerRepositoryCustom;

@Repository
public class BuyerRepositoryCustomImpl implements BuyerRepositoryCustom {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	@Override
	public boolean setPassword(String email, String hashedPassword) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaUpdate<Buyer> cu = cb.createCriteriaUpdate(Buyer.class);
		Root<Buyer> root = cu.from(Buyer.class);
		
		cu.set(root.get(Buyer_.password), hashedPassword);
		cu.where(cb.equal(root.get(Buyer_.email), email));
		
		return em.createQuery(cu).executeUpdate() != 0;
	}
	
	@Override
	public Long getIdByEmail(String email) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<Buyer> root = cq.from(Buyer.class);
		
		cq.select(root.get(Buyer_.id)).where(cb.equal(root.get(Buyer_.email), email));
		try {
			return em.createQuery(cq).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
	@Transactional
	@Override
	public boolean setEmailConfirmed(Long id) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaUpdate<Buyer> cu = cb.createCriteriaUpdate(Buyer.class);
		Root<Buyer> root = cu.from(Buyer.class);
		
		cu.set(root.get(Buyer_.emailConfirmed), Boolean.TRUE);
		cu.where(cb.equal(root.get(Buyer_.id), id));
		
		return em.createQuery(cu).executeUpdate() != 0;
	}
}
