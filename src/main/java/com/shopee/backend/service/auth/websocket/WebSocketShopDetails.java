package com.shopee.backend.service.auth.websocket;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.shopee.backend.model.MediaResource;
import com.shopee.backend.model.Shop;
import com.shopee.backend.service.auth.impl.UserDetailsImpl;
import com.shopee.backend.utility.RolePrefix;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class WebSocketShopDetails extends UserDetailsImpl {
	private static final long serialVersionUID = -7922651730893684620L;
	private String phone;
	private Boolean phoneConfirmed;
	private String email;
	private Boolean emailConfirmed;
	private MediaResource avatar;
	private MediaResource coverImage;
	private String shopName;
	private String address;
	private String defaultReplyMessage;
	private Long totalRating;
	private Double averageRating;
	private Boolean isActive;

	public WebSocketShopDetails() {
		super();
	}
	
	public WebSocketShopDetails(Long id, String username, String password, String phone, Boolean phoneConfirmed,
			String email, Boolean emailConfirmed, MediaResource avatar, MediaResource coverImage, String shopName,
			String address, String defaultReplyMessage, Long totalRating, Double averageRating,
			Boolean isActive, Collection<? extends GrantedAuthority> authorities) {
		super(id, RolePrefix.SHOP + username, password, authorities);
		this.phone = phone;
		this.phoneConfirmed = phoneConfirmed;
		this.email = email;
		this.emailConfirmed = emailConfirmed;
		this.avatar = avatar;
		this.coverImage = coverImage;
		this.shopName = shopName;
		this.address = address;
		this.defaultReplyMessage = defaultReplyMessage;
		this.totalRating = totalRating;
		this.averageRating = averageRating;
		this.isActive = isActive;
	}

	public static WebSocketShopDetails build(Shop user) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority("ROLE_SHOP"));
		
		return new WebSocketShopDetails(
				user.getId(), 
				user.getUsername(), 
				user.getPassword(), 
				user.getPhone(), 
				user.getPhoneConfirmed(), 
				user.getEmail(),
				user.getEmailConfirmed(), 
				user.getAvatar(),
				user.getCoverImage(),
				user.getName(),
				user.getAddress(),
				user.getDefaultReplyMessage(),
				user.getTotalRating(),
				user.getAverageRating(),
				user.getIsActive(),
				authorities);
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return isActive;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return isActive;
	}
}
