package com.shopee.backend.payload.request.producttype;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.shopee.backend.dto.base.AbstractDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateProductTypeRequest extends AbstractDTO {
    @NotBlank(message = "{data.notempty}")
	@Size(min = 1, max = 100)
	private String name;

	private Boolean status;

	private Long idParent;
}
