package com.shopee.backend.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.shopee.backend.model.OrderDetail;
import com.shopee.backend.model.pk.OrderDetailPK;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, OrderDetailPK> {
	@Query(value = "SELECT * FROM order_detail WHERE id_shop_order = :shopOderId", nativeQuery = true)
	public List<OrderDetail> findByShopOrderId(@Param("shopOderId") Long shopOderId);
}

