package com.shopee.backend.dto;

import com.shopee.backend.dto.base.AbstractDTO;

import lombok.Data;

@Data
public class NotificationsDTO extends AbstractDTO {
	private Long userId;
	private String image;
	private String title;
	private String content;
	private Boolean seen;

}
