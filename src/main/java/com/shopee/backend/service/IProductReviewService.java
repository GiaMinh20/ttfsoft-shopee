package com.shopee.backend.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.dto.ProductReviewDTO;
import com.shopee.backend.model.ProductClassification;
import com.shopee.backend.model.ProductReview;
import com.shopee.backend.payload.response.BaseResponse;
import com.shopee.backend.payload.response.ListResponse;
import com.shopee.backend.payload.response.ListWithPagingResponse;
import com.shopee.backend.utility.Page;

public interface IProductReviewService {

	void recaculate(ProductClassification pc);

	Map<String, Long> getProductRating(ProductReview pr);

	BaseResponse deleteReview(Long idOrder, Long idShopOrder, Long idProductClassification);

	BaseResponse updateContent(Long idOrder, Long idShopOrder, Long idProductClassification, String content,
			Integer rating) throws IOException;

	BaseResponse createReview(Long idOrder, Long idShopOrder, Long idProductClassification, String content,
			Integer rating, List<MultipartFile> images) throws IOException;

	ListWithPagingResponse<ProductReviewDTO> getProductReview(Long idProduct, Long rate, Page page);

	Long countProductReview(Long idProduct);

}
