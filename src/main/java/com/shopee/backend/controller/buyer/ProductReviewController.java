package com.shopee.backend.controller.buyer;

import java.io.IOException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.payload.request.review.CreateReviewRequest;
import com.shopee.backend.service.IProductReviewService;
import com.shopee.backend.utility.Page;

@RestController
public class ProductReviewController {

	@Autowired
	IProductReviewService productReviewService;

	@GetMapping("/api/product-review")
	public ResponseEntity<?> productReview(@RequestParam("idProduct") Long idProduct,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "limit", required = false) Integer limit,
			@RequestParam(value = "rate", required = false) Long rate) {
		Long countReview = productReviewService.countProductReview(idProduct);
		return ResponseEntity
				.ok(productReviewService.getProductReview(idProduct, rate, new Page(page, limit, countReview.intValue())));
	}

	@PostMapping(value = "/api/buyer/review", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> review(@RequestPart("info") @Valid CreateReviewRequest request,
			@RequestPart(name = "images", required = false) List<MultipartFile> images) throws IOException {
		return ResponseEntity.ok(productReviewService.createReview(request.getIdOrder(), request.getIdShopOrder(),
				request.getIdProductClassification(), request.getContent(), request.getRating(), images));
	}

	@PutMapping("/api/buyer/review")
	public ResponseEntity<?> review(@RequestBody @Valid CreateReviewRequest request) throws IOException {
		return ResponseEntity.ok(productReviewService.updateContent(request.getIdOrder(), request.getIdShopOrder(),
				request.getIdProductClassification(), request.getContent(), request.getRating()));

	}

	@DeleteMapping("/api/buyer/review")
	public ResponseEntity<?> review(@RequestParam("idOrder") Long idOrder,
			@RequestParam("idShopOrder") Long idShopOrder,
			@RequestParam("idProductClassification") Long idProductClassification) {
		return ResponseEntity.ok(productReviewService.deleteReview(idOrder, idShopOrder, idProductClassification));

	}
}