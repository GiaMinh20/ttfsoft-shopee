package com.shopee.backend.service;

import com.shopee.backend.dto.NotificationsDTO;
import com.shopee.backend.payload.request.notification.PostNotificationRequest;
import com.shopee.backend.payload.response.DataResponse;
import com.shopee.backend.payload.response.ListResponse;

public interface INotificationService {

	void postNotification(Long idBuyer, PostNotificationRequest request);

	ListResponse<NotificationsDTO> getNotificationsByUserId(Long userId);

	DataResponse<NotificationsDTO> getNotificationDetail(Long userId, Long id);

}
