package com.shopee.backend.payload.request.auth;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PasswordUpdateRequest {
	@NotEmpty
	@Size(min = 8, max = 30, message = "Password should have at least 8 characters")
	private String newPassword;
	
	@NotEmpty
	@Size(min = 8, max = 30, message = "Password should have at least 8 characters")
	private String oldPassword;
}
