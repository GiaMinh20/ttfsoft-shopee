package com.shopee.backend.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.shopee.backend.model.listener.ProductClassificationListener;
import com.shopee.backend.utility.datatype.EProductClassificationStatus;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@EntityListeners(ProductClassificationListener.class)
@Entity
@Table(name = "product_classification")
public class ProductClassification {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_product")
	private Product product;
	
	@OneToMany(mappedBy = "productClassification", fetch = FetchType.LAZY)
	private List<ProductReview> reviews;

	@Column(length = 120)
	@NotBlank
	private String variationName;
	
	@NotNull
	private Long price;
	
	@NotNull
	@Min(value = 0)
	private Long quantity;
	
	@NotNull
	@Enumerated(EnumType.ORDINAL)
	private EProductClassificationStatus status;
	
	private Double discount;

	public ProductClassification(Product product, @NotBlank String variationName, @NotNull Long price,
			@NotNull Long quantity, Double discount) {
		this.product = product;
		this.variationName = variationName;
		this.price = price;
		this.quantity = quantity;
		this.status = EProductClassificationStatus.ACTIVE;
		this.discount = discount;
	}	
}
