package com.shopee.backend.model.pk;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import lombok.Data;

@Data
@Embeddable
public class ProductReviewMediaPK {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private ProductReviewPK productReviewPK;
}
