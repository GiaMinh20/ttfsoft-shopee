package com.shopee.backend.model.listener;

import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import com.shopee.backend.model.ProductClassification;
import com.shopee.backend.service.IProductService;

@Component
public class ProductClassificationListener {
	IProductService productService;
	
	public ProductClassificationListener(@Lazy IProductService productService) {
		this.productService = productService;
	}
	
	@PrePersist
	public void updateProductPriceWhenPersist(ProductClassification pc) {
		productService.updatePriceWhenVariationCreated(pc);
	}
	
    @PreUpdate
    @PreRemove
	public void updateProductPriceWhenUpdate(ProductClassification pc) {
		productService.updatePriceWhenVariationChange(pc);
	}
}
