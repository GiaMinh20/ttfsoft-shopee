package com.shopee.backend.repository;

import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.shopee.backend.model.CartDetail;
import com.shopee.backend.model.pk.CartDetailPK;

public interface CartDetailRepository extends JpaRepository<CartDetail, CartDetailPK> {

    @Query(value = "SELECT c FROM CartDetail c WHERE c.buyer.id = :idBuyer ORDER BY c.productClassification.product.shop.id")
    List<CartDetail> findAllByBuyerOrderByShop(Long idBuyer);

    @Query(value = "SELECT sum(quantity) FROM cart_detail WHERE id_buyer =?1", nativeQuery = true)
    Long countQuantityByBuyer(Long idBuyer);

    @Query(value = "DELETE FROM CartDetail c WHERE c.buyer.id = :idBuyer")
    @Transactional
    @Modifying
    void deleteAllByBuyerId(@Param("idBuyer") Long idBuyer);

    @Query(value = "SELECT c FROM CartDetail c WHERE c.buyer.id = :idBuyer AND c.productClassification.id IN :idPs")
    List<CartDetail> findAllById(@Param("idPs") Collection<Long> idPs, @Param("idBuyer") Long idBuyer);
    
}
