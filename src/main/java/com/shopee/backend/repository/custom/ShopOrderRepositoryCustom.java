package com.shopee.backend.repository.custom;

import java.util.List;

public interface ShopOrderRepositoryCustom {
	boolean confirmPayment(List<Long> idShopOrders) throws Exception;
    Boolean cancel(Long id, Long idBuyer);
}