package com.shopee.backend.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.shopee.backend.model.ShopCategory;

public interface ShopCategoryRepository extends JpaRepository<ShopCategory, Long> {
	@Query(value = "SELECT * FROM shop_category WHERE id_shop = :id_shop ", nativeQuery = true)
	public List<ShopCategory> findAllByShop(@Param("id_shop") Long idShop);

	@Query(value = "SELECT * FROM shop_category WHERE id_shop = :id_shop AND name LIKE %:name%", nativeQuery = true)
	public List<ShopCategory> findByShopAndName(@Param("id_shop") Long idShop, @Param("name") String name);

	@Query(value = "SELECT * FROM shop_category  WHERE name LIKE %:name%", nativeQuery = true)
	public List<ShopCategory> findByName(@Param("name") String name);

	@Modifying
	@Transactional
	@Query(value = "UPDATE product SET id_shop_category = null WHERE id_shop_category = :id", nativeQuery = true)
	public void deleteShopCategoryFromProduct(@Param("id") Long id);
}
