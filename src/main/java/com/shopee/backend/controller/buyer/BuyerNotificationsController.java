package com.shopee.backend.controller.buyer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.service.INotificationService;
import com.shopee.backend.service.auth.BuyerDetails;

@RestController
@RequestMapping("/api/buyer/notifications")
public class BuyerNotificationsController {
	@Autowired
	INotificationService service;

	@GetMapping()
	public ResponseEntity<?> getNotifications(@AuthenticationPrincipal BuyerDetails buyer) {
		return ResponseEntity.ok(service.getNotificationsByUserId(buyer.getId()));
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getNotifications(@AuthenticationPrincipal BuyerDetails buyer,
			@PathVariable("id") Long id) {
		return ResponseEntity.ok(service.getNotificationDetail(buyer.getId(), id));
	}
}
