package com.shopee.backend.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.shopee.backend.model.base.BaseModel;
import com.shopee.backend.utility.datatype.EOrderStatus;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "shop_orders")
public class ShopOrder extends BaseModel {
	@ManyToOne
	@JoinColumn(name = "id_order")
	private Order order;

	@ManyToOne
	@JoinColumn(name = "id_shop")
	private Shop shop;

	@OneToMany(mappedBy = "shopOrder", cascade = CascadeType.PERSIST)
	private List<OrderDetail> orderDetails = new ArrayList<>();

	@Column(name = "shop_voucher")
	private String shopVoucher;
	
	@Column(name = "original_price")
	private Long originalPrice = 0L;

	@Enumerated(EnumType.ORDINAL)
	@NotNull
	private EOrderStatus status;

	@Column(name = "ship_price")
	private Long shipPrice;
	
	@Column(name = "platform_price_off")
	private Long platformPriceOff = 0L;// của shopee

	@Column(name = "ship_price_off")
	private Long shipPriceOff = 0L; // của shopee

	@Column(name = "shop_price_off")
	private Long shopPriceOff = 0L;
	
	@Column(name = "total_price") // originalPrice + shipPrice - shopPriceOff
	private Long totalPrice = 0L;
	
	@Column(name = "total_pay_price") // total_price - shipPriceOff - platformPriceOff
	private Long totalPayPrice = 0L;

	@Column(name = "price_off")
	private Long priceOff = 0L;

	private Date complete;

	@Column(name = "buyer_note")
	private String buyerNote;

	@Column(name = "prepare_date")
	private Long prepareDate;

	public void addOriginalPrice(Long price){
		originalPrice += price;
	}

	@PrePersist
	public void setTotalPrice(){
		totalPrice = originalPrice + shipPrice - shopPriceOff;
		totalPayPrice = totalPrice - shipPriceOff - platformPriceOff;
	}
}