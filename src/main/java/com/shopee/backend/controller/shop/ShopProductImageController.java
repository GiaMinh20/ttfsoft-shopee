package com.shopee.backend.controller.shop;

import java.io.IOException;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.payload.response.BaseResponse;
import com.shopee.backend.service.auth.ShopDetails;
import com.shopee.backend.service.impl.ProductImageService;

@RestController
@RequestMapping("/api/shop/product/{idProduct}/image")
public class ShopProductImageController {
	@Autowired
	ProductImageService productImageService;
	
	@PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> add(
			@AuthenticationPrincipal ShopDetails shop,
			@PathVariable("idProduct") Long idProduct,
			@RequestPart("images") @Valid @NotEmpty List<MultipartFile> images) throws IOException {
		
		return ResponseEntity.ok(productImageService.create(shop.getId(), idProduct, images));
	}
	
	@DeleteMapping
	public ResponseEntity<?> delete(
			@AuthenticationPrincipal ShopDetails shop, 
			@PathVariable("idProduct") Long idProduct,
			@RequestParam List<Long> id)  {
		
		productImageService.delete(shop.getId(),idProduct, id);
		return ResponseEntity.ok(new BaseResponse());
	}
}
