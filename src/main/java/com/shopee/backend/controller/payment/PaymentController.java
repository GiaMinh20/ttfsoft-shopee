package com.shopee.backend.controller.payment;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.shopee.backend.payload.request.payment.ShopOrderPaymentRequest;
import com.shopee.backend.service.auth.BuyerDetails;
import com.shopee.backend.service.impl.PaymentService;

@RestController
@RequestMapping("/api/buyer/payment")
public class PaymentController {
	@Autowired
	PaymentService paymentService;
	
	@PostMapping("/shoporder")
	public ResponseEntity<?> shopOrderPaymentConfirmation(
			HttpServletRequest req,
			@AuthenticationPrincipal BuyerDetails buyer,
			@RequestBody @Valid ShopOrderPaymentRequest request) {
		
		if (StringUtils.isBlank(request.getReturnUrl()))
			request.setReturnUrl(String.format("%s://%s/api/returnmomo", req.getScheme(), req.getServerName()));
		
		return ResponseEntity.ok(paymentService.shopOrderConfirm(buyer.getId(), request, req));
	}
}
