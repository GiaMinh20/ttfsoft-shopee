package com.shopee.backend.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.service.IProductCategoryService;

@RestController
@RequestMapping("/api")
public class ProductCategoryController {
	@Autowired
	private IProductCategoryService productCategoryService;

	@GetMapping("/productcategory")
	public ResponseEntity<?> getProductCategories(@RequestParam(required = false) String searchName) {
		if (StringUtils.isNotBlank(searchName))
			return ResponseEntity.ok(productCategoryService.findByName(searchName));
		else
			return ResponseEntity.ok(productCategoryService.getAll());
	}

	@GetMapping("/productcategory/{id}")
	public ResponseEntity<?> getProductCategoryById(@PathVariable("id") long productCategoryId) {
		return ResponseEntity.ok(productCategoryService.getById(productCategoryId));
	}
}
