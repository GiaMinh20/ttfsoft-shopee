package com.shopee.backend.dto.holder;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AdminDataHolder {
	private Long saleTotalShop = 0L;
	private Long saleTotalShopee = 0L;
	private Long totalShipAmount = 0L;
	private Long shipPriceOff = 0L;
	private Long totalPlatFormPriceOff = 0L;
	private Long revenue = 0L;

	public AdminDataHolder(Long saleTotalShop, Long saleTotalShopee, Long totalShipAmount, Long shipPriceOff,
			Long totalPlatFormPriceOff) {
		this.saleTotalShop = saleTotalShop;
		this.saleTotalShopee = saleTotalShopee;
		this.totalShipAmount = totalShipAmount;
		this.shipPriceOff = shipPriceOff;
		this.totalPlatFormPriceOff = totalPlatFormPriceOff;
		this.revenue = saleTotalShopee + totalShipAmount - shipPriceOff;
	}
	
	public void setRevenue()
	{
		revenue = saleTotalShopee + totalShipAmount - shipPriceOff;

	}
}
