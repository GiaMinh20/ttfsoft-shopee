package com.shopee.backend.config.websocket;

import com.shopee.backend.utility.datatype.EMessageContentType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class OutboundChatMessage {
	@NonNull
	private EMessageContentType contentType;
	
	/**
	 * Text plain or base64 encoded if type is binary
	 */
	@NonNull
	private String data;
}
