package com.shopee.backend.payload.request.auth;

import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class EmailConfirmRequest {
	@NotEmpty
	private String confirmCode;
	
	public EmailConfirmRequest(String confirmCode) {
		this.confirmCode = confirmCode;
	}
}
