package com.shopee.backend.payload.request.payment;

import com.shopee.backend.payload.response.BaseResponse;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class PaymentResponse extends BaseResponse {
	private String payUrl;

	public PaymentResponse(Boolean success, String message, String payUrl) {
		super(success, message);
		this.payUrl = payUrl;
	}
	
	public PaymentResponse(String payUrl) {
		super();
		this.payUrl = payUrl;
	}
}
