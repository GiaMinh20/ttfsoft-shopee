package com.shopee.backend.controller.admin;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.payload.request.producttype.CreateProductTypeRequest;
import com.shopee.backend.payload.request.producttype.EditProductTypeRequest;
import com.shopee.backend.payload.response.BaseResponse;
import com.shopee.backend.service.IProductTypeService;

@RestController
@RequestMapping("/api/admin/producttype")
public class AdminProductTypeController {
	@Autowired
	private IProductTypeService productTypeService;

	@GetMapping
	public ResponseEntity<?> getAllProductTypes() {
		return ResponseEntity.ok(productTypeService.getAll());
	}

	@PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> createProductType(@RequestPart @Valid CreateProductTypeRequest request,
			@RequestPart(name = "image", required = false) MultipartFile image) throws IOException {
		return ResponseEntity.ok(productTypeService.create(request, image));
	}

	@PutMapping(value = "/{id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> editProductType(@PathVariable("id") long id,
			@RequestPart @Valid EditProductTypeRequest productTypeRequest,
			@RequestPart(name = "image", required = false) MultipartFile image) throws IOException {

		return ResponseEntity.ok(productTypeService.update(id, productTypeRequest, image));
	}

	@PutMapping("status/{id}")
	public ResponseEntity<?> deleteProductType(@PathVariable("id") long id) {

		if (!productTypeService.delete(id))
			return ResponseEntity.ok(new BaseResponse(false, "Update status product type failed"));
		else
			return ResponseEntity.ok(new BaseResponse());
	}

	@PutMapping("/producttype")
	public ResponseEntity<?> updateparent(@RequestParam("id") Long id,
			@RequestParam(name = "parent", required = false) Long parent) {
		return ResponseEntity.ok(productTypeService.updateParent(id, parent));
	}
}
