package com.shopee.backend.controller.auth;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.payload.request.auth.LoginRequest;
import com.shopee.backend.service.auth.LoginService;
import com.shopee.backend.utility.RolePrefix;
import com.shopee.backend.utility.jwt.JwtUtils;

@RestController
@RequestMapping("/api")
public class LoginController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	JwtUtils jwtUtils;
	
	@Autowired
	LoginService loginService;

	@PostMapping("/admin/login")
	public ResponseEntity<?> authenticateUsers(@Valid @RequestBody LoginRequest request) {
		return ResponseEntity.ok(
				loginService.authenticateUser(request.getUsername(), request.getPassword(), RolePrefix.ADMIN)
			);
	}
	
	@PostMapping("/buyer/login")
	public ResponseEntity<?> authenticateBuyer(@Valid @RequestBody LoginRequest request) {
		return ResponseEntity.ok(
				loginService.authenticateUser(request.getUsername(), request.getPassword(), RolePrefix.BUYER)
			);
	}
	
	@PostMapping("/shop/login")
	public ResponseEntity<?> authenticateShop(@Valid @RequestBody LoginRequest request) {
		return ResponseEntity.ok(
				loginService.authenticateUser(request.getUsername(), request.getPassword(), RolePrefix.SHOP)
			);
	}
}
