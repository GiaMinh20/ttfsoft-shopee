package com.shopee.backend.payload.request.producttype;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EditProductTypeRequest {
	@NotBlank(message = "{data.notempty}")
	@Size(max = 100, min = 1)
	private String name;

	private Boolean status;
	private Long idParent;

}
