package com.shopee.backend.payload.request.payment;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class ShopOrderPaymentRequest {
	@NotNull
	private Long idShopOrder;
	
	private String returnUrl;
}
