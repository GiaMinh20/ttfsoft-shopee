package com.shopee.backend.controller.buyer;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.payload.request.follow.FollowRequest;
import com.shopee.backend.service.auth.BuyerDetails;
import com.shopee.backend.service.impl.FollowService;
import com.shopee.backend.utility.sort.ModelSorting;

@RestController
@RequestMapping("/api/buyer/follow")
public class FollowController {
	@Autowired
	FollowService followService;
	
	@GetMapping
	public ResponseEntity<?> getFollows(
			@AuthenticationPrincipal BuyerDetails buyer,
			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer size,
			@RequestParam(required = false) Integer sortBy) {
		
		return ResponseEntity.ok(
				followService.get(
						buyer.getId(), 
						page, 
						size, 
						ModelSorting.getFollowSort(sortBy)
				)
			);
	}
	
	@PostMapping
	public ResponseEntity<?> follow(
			@AuthenticationPrincipal BuyerDetails buyer,
			@RequestBody @Valid FollowRequest request) {
		return ResponseEntity.ok(followService.add(buyer.getId(), request));
	}
	
	@DeleteMapping("/{idShop}")
	public ResponseEntity<?> unfollow(
			@AuthenticationPrincipal BuyerDetails buyer,
			@PathVariable("idShop") Long idShop) {
		return ResponseEntity.ok(followService.remove(buyer.getId(), idShop));
	}
}
