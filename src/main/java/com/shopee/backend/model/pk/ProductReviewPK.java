package com.shopee.backend.model.pk;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class ProductReviewPK implements Serializable {
	
	private static final long serialVersionUID = 2505907334638897958L;
	
	@Column(name = "id_order")
	private Long idOrder;
	
	@Column(name="id_shop_order")
	private Long idShopOrder;
	
	@Column(name="id_product_classification")
	private Long idProductClassification;
}
