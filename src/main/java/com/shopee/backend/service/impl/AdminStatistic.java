package com.shopee.backend.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopee.backend.payload.response.statistic.StatisticResponse;
import com.shopee.backend.repository.AdminRepository;

@Service
public class AdminStatistic {

	@Autowired
	AdminRepository adminRepository;

	public StatisticResponse getStatistic(Date dStart, Date dEnd, Long dLast) {
		return adminRepository.customStatistic(dStart, dEnd, dLast);
	}
}
