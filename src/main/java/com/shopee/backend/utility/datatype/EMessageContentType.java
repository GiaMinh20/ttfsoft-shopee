package com.shopee.backend.utility.datatype;

public enum EMessageContentType {
	TEXT,
	IMAGE
}
