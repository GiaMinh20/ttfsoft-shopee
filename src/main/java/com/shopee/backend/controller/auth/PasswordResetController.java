package com.shopee.backend.controller.auth;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.payload.request.auth.PasswordResetRequest;
import com.shopee.backend.payload.request.auth.SendVerifyTokenRequest;
import com.shopee.backend.service.IPasswordResetService;
import com.shopee.backend.utility.RolePrefix;

@RestController
@RequestMapping("/api")
public class PasswordResetController {
	@Autowired
	IPasswordResetService passwordResetService; 
	
	@PostMapping("/shop/sendpasswordresetcode")
	public ResponseEntity<?> shopSendVerifyToken(@RequestBody @Valid SendVerifyTokenRequest request) {
		return ResponseEntity.ok(passwordResetService.sendVerifyCode(RolePrefix.SHOP, request));
	}
	
	@PostMapping("/buyer/sendpasswordresetcode")
	public ResponseEntity<?> buyerSendVerifyToken(@RequestBody @Valid SendVerifyTokenRequest request) {
		return ResponseEntity.ok(passwordResetService.sendVerifyCode(RolePrefix.BUYER, request));
	}
	
	@PostMapping("/shop/resetpassword")
	public ResponseEntity<?> shopResetPassword(@RequestBody @Valid PasswordResetRequest request) {
		return ResponseEntity.ok(passwordResetService.reset(RolePrefix.SHOP, request));
	}
	
	@PostMapping("/buyer/resetpassword")
	public ResponseEntity<?> buyerResetPassword(@RequestBody @Valid PasswordResetRequest request) {
		return ResponseEntity.ok(passwordResetService.reset(RolePrefix.BUYER, request));
	}
}
