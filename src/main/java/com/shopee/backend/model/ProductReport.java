package com.shopee.backend.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import com.shopee.backend.model.pk.ProductReportPK;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter @Setter @RequiredArgsConstructor
@Entity
@Table(name = "product_report")
public class ProductReport {
	@EmbeddedId
	private ProductReportPK id;
	
	@ManyToOne
	@MapsId("idOrder")
	@JoinColumn(name="id_order", insertable=false, updatable=false)
	private Order order;
	
	@ManyToOne
	@MapsId("idProduct")
	@JoinColumn(name="id_product", insertable=false, updatable=false)
	private Product product;
	
	@Lob
	@Column(length = 65535)
	@NotBlank
	private String content;
	
	private Date date;
}
