package com.shopee.backend.service;

import com.shopee.backend.dto.BuyerCouponDTO;
import com.shopee.backend.dto.CouponCodeDTO;
import com.shopee.backend.model.CouponCode;
import com.shopee.backend.payload.response.CouponCodeResponse;
import com.shopee.backend.payload.response.ResponseData;

public interface ICouponCodeService {
	
	ResponseData<CouponCodeResponse> getAllCouponCodes();
	ResponseData<CouponCodeResponse> getCouponCodeById(Long id);
	ResponseData<CouponCodeResponse> getAllCouponCodesShopee();
	ResponseData<CouponCodeResponse> getAllCouponCodesByIdShop(Long idShop);
	ResponseData<CouponCodeDTO> editCouponCode(CouponCodeDTO newCouponCodeDTO, Long id);
	ResponseData<CouponCodeDTO> createCouponCode(CouponCodeDTO couponCodeDTO);
	ResponseData<CouponCodeDTO> detleteCouponCode(Long id);
	ResponseData<BuyerCouponDTO> addToList(Long id);
	ResponseData<CouponCodeResponse> getAllCouponsByIdBuyer();
	CouponCode findByCode(String code);
	Long applyCoupon(Long priceOriginal, CouponCode coupon);
}
