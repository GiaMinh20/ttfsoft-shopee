package com.shopee.backend.service.impl.onlinepayment;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.utility.RandomString;
import com.shopee.backend.utility.onlinepayment.ExecuteRequest;
import com.shopee.backend.utility.onlinepayment.momo.MomoPaymentConfirm;
import com.shopee.backend.utility.onlinepayment.momo.MomoPaymentCreate;

@Service
public class MomoService {
	@Autowired
	Environment environment;
	
	@Value("${momo.secret-key}")
	private String secretKey;
	
	@Value("${momo.access-key}")
	private String accessKey;
	
	@Value("${momo.partner-code}")
	private String partnerCode;
	
	@Value("${momo.url.payment-create}")
	private String paymentCreate;
	
	@Value("${momo.url.payment-confirm}")
	private String paymentConfirm;
	
	public String getOrderPayUrl(Long idOrder, String notifyUrl, String returnUrl, String amount) {
		String encodedIdOrder = String.format("%d_%s_%s", idOrder, "order", RandomString.get());
		return getPayUrl(encodedIdOrder, notifyUrl, returnUrl, amount);
	}
	
	public String getShopOrderPayUrl(Long idShopOrder, String notifyUrl, String returnUrl, String amount) {
		String encodedIdOrder = String.format("%d_%s_%s", idShopOrder, "shoporder", RandomString.get());
		return getPayUrl(encodedIdOrder, notifyUrl, returnUrl, amount);
	}
	
	public boolean confirmPayment(String orderId, String amount, boolean isConfirm) {
		String resp = "";
		try {
			resp = ExecuteRequest.execute(
					paymentConfirm, 
					new MomoPaymentConfirm(
							accessKey,
							secretKey,
							partnerCode,
							UUID.randomUUID().toString(),
							orderId,
							amount,
							isConfirm)
					);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		try {
			Map<String, Object> m = new ObjectMapper().readValue(resp, HashMap.class);
			if ((int)m.get("resultCode") == 0) {
				return true;
			}
			else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	
	private String getPayUrl(String idOrder, String ipnUrl, String redirectUrl, String amount) {
		String resp = "";
		try {
			resp = ExecuteRequest.execute(
					paymentCreate, 
					new MomoPaymentCreate(
							ipnUrl,
							redirectUrl, 
							idOrder,
							amount, 
							"TTFSOFT-Shopee", 
							UUID.randomUUID().toString(),
							secretKey,
							accessKey,
							partnerCode)
					);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
		
		try {
			Map<String, Object> m = new ObjectMapper().readValue(resp, HashMap.class);
			if ((int)m.get("resultCode") == 0) {
				return (String)m.get("payUrl");
			}
			else {
				throw new CommonRuntimeException(String.format("Generate Momo bill failed (%s)", resp));
			}
		} catch (JsonProcessingException  e) {
			throw new CommonRuntimeException(e.getMessage());
		}
	}
}
