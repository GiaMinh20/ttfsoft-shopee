package com.shopee.backend.controller.shop;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.service.IShopOrderService;
import com.shopee.backend.service.auth.BuyerDetails;
import com.shopee.backend.service.auth.ShopDetails;
import com.shopee.backend.utility.datatype.EOrderStatus;

@RestController
@RequestMapping("/api")
public class ShopOrderController {
	@Autowired
	private IShopOrderService shopOrderService;

	@GetMapping("/shop/shoporder/{from}&{to}")
	public ResponseEntity<?> getShopOrderByShop(

			@AuthenticationPrincipal ShopDetails shop,

			@PathVariable("from") String from, @PathVariable("to") String to) {

		return ResponseEntity.ok(shopOrderService.getOrderByShop(shop.getId(), from, to));
	}

	@GetMapping("/shop/shoporder/{status}/{from}&{to}")
	public ResponseEntity<?> getShopOrderByShopAndStatus(

			@AuthenticationPrincipal ShopDetails shop,

			@PathVariable("status") EOrderStatus status,

			@PathVariable("from") String from, @PathVariable("to") String to) {

		return ResponseEntity.ok(shopOrderService.getOrderByShopAndStatus(shop.getId(), status, from, to));
	}

	@GetMapping("/shop/shoporder/id={id}")
	public ResponseEntity<?> getShopOrderByShopAndId(@AuthenticationPrincipal ShopDetails shop,
			@PathVariable("id") Long id) {

		return ResponseEntity.ok(shopOrderService.getOrderByShopAndId(shop.getId(), id));
	}

	@PutMapping("/shop/shoporder/{id}")
	public ResponseEntity<?> updateShopOrder(

			@AuthenticationPrincipal ShopDetails shop,

			@PathVariable("id") Long id, EOrderStatus status) {

		return ResponseEntity.ok(shopOrderService.update(shop.getId(), id, status));
	}

	@GetMapping("/shop/shoporder/export/{from}&{to}")
	public ResponseEntity<?> exportData(

			@AuthenticationPrincipal ShopDetails shop,

			@PathVariable("from") String from, @PathVariable("to") String to) {

		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());

		String fileName = "shoporderdata_" + currentDateTime + ".xlsx";
		InputStreamResource file = new InputStreamResource(shopOrderService.exportData(shop.getId(), from, to));

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName)
				.contentType(MediaType.parseMediaType("application/vnd.ms-excel")).body(file);
	}

	@GetMapping("shop/shoporder/export/{status}/{from}&{to}")
	public ResponseEntity<?> exportDataWithStatus(

			@AuthenticationPrincipal ShopDetails shop,

			@PathVariable("status") EOrderStatus status,

			@PathVariable("from") String from, @PathVariable("to") String to) {

		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());

		String fileName = "shoporderdata_status_" + status + "_" + currentDateTime + ".xlsx";
		InputStreamResource file = new InputStreamResource(
				shopOrderService.exportDataWithStatus(shop.getId(), status, from, to));

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName)
				.contentType(MediaType.parseMediaType("application/vnd.ms-excel")).body(file);
	}

	@PutMapping("buyer/shoporder/cancel")
	public ResponseEntity<?> cancelShopOrder(@NotNull Long id, @AuthenticationPrincipal BuyerDetails buyer) {
		return ResponseEntity.ok(shopOrderService.cancel(id, buyer.getId()));
	}
}
