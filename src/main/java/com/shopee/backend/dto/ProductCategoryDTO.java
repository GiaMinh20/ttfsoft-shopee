package com.shopee.backend.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.shopee.backend.dto.base.AbstractDTO;

import lombok.Data;

@Data
public class ProductCategoryDTO extends AbstractDTO {
	private String name;
	private String image;
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date dod;
}
