package com.shopee.backend.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.dto.DeliveryAddressDTO;
import com.shopee.backend.payload.response.BaseResponse;
import com.shopee.backend.service.IDeliveryAddress;
import com.shopee.backend.service.auth.BuyerDetails;

@RestController
@RequestMapping("/api/buyer/delivery-address")
public class DeliveryAddressController {

	@Autowired
	IDeliveryAddress deliveryAddressService;

	@GetMapping
	public ResponseEntity<?> getBuyerAddress() {
		BuyerDetails buyer = (BuyerDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return ResponseEntity.ok(deliveryAddressService.getBuyerDeliveryAddress(buyer.getId()));
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> get1BuyerAddress(@PathVariable("id") Long id) {
		BuyerDetails buyer = (BuyerDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return ResponseEntity.ok(deliveryAddressService.getBuyerDeliveryAddress(id, buyer.getId()));
	}

	@PostMapping
	public ResponseEntity<?> addBuyerAddress(@RequestBody @Valid DeliveryAddressDTO request) {
		return ResponseEntity.ok(deliveryAddressService.save(request));
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> updateBuyerAddress(@RequestBody @Valid DeliveryAddressDTO request, @PathVariable("id") Long id) {
		return ResponseEntity.ok(deliveryAddressService.update(id, request));
	}

	@DeleteMapping
	public ResponseEntity<?> deleteBuyerAddress(@RequestBody Long[] ids,
			@AuthenticationPrincipal BuyerDetails buyer){
		deliveryAddressService.delete(ids, buyer.getId());
		return ResponseEntity.ok(new BaseResponse(true, "Delete Successfully !"));
	}

}
