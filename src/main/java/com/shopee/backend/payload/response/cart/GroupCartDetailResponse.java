package com.shopee.backend.payload.response.cart;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class GroupCartDetailResponse {
    private Long idShop;
    private String nameShop;
    private List<CartDetailResponse> items = new ArrayList<>();
}
