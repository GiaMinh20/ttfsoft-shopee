package com.shopee.backend.model.pk;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
@Embeddable
public class OrderDetailPK implements Serializable {
	private static final long serialVersionUID = -6504161601883458904L;

	@Column(name="id_shop_order")
	private Long idShopOrder;
	
	@Column(name="id_product_classification")
	private Long idProductClassification;
}
