package com.shopee.backend.service;

import java.util.Collection;
import java.util.List;

import com.shopee.backend.model.FavoriteBuyer;

public interface IFavoriteService {
    List<FavoriteBuyer> getAllByBuyer(Long idBuyer);

    List<FavoriteBuyer> getAllByProduct(Long idP);

    void addFavorites(Long idBuyer, Collection<Long> idProducts);

    void unFavorites(Long idBuyer, Collection<Long> idProducts);

    Boolean isFavorite(Long idBuyer, Long idP);
}
