package com.shopee.backend.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.shopee.backend.model.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "product_type")
public class ProductType extends BaseModel {
	@OneToMany(mappedBy = "productType")
	private List<Product> products = new ArrayList<>();

	@Column(length = 100)
	@NotBlank
	private String name;

	@OneToOne
	@JoinColumn(name = "id_image")
	private MediaResource image;

	private Boolean status;

	public ProductType() {

	}

	@ManyToOne
	private ProductType parent;
	
	@OneToMany(mappedBy = "parent")
	private List<ProductType> children = new ArrayList<>();
	
	private Long level;

	public ProductType(MediaResource image, @NotBlank String name, Boolean status) {
		this.image = image;
		this.name = name;
		this.status = status;
	}
	
	@PreRemove
	private void preRemove() {
		for (Product p : products) {
			p.setProductType(null);
		}
	}

	public ProductType(@NotBlank String name, MediaResource image, Boolean status, Long level) {
		this.name = name;
		this.image = image;
		this.status = status;
		this.level = level;
	}
}
