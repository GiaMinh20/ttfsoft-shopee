package com.shopee.backend.service.impl;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopee.backend.dto.NotificationsDTO;
import com.shopee.backend.model.Buyer;
import com.shopee.backend.model.MediaResource;
import com.shopee.backend.model.Notification;
import com.shopee.backend.payload.request.notification.PostNotificationRequest;
import com.shopee.backend.payload.response.DataResponse;
import com.shopee.backend.payload.response.ListResponse;
import com.shopee.backend.repository.BuyerRepository;
import com.shopee.backend.repository.MediaResourceRepository;
import com.shopee.backend.repository.NotificationRepository;
import com.shopee.backend.service.INotificationService;

@Service
public class NotificationService implements INotificationService {
	@Autowired
	NotificationRepository repository;
	@Autowired
	MediaResourceRepository mediaResourceRepository;
	@Autowired
	BuyerRepository buyerRepository;
	@Autowired
	ModelMapper modelMapper;

	@Override
	public void postNotification(Long idBuyer, PostNotificationRequest request) {
		List<MediaResource> images = mediaResourceRepository.findMediaResourceById(request.getImageId());
		Buyer buyer = buyerRepository.getReferenceById(idBuyer);
		Notification notification = new Notification(buyer, images.get(0), request.getTitle(), request.getContent(),
				false);
		repository.save(notification);
	}

	@Override
	public ListResponse<NotificationsDTO> getNotificationsByUserId(Long userId) {
		List<Notification> notifications = repository.getNotificationsByUserId(userId);
		return new ListResponse<>(notifications.stream().map(c -> modelMapper.map(c, NotificationsDTO.class))
				.toList());
	}

	@Override
	public DataResponse<NotificationsDTO> getNotificationDetail(Long userId, Long id) {
		Notification notification = repository.getReferenceById(id);
		notification.setSeen(true);
		repository.save(notification);
		return new DataResponse<>(modelMapper.map(notification, NotificationsDTO.class));
	}
}
