package com.shopee.backend.controller.exception;

public class CouponCodeException extends RuntimeException{

    public CouponCodeException(String message){
        super(message);
    }
}
