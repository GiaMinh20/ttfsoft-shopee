package com.shopee.backend.repository.custom;

import java.util.List;
import java.util.Map;

import com.shopee.backend.model.Product;
import com.shopee.backend.utility.Page;
import com.shopee.backend.utility.datatype.EProductStatus;

public interface ProductRepositoryCustom {	
	
	List<Product> get(Long idShop, Long idCategory, Long idType, Long idShopCategory,
			Long idSellPlace, String searchName, EProductStatus status, Boolean isNew, Page page);
	
	Long count(Long idShop, Long idCategory, Long idType, Long idShopCategory,
			Long idSellPlace, String searchName, EProductStatus status, Boolean isNew);
	
	void refresh(Product product);
	
	List<Product> getTopBuy();

	Map<String, Long> getProductRating(Long idProduct);

	Map<String, Double> getShopRating(Long idShop);
}
