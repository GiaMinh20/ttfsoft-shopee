package com.shopee.backend.controller.auth;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.payload.request.auth.BuyerProfileCreateRequest;
import com.shopee.backend.payload.request.auth.ShopProfileCreateRequest;
import com.shopee.backend.service.IAdminService;
import com.shopee.backend.service.IBuyerService;
import com.shopee.backend.service.IShopService;

@RestController
@RequestMapping("/api")
public class SignupController {
	@Autowired
	IBuyerService buyerService;

	@Autowired
	IShopService shopService;

	@Autowired
	IAdminService adminService;

	@PostMapping(value = "/buyer/signup", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> createBuyer(@RequestPart("info") @Valid BuyerProfileCreateRequest request,
			@RequestPart(name = "avatar", required = false) MultipartFile avatar) throws IOException {

		return ResponseEntity.ok(buyerService.create(request, avatar));
	}

	@PostMapping(value = "/shop/signup", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> createShop(@RequestPart("info") @Valid ShopProfileCreateRequest request,
			@RequestPart(name = "avatar", required = false) MultipartFile avatar,
			@RequestPart(name = "coverImage", required = false) MultipartFile coverImage) throws IOException {

		return ResponseEntity.ok(shopService.create(request, avatar, coverImage));
	}
}
