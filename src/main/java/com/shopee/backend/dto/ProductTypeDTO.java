package com.shopee.backend.dto;

import com.shopee.backend.dto.base.AbstractDTO;
import com.shopee.backend.model.MediaResource;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductTypeDTO extends AbstractDTO {
	private String name;
	private MediaResource image;
	private Boolean status;
	private Long idParent;
	private String nameParent;
	private Long level;

	@Override
	public String toString() {
		return "ProductTypeDTO [name=" + name + ", image=" + image + ", status=" + status + ", idParent=" + idParent
				+ ", nameParent=" + nameParent + ", level=" + level + "]";
	}

}
