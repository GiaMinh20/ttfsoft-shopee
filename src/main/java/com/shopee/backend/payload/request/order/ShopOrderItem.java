package com.shopee.backend.payload.request.order;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class ShopOrderItem<T> {
    @NotNull(message = "id shop must not be null")
    private Long idShop;
    @NotEmpty(message = "List of id must not be empty")
    private Set<T> idProductClassifications = new HashSet<>();
    private String couponCode;
    private String buyerNote;
}
