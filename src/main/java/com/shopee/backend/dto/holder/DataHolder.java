package com.shopee.backend.dto.holder;

import com.shopee.backend.utility.datatype.EOrderStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DataHolder {
	private EOrderStatus status;
	private Long value;

	public DataHolder(Long value, EOrderStatus status) {
		this.status = status;
		this.value = value;
	}
}
