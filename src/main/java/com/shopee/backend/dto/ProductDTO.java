package com.shopee.backend.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.shopee.backend.dto.base.AbstractDTO;
import com.shopee.backend.model.ProductClassification;
import com.shopee.backend.utility.datatype.EProductStatus;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ProductDTO extends AbstractDTO {
	private Long idType;
	private Long idCategory;
	private Long idShop;
	private Long idShopCategory;
	private Long idSellPlace;

	private String sellPlace;

	private String name;
	private String avatar;
	private String description;

	private Long price;
	private Long maxPrice;
	private Long sales;
	private Long view;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date discountStart;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date discountEnd;
	private Double averageRating;
	private EProductStatus status;
	private Long reviewCount;

	private Boolean isNew;

	private List<ProductImageDTO> images;
	private List<ProductClassification> classifications;

	
}
