package com.shopee.backend.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.shopee.backend.model.base.BaseModel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "shop")
public class Shop extends BaseModel {
	@OneToMany(mappedBy = "shop", fetch = FetchType.LAZY)
	private List<ShopCategory> shopCategories;

	@OneToMany(mappedBy = "shop", fetch = FetchType.LAZY)
	private List<Follow> followers;

	@OneToMany(mappedBy = "shop", fetch = FetchType.LAZY)
	private List<Message> messages;

	@OneToMany(mappedBy = "shop", fetch = FetchType.LAZY)
	private List<ShopOrder> shopOrders;

	@OneToMany(mappedBy = "shop", fetch = FetchType.LAZY)
	private List<CouponCode> couponCodes;

	@OneToMany(mappedBy = "shop")
	private List<Product> products;

	@OneToOne
	@JoinColumn(name = "id_avatar")
	private MediaResource avatar;

	@OneToOne
	@JoinColumn(name = "id_cover_image")
	private MediaResource coverImage;

	@NotEmpty
	@Column(length = 50)
	private String username;

	@NotEmpty
	@Column(length = 120)
	private String password;

	@NotEmpty
	@Column(length = 10, unique = true)
	private String phone;

	@Column(name = "phone_confirmed")
	@NotNull
	private Boolean phoneConfirmed;

	@NotEmpty
	@Column(length = 50, unique = true)
	private String email;

	@Column(name = "email_confirmed")
	@NotNull
	private Boolean emailConfirmed;

	@Column(name = "shop_name", length = 50)
	@NotEmpty
	private String name;

	@NotEmpty
	private String address;

	@Lob
	@Column(name = "default_reply_message", length = 65535)
	private String defaultReplyMessage;

	@Column(name = "total_rating")
	@NotNull
	private Long totalRating;

	@Column(name = "average_rating")
	@NotNull
	private Double averageRating;

	@Column(name = "is_active")
	@NotNull
	private Boolean isActive;

	private String verificationToken;

	@Version
	@Column(columnDefinition = "bigint DEFAULT 0", nullable = false)
	private Long version = 0L;

	public Shop(@NotBlank String username, @NotBlank String password, @NotBlank String phone,
			@NotNull Boolean phoneConfirmed, @NotBlank String email, @NotNull Boolean emailConfirmed,
			@NotBlank String name, @NotBlank String address, @NotNull Boolean isActive) {
		super();
		this.username = username;
		this.password = password;
		this.phone = phone;
		this.phoneConfirmed = phoneConfirmed;
		this.email = email;
		this.emailConfirmed = emailConfirmed;
		this.name = name;
		this.address = address;
		this.isActive = isActive;

		this.totalRating = 0L;
		this.averageRating = 0.0;
	}
}
