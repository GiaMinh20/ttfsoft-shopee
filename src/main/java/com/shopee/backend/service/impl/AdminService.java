package com.shopee.backend.service.impl;

import java.io.IOException;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.dto.AdminDTO;
import com.shopee.backend.model.Admin;
import com.shopee.backend.model.MediaResource;
import com.shopee.backend.payload.request.auth.AdminProfileCreateRequest;
import com.shopee.backend.payload.request.auth.AdminProfileUpdateRequest;
import com.shopee.backend.payload.request.auth.PasswordUpdateRequest;
import com.shopee.backend.payload.response.DataResponse;
import com.shopee.backend.repository.AdminRepository;
import com.shopee.backend.service.IAdminService;
import com.shopee.backend.service.IMediaResourceService;

@Service
public class AdminService implements IAdminService {
	@Autowired
	ModelMapper modelMapper;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	AdminRepository repository;

	@Autowired
	IMediaResourceService mediaResourceService;

	@Override
	public DataResponse<AdminDTO> getById(Long id) {
		Optional<Admin> admin = repository.findById(id);
		if (admin.isEmpty()) {
			throw new CommonRuntimeException("Admin not found with id " + id.toString());
		}
		return dataResponseMakeup(admin.get());
	}

	@Override
	public DataResponse<AdminDTO> create(AdminProfileCreateRequest request, MultipartFile avatar) throws IOException {
		if (!StringUtils.isBlank(request.getEmail()) && repository.existsByEmail(request.getEmail())) {
			throw new CommonRuntimeException("Email already exists");
		}

		if (!StringUtils.isBlank(request.getUsername()) && repository.existsByUsername(request.getUsername())) {
			throw new CommonRuntimeException("Username already exists");
		}

		if (!StringUtils.isBlank(request.getPhone()) && repository.existsByPhone(request.getPhone())) {
			throw new CommonRuntimeException("Phone number already exists");
		}

		Admin admin = new Admin(request.getUsername(), passwordEncoder.encode(request.getPassword()),
				request.getPhone(), request.getEmail(), request.getName(), request.getAddress());

		if (avatar != null) {
			MediaResource img = mediaResourceService.save(avatar.getBytes());
			admin.setAvatar(img);
		}

		return dataResponseMakeup(repository.save(admin));
	}

	@Override
	public DataResponse<AdminDTO> update(Long id, AdminProfileUpdateRequest request, MultipartFile avatar)
			throws IOException {
		Admin admin = repository.getReferenceById(id);

		if (request != null) {
			if (!StringUtils.isBlank(request.getEmail()) && repository.existsByEmail(request.getEmail())
					&& !request.getEmail().equals(admin.getEmail())) {
				throw new CommonRuntimeException("Email already exists");
			}
			if (!StringUtils.isBlank(request.getPhone()) && repository.existsByPhone(request.getPhone())
					&& !request.getPhone().equals(admin.getPhone())) {
				throw new CommonRuntimeException("Phone number already exists");
			}

			if (StringUtils.isNotBlank(request.getPhone()) && !request.getPhone().equals(admin.getPhone())) {
				admin.setPhone(request.getPhone());
			}

			if (StringUtils.isNotBlank(request.getEmail()) && !request.getEmail().equals(admin.getEmail())) {
				admin.setEmail(request.getEmail());
			}

			if (StringUtils.isNotBlank(request.getName()) && !request.getName().equals(admin.getName())) {
				admin.setName(request.getName());
			}

			if (StringUtils.isNotBlank(request.getAddress()) && !request.getAddress().equals(admin.getAddress())) {
				admin.setAddress(request.getAddress());
			}
		}

		if (avatar != null) {
			if (admin.getAvatar() != null) {
				MediaResource oldAvatar = admin.getAvatar();
				admin.setAvatar(null);
				if (!mediaResourceService.delete(oldAvatar.getId())) {
					throw new CommonRuntimeException("Cannot delete old avatar image");
				}
			}
			MediaResource img = mediaResourceService.save(avatar.getBytes());
			admin.setAvatar(img);
		}

		return dataResponseMakeup(repository.save(admin));
	}

	@Override
	public DataResponse<AdminDTO> updatePassword(Long id, PasswordUpdateRequest request) {
		Admin admin = repository.getReferenceById(id);

		if (passwordEncoder.matches(request.getOldPassword(), admin.getPassword())) {
			admin.setPassword(passwordEncoder.encode(request.getNewPassword()));
		} else {
			throw new CommonRuntimeException("Wrong password");
		}

		return dataResponseMakeup(repository.save(admin));
	}

	private DataResponse<AdminDTO> dataResponseMakeup(Admin admin) {
		return new DataResponse<>(modelMapper.map(admin, AdminDTO.class));
	}
}
