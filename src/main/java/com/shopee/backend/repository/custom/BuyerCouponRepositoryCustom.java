package com.shopee.backend.repository.custom;

import com.shopee.backend.model.BuyerCoupon;

public interface BuyerCouponRepositoryCustom{
    BuyerCoupon pickCouponByCode(Long idBUyer, String code);
}
