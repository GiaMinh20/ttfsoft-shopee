package com.shopee.backend.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.shopee.backend.dto.base.AbstractDTO;

import lombok.Data;
@Data

public class ShopOrderDetailDTO extends AbstractDTO {
	private Long idOrder;
	private Long idShop;
	private String shopVoucher;
	private Long originalPrice;
	private String status;
	private Long shipPrice;
	private Long priceOff;
	private Long totalPrice;
	private Long totalPrice1;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date complete;
	private String buyerNote;
}
