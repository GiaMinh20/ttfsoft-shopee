package com.shopee.backend.service.impl;

import java.io.IOException;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.dto.BuyerDTO;
import com.shopee.backend.model.Buyer;
import com.shopee.backend.model.MediaResource;
import com.shopee.backend.payload.request.auth.BuyerProfileCreateRequest;
import com.shopee.backend.payload.request.auth.BuyerProfileUpdateRequest;
import com.shopee.backend.payload.request.auth.PasswordUpdateRequest;
import com.shopee.backend.payload.response.DataResponse;
import com.shopee.backend.repository.BuyerRepository;
import com.shopee.backend.service.IBuyerService;
import com.shopee.backend.service.IMediaResourceService;

@Service
public class BuyerService implements IBuyerService {
	@Autowired
	ModelMapper modelMapper;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	BuyerRepository repository;

	@Autowired
	IMediaResourceService mediaResourceService;

	@Override
	public DataResponse<BuyerDTO> getById(Long id) {
		Optional<Buyer> buyer = repository.findById(id);
		if (buyer.isEmpty()) {
			throw new CommonRuntimeException("Buyer not found with id " + id.toString());
		}
		return dataResponseMakeup(buyer.get());
	}

	@Override
	public DataResponse<BuyerDTO> create(BuyerProfileCreateRequest request, MultipartFile avatar) throws IOException {
		if (!StringUtils.isBlank(request.getEmail()) && repository.existsByEmail(request.getEmail())) {
			throw new CommonRuntimeException("Email already exists");
		}
		if (!StringUtils.isBlank(request.getUsername()) && repository.existsByUsername(request.getUsername())) {
			throw new CommonRuntimeException("Username already exists");
		}
		if (!StringUtils.isBlank(request.getPhone()) && repository.existsByPhone(request.getPhone())) {
			throw new CommonRuntimeException("Phone number already exists");
		}

		Buyer buyer = new Buyer(request.getUsername(), passwordEncoder.encode(request.getPassword()),
				request.getPhone(), false, request.getEmail(), false, request.getName(), request.getGender(),
				request.getDob(), true, 0L);

		if (avatar != null) {
			MediaResource img = mediaResourceService.save(avatar.getBytes());
			buyer.setAvatar(img);
		}

		return dataResponseMakeup(repository.save(buyer));
	}

	@Override
	public DataResponse<BuyerDTO> update(Long id, BuyerProfileUpdateRequest request, MultipartFile avatar)
			throws IOException {
		Buyer buyer = repository.getReferenceById(id);

		if (request != null) {
			if (!StringUtils.isBlank(request.getEmail()) && repository.existsByEmail(request.getEmail())
					&& !request.getEmail().equals(buyer.getEmail())) {
				throw new CommonRuntimeException("Email already exists");
			}
			if (!StringUtils.isBlank(request.getPhone()) && repository.existsByPhone(request.getPhone())
					&& !request.getPhone().equals(buyer.getPhone())) {
				throw new CommonRuntimeException("Phone number already exists");
			}

			if (StringUtils.isNotBlank(request.getEmail()) && !request.getEmail().equals(buyer.getEmail())) {
				buyer.setEmail(request.getEmail());
				buyer.setEmailConfirmed(false);
			}
			if (StringUtils.isNotBlank(request.getPhone()) && !request.getPhone().equals(buyer.getPhone())) {
				buyer.setPhone(request.getPhone());
			}
			if (StringUtils.isNotBlank(request.getName()) && !request.getName().equals(buyer.getName())) {
				buyer.setName(request.getName());
			}
			if (request.getGender() != null && !request.getGender().equals(buyer.getGender())) {
				buyer.setGender(request.getGender());
			}
			if (request.getDob() != null && !request.getDob().equals(buyer.getDob())) {
				buyer.setDob(request.getDob());
			}
		}

		if (avatar != null) {
			if (buyer.getAvatar() != null) {
				MediaResource oldAvatar = buyer.getAvatar();
				buyer.setAvatar(null);
				if (!mediaResourceService.delete(oldAvatar.getId())) {
					throw new CommonRuntimeException("Cannot delete old avatar image");
				}
			}
			MediaResource img = mediaResourceService.save(avatar.getBytes());
			buyer.setAvatar(img);
		}

		return dataResponseMakeup(repository.save(buyer));
	}

	@Override
	public DataResponse<BuyerDTO> updatePassword(Long id, PasswordUpdateRequest request) {
		Buyer buyer = repository.getReferenceById(id);

		if (passwordEncoder.matches(request.getOldPassword(), buyer.getPassword())) {
			buyer.setPassword(passwordEncoder.encode(request.getNewPassword()));
		} else {
			throw new CommonRuntimeException("Wrong password");
		}
		return dataResponseMakeup(repository.save(buyer));
	}

	private DataResponse<BuyerDTO> dataResponseMakeup(Buyer buyer) {
		return new DataResponse<>(modelMapper.map(buyer, BuyerDTO.class));
	}
}
