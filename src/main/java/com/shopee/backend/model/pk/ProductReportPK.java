package com.shopee.backend.model.pk;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class ProductReportPK implements Serializable {
	private static final long serialVersionUID = 2582959443207987688L;

	@Column(name = "id_order")
	private Long idOrder;

	@Column(name = "id_product")
	private Long idProduct;
}
