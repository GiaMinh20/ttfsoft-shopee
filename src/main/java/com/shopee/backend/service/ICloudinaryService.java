package com.shopee.backend.service;

import java.io.IOException;
import com.shopee.backend.service.impl.CloudinaryService.CloudinaryUploadResponse;

public interface ICloudinaryService {
	CloudinaryUploadResponse upload(byte[] data) throws IOException;
	boolean delete(String publicId) throws IOException;
	boolean deleteVideo(String publicId) throws IOException;
}
