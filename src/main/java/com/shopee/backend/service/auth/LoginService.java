package com.shopee.backend.service.auth;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.dto.AdminDTO;
import com.shopee.backend.dto.BuyerDTO;
import com.shopee.backend.dto.ShopDTO;
import com.shopee.backend.payload.response.JwtResponse;
import com.shopee.backend.repository.AdminRepository;
import com.shopee.backend.repository.BuyerRepository;
import com.shopee.backend.repository.ShopRepository;
import com.shopee.backend.utility.RolePrefix;
import com.shopee.backend.utility.jwt.JwtUtils;

@Service
public class LoginService {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	JwtUtils jwtUtils;
	
	@Autowired
	BuyerRepository buyerRepository; 
	
	@Autowired
	ShopRepository shopRepository; 
	
	@Autowired
	AdminRepository adminRepository; 
	
	@Autowired
	ModelMapper modelMapper;
	
	public JwtResponse<?> authenticateUser(String username, String password, String rolePrefix) {
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(rolePrefix + username, password));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		String token = jwtUtils.generateJwtToken(authentication, rolePrefix);
		
		if (rolePrefix.equals(RolePrefix.ADMIN)) {
			AdminDetails admin = (AdminDetails)authentication.getPrincipal();
			
			return new JwtResponse<>(
					token, 
					modelMapper.map(
						adminRepository.getReferenceById(admin.getId()), 
						AdminDTO.class
						)
			);
		}
		else if (rolePrefix.equals(RolePrefix.SHOP)) {
			ShopDetails shop = (ShopDetails)authentication.getPrincipal();
			
			return new JwtResponse<>(
					token, 
					modelMapper.map(
						shopRepository.getReferenceById(shop.getId()), 
						ShopDTO.class
					)
			);
		}
		else if (rolePrefix.equals(RolePrefix.BUYER)) {
			BuyerDetails buyer = (BuyerDetails)authentication.getPrincipal();
			
			return new JwtResponse<>(
					token, 
					modelMapper.map(
						buyerRepository.getReferenceById(buyer.getId()), 
						BuyerDTO.class
					)
			);
		}
		
		throw new CommonRuntimeException("Invalid RolePrefix");
	}
}
