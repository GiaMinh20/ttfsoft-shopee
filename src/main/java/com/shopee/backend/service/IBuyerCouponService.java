package com.shopee.backend.service;

import com.shopee.backend.model.BuyerCoupon;

public interface IBuyerCouponService {
    BuyerCoupon findByCode(Long idBuyer, String coupon);
    BuyerCoupon pickCouponByCode(Long idBuyer, String code);
}
