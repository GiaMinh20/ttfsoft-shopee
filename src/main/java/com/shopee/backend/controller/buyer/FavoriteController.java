package com.shopee.backend.controller.buyer;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.constraints.NotEmpty;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.dto.BuyerDTO;
import com.shopee.backend.dto.FavoriteDTO;
import com.shopee.backend.payload.response.BaseResponse;
import com.shopee.backend.payload.response.DataResponse;
import com.shopee.backend.payload.response.ListResponse;
import com.shopee.backend.service.IFavoriteService;
import com.shopee.backend.service.auth.BuyerDetails;

@RestController
@Validated
@RequestMapping("/api/buyer/favorite")
public class FavoriteController {
    @Autowired
    IFavoriteService service;
    @Autowired
    ModelMapper mapper;
    
    @GetMapping
    public ResponseEntity<?> getFavorites(@AuthenticationPrincipal BuyerDetails buyer){
        List<FavoriteDTO> favorites = service.getAllByBuyer(buyer.getId()).stream()
                .map(f -> mapper.map(f, FavoriteDTO.class))
                .collect(Collectors.toList());

        return ResponseEntity.ok(new ListResponse<FavoriteDTO>(favorites));
    }

    /** return all buyer that like this product */
    @GetMapping("/product/{idP}/all")
    public ResponseEntity<?> getFavorites(@PathVariable Long idP){
        List<BuyerDTO> buyers = service.getAllByProduct(idP).stream()
                .map(f -> mapper.map(f.getBuyer(), BuyerDTO.class))
                .collect(Collectors.toList());

        return ResponseEntity.ok(new ListResponse<BuyerDTO>(buyers));
    }

    @PostMapping
    public ResponseEntity<?> addFavorites(@AuthenticationPrincipal BuyerDetails buyer,
            @RequestBody @NotEmpty(message = "Id products must not be empty") Set<Long> idProducts) {
        service.addFavorites(buyer.getId(), idProducts);
        return ResponseEntity.ok(new BaseResponse(true, "Successfully !"));
    }

    @DeleteMapping
    public ResponseEntity<?> unFavorites(@AuthenticationPrincipal BuyerDetails buyer,
            @RequestBody @NotEmpty(message = "Id products must not be empty") Set<Long> idProducts) {
        service.unFavorites(buyer.getId(), idProducts);
        return ResponseEntity.ok(new BaseResponse(true, "Successfully !"));
    }

    @GetMapping("/product/{idP}")
    public ResponseEntity<?> getFavorite(@AuthenticationPrincipal BuyerDetails buyer, @PathVariable Long idP){
        return ResponseEntity.ok(new DataResponse<Boolean>(
            service.isFavorite(buyer.getId(), idP))
        );
    }
}
