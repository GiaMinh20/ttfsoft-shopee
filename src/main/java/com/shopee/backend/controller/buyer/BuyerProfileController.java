package com.shopee.backend.controller.buyer;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.payload.request.auth.BuyerProfileUpdateRequest;
import com.shopee.backend.payload.request.auth.PasswordUpdateRequest;
import com.shopee.backend.service.IBuyerService;
import com.shopee.backend.service.auth.BuyerDetails;

@RestController
@RequestMapping("/api/buyer/profile")
public class BuyerProfileController {	
	@Autowired
	IBuyerService buyerService; 
	
	@GetMapping
    public ResponseEntity<?> get(@AuthenticationPrincipal BuyerDetails buyer) {
		return ResponseEntity.ok(buyerService.getById(buyer.getId()));
    }
	
	@PutMapping(consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<?> update(
			@AuthenticationPrincipal BuyerDetails buyer,
			@RequestPart(value = "info", required = false) @Valid BuyerProfileUpdateRequest request,
			@RequestPart(name = "avatar", required = false) MultipartFile avatar) throws IOException {
		
		return ResponseEntity.ok(buyerService.update(buyer.getId(), request, avatar));
	}
	
	@PutMapping("/password")
	public ResponseEntity<?> updatePassword(
			@AuthenticationPrincipal BuyerDetails buyer,
			@RequestBody @Valid PasswordUpdateRequest request) {
		
		return ResponseEntity.ok(buyerService.updatePassword(buyer.getId(), request));
	}
}
