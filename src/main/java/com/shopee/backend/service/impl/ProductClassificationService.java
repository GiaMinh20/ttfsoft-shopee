package com.shopee.backend.service.impl;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.dto.ProductClassificationDTO;
import com.shopee.backend.model.Product;
import com.shopee.backend.model.ProductClassification;
import com.shopee.backend.payload.request.product.CreateProductVariationRequest;
import com.shopee.backend.payload.request.product.EditProductVariationRequest;
import com.shopee.backend.payload.response.DataResponse;
import com.shopee.backend.payload.response.ListResponse;
import com.shopee.backend.repository.ProductClassificationRepository;
import com.shopee.backend.repository.ProductRepository;
import com.shopee.backend.repository.custom.ProductClassificationRepositoryCustom;
import com.shopee.backend.service.IProductClassificationService;
import com.shopee.backend.utility.PlatformPolicy;
import com.shopee.backend.utility.datatype.EProductClassificationStatus;

@Service
public class ProductClassificationService implements IProductClassificationService {
	@Autowired
	ModelMapper modelMapper;
	@Autowired
	ProductRepository productRepository;
	@Autowired
	ProductClassificationRepository productClassificationRepository;
	@Autowired
	ProductClassificationRepositoryCustom productClassificationRepositoryCustom;

	@Value("${productclassification.notfoundwithid}")
	private String pcNotFoundId;

	@Override
	public DataResponse<ProductClassificationDTO> create(Long idShop, CreateProductVariationRequest request) {

		Product p = productRepository.getReferenceById(request.getIdProduct());

		if (!p.getShop().getId().equals(idShop)) {
			throw new CommonRuntimeException("You can't not create variation on other shop's product");
		}

		ProductClassification pc = new ProductClassification(p, request.getVariationName(), request.getPrice(),
				request.getAvailableQuantity(), request.getDiscount());
		if (request.getDiscount() != null) {
			pc.setDiscount(request.getDiscount());
		}

		return new DataResponse<>(
				modelMapper.map(productClassificationRepository.save(pc), ProductClassificationDTO.class));
	}

	@Override
	public DataResponse<ProductClassificationDTO> update(Long idShop, EditProductVariationRequest request) {
		ProductClassification pc = productClassificationRepository.getReferenceById(request.getIdVariation());

		if (!pc.getProduct().getShop().getId().equals(idShop)) {
			throw new CommonRuntimeException("You can not update variation on other shop's product");
		}

		if (StringUtils.isNotBlank(request.getVariationName())
				&& !request.getVariationName().equals(pc.getVariationName())) {
			pc.setVariationName(request.getVariationName());
		}

		if (request.getPrice() != null && !request.getPrice().equals(pc.getPrice())) {
			pc.setPrice(request.getPrice());
		}

		if (request.getAvailableQuantity() != null && !request.getAvailableQuantity().equals(pc.getQuantity())) {
			pc.setQuantity(request.getAvailableQuantity());
		}

		if (request.getDiscount() != null && !request.getDiscount().equals(pc.getDiscount())) {
			pc.setDiscount(request.getDiscount());
		}

		if (request.getStatus() != null && !request.getStatus().equals(pc.getStatus())) {
			pc.setStatus(request.getStatus());
		}

		return new DataResponse<>(
				modelMapper.map(productClassificationRepository.save(pc), ProductClassificationDTO.class));
	}

	@Override
	public void delete(Long id, Long idShop) {
		ProductClassification pc = productClassificationRepository.getReferenceById(id);
		if (!pc.getProduct().getShop().getId().equals(idShop)) {
			throw new CommonRuntimeException("You can not delete variation of other shop's product");
		}

		delete(pc);
	}

	@Override
	public DataResponse<ProductClassificationDTO> getById(Long id) {
		return getById(id, null);
	}

	@Override
	public DataResponse<ProductClassificationDTO> getById(Long id, Long idShop) {
		Optional<ProductClassification> pc = productClassificationRepository.findById(id);
		if (pc.isEmpty()) {
			throw new CommonRuntimeException(String.format("Product variation with id %d is not found", id));
		} else if (idShop != null && !pc.get().getProduct().getShop().getId().equals(idShop)) {
			throw new CommonRuntimeException("You can not get product variation information of other shop's product");
		}

		return new DataResponse<>(modelMapper.map(pc.get(), ProductClassificationDTO.class));
	}

	@Override
	public ProductClassification findById(Long id) {
		return productClassificationRepository.findById(id)
				.orElseThrow(() -> new CommonRuntimeException(String.format(pcNotFoundId, id)));
	}

	private void delete(ProductClassification pc) {
		if (pc.getProduct().getClassifications().size() <= PlatformPolicy.MIN_ALLOWED_PRODUCT_VARIATION) {
			throw new CommonRuntimeException(String.format("A product must have at least %s product(s) classification",
					PlatformPolicy.MIN_ALLOWED_PRODUCT_VARIATION));
		}
		pc.setStatus(EProductClassificationStatus.HIDDEN);
		productClassificationRepository.save(pc);
	}

	public ProductClassification descreaseQuantity(ProductClassification entity, Long quantity) {
		return productClassificationRepositoryCustom.descreaseQuantity(entity.getId(), quantity);
	}

	public ProductClassification checkAvailable(ProductClassification entity, Long quantity) {
		return productClassificationRepositoryCustom.checkProductClassification(entity, quantity);
	}

	@Override
	public ListResponse<ProductClassificationDTO> getByProductId(Long idProduct, Long id) {
		Product p = productRepository.getReferenceById(idProduct);
		List<ProductClassificationDTO> lst = productClassificationRepository.findByProduct(p).stream()
				.map(p1 -> modelMapper.map(p1, ProductClassificationDTO.class)).toList();
		ListResponse<ProductClassificationDTO> result = new ListResponse<>();
		result.setSuccess(true);
		result.setData(lst);
		return result;
	}
}
