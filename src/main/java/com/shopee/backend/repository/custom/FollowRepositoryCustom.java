package com.shopee.backend.repository.custom;

import java.util.List;

import com.shopee.backend.model.Follow;
import com.shopee.backend.utility.Page;

public interface FollowRepositoryCustom {

	List<Follow> getBuyerFollowList(Long idBuyer, Page page);
	
	Long countBuyerFollowList(Long idBuyer);

}