package com.shopee.backend.repository;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.shopee.backend.model.BuyerCoupon;
import com.shopee.backend.model.pk.BuyerCouponPK;

@Repository
public interface BuyerCouponRepository extends JpaRepository<BuyerCoupon, BuyerCouponPK>{
	@Query(value = "SELECT * FROM buyer_coupon where id_coupon = :idCoupon and id_buyer = :idBuyer", nativeQuery = true)
	public BuyerCoupon findByIdCoupon(@Param("idCoupon") Long idCoupon, @Param("idBuyer") Long idBuyer);
	
	@Query(value = "SELECT * FROM buyer_coupon where id_buyer = :idBuyer", nativeQuery = true)
	public List<BuyerCoupon> getAllCouponByIdBuyer(@Param("idBuyer") Long idBuyer);

	@Query("SELECT bc FROM BuyerCoupon bc JOIN bc.couponCode c WHERE bc.buyer.id = ?1 AND c.code = ?2")
	BuyerCoupon findByCode(Long idBuyer, String coupon);
		
	@Query(value = "SELECT * FROM buyer_coupon where id_coupon = :idCoupon", nativeQuery = true)
	public BuyerCoupon findByIdCouponWithouBuyer(@Param("idCoupon") Long idCoupon);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE buyer_coupon SET is_used = false WHERE id_coupon = :idCoupon", nativeQuery = true)
	public void updateCouponStatus(@Param("idCoupon") Long idCoupon);
}

