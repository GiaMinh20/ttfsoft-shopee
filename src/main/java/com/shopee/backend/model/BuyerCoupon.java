package com.shopee.backend.model;

import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.validation.constraints.NotNull;

import com.shopee.backend.model.pk.BuyerCouponPK;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
public class BuyerCoupon {
	@EmbeddedId
	private BuyerCouponPK id = new BuyerCouponPK();

	@ManyToOne
	@MapsId("idBuyer")
	@JoinColumn(name = "id_buyer", insertable = false, updatable = false)
	private Buyer buyer;

	@ManyToOne
	@MapsId("idCoupon")
	@JoinColumn(name = "id_coupon", insertable = false, updatable = false)
	private CouponCode couponCode;

	@NotNull
	private Date time;

	@NotNull
	private int isUsed;
}
