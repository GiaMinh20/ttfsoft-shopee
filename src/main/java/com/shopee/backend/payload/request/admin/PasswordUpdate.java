package com.shopee.backend.payload.request.admin;

import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PasswordUpdate {
	private Long id;
	@Size(min = 8)
	private String newpassword;
}
