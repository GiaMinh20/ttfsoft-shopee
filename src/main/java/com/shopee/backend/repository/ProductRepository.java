package com.shopee.backend.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.shopee.backend.model.Product;
import com.shopee.backend.repository.custom.ProductRepositoryCustom;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>, ProductRepositoryCustom {

	@Transactional
	@Modifying
	@Query(value = "update product set view = view + 1 where id = :id", nativeQuery = true)
	void setViewProduct(@Param("id") Long id);
}
