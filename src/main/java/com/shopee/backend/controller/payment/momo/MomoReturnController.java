package com.shopee.backend.controller.payment.momo;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/api")
public class MomoReturnController {
	@GetMapping("/returnmomo")
	private String newPost2(HttpServletRequest req) {
		try {
			return new ObjectMapper().writeValueAsString(req.getParameterMap());
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}

}
