package com.shopee.backend.service;

import com.shopee.backend.payload.response.BaseResponse;

public interface IAdminProductManagement {

	BaseResponse banProduct(Long[] ids);

	BaseResponse deleteProduct(Long[] id);

	BaseResponse show_hide_Product(Long[] ids);
}
