package com.shopee.backend.dto;


import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ProductImageDTO {
	private Long id;
	private Long idProduct;
	private String url;
	private String resourceType;
}
