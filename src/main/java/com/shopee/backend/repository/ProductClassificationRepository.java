package com.shopee.backend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.shopee.backend.model.Product;
import com.shopee.backend.model.ProductClassification;

public interface ProductClassificationRepository extends JpaRepository<ProductClassification, Long> {
    
    List<ProductClassification> findByProduct(Product product);
    @Query(value = "SELECT id from product_classification where id_product = :id", nativeQuery = true)
    List<Long> findidByProduct(@Param("id")Long idProduct);
}
