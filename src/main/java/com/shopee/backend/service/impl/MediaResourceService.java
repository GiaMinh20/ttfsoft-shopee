package com.shopee.backend.service.impl;

import java.io.IOException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.model.MediaResource;
import com.shopee.backend.repository.MediaResourceRepository;
import com.shopee.backend.service.ICloudinaryService;
import com.shopee.backend.service.IMediaResourceService;
import com.shopee.backend.service.impl.CloudinaryService.CloudinaryUploadResponse;

@Service
public class MediaResourceService implements IMediaResourceService {
	@Autowired
	private MediaResourceRepository repository;
	
	@Autowired
	ICloudinaryService cloudinaryService;
	
	@Override
	public MediaResource save(byte[] data) {
		try {
			CloudinaryUploadResponse resp =  cloudinaryService.upload(data);
			MediaResource m = new MediaResource(
					resp.getUrl(), 
					resp.getPublicId(),
					resp.getResourceType());
			return repository.save(m);
		} catch (IOException e) {
			throw new CommonRuntimeException("IOException occurred when upload data to Cloudinary service (" + e.getMessage() + ")");
		}
	}
	
	@Override
	public boolean delete(Long id) {
		Optional<MediaResource> m = repository.findById(id);
		if (!m.isPresent()) {
			throw new CommonRuntimeException("Media resource can not be found");
		}
		
		try {
			if(m.get().getUrl().endsWith(".mp4"))
				if (cloudinaryService.deleteVideo(m.get().getPublicId())) {
					repository.delete(m.get());
					return true;
				}
	
			if (cloudinaryService.delete(m.get().getPublicId())) {
				repository.delete(m.get());
				return true;
			}
			else {
				return false;
			}
		} catch (IOException e) {
			throw new CommonRuntimeException("IOException occurred when delete data from Cloudinary service (" + e.getMessage() + ")");
		}
	}
}
