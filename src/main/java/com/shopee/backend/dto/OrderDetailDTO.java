package com.shopee.backend.dto;

import lombok.Data;

@Data
public class OrderDetailDTO {
	private Long idOrderShop;
	private Long idProductClassification;
	private Long quantity;
	private Long unitPrice;
}
