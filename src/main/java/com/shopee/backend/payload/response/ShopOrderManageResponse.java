package com.shopee.backend.payload.response;

import java.util.Date;
import java.util.List;

//gitlab.com/GiaMinh20/ttfsoft-shopee.git
import com.shopee.backend.utility.datatype.EOrderStatus;
import com.shopee.backend.utility.datatype.EPaymentMethod;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShopOrderManageResponse {
	private Long idShop;
	private Long idOrder;
	private String shopVoucher;
	private EOrderStatus status;
	private Long shipPrice;
	private Long totalPrice;
	private Date complete;
	private Date createdDate;
	private Long prepareDate;

	private Long idUser;
	private Long idAddress;
	private EPaymentMethod paymentMethod;
	private List<ProductOfShopOrderResponse> productResponse;

}
