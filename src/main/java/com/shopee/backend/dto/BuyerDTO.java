package com.shopee.backend.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.shopee.backend.dto.base.UserDTO;
import com.shopee.backend.utility.datatype.EGender;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class BuyerDTO extends UserDTO {
	private EGender gender;
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date dob;
	private Boolean isActive;
	private Boolean phoneConfirmed;
	private Boolean emailConfirmed;
	private Long numDeliveryAddress;
	private List<DeliveryAddressDTO> addresses;
}
