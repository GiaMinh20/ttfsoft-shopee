package com.shopee.backend.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.dto.DeliveryAddressDTO;
import com.shopee.backend.dto.MediaResourceDTO;
import com.shopee.backend.dto.ShopDTO;
import com.shopee.backend.dto.ShopOrderDTO;
import com.shopee.backend.model.Buyer;
import com.shopee.backend.model.BuyerCoupon;
import com.shopee.backend.model.CartDetail;
import com.shopee.backend.model.CouponCode;
import com.shopee.backend.model.DeliveryAddress;
import com.shopee.backend.model.Notification;
import com.shopee.backend.model.Order;
import com.shopee.backend.model.OrderDetail;
import com.shopee.backend.model.Product;
import com.shopee.backend.model.ProductClassification;
import com.shopee.backend.model.ShopOrder;
import com.shopee.backend.payload.request.order.CreateOrderRequest;
import com.shopee.backend.payload.request.order.ShopOrderItem;
import com.shopee.backend.payload.response.CreateOrderResponse;
import com.shopee.backend.payload.response.OrderManageResponse;
import com.shopee.backend.payload.response.ProductOrderRespone;
import com.shopee.backend.repository.BuyerRepository;
import com.shopee.backend.repository.CouponRepository;
import com.shopee.backend.repository.DeliveryAddressRepository;
import com.shopee.backend.repository.NotificationRepository;
import com.shopee.backend.repository.OrderDetailRepository;
import com.shopee.backend.repository.OrderRepository;
import com.shopee.backend.repository.ShopOrderRepository;
import com.shopee.backend.repository.ShopRepository;
import com.shopee.backend.service.IBuyerCouponService;
import com.shopee.backend.service.ICartDetailService;
import com.shopee.backend.service.IOrderService;
import com.shopee.backend.utility.datatype.ECouponCodeType;
import com.shopee.backend.utility.datatype.EOrderStatus;

@Service
public class OrderService implements IOrderService {

	@Autowired
	CouponCodeService couponService;

	@Autowired
	IBuyerCouponService buyerCouponService;

	@Autowired
	ProductClassificationService productClassificationService;

	@Autowired
	OrderRepository orderRepository;

	@Autowired
	ShopOrderRepository shopOrderRepository;

	@Autowired
	OrderDetailRepository orderDetailRepository;

	@Autowired
	BuyerRepository buyerRepository;

	@Autowired
	ShopRepository shopRepository;

	@Autowired
	DeliveryAddressRepository deliveryAddressRepository;

	@Autowired
	CouponRepository couponRepository;

	@Autowired
	ModelMapper modelMapper;

	@Autowired
	ICartDetailService cartDetailService;

	@Autowired
	NotificationRepository notificationRepository;

	@Value("${coupon.notfoundinshop}")
	private String couponNotFoundInShop;
	@Value("${coupon.notfoundincollection}")
	private String couponNotFoundInCollection;
	@Value("${coupon.notShopee}")
	private String couponNotShopee;
	@Value("${coupon.notFreeship}")
	private String couponNotFreeship;
	@Value("${order.missMatchPrice}")
	private String missMatchPrice;
	@Value("${order.notfoundwithidbuyer}")
	private String ordernotfoundwithidbuyer;
	@Value("${order.notfound}")
	private String ordernotfound;

	@Override
	@Transactional
	public CreateOrderResponse createOrder(CreateOrderRequest body, Long idBuyer) {
		Order orderEntity = modelMapper.map(body, Order.class);
		orderEntity.setBuyer(buyerRepository.getReferenceById(idBuyer));
		orderEntity.setAddress(findAddress(body.getIdAddress(), orderEntity.getBuyer()));
		orderEntity.setCreateTime(new Date());
		// create shop order
		for (ShopOrderItem<Long> shopOrderItem : body.getShopOrderItems()) {
			ShopOrder shopOrder = createShopOrder(shopOrderItem, idBuyer);
			shopOrder.setOrder(orderEntity);
			orderEntity.addPriceOff(shopOrder.getShopPriceOff());
			orderEntity.getShopOrders().add(shopOrder);

			orderEntity.addPriceOriginal(shopOrder.getOriginalPrice() + shopOrder.getShipPrice());
			orderEntity.addPriceOff(shopOrder.getPriceOff());
		}
		// apply freeship voucher
		applyShopeeCoupon(orderEntity, idBuyer, orderEntity.getFreeshipCoupon(), true);
		// apply platform voucher shopee
		applyShopeeCoupon(orderEntity, idBuyer, orderEntity.getPlatformCoupon(), false);
		orderEntity = orderRepository.save(orderEntity);
		//create notification
		createNotification(orderEntity.getBuyer());
		
		return new CreateOrderResponse(orderEntity.getId(), orderEntity.getPaymentMethod(),
				orderEntity.getPriceFinal());
	}

	private void createNotification(Buyer buyer) {
		Notification entity = new Notification(buyer, null, "Create order successfully","Create order successfully", false);
		notificationRepository.save(entity);
	}

	private DeliveryAddress findAddress(Long idAddress, Buyer buyer) {
		Optional<DeliveryAddress> d = buyer.getAddresses().stream()
				.filter(a -> a.getId().equals(idAddress)).findFirst();
		if (d.isEmpty())
			throw new CommonRuntimeException("Delivery address not found !");
		return d.get();
	}

	private ShopOrder createShopOrder(ShopOrderItem<Long> shopOrderItem, Long idBuyer) {
		final ShopOrder shopOrder = modelMapper.map(shopOrderItem, ShopOrder.class);
		shopOrder.setShop(shopRepository.findById(shopOrderItem.getIdShop())
				.orElseThrow(() -> new CommonRuntimeException("Shop not found with id " + shopOrderItem.getIdShop())));
		shopOrder.setShipPrice(8000L);
		shopOrder.setStatus(EOrderStatus.WAIT_FOR_PAYMENT);
		List<CartDetail> cds = cartDetailService.findAllById(shopOrderItem.getIdProductClassifications(), idBuyer);
		if(cds.size() < shopOrderItem.getIdProductClassifications().size())
			findMissCartDetail(shopOrderItem.getIdProductClassifications(), cds);
		
		// create order details
		cds.stream().forEach(cd -> {
			checkShop(shopOrderItem.getIdShop(), cd);
			OrderDetail orderDetail = createOrderDetail(shopOrder, cd);
			shopOrder.addOriginalPrice(orderDetail.getUnitPrice() * orderDetail.getQuantity());
			shopOrder.getOrderDetails().add(orderDetail);
		});
		// apply shop voucher
		if (StringUtils.isNotBlank(shopOrder.getShopVoucher())) {
			shopOrder.setShopPriceOff(applyShopCoupon(shopOrder, idBuyer));
		}
		return shopOrder;
	}

	private void checkShop(Long idShop, CartDetail cd) {
		Long cIdShop = cd.getProductClassification().getProduct().getShop().getId();
		if (!idShop.equals(cIdShop))
			throw new CommonRuntimeException(
				String.format(" Shop %s is not have product classification %s", idShop,  cd.getProductClassification().getId()));
	}

	private void findMissCartDetail(Collection<Long> idPs, List<CartDetail> cds) {
		List<Long> cIdP = cds.stream().map(cd -> cd.getProductClassification().getId()).collect(Collectors.toList());
		Long idPMiss = idPs.stream().filter(idP -> !cIdP.contains(idP)).findFirst().get();
		throw new CommonRuntimeException("Cannot find cart detail with id product classification " + idPMiss);
	}

	private OrderDetail createOrderDetail(ShopOrder shopOrder, CartDetail cartDetail) {
		OrderDetail orderDetail = modelMapper.map(cartDetail, OrderDetail.class);
		orderDetail.setShopOrder(shopOrder);
		orderDetail.setProductClassification(productClassificationService
				.checkAvailable(cartDetail.getProductClassification(), cartDetail.getQuantity()));
		Double discount = cartDetail.getProductClassification().getDiscount();
		if (discount == null)
			discount = 0.0;
		Long priceWithDiscount = Math.round(cartDetail.getProductClassification().getPrice() * (1 - discount));
		orderDetail.setUnitPrice(priceWithDiscount);

		cartDetailService.delete(cartDetail);
		return orderDetail;
	}

	private Long applyShopCoupon(ShopOrder shopOrder, Long idBuyer) {
		BuyerCoupon cpShop = buyerCouponService.pickCouponByCode(idBuyer, shopOrder.getShopVoucher());
		if (cpShop == null || cpShop.getCouponCode().getShop() != shopOrder.getShop())
			throw new CommonRuntimeException(
					String.format(couponNotFoundInShop, shopOrder.getShopVoucher(), shopOrder.getShop().getName()));

		return couponService.applyCoupon(shopOrder.getOriginalPrice(), cpShop.getCouponCode());
	}

	private Order applyShopeeCoupon(Order orderEntity, Long idBuyer, String code, Boolean isFreeship) {
		if (StringUtils.isBlank(code))
			return orderEntity;
		BuyerCoupon cp = buyerCouponService.pickCouponByCode(idBuyer, code);
		if (cp == null)
			throw new CommonRuntimeException(
					String.format(couponNotFoundInCollection, orderEntity.getPlatformCoupon()));

		if (cp.getCouponCode().getShop() != null)
			throw new CommonRuntimeException(String.format(couponNotShopee, cp.getCouponCode().getCode()));

		if (isFreeship) {
			if (cp.getCouponCode().getType() != ECouponCodeType.FREESHIP)
				throw new CommonRuntimeException(String.format(couponNotFreeship, cp.getCouponCode().getCode()));
			for (ShopOrder shopOrder : orderEntity.getShopOrders()) {
				Long shipPriceOff = couponService.applyCoupon(shopOrder.getShipPrice(), cp.getCouponCode());
				shopOrder.setShipPriceOff(shipPriceOff);
				orderEntity.addPriceOff(shipPriceOff);
			}
		} else
			seperatePriceOff(orderEntity, cp.getCouponCode());

		return orderEntity;
	}

	private Order seperatePriceOff(Order order, CouponCode coupon) {
		if (coupon.getType() == ECouponCodeType.PERCENT_DISCOUNT) {
			order.getShopOrders().stream().forEach(shopOrder -> {
				Long shopPriceOff = shopOrder.getOriginalPrice() * coupon.getDiscount() / 100;
				shopOrder.setPlatformPriceOff(shopPriceOff);
				order.addPriceOff(shopPriceOff);
			});

		} else {
			Long priceOff = couponService.applyCoupon(order.getPriceOrginal(), coupon);

			for (ShopOrder shopOrder : order.getShopOrders()) {
				Long shopPriceOff = Math.min(priceOff, shopOrder.getOriginalPrice());
				shopOrder.setPlatformPriceOff(shopPriceOff);
				order.addPriceOff(shopPriceOff);

				priceOff -= shopPriceOff;
				if (priceOff <= 0)
					break;
			}
		}
		return order;
	}

	@Override
	public List<OrderManageResponse> getOrdersByBuyer(Long id) {
		List<OrderManageResponse> listOrderManageResponses = new ArrayList<>();
		List<ShopOrder> shopOrders = shopOrderRepository.getShopOrdersByBuyer(id);
		if (shopOrders.isEmpty()) {
			throw new EntityNotFoundException(ordernotfound);
		}
		for (ShopOrder shopOrder : shopOrders) {
			OrderManageResponse om = new OrderManageResponse();
			List<OrderDetail> orderDetails = orderDetailRepository.findByShopOrderId(shopOrder.getId());
			List<ProductOrderRespone> listProduct = createListProduct(orderDetails);
			Order order = shopOrder.getOrder();
			om.setListProduct(listProduct);
			om.setAddress(modelMapper.map(order.getAddress(), DeliveryAddressDTO.class));
			om.setOrderId(order.getId());
			// check here
			om.setPayment(order.getPaymentMethod());
			om.setShop(modelMapper.map(shopOrder.getShop(), ShopDTO.class));
			om.setShopOrder(modelMapper.map(shopOrder, ShopOrderDTO.class));

			listOrderManageResponses.add(om);

		}
		return listOrderManageResponses;
	}

	@Override
	public OrderManageResponse orderDetail(Long id) {
		ShopOrder shopOrder = shopOrderRepository.getReferenceById(id);
		Buyer buyer = modelMapper.map(SecurityContextHolder.getContext().getAuthentication().getPrincipal(),
				Buyer.class);
		Order order = orderRepository.getReferenceById(shopOrder.getOrder().getId());
		if (buyer.getId() != order.getBuyer().getId()) {
			throw new EntityNotFoundException(ordernotfoundwithidbuyer);
		}
		OrderManageResponse om = new OrderManageResponse();
		List<OrderDetail> orderDetails = orderDetailRepository.findByShopOrderId(shopOrder.getId());
		List<ProductOrderRespone> listProduct = createListProduct(orderDetails);

		om.setAddress(modelMapper.map(order.getAddress(), DeliveryAddressDTO.class));
		om.setListProduct(listProduct);
		om.setShopOrder(modelMapper.map(shopOrder, ShopOrderDTO.class));
		om.setShop(modelMapper.map(shopOrder.getShop(), ShopDTO.class));
		return om;
	}

	public Boolean checkMatchPrice(CreateOrderRequest body, Order order) {
		if (body.getPriceFinal() != order.getPriceFinal())
			throw new CommonRuntimeException(
					String.format(missMatchPrice, body.getPriceFinal(), order.getPriceFinal()));
		return true;
	}

	@Override
	public List<OrderManageResponse> getOrderByStatus(Long buyerId, Long statusId) {
		List<OrderManageResponse> listOrderManageResponses = new ArrayList<>();
		List<ShopOrder> shopOrders = shopOrderRepository.getShopOrderByStatus(buyerId, statusId);
		for (ShopOrder shopOrder : shopOrders) {
			OrderManageResponse om = new OrderManageResponse();
			List<OrderDetail> orderDetails = orderDetailRepository.findByShopOrderId(shopOrder.getId());
			List<ProductOrderRespone> listProduct = createListProduct(orderDetails);
			om.setListProduct(listProduct);
			om.setShopOrder(modelMapper.map(shopOrder, ShopOrderDTO.class));
			om.setShop(modelMapper.map(shopOrder.getShop(), ShopDTO.class));
			listOrderManageResponses.add(om);
		}
		return listOrderManageResponses;
	}

	@Override
	public List<OrderManageResponse> searchOrders(Long id, String keyword) {
		List<ShopOrder> shopOrders = shopOrderRepository.search(id, keyword);
		List<OrderManageResponse> listOm = new ArrayList<>();
		for (ShopOrder shopOrder : shopOrders) {
			OrderManageResponse om = new OrderManageResponse();
			List<OrderDetail> orderDetails = orderDetailRepository.findByShopOrderId(shopOrder.getId());
			List<ProductOrderRespone> listProduct = createListProduct(orderDetails);
			om.setAddress(modelMapper.map(orderRepository.getById(shopOrder.getOrder().getId()).getAddress(),
					DeliveryAddressDTO.class));
			om.setListProduct(listProduct);
			om.setShopOrder(modelMapper.map(shopOrder, ShopOrderDTO.class));
			om.setShop(modelMapper.map(shopOrder.getShop(), ShopDTO.class));
			om.setPayment(shopOrder.getOrder().getPaymentMethod());
			listOm.add(om);
		}

		return listOm;
	}

	private List<ProductOrderRespone> createListProduct(List<OrderDetail> orderDetails) {
		List<ProductOrderRespone> listProduct = new ArrayList<>();
		for (OrderDetail orderDetail : orderDetails) {
			ProductClassification pdc = orderDetail.getProductClassification();
			Product product = pdc.getProduct();
			ProductOrderRespone por = new ProductOrderRespone();
			if (product.getAvatar() != null) {
				MediaResourceDTO avatar = modelMapper.map(product.getAvatar(), MediaResourceDTO.class);
				por.setAvatar(avatar);
			}
			por.setClassification(pdc.getVariationName());
			por.setName(product.getName());
			por.setPriceOrigin(pdc.getPrice());
			por.setQuantity(orderDetail.getQuantity());
			listProduct.add(por);
		}
		return listProduct;
	}
}
