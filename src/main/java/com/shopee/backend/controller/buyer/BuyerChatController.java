package com.shopee.backend.controller.buyer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.service.auth.BuyerDetails;
import com.shopee.backend.service.impl.ChatService;

@RestController
@RequestMapping("/api/buyer/chat")
public class BuyerChatController {
	@Autowired
	ChatService chatService;
	
	@GetMapping("/{idShop}")
	public ResponseEntity<?> fetchMessages(
			@AuthenticationPrincipal BuyerDetails buyer, 
			@PathVariable("idShop") Long idShop,
			@RequestParam(name = "page", required = false) Integer page) {
		return ResponseEntity.ok(chatService.fetchMessage(buyer.getId(), idShop, page));
	}
}
