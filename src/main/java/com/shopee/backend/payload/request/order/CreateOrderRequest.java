package com.shopee.backend.payload.request.order;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.shopee.backend.utility.datatype.EPaymentMethod;

import lombok.Data;

@Data
public class CreateOrderRequest{
    private List<@Valid ShopOrderItem<Long>> shopOrderItems = new ArrayList<>();
    @NotNull(message = "Payment must not be null")
    private EPaymentMethod paymentMethod;
    @NotNull(message = "id address must not be null")
    private Long idAddress;
    
    private Long priceOriginal;
    private Long shipPrice;
    private Long shipPriceOff;
    private Long platformPriceOff;

    private String freeshipCoupon;
    private String platformCoupon;

    private String returnUrl;

    public Long getPriceFinal(){
        return priceOriginal + shipPrice - shipPriceOff - platformPriceOff;
    }
}
