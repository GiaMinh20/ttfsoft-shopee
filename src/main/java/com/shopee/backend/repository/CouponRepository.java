package com.shopee.backend.repository;

import java.util.List;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.shopee.backend.model.CouponCode;

@Repository
public interface CouponRepository extends JpaRepository<CouponCode, Long>{
	@Query(value = "SELECT * FROM coupon_code where id_shop = :id_shop", nativeQuery = true)
	public List<CouponCode> getAllCouponCodesByIdShop(@Param("id_shop") Long idShop);
	
	@Query(value = "SELECT * FROM coupon_code where id_shop is null", nativeQuery = true)
	public List<CouponCode> getAllCouponCodesShopee();

	@Transactional
	@Lock(LockModeType.PESSIMISTIC_READ)
	CouponCode findByCode(String code);

}
