package com.shopee.backend.utility.datatype;

public enum EGender {
	MALE,
	FEMALE,
	OTHER
}
