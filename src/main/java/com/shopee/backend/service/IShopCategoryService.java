package com.shopee.backend.service;

import com.shopee.backend.dto.ShopCategoryDTO;
import com.shopee.backend.payload.request.shopcategory.CreateShopCategoryRequest;
import com.shopee.backend.payload.request.shopcategory.EditShopCategoryRequest;
import com.shopee.backend.payload.response.DataResponse;
import com.shopee.backend.payload.response.ListResponse;

public interface IShopCategoryService {

	boolean delete(Long idShop, Long id);

	DataResponse<ShopCategoryDTO> update(Long idShop, Long id, EditShopCategoryRequest request);

	ListResponse<ShopCategoryDTO> findByName(String name);

	DataResponse<ShopCategoryDTO> getById(Long id);

	ListResponse<ShopCategoryDTO> findByShopAndName(Long idShop, String name);

	ListResponse<ShopCategoryDTO> getAllByShop(Long idShop);

	DataResponse<ShopCategoryDTO> create(Long idShop, CreateShopCategoryRequest request);



}
