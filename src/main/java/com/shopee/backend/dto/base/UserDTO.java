package com.shopee.backend.dto.base;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.shopee.backend.model.MediaResource;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public abstract class UserDTO extends AbstractDTO {
	@NotEmpty
	@Size(min = 5, max = 30, message = "Username should have at least 5 characters")
	private String username;
	
	@Pattern(regexp="0[0-9]+", message = "Phone number must contain only number character and begin with 0")
	@Size(min = 10, max = 10, message = "Phone number must have 10 number character")
	private String phone;
	
	@NotEmpty
	@Size(min = 2, message = "Name should have at least 2 characters")
	private String name;
	
	@Email
	private String email;
	private MediaResource avatar;
}
