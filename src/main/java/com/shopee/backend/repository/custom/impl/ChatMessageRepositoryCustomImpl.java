package com.shopee.backend.repository.custom.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.shopee.backend.model.Buyer_;
import com.shopee.backend.model.ChatMessage;
import com.shopee.backend.model.ChatMessage_;
import com.shopee.backend.model.Shop_;
import com.shopee.backend.repository.custom.ChatMessageRepositoryCustom;
import com.shopee.backend.utility.Page;

@Repository
public class ChatMessageRepositoryCustomImpl implements ChatMessageRepositoryCustom {
	@PersistenceContext
	private EntityManager em;
	
	@Override
	public List<ChatMessage> get(Long idBuyer, Long idShop, Page page) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ChatMessage> cq = cb.createQuery(ChatMessage.class);
		Root<ChatMessage> root = cq.from(ChatMessage.class);
		
		cq.select(root).where(
				cb.and(
						cb.equal(root.get(ChatMessage_.buyer).get(Buyer_.id), idBuyer),
						cb.equal(root.get(ChatMessage_.shop).get(Shop_.id), idShop)
					)
			);
		
		cq.orderBy(cb.desc(root.get(ChatMessage_.time)));
		
		TypedQuery<ChatMessage> query = em.createQuery(cq);
		if (page != null) {
			query.setFirstResult(page.getPageNumber() * page.getPageSize());
			query.setMaxResults(page.getPageSize());
		}
		
		return query.getResultList();
	}
	
	@Override
	public Long count(Long idBuyer, Long idShop) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<ChatMessage> root = cq.from(ChatMessage.class);
		
		cq.select(cb.count(root)).where(
				cb.and(
						cb.equal(root.get(ChatMessage_.buyer).get(Buyer_.id), idBuyer),
						cb.equal(root.get(ChatMessage_.shop).get(Shop_.id), idShop)
					)
			);
		
		return em.createQuery(cq).getSingleResult();
	}
}
