package com.shopee.backend.utility.datatype;

public enum ECouponCodeType {
	FREESHIP,
	PERCENT_DISCOUNT,
	DISCOUNT
}
