package com.shopee.backend.service.impl;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.dto.ShopDTO;
import com.shopee.backend.dto.holder.DataHolder;
import com.shopee.backend.model.MediaResource;
import com.shopee.backend.model.Shop;
import com.shopee.backend.payload.request.auth.PasswordUpdateRequest;
import com.shopee.backend.payload.request.auth.ShopProfileCreateRequest;
import com.shopee.backend.payload.request.auth.ShopProfileUpdateRequest;
import com.shopee.backend.payload.response.DataResponse;
import com.shopee.backend.payload.response.ShopCustomCount;
import com.shopee.backend.payload.response.WorkPerformance;
import com.shopee.backend.repository.ShopRepository;
import com.shopee.backend.service.IMediaResourceService;
import com.shopee.backend.service.IShopService;
import com.shopee.backend.utility.datatype.EOrderStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Service
public class ShopService implements IShopService {
	@Autowired
	ModelMapper modelMapper;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	ShopRepository repository;

	@Autowired
	IMediaResourceService mediaResourceService;

	@Override
	public DataResponse<ShopDTO> getById(Long id) {
		Optional<Shop> shop = repository.findById(id);
		if (shop.isEmpty()) {
			throw new CommonRuntimeException("Shop not found with id " + id.toString());
		}

		return dataResponseMakeup(shop.get());
	}

	@Override
	public DataResponse<ShopDTO> create(ShopProfileCreateRequest request, MultipartFile avatar,
			MultipartFile coverImage) throws IOException {
		if (!StringUtils.isBlank(request.getEmail()) && repository.existsByEmail(request.getEmail())) {
			throw new CommonRuntimeException("Email already exists");
		}
		if (!StringUtils.isBlank(request.getUsername()) && repository.existsByUsername(request.getUsername())) {
			throw new CommonRuntimeException("Username already exists");
		}
		if (!StringUtils.isBlank(request.getPhone()) && repository.existsByPhone(request.getPhone())) {
			throw new CommonRuntimeException("Phone number already exists");
		}

		Shop shop = new Shop(request.getUsername(), passwordEncoder.encode(request.getPassword()), request.getPhone(),
				false, request.getEmail(), false, request.getName(), request.getAddress(), true);

		if (avatar != null) {
			MediaResource img = mediaResourceService.save(avatar.getBytes());
			shop.setAvatar(img);
		}

		if (coverImage != null) {
			MediaResource img = mediaResourceService.save(coverImage.getBytes());
			shop.setCoverImage(img);
		}

		return dataResponseMakeup(repository.save(shop));
	}

	@Override
	public DataResponse<ShopDTO> update(Long id, ShopProfileUpdateRequest request, MultipartFile avatar,
			MultipartFile coverImage) throws IOException {
		Shop shop = repository.getReferenceById(id);

		if (request != null) {
			if (!StringUtils.isBlank(request.getEmail()) && repository.existsByEmail(request.getEmail())
					&& !request.getEmail().equals(shop.getEmail())) {
				throw new CommonRuntimeException("Email already exists");
			}
			if (!StringUtils.isBlank(request.getPhone()) && repository.existsByPhone(request.getPhone())
					&& !request.getPhone().equals(shop.getPhone())) {
				throw new CommonRuntimeException("Phone number already exists");
			}

			if (StringUtils.isNotBlank(request.getPhone()) && !request.getPhone().equals(shop.getPhone())) {
				shop.setPhone(request.getPhone());
			}

			if (StringUtils.isNotBlank(request.getEmail()) && !request.getEmail().equals(shop.getEmail())) {
				shop.setEmail(request.getEmail());
				shop.setEmailConfirmed(false);
			}

			if (StringUtils.isNotBlank(request.getName()) && !request.getName().equals(shop.getName())) {
				shop.setName(request.getName());
			}

			if (StringUtils.isNotBlank(request.getAddress()) && !request.getAddress().equals(shop.getAddress())) {
				shop.setAddress(request.getAddress());
			}

			if (StringUtils.isNotBlank(request.getDefaultReplyMessage())
					&& !request.getDefaultReplyMessage().equals(shop.getDefaultReplyMessage())) {
				shop.setDefaultReplyMessage(request.getDefaultReplyMessage());
			}
		}

		if (avatar != null) {
			if (shop.getAvatar() != null) {
				MediaResource oldAvatar = shop.getAvatar();
				shop.setAvatar(null);
				if (!mediaResourceService.delete(oldAvatar.getId())) {
					throw new CommonRuntimeException("Cannot delete old avatar image");
				}
			}
			MediaResource img = mediaResourceService.save(avatar.getBytes());
			shop.setAvatar(img);
		}

		if (coverImage != null) {
			if (shop.getCoverImage() != null) {
				MediaResource oldCoverImage = shop.getCoverImage();
				shop.setCoverImage(null);
				if (!mediaResourceService.delete(oldCoverImage.getId())) {
					throw new CommonRuntimeException("Cannot delete old cover image");
				}
			}
			MediaResource img = mediaResourceService.save(coverImage.getBytes());
			shop.setCoverImage(img);
		}

		return dataResponseMakeup(repository.save(shop));
	}

	@Override
	public DataResponse<ShopDTO> updatePassword(Long id, PasswordUpdateRequest request) {
		Shop shop = repository.getReferenceById(id);

		if (passwordEncoder.matches(request.getOldPassword(), shop.getPassword())) {
			shop.setPassword(passwordEncoder.encode(request.getNewPassword()));
		} else {
			throw new CommonRuntimeException("Wrong password");
		}
		return dataResponseMakeup(repository.save(shop));
	}

	@Override
	public ShopCustomCount getCustomCountProduct(Shop shop) {

		ShopCustomCount shopCustomCount = new ShopCustomCount();
		List<DataHolder> lst = repository.countOrderByStatus(shop);
		Long countOutOfStock = repository.countOutOfStock(shop.getId());

		Long countAccessTimesShop = repository.countAccessTimesShop(shop);

		Long countOrderConfirmed = repository.countOrderConfirmed(shop);

		shopCustomCount.setCountAccessTimesShop(countAccessTimesShop);
		shopCustomCount.setCountOrderConfirmed(countOrderConfirmed);
		shopCustomCount.setPOutOfStock(countOutOfStock);

		/*
		 * WAIT_FOR_PAYMENT, WAIT_FOR_CONFIRMATION, WAIT_FOR_SENDING, DELIVERING,
		 * DELIVERED, CANCELLED
		 */
		for (int i = 0; i < lst.size(); i++) {
			if (lst.get(i).getStatus() == EOrderStatus.WAIT_FOR_PAYMENT)
				shopCustomCount.setPWAITFORPAYMENT(lst.get(i).getValue());
			if (lst.get(i).getStatus() == EOrderStatus.WAIT_FOR_CONFIRMATION)
				shopCustomCount.setPWAITFORCONFIRMATION(lst.get(i).getValue());
			if (lst.get(i).getStatus() == EOrderStatus.WAIT_FOR_SENDING)
				shopCustomCount.setPWAITFORSENDING(lst.get(i).getValue());
			if (lst.get(i).getStatus() == EOrderStatus.DELIVERING)
				shopCustomCount.setPDELIVERING(lst.get(i).getValue());
			if (lst.get(i).getStatus() == EOrderStatus.DELIVERED)
				shopCustomCount.setPDELIVERED(lst.get(i).getValue());
			if (lst.get(i).getStatus() == EOrderStatus.CANCELLED)
				shopCustomCount.setPCANCELLED(lst.get(i).getValue());
		}
		WorkPerformance performance = repository.getWorkPerformance(shop);
		shopCustomCount.setWorkPerformance(performance);

		shopCustomCount.setSuccess(true);
		return shopCustomCount;
	}

	@Getter
	@Setter
	@AllArgsConstructor
	@NoArgsConstructor
	public class SalesIndicator {
		private Long sales;
		private Long salesValue;

	}

	@Override
	public DataResponse<SalesIndicator> getSales(Shop shop, Date dStart, Date dEnd, Long dLast) {
		Long sales = repository.sales(shop, dStart, dEnd, dLast);
		Long salesValue = repository.salesValue(shop, dStart, dEnd, dLast);

		return new DataResponse<>(new SalesIndicator(sales, salesValue));
	}

	private DataResponse<ShopDTO> dataResponseMakeup(Shop shop) {
		return new DataResponse<>(modelMapper.map(shop, ShopDTO.class));
	}
}
