package com.shopee.backend.service.auth;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import com.shopee.backend.model.Buyer;
import com.shopee.backend.model.MediaResource;
import com.shopee.backend.service.auth.impl.UserDetailsImpl;
import com.shopee.backend.utility.datatype.EGender;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class BuyerDetails extends UserDetailsImpl {
	private static final long serialVersionUID = 5049020068819796467L;

	private String phone;
	private Boolean phoneConfirmed;
	private String email;
	private Boolean emailConfirmed;
	private MediaResource avatar;
	private String fullname;
	private EGender gender;
	private Date dob;
	private Boolean isActive;
	
	public BuyerDetails() {
		super();
	}

	public BuyerDetails(Long id, String username, String password, String phone, Boolean phoneConfirmed,
			String email, Boolean emailConfirmed, MediaResource avatar, String fullname, EGender gender, Date dob,
			Boolean isActive, Collection<? extends GrantedAuthority> authorities) {
		super(id, username, password, authorities);
		this.phone = phone;
		this.phoneConfirmed = phoneConfirmed;
		this.email = email;
		this.emailConfirmed = emailConfirmed;
		this.avatar = avatar;
		this.fullname = fullname;
		this.gender = gender;
		this.dob = dob;
		this.isActive = isActive;
	}
	
	public static BuyerDetails build(Buyer user) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority("ROLE_BUYER"));
		
		return new BuyerDetails(
				user.getId(), 
				user.getUsername(), 
				user.getPassword(), 
				user.getPhone(), 
				user.getPhoneConfirmed(), 
				user.getEmail(), 
				user.getEmailConfirmed(), 
				user.getAvatar(),
				user.getName(),
				user.getGender(),
				user.getDob(),
				user.getIsActive(),
				authorities);
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return isActive;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return isActive;
	}
}
