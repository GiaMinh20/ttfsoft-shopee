package com.shopee.backend.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.dto.BuyerCouponDTO;
import com.shopee.backend.dto.CouponCodeDTO;
import com.shopee.backend.dto.ShopDTO;
import com.shopee.backend.model.Buyer;
import com.shopee.backend.model.BuyerCoupon;
import com.shopee.backend.model.CouponCode;
import com.shopee.backend.model.Shop;
import com.shopee.backend.model.pk.BuyerCouponPK;
import com.shopee.backend.payload.response.CouponCodeResponse;
import com.shopee.backend.payload.response.ResponseData;
import com.shopee.backend.repository.BuyerCouponRepository;
import com.shopee.backend.repository.BuyerRepository;
import com.shopee.backend.repository.CouponRepository;
import com.shopee.backend.repository.ShopRepository;
import com.shopee.backend.service.ICouponCodeService;
import com.shopee.backend.service.IShopService;
import com.shopee.backend.service.auth.ShopDetails;
import com.shopee.backend.utility.datatype.ECouponCodeType;

@Service
public class CouponCodeService implements ICouponCodeService {

	@Autowired
	private CouponRepository repository;
	@Autowired
	private final ModelMapper mapper;
	@Autowired
	private ShopRepository shopRepository;
	@Autowired
	private IShopService shopService;
	@Autowired
	private BuyerCouponRepository buyerCouponRepository;
	@Autowired
	private BuyerRepository buyerRepository;
	
	@Value("${coupon.notfound}")
	private String couponnotfound;
	
	@Value("${coupon.notfoundwithid}")
	private String couponnotfoundwithid;
	
	@Value("${shop.shopnotfound}")
	private String shopshopnotfound;
	
	@Value("${coupon.updatecouponfoundbyshop}")
	private String couponupdatecouponfoundbyshop;
	
	@Value("${coupon.updatecouponfoundbyadmin}")
	private String couponupdatecouponfoundbyadmin;
	
	@Value("${coupon.delete}")
	private String coupondelete;
	
	@Value("${coupon.quantitynull}")
	private String couponquantitynull;

	@Value("${coupon.isnull}")
	private String couponNull;

	@Value("${coupon.addMoreProduct}")
	private String addMoreProduct;


	public CouponCodeService(CouponRepository repository, ModelMapper mapper, ShopRepository shopRepository,
			IShopService shopService, BuyerCouponRepository buyerCouponRepository, BuyerRepository buyerRepository) {
		super();
		this.repository = repository;
		this.mapper = mapper;
		this.shopRepository = shopRepository;
		this.shopService = shopService;
		this.buyerCouponRepository = buyerCouponRepository;
		this.buyerRepository = buyerRepository;
	}

	@Override
	public ResponseData<CouponCodeResponse> getAllCouponCodes() {
		ResponseData<CouponCodeResponse> rs = new ResponseData<>();
		List<CouponCodeResponse> listCouponCodes = new ArrayList<>();
		List<CouponCode> couponCodes = repository.findAll();
		if(couponCodes.isEmpty()) {
			rs.setSuccess(false);
			rs.setMessage(couponnotfound);
		}
		for (CouponCode couponCode : couponCodes) {
			CouponCodeResponse couponCodeResponse = new CouponCodeResponse();
			couponCodeResponse.setCoupon(mapper.map(couponCode, CouponCodeDTO.class));
			if (couponCode.getShop() != null) {
				couponCodeResponse.setShop(mapper.map(couponCode.getShop(), ShopDTO.class));
			} else {
				couponCodeResponse.setShop(null);
			}
			listCouponCodes.add(couponCodeResponse);
		}
		rs.setSuccess(true);
		rs.setDatas(listCouponCodes);
		return rs;
	}

	@Override
	public ResponseData<CouponCodeResponse> getCouponCodeById(Long id) {
		ResponseData<CouponCodeResponse> rs = new ResponseData<>();
		Optional<CouponCode> couponCode = repository.findById(id);

		if (couponCode.isEmpty()) {
			rs.setSuccess(false);
			rs.setMessage(couponnotfoundwithid + id);
			return rs;
		}
		CouponCodeResponse couponCodeResponse = new CouponCodeResponse();
		couponCodeResponse.setCoupon(mapper.map(couponCode, CouponCodeDTO.class));
		if (couponCode.get().getShop() != null) {
			couponCodeResponse.setShop(mapper.map(couponCode.get().getShop(), ShopDTO.class));
		} else {
			couponCodeResponse.setShop(null);
		}
		rs.setSuccess(true);
		rs.setData(couponCodeResponse);
		return rs;
	}

	@Override
	public ResponseData<CouponCodeResponse> getAllCouponCodesByIdShop(Long idShop) {
		ResponseData<CouponCodeResponse> rs = new ResponseData<>();
		Optional<Shop> shop = shopRepository.findById(idShop);
		if (shop.isEmpty()) {
			rs.setSuccess(false);
			rs.setMessage(shopshopnotfound);
			return rs;
		}
		List<CouponCodeResponse> listCouponCodes = new ArrayList<>();
		List<CouponCode> couponCodes = repository.getAllCouponCodesByIdShop(idShop);
		for (CouponCode couponCode : couponCodes) {
			CouponCodeResponse couponCodeResponse = new CouponCodeResponse();
			couponCodeResponse.setCoupon(mapper.map(couponCode, CouponCodeDTO.class));
			couponCodeResponse.setShop(mapper.map(shop, ShopDTO.class));
			listCouponCodes.add(couponCodeResponse);
		}
		rs.setSuccess(true);
		rs.setDatas(listCouponCodes);
		return rs;
	}

	@Override
	public ResponseData<CouponCodeDTO> editCouponCode(CouponCodeDTO newCouponCodeDTO, Long id) {
		ResponseData<CouponCodeDTO> rs = new ResponseData<>();
		CouponCode entity = repository.getReferenceById(id);
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String role = authentication.getAuthorities().iterator().next().getAuthority();
		// Xử lý khi đang nhập quyền Shop
		if (role.equals("ROLE_SHOP")) {
			Long userId = ((ShopDetails) authentication.getPrincipal()).getId();
			// Nếu voucher không thuộc shop nào hoặc thuộc shop khác thì ko có quyền sửa
			if (entity.getShop() == null || !entity.getShop().getId().equals(userId)) {
				rs.setSuccess(false);
				rs.setMessage(couponupdatecouponfoundbyshop);
				return rs;
			}
			// if it is shop's voucher
			if (entity.getShop() != null && entity.getShop().getId().equals(userId)) {
				entity.setCode(newCouponCodeDTO.getCode());
				entity.setDescription(newCouponCodeDTO.getDescription());
				entity.setDod(newCouponCodeDTO.getDod());
				entity.setName(newCouponCodeDTO.getName());
				entity.setQuantity(newCouponCodeDTO.getQuantity());
				entity.setStatus(newCouponCodeDTO.getStatus());
				entity.setType(newCouponCodeDTO.getType());
				entity.setDiscount(newCouponCodeDTO.getDiscount());
				entity.setDiscountLimit(newCouponCodeDTO.getDiscountLimit());
				entity.setMinPrice(newCouponCodeDTO.getMinPrice());
				entity = repository.save(entity);
				CouponCodeDTO couponCodeDTO = mapper.map(entity, CouponCodeDTO.class);
				couponCodeDTO.setIdShop(entity.getShop().getId());
				rs.setSuccess(true);
				rs.setData(couponCodeDTO);
				return rs;
			}
		}
		// Xử lý khi đăng nhập quyền Admin
		// Nếu voucher thuộc shop thì admin ko có quyền sửa
		if (entity.getShop() != null) {
			rs.setSuccess(false);
			rs.setMessage(couponupdatecouponfoundbyadmin);
			return rs;
		} else {
			entity.setCode(newCouponCodeDTO.getCode());
			entity.setDescription(newCouponCodeDTO.getDescription());
			entity.setDod(newCouponCodeDTO.getDod());
			entity.setName(newCouponCodeDTO.getName());
			entity.setQuantity(newCouponCodeDTO.getQuantity());
			entity.setStatus(newCouponCodeDTO.getStatus());
			entity.setType(newCouponCodeDTO.getType());
			entity.setDiscount(newCouponCodeDTO.getDiscount());
			entity.setDiscountLimit(newCouponCodeDTO.getDiscountLimit());
			entity.setMinPrice(newCouponCodeDTO.getMinPrice());
			entity = repository.save(entity);
			rs.setSuccess(true);
			rs.setData(mapper.map(entity, CouponCodeDTO.class));
			return rs;
		}

	}

	@Override
	public ResponseData<CouponCodeDTO> createCouponCode(CouponCodeDTO couponCodeDTO) {
		ResponseData<CouponCodeDTO> rs = new ResponseData<>();
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String role = authentication.getAuthorities().iterator().next().getAuthority();
		CouponCode couponCode = mapper.map(couponCodeDTO, CouponCode.class);
		if (role.equals("ROLE_SHOP")) {
			Shop shop = mapper.map(SecurityContextHolder.getContext().getAuthentication().getPrincipal(), Shop.class);
			couponCode.setShop(shop);
			repository.save(couponCode);
			CouponCodeDTO couponCodeDTO2 = mapper.map(couponCode, CouponCodeDTO.class);
			couponCodeDTO2.setIdShop(couponCode.getShop().getId());
			rs.setSuccess(true);
			rs.setData(couponCodeDTO2);
		} else {
			repository.save(couponCode);
			CouponCodeDTO couponCodeDTO2 = mapper.map(couponCode, CouponCodeDTO.class);
			rs.setSuccess(true);
			rs.setData(couponCodeDTO2);
		}

		return rs;
	}

	@Override
	public ResponseData<CouponCodeDTO> detleteCouponCode(Long id) {
		ResponseData<CouponCodeDTO> rs = new ResponseData<>();
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String role = authentication.getAuthorities().iterator().next().getAuthority();
		Optional<CouponCode> couponCode = repository.findById(id);
		if (couponCode.isEmpty()) {
			rs.setSuccess(false);
			rs.setMessage(couponnotfoundwithid + id);
			return rs;
		}
		CouponCode entity = couponCode.get();
		if (role.equals("ROLE_SHOP")) {
			Long userId = ((ShopDetails) authentication.getPrincipal()).getId();
			if (entity.getShop() == null || !entity.getShop().getId().equals(userId)) {
				rs.setSuccess(false);
				rs.setMessage(couponupdatecouponfoundbyshop);
				return rs;
			}
			repository.deleteById(id);
			rs.setSuccess(true);
			rs.setMessage(coupondelete);
			return rs;
		}
		if (entity.getShop() != null) {
			rs.setSuccess(false);
			rs.setMessage(couponupdatecouponfoundbyadmin);
			return rs;
		}
		repository.deleteById(id);
		rs.setSuccess(true);
		rs.setMessage(coupondelete);
		return rs;
	}

	@Override
	public ResponseData<CouponCodeResponse> getAllCouponCodesShopee() {
		ResponseData<CouponCodeResponse> rs = new ResponseData<>();
		List<CouponCodeResponse> listCouponCodes = new ArrayList<>();
		List<CouponCode> couponCodes = repository.getAllCouponCodesShopee();
		for (CouponCode couponCode : couponCodes) {
			CouponCodeResponse couponCodeResponse = new CouponCodeResponse();
			couponCodeResponse.setCoupon(mapper.map(couponCode, CouponCodeDTO.class));
			listCouponCodes.add(couponCodeResponse);
		}
		rs.setSuccess(true);
		rs.setDatas(listCouponCodes);
		return rs;
	}

	@Override
	public ResponseData<BuyerCouponDTO> addToList(Long id) {
		ResponseData<BuyerCouponDTO> rs = new ResponseData<>();
		Optional<CouponCode> couponCode = repository.findById(id);
		if (couponCode.isEmpty()) {
			rs.setSuccess(false);
			rs.setMessage(couponnotfoundwithid + id);
			return rs;
		}
		CouponCode entity = couponCode.get();

		if (entity.getQuantity() <= 0) {
			rs.setSuccess(false);
			rs.setMessage(couponquantitynull);
			return rs;
		}
		Buyer buyer = mapper.map(SecurityContextHolder.getContext().getAuthentication().getPrincipal(), Buyer.class);
		Long idBuyer = buyer.getId();
		Long idCoupon = entity.getId();
		BuyerCoupon buyerCoupon = buyerCouponRepository.findByIdCoupon(idCoupon, idBuyer);
		if (buyerCoupon == null) {
			buyerCoupon = new BuyerCoupon();
			buyerCoupon.setId(new BuyerCouponPK(idBuyer, idCoupon));
			buyerCoupon.setBuyer(buyerRepository.getReferenceById(idBuyer));
			buyerCoupon.setCouponCode(repository.getReferenceById(idCoupon));
		}
		buyerCoupon.setTime(entity.getDod());
		buyerCoupon.setIsUsed(0);
		BuyerCouponDTO buyerCouponDTO = mapper.map(buyerCoupon, BuyerCouponDTO.class);
		buyerCouponDTO.setIdBuyer(buyerCoupon.getId().getIdBuyer());
		buyerCouponDTO.setIdCoupon(buyerCoupon.getId().getIdCoupon());
		entity.setQuantity(entity.getQuantity() - 1);
		buyerCouponRepository.save(buyerCoupon);
		repository.save(entity);
		rs.setSuccess(true);
		rs.setData(buyerCouponDTO);

		return rs;

	}

	@Override
	public ResponseData<CouponCodeResponse> getAllCouponsByIdBuyer() {
		ResponseData<CouponCodeResponse> rs = new ResponseData<>();
		Buyer buyer = mapper.map(SecurityContextHolder.getContext().getAuthentication().getPrincipal(), Buyer.class);
		Long idBuyer = buyer.getId();
		List<BuyerCoupon> buyerCoupons = buyerCouponRepository.getAllCouponByIdBuyer(idBuyer);
		if (buyerCoupons.isEmpty()) {
			rs.setSuccess(false);
			rs.setMessage(couponnotfound);
			return rs;
		}
		List<CouponCodeResponse> lisCouponCode = new ArrayList<>();
		for (BuyerCoupon buyerCoupon : buyerCoupons) {
			CouponCodeResponse couponCodeResponse = new CouponCodeResponse();
			CouponCodeDTO couponCodeDTO = mapper.map(buyerCoupon.getCouponCode(), CouponCodeDTO.class);
			couponCodeDTO.setStatus(buyerCoupon.getIsUsed());
			couponCodeResponse.setCoupon(couponCodeDTO);
			if (buyerCoupon.getCouponCode().getShop() != null) {
				couponCodeResponse.setShop(mapper.map(buyerCoupon.getCouponCode().getShop(), ShopDTO.class));
			} else {
				couponCodeResponse.setShop(null);
			}
			lisCouponCode.add(couponCodeResponse);
		}
		rs.setSuccess(true);
		rs.setDatas(lisCouponCode);
		return rs;
	}

	@Override
	public Long applyCoupon(Long priceOriginal, CouponCode coupon) {
		if (coupon == null)
			throw new CommonRuntimeException(couponNull);

		if (coupon.getMinPrice() != null && priceOriginal < coupon.getMinPrice())
			throw new CommonRuntimeException(
					String.format(addMoreProduct, coupon.getMinPrice() - priceOriginal, coupon.getCode()));

		if (coupon.getType() == ECouponCodeType.FREESHIP)
			return Math.min(coupon.getDiscount(), priceOriginal);
		else {
			if (coupon.getType() == ECouponCodeType.PERCENT_DISCOUNT)
				return Math.min(coupon.getDiscount() * priceOriginal / 100, coupon.getDiscountLimit());
			else if (coupon.getType() == ECouponCodeType.DISCOUNT)
				return coupon.getDiscount();
		}
		return 0L;
	}

	@Override
	public CouponCode findByCode(String code) {
		return repository.findByCode(code);
	}
}
