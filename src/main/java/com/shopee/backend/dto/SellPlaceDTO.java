package com.shopee.backend.dto;

import javax.validation.constraints.NotBlank;

import com.shopee.backend.dto.base.AbstractDTO;

import lombok.Data;

@Data
public class SellPlaceDTO extends AbstractDTO{
	@NotBlank
	private String code;
	@NotBlank
	private String name;
}
