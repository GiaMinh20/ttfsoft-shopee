package com.shopee.backend.dto;

import java.util.Date;

import com.shopee.backend.model.MediaResource;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class ChatMessageDTO {
	private Long idBuyer;
	private Long idShop;
	
	private String buyerUsername;
	private String shopUsername;
	
	private MediaResource image;
	private String content;
	private Boolean isBuyerSent;
	
	private Boolean seen;
	private Date time;
}
