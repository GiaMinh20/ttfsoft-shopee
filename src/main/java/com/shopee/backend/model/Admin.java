package com.shopee.backend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "admin")
public class Admin {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@OneToOne
	@JoinColumn(name = "id_avatar")
	private MediaResource avatar;

	@NotEmpty
	@Column(length = 50)
	private String username;

	@NotEmpty
	@Column(length = 120)
	private String password;

	@NotEmpty
	@Column(length = 50)
	private String email;

	@NotEmpty
	@Column(length = 50)
	private String name;

	@Column(length = 10)
	private String phone;
	
	private String address;

	public Admin(@NotBlank String username, @NotBlank String password, String phone, @NotBlank String email,
			@NotBlank String name, String address) {
		super();
		this.username = username;
		this.password = password;
		this.phone = phone;
		this.email = email;
		this.name = name;
		this.address = address;
	}
}
