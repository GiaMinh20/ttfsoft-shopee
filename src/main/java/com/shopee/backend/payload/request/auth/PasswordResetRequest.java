package com.shopee.backend.payload.request.auth;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PasswordResetRequest {
	@Schema(type = "string", format = "email")
	@NotEmpty
	private String email;
	
	@NotEmpty
	private String resetCode;
	
	@NotEmpty
	@Size(min = 8, max = 30, message = "Password should have at least 8 characters")
	private String newPassword;
	
	public PasswordResetRequest(String resetCode, String newPassword) {
		this.resetCode = resetCode;
		this.newPassword = newPassword;
	}
	
	public PasswordResetRequest() {
		
	}
}
