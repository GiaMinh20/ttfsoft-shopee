package com.shopee.backend.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.dto.DeliveryAddressDTO;
import com.shopee.backend.model.Buyer;
import com.shopee.backend.model.DeliveryAddress;
import com.shopee.backend.payload.response.ResponseData;
import com.shopee.backend.repository.BuyerRepository;
import com.shopee.backend.repository.DeliveryAddressRepository;
import com.shopee.backend.service.IDeliveryAddress;
import com.shopee.backend.service.auth.BuyerDetails;

@Service
public class DeliveryAddressService implements IDeliveryAddress {

	@Autowired
	DeliveryAddressRepository deliveryAddressRepository;
	@Autowired
	BuyerRepository buyerRepository;
	@Autowired
	private ModelMapper modelMapper;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public ResponseData<DeliveryAddressDTO> getBuyerDeliveryAddress(Long idBuyer) {
		ResponseData<DeliveryAddressDTO> result = new ResponseData<>();
		List<DeliveryAddressDTO> lst = new ArrayList<>();
		for (DeliveryAddress del : deliveryAddressRepository.findByBuyer(buyerRepository.getReferenceById(idBuyer))) {
			DeliveryAddressDTO temp = modelMapper.map(del, DeliveryAddressDTO.class);
			temp.setIdBuyer(del.getBuyer().getId());
			lst.add(temp);
		}

		if (!lst.isEmpty()) {
			result.setDatas(lst);
			result.setSuccess(true);
		} else {
			result.setSuccess(false);
			result.setMessage("You do not have delivery address !");
		}

		return result;
	}

	@Transactional
	@Override
	public ResponseData<DeliveryAddressDTO> getBuyerDeliveryAddress(Long id, Long idBuyer) {
		Optional<DeliveryAddress> result = deliveryAddressRepository.findById(id);
		ResponseData<DeliveryAddressDTO> res = new ResponseData<>();
		if (result.isPresent() && result.get().getBuyer().getId().equals(idBuyer)) {
			DeliveryAddressDTO output = modelMapper.map(result.get(), DeliveryAddressDTO.class);
			output.setIdBuyer(idBuyer);
			res.setSuccess(true);
			res.setData(output);
		} else {
			res.setSuccess(false);
			res.setMessage("Delivery address not found !");
		}

		return res;
	}

	@Transactional
	@Override
	public ResponseData<DeliveryAddressDTO> save(DeliveryAddressDTO input) {
		Long idBuyer = ((BuyerDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
		// Lock buyer
		Buyer buyer = entityManager.find(Buyer.class, idBuyer, LockModeType.PESSIMISTIC_WRITE);

		Long amount = buyer.getDeliveryAddresses();
		if (amount >= 5)
			return new ResponseData<>(true, "Your number of delivery address is limited at 5 !", null);

		if (input.getIsDefault())
			setIsDefault(idBuyer);

		DeliveryAddress entity = modelMapper.map(input, DeliveryAddress.class);

		if (input.getName() == null)
			entity.setName(buyer.getName());
		if (input.getPhone() == null)
			entity.setPhone(buyer.getPhone());
		entity.setBuyer(buyer);
		entity = deliveryAddressRepository.save(entity);
		input = modelMapper.map(entity, DeliveryAddressDTO.class);

		buyer.setDeliveryAddresses(amount + 1);
		entityManager.persist(buyer);

		return new ResponseData<>(true, "Add new delivery address successfully !", input);
	}

	@Transactional
	@Override
	public ResponseData<DeliveryAddressDTO> update(Long id, DeliveryAddressDTO input) {
		DeliveryAddress entity = entityManager.find(DeliveryAddress.class, id, LockModeType.OPTIMISTIC);
		if (entity == null)
			throw new EntityNotFoundException("Delivery address not found with id:" + id);

		Long idBuyer = ((BuyerDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();

		if (!idBuyer.equals(entity.getBuyer().getId()))
			throw new CommonRuntimeException("You do not have right to change other user's delivery address !");

		if (input.getIsDefault())
			setIsDefault(idBuyer);

		entity.setAddress(input.getAddress());
		entity.setCity(input.getCity());
		entity.setState(input.getState());
		entity.setCountry(input.getCountry());
		if (input.getName() != null)
			entity.setName(input.getName());
		if (input.getPhone() != null)
			entity.setPhone(input.getPhone());
		entity.setIsDefault(input.getIsDefault());

		entityManager.persist(entity);

		input = modelMapper.map(entity, DeliveryAddressDTO.class);

		return new ResponseData<>(true, "Update delivery address successfully !", input);

	}

	@Override
	@Transactional
	public void delete(Long[] ids, Long idBuyer) {
		for (Long id : ids) {
			DeliveryAddress d = deliveryAddressRepository.getReferenceById(id);
			if (!d.getBuyer().getId().equals(idBuyer))
				throw new CommonRuntimeException("Delivery address not found with id: " + id);
		}
		deliveryAddressRepository.deleteAllById(Arrays.asList(ids));

		Buyer buyer = buyerRepository.getReferenceById(idBuyer);

		buyer.setDeliveryAddresses(buyer.getDeliveryAddresses() - ids.length);
		buyerRepository.save(buyer);
	}

	private void setIsDefault(Long idBuyer) {
		List<DeliveryAddress> lst = deliveryAddressRepository.findByBuyer(buyerRepository.getReferenceById(idBuyer));

		if (lst == null || lst.isEmpty())
			return;

		lst.stream().forEach(d -> d.setIsDefault(false));

		deliveryAddressRepository.saveAll(lst);
	}
}
