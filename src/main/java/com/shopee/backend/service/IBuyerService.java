package com.shopee.backend.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.dto.BuyerDTO;
import com.shopee.backend.payload.request.auth.BuyerProfileCreateRequest;
import com.shopee.backend.payload.request.auth.BuyerProfileUpdateRequest;
import com.shopee.backend.payload.request.auth.PasswordUpdateRequest;
import com.shopee.backend.payload.response.DataResponse;

public interface IBuyerService {

	DataResponse<BuyerDTO> getById(Long id);

	DataResponse<BuyerDTO> create(BuyerProfileCreateRequest request, MultipartFile avatar) throws IOException;

	DataResponse<BuyerDTO> update(Long id, BuyerProfileUpdateRequest request, MultipartFile avatar) throws IOException;

	DataResponse<BuyerDTO> updatePassword(Long id, PasswordUpdateRequest request);

}