package com.shopee.backend.controller.shop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.service.auth.ShopDetails;
import com.shopee.backend.service.impl.ChatService;

@RestController
@RequestMapping("/api/shop/chat")
public class ShopChatController {
	@Autowired
	ChatService chatService;
	
	@GetMapping("/{idBuyer}")
	public ResponseEntity<?> fetchMessages(
			@AuthenticationPrincipal ShopDetails shop, 
			@PathVariable("idBuyer") Long idBuyer,
			@RequestParam(name = "page", required = false) Integer page) {
		return ResponseEntity.ok(chatService.fetchMessage(idBuyer, shop.getId(), page));
	}
}
