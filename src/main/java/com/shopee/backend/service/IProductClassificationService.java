package com.shopee.backend.service;

import com.shopee.backend.dto.ProductClassificationDTO;
import com.shopee.backend.model.ProductClassification;
import com.shopee.backend.payload.request.product.CreateProductVariationRequest;
import com.shopee.backend.payload.request.product.EditProductVariationRequest;
import com.shopee.backend.payload.response.DataResponse;
import com.shopee.backend.payload.response.ListResponse;

public interface IProductClassificationService {

	DataResponse<ProductClassificationDTO> getById(Long id);
	
	DataResponse<ProductClassificationDTO> getById(Long id, Long idShop);

	ProductClassification findById(Long id);

	void delete(Long id, Long idShop);

	DataResponse<ProductClassificationDTO> create(Long idShop, CreateProductVariationRequest request);
	
	DataResponse<ProductClassificationDTO> update(Long idShop, EditProductVariationRequest request);

	ListResponse<ProductClassificationDTO> getByProductId(Long idProduct, Long id);
	
}
