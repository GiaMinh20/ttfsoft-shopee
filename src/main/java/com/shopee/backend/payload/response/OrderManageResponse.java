package com.shopee.backend.payload.response;

import java.util.List;

import com.shopee.backend.dto.DeliveryAddressDTO;
import com.shopee.backend.dto.ShopDTO;
import com.shopee.backend.dto.ShopOrderDTO;
import com.shopee.backend.utility.datatype.EOrderStatus;
import com.shopee.backend.utility.datatype.EPaymentMethod;

import lombok.Data;

@Data
public class OrderManageResponse {
	private Long orderId;
	private EPaymentMethod payment;
	private ShopOrderDTO shopOrder;
	private ShopDTO shop;
	private List<ProductOrderRespone> listProduct;
	private DeliveryAddressDTO address;
}
