package com.shopee.backend.payload.request.auth;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ShopProfileCreateRequest {
	@NotEmpty
	@Size(min = 5, max = 30, message = "Username should have at least 5 characters")
	private String username;

	@NotEmpty
	@Size(min = 8, max = 30, message = "Password should have at least 8 characters")
	private String password;

	@NotEmpty
	@Pattern(regexp="0[0-9]+", message = "Phone number must contain only number character and begin with 0")
	@Size(min = 10, max = 10, message = "Phone number must have 10 number character")
	private String phone;
	
	@NotEmpty
	@Schema(type = "string", format = "email")
	@Email
	private String email;

	@NotEmpty
	@Size(min = 2, message = "Name should have at least 2 characters")
	private String name;

	@NotEmpty
	@Size(min = 10)
	private String address;

	private String defaultReplyMessage;
}
