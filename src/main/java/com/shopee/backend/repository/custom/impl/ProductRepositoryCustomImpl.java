package com.shopee.backend.repository.custom.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.data.jpa.repository.query.QueryUtils;
import org.springframework.stereotype.Repository;

import com.shopee.backend.model.Product;
import com.shopee.backend.model.ProductCategory_;
import com.shopee.backend.model.ProductClassification;
import com.shopee.backend.model.ProductClassification_;
import com.shopee.backend.model.ProductReview;
import com.shopee.backend.model.ProductReview_;
import com.shopee.backend.model.ProductType_;
import com.shopee.backend.model.Product_;
import com.shopee.backend.model.SellPlace_;
import com.shopee.backend.model.ShopCategory_;
import com.shopee.backend.model.Shop_;
import com.shopee.backend.repository.custom.ProductRepositoryCustom;
import com.shopee.backend.utility.Page;
import com.shopee.backend.utility.datatype.EProductStatus;

@Repository
public class ProductRepositoryCustomImpl implements ProductRepositoryCustom {

	/*
	 * Get product for buyer -> Only get product with status: Active
	 */
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Product> get(Long idShop, Long idCategory, Long idType, Long idShopCategory, Long idSellPlace,
			String searchName, EProductStatus status, Boolean isNew, Page page) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Product> cq = cb.createQuery(Product.class);
		Root<Product> root = cq.from(Product.class);

		List<Predicate> filters = new ArrayList<>();
		if (idShop != null) {
			filters.add(cb.equal(root.get(Product_.shop).get(Shop_.id), idShop));
		}
		if (idCategory != null) {
			filters.add(cb.equal(root.get(Product_.productCategory).get(ProductCategory_.id), idCategory));
		}
		if (idType != null) {
			filters.add(cb.equal(root.get(Product_.productType).get(ProductType_.id), idType));	
		}
		if (idShopCategory != null) {
			filters.add(cb.equal(root.get(Product_.shopCategory).get(ShopCategory_.id), idShopCategory));
		}
		if (idSellPlace != null) {
			filters.add(cb.equal(root.get(Product_.sellPlace).get(SellPlace_.id), idSellPlace));
		}
		if (searchName != null) {
			filters.add(cb.like(root.get(Product_.name), "%" + searchName + "%"));
		}
		if (status != null) {
			filters.add(cb.equal(root.get(Product_.status), status));
		}
		if (isNew != null) {
			filters.add(cb.equal(root.get(Product_.isNew), isNew));
		}

		if (page != null) {
			cq.orderBy(QueryUtils.toOrders(page.getSort(), root, cb));
		}

		Predicate filter = cb.and(filters.toArray(new Predicate[0]));
		cq.select(root).where(filter);
		TypedQuery<Product> query = em.createQuery(cq);

		if (page != null) {
			query.setFirstResult(page.getPageNumber() * page.getPageSize());
			query.setMaxResults(page.getPageSize());
		}
		return query.getResultList();
	}

	@Override
	public Long count(Long idShop, Long idCategory, Long idType, Long idShopCategory, Long idSellPlace,
			String searchName, EProductStatus status, Boolean isNew) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<Product> root = cq.from(Product.class);
		List<Predicate> filters = new ArrayList<>();
		if (idShop != null) {
			filters.add(cb.equal(root.get(Product_.shop).get(Shop_.id), idShop));
		}
		if (idCategory != null) {
			filters.add(cb.equal(root.get(Product_.productCategory).get(ProductCategory_.id), idCategory));
		}
		if (idType != null) {
			filters.add(cb.equal(root.get(Product_.productType).get(ProductType_.id), idType));
		}
		if (idShopCategory != null) {
			filters.add(cb.equal(root.get(Product_.shopCategory).get(ShopCategory_.id), idShopCategory));
		}
		if (idSellPlace != null) {
			filters.add(cb.equal(root.get(Product_.sellPlace).get(SellPlace_.id), idSellPlace));
		}
		if (searchName != null) {
			filters.add(cb.like(root.get(Product_.name), "%" + searchName + "%"));
		}
		if (status != null) {
			filters.add(cb.equal(root.get(Product_.status), status));
		}
		if (isNew != null) {
			filters.add(cb.equal(root.get(Product_.isNew), isNew));
		}

		Predicate filter = cb.and(filters.toArray(new Predicate[0]));
		cq.select(cb.count(root)).where(filter);
		return em.createQuery(cq).getSingleResult();
	}

	@Transactional
	@Override
	public void refresh(Product product) {
		em.refresh(product);
	}

	@Override
	public List<Product> getTopBuy() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Product> cq = cb.createQuery(Product.class);
		Root<Product> root = cq.from(Product.class);
		Sort sort = Sort.unsorted();
		sort = sort.and(JpaSort.of(Product_.sales).descending());

		cq.orderBy(QueryUtils.toOrders(sort, root, cb));
		cq.select(root);
		TypedQuery<Product> query = em.createQuery(cq);
		query.setMaxResults(10);
		return query.getResultList();
	}

	@Override
	public Map<String, Long> getProductRating(Long idProduct) {
		Map<String, Long> result = new HashMap<>();

		// Get list classification
		List<Long> idsClass = getListClassification(idProduct);
		// count and sum in classification id in product review
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Tuple> cq = cb.createQuery(Tuple.class);
		Root<ProductReview> root = cq.from(ProductReview.class);
		Expression<Long> lst = root.get(ProductReview_.productClassification).get(ProductClassification_.id);
		Predicate predicate = lst.in(idsClass);

		cq.multiselect(cb.count(root).alias("amount"), cb.sum(root.get(ProductReview_.rating)).alias("sum"))
				.where(predicate);
		Tuple t = em.createQuery(cq).getSingleResult();
		result.put("amount", (long) t.get("amount"));
		result.put("sum", (long) t.get("sum"));

		return result;
	}

	@Override
	public Map<String, Double> getShopRating(Long idShop) {
		Map<String, Double> result = new HashMap<>();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Tuple> cq = cb.createQuery(Tuple.class);
		Root<Product> root = cq.from(Product.class);

		Predicate predicate = cb.equal(root.get(Product_.shop).get(Shop_.id), idShop);
		Predicate predicate2 = cb.greaterThan(root.get(Product_.reviewCount), 0L);

		cq.multiselect(cb.avg(root.get(Product_.averageRating)).alias("average_rating"),
				cb.count(root).alias("amount_rating")).where(predicate, predicate2);

		Tuple t = em.createQuery(cq).getSingleResult();

		Double averageRating = (Double) t.get("average_rating");
		Double amountRating = Double.parseDouble(t.get("amount_rating").toString());
		averageRating = (double) Math.round(averageRating * 100) / 100;

		result.put("average_rating", averageRating);
		result.put("amount_rating", amountRating);

		return result;
	}

	private List<Long> getListClassification(Long idProduct) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<Product> root = cq.from(Product.class);
		Join<Product, ProductClassification> join = root.join(Product_.classifications, JoinType.INNER);
		Predicate predicate = cb.equal(root.get(Product_.id), idProduct);

		cq.select(join.get(ProductClassification_.id)).where(predicate);

		return em.createQuery(cq).getResultList();

	}
}
