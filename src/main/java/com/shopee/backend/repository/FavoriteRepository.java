package com.shopee.backend.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.shopee.backend.model.Buyer;
import com.shopee.backend.model.FavoriteBuyer;
import com.shopee.backend.model.Product;
import com.shopee.backend.model.pk.FavoriteBuyerPK;

public interface FavoriteRepository extends JpaRepository<FavoriteBuyer, FavoriteBuyerPK> {
    List<FavoriteBuyer> getAllByBuyer(Buyer buyer);

    List<FavoriteBuyer> getAllByProduct(Product product);

    @Transactional @Modifying
    @Query(value = "DELETE FROM FavoriteBuyer f WHERE f.buyer.id = :idBuyer AND f.product.id IN :idPs")
    void deleteAllById(@Param("idBuyer") Long idBuyer, @Param("idPs") Collection<Long> idPs);
}
