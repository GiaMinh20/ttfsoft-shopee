package com.shopee.backend.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.shopee.backend.dto.base.AbstractDTO;
import com.shopee.backend.utility.datatype.ECouponCodeType;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class CouponCodeDTO extends AbstractDTO {
	private String name;
	private Long idShop;
	private Integer status;
	private String description;
	private String code;
	private Integer quantity;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date dod;
	private ECouponCodeType type;
	private Long discount;
	private Long minPrice;
	private Long discountLimit;

}
