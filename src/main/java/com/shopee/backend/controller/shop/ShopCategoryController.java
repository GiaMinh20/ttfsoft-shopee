package com.shopee.backend.controller.shop;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.payload.request.shopcategory.EditShopCategoryRequest;
import com.shopee.backend.payload.response.BaseResponse;
import com.shopee.backend.service.IShopCategoryService;
import com.shopee.backend.service.auth.ShopDetails;
import com.shopee.backend.payload.request.shopcategory.CreateShopCategoryRequest;

@RestController
@RequestMapping("/api/shop/shopcategory")
public class ShopCategoryController {
	@Autowired
	private IShopCategoryService shopCategoryService;
	
	@GetMapping
	public ResponseEntity<?> get(
			@AuthenticationPrincipal ShopDetails shop,
			@RequestParam(required = false) String searchName) {
		
		if (StringUtils.isNotBlank(searchName))
			return ResponseEntity.ok(shopCategoryService.findByShopAndName(shop.getId(), searchName));
		else
			return ResponseEntity.ok(shopCategoryService.getAllByShop(shop.getId()));
	}

	@PostMapping
	public ResponseEntity<?> create(
			@AuthenticationPrincipal ShopDetails shop,
			@RequestBody CreateShopCategoryRequest request) {
		return ResponseEntity.ok(shopCategoryService.create(shop.getId(), request));
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> update(@AuthenticationPrincipal ShopDetails shop, @PathVariable("id") long id,
			@RequestBody EditShopCategoryRequest request) {
		return ResponseEntity.ok(shopCategoryService.update(shop.getId(), id, request));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(
			@AuthenticationPrincipal ShopDetails shop,
			@PathVariable("id") long id) {
		if (!shopCategoryService.delete(shop.getId(), id))
			return ResponseEntity.ok(new BaseResponse(false, "Delete shop category failed"));
		else
			return ResponseEntity.ok(new BaseResponse());
	}
}
