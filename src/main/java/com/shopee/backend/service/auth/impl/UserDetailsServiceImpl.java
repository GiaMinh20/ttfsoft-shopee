package com.shopee.backend.service.auth.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.shopee.backend.model.Admin;
import com.shopee.backend.model.Buyer;
import com.shopee.backend.model.Shop;
import com.shopee.backend.repository.AdminRepository;
import com.shopee.backend.repository.BuyerRepository;
import com.shopee.backend.repository.ShopRepository;
import com.shopee.backend.service.auth.AdminDetails;
import com.shopee.backend.service.auth.BuyerDetails;
import com.shopee.backend.service.auth.ShopDetails;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	AdminRepository adminRepository;
	@Autowired
	BuyerRepository buyerRepository;
	@Autowired
	ShopRepository shopRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String usernameWithRolePrefix) throws UsernameNotFoundException {
		String role = usernameWithRolePrefix.substring(0, usernameWithRolePrefix.indexOf('@'));
		String username = usernameWithRolePrefix.substring(usernameWithRolePrefix.indexOf('@') + 1,
				usernameWithRolePrefix.length());

		switch (role) {
		case "admin":
			Admin admin = adminRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(
					String.format("%s user not found with username: %s", role, username)));
			return AdminDetails.build(admin);
		case "buyer":
			Buyer buyer = buyerRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(
					String.format("%s user not found with username: %s", role, username)));
			return BuyerDetails.build(buyer);
		case "shop":
			Shop shop = shopRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(
					String.format("%s user not found with username: %s", role, username)));
			return ShopDetails.build(shop);
		default:
			throw new UsernameNotFoundException(String.format("Invalid role prefix: %s", role));
		}
	}
}
