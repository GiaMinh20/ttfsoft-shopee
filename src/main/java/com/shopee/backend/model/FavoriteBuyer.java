package com.shopee.backend.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import com.shopee.backend.model.pk.FavoriteBuyerPK;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "favorite_buyer")
@Getter
@Setter
@RequiredArgsConstructor
public class FavoriteBuyer {
	@EmbeddedId
	FavoriteBuyerPK id;

	@MapsId("idBuyer")
	@ManyToOne
	@JoinColumn(name = "id_buyer", insertable = false, updatable = false)
	private Buyer buyer;

	@MapsId("idProduct")
	@ManyToOne
	@JoinColumn(name = "id_product", insertable = false, updatable = false)
	private Product product;
}
