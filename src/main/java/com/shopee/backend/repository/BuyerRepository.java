package com.shopee.backend.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shopee.backend.model.Buyer;
import com.shopee.backend.repository.custom.BuyerRepositoryCustom;

@Repository
public interface BuyerRepository extends JpaRepository<Buyer, Long>, BuyerRepositoryCustom {
	Optional<Buyer> findByUsername(String username);

	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);
	
	Boolean existsByPhone(String phone);
	
	Optional<Buyer> findByEmail(String email);
}
