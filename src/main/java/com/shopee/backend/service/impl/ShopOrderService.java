package com.shopee.backend.service.impl;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.dto.OrderDetailDTO;
import com.shopee.backend.dto.ProductClassificationDTO;
import com.shopee.backend.model.BuyerCoupon;
import com.shopee.backend.model.CouponCode;
import com.shopee.backend.model.Order;
import com.shopee.backend.model.Product;
import com.shopee.backend.model.ProductClassification;
import com.shopee.backend.model.Shop;
import com.shopee.backend.model.ShopOrder;
import com.shopee.backend.payload.request.notification.PostNotificationRequest;
import com.shopee.backend.payload.response.BaseResponse;
import com.shopee.backend.payload.response.DataResponse;
import com.shopee.backend.payload.response.ListResponse;
import com.shopee.backend.payload.response.ProductOfShopOrderResponse;
import com.shopee.backend.payload.response.ShopOrderManageResponse;
import com.shopee.backend.repository.BuyerCouponRepository;
import com.shopee.backend.repository.CouponRepository;
import com.shopee.backend.repository.OrderDetailRepository;
import com.shopee.backend.repository.OrderRepository;
import com.shopee.backend.repository.ProductClassificationRepository;
import com.shopee.backend.repository.ProductRepository;
import com.shopee.backend.repository.ShopOrderRepository;
import com.shopee.backend.repository.ShopRepository;
import com.shopee.backend.service.INotificationService;
import com.shopee.backend.service.IProductCategoryService;
import com.shopee.backend.service.IProductClassificationService;
import com.shopee.backend.service.IProductTypeService;
import com.shopee.backend.service.IShopOrderService;
import com.shopee.backend.utility.datatype.EOrderStatus;
import com.shopee.backend.utility.helpers.ExportDataToCSV;

@Service
public class ShopOrderService implements IShopOrderService {
	@Autowired
	ShopOrderRepository repository;

	@Autowired
	OrderDetailRepository orderDetailRepository;

	@Autowired
	ProductRepository productRepository;

	@Autowired
	IProductCategoryService productCategoryService;

	@Autowired
	IProductTypeService productTypeService;

	@Autowired
	IProductClassificationService productClassificationService;

	@Autowired
	ProductClassificationRepository productClassificationRepository;

	@Autowired
	ShopRepository shopRepository;

	@Autowired
	OrderRepository orderRepository;

	@Autowired
	CouponRepository couponRepository;

	@Autowired
	BuyerCouponRepository buyerCouponRepository;

	@Autowired
	INotificationService notificationService;

	@Autowired
	ModelMapper modelMapper;

	@Value("${shoporder.notfound}")
	private String notfound;
	@Value("${shoporder.dontchange}")

	private String dontchange;
	@Value("${product.notfound}")
	private String productnotfound;

	@Value("${shop.shopnotfound}")
	private String shopnotfound;

	@Value("${shoporder.changestatus}")
	private String changestatus;

	@Value("${shoporder.movestatus}")
	private String movestatus;

	@Override
	public ListResponse<ShopOrderManageResponse> getOrderByShop(Long idShop, String from, String to) {
		checkShopExist(idShop);
		List<ShopOrder> shopOrders = repository.findByIdShop(idShop, from, to);
		return new ListResponse<>(getResponses(idShop, shopOrders));

	}

	@Override
	public ListResponse<ShopOrderManageResponse> getOrderByShopAndStatus(Long idShop, EOrderStatus status, String from,
			String to) {
		checkShopExist(idShop);

		List<ShopOrder> shopOrders = repository.findByIdShopAndStatus(idShop, status.ordinal(), from, to);
		return new ListResponse<>(getResponses(idShop, shopOrders));

	}

	@Override
	public DataResponse<ShopOrderManageResponse> getOrderByShopAndId(Long idShop, Long id) {
		checkShopExist(idShop);
		ShopOrder shopOrder = repository.findByShopAndId(idShop, id);
		if (shopOrder != null) {
			// Create 1 response
			ShopOrderManageResponse shopOrderManageResponse = new ShopOrderManageResponse();
			modelMapper.map(shopOrder, shopOrderManageResponse);
			Order order = orderRepository.getReferenceById(shopOrder.getOrder().getId());
			modelMapper.map(order, shopOrderManageResponse);
			// Get OrderDetail
			List<OrderDetailDTO> orderDetailDTOs = orderDetailRepository.findByShopOrderId(shopOrder.getId()).stream()
					.map(p -> modelMapper.map(p, OrderDetailDTO.class)).toList();
			List<ProductOfShopOrderResponse> productResponses = new ArrayList<>();
			for (OrderDetailDTO orderDetailDTO : orderDetailDTOs) {
				ProductOfShopOrderResponse productResponse = new ProductOfShopOrderResponse();
				// Get Product Classification
				ProductClassificationDTO productClassificationDTO = productClassificationService
						.getById(orderDetailDTO.getIdProductClassification()).getData();
				modelMapper.map(productClassificationDTO, productResponse);
				productResponse.setQuantityBuy(orderDetailDTO.getQuantity());
				// Get Product
				Product product = productRepository.getReferenceById(productClassificationDTO.getIdProduct());
				modelMapper.map(product, productResponse);
				productResponses.add(productResponse);
			}
			shopOrderManageResponse.setProductResponse(productResponses);

			return new DataResponse<>(shopOrderManageResponse);
		} else {
			throw new EntityNotFoundException(notfound);
		}
	}

	@Override
	public ByteArrayInputStream exportData(Long idShop, String from, String to) {
		checkShopExist(idShop);
		List<ShopOrderManageResponse> datas = getOrderByShop(idShop, from, to).getData();
		return ExportDataToCSV.shopOrderToExcel(datas);
	}

	@Override
	public ByteArrayInputStream exportDataWithStatus(Long idShop, EOrderStatus status, String from, String to) {
		checkShopExist(idShop);
		List<ShopOrderManageResponse> datas = getOrderByShopAndStatus(idShop, status, from, to).getData();
		return ExportDataToCSV.shopOrderToExcel(datas);
	}

	@Override
	public BaseResponse update(Long idShop, Long id, EOrderStatus status) {
		checkShopExist(idShop);
		switch (status) {
		case WAIT_FOR_SENDING: {
			updateShopOrderFromConfirmToSending(idShop, id, status);
			break;
		}
		case DELIVERING: {
			updateShopOrderFromSendingToDelivering(idShop, id, status);
			break;
		}
		case DELIVERED: {
			updateShopOrderFromDeliveringToDelivered(idShop, id, status);
			break;
		}
		case CANCELLED: {
			updateShopOrderToCanceled(idShop, id, status);
			break;
		}
		default:
			throw new CommonRuntimeException(dontchange);
		}
		return new BaseResponse(true, "Successfull");
	}

	private void createNotification(ShopOrderManageResponse request, String title, String content) {
		List<ProductOfShopOrderResponse> productRp = request.getProductResponse();
		Long buyerId = request.getIdUser();
		for (ProductOfShopOrderResponse product : productRp) {
			Long idMediaResource = product.getMediaId();
			PostNotificationRequest notify = new PostNotificationRequest(idMediaResource, title, content);
			notificationService.postNotification(buyerId, notify);
		}
	}

	private BaseResponse updateShopOrderFromConfirmToSending(Long idShop, Long id, EOrderStatus status) {
		ShopOrderManageResponse shopOrderManageResponse = getOrderByShopAndId(idShop, id).getData();
		if (!shopOrderManageResponse.getStatus().equals(EOrderStatus.WAIT_FOR_CONFIRMATION))
			throw new CommonRuntimeException(dontchange);
		Date createOrderDate = shopOrderManageResponse.getCreatedDate();
		Long prepareDate;
		List<ProductOfShopOrderResponse> prs = shopOrderManageResponse.getProductResponse();
		for (ProductOfShopOrderResponse pr : prs) {
			ProductClassification pc = productClassificationRepository.getReferenceById(pr.getId());
			Long afterQuantity = pc.getQuantity() - pr.getQuantityBuy();
			repository.updateProductQuantity(pr.getId(), afterQuantity);
			Long sellNumber = pc.getProduct().getSales();
			repository.updateProductSale(pc.getProduct().getId(), sellNumber + pr.getQuantityBuy());
			pr.setQuantityInStock(afterQuantity);
		}
		prepareDate = (new Date().getTime() - createOrderDate.getTime());
		ShopOrder shopOrder = repository.getReferenceById(id);
		shopOrder.setStatus(status);
		if (prepareDate >= 0)
			shopOrder.setPrepareDate(prepareDate);
		repository.save(shopOrder);
		createNotification(shopOrderManageResponse, "Order is sending", movestatus + status.toString());
		return new BaseResponse(true, changestatus);
	}

	private BaseResponse updateShopOrderFromSendingToDelivering(Long idShop, Long id, EOrderStatus status) {
		ShopOrderManageResponse shopOrderManageResponse = getOrderByShopAndId(idShop, id).getData();
		if (!shopOrderManageResponse.getStatus().equals(EOrderStatus.WAIT_FOR_SENDING))
			throw new CommonRuntimeException(dontchange);
		ShopOrder shopOrder = repository.getReferenceById(id);
		shopOrder.setStatus(status);
		repository.save(shopOrder);
		createNotification(shopOrderManageResponse, "Order is delivering", movestatus + status.toString());
		return new BaseResponse(true, changestatus);
	}

	private BaseResponse updateShopOrderFromDeliveringToDelivered(Long idShop, Long id, EOrderStatus status) {
		ShopOrderManageResponse shopOrderManageResponse = getOrderByShopAndId(idShop, id).getData();
		if (!shopOrderManageResponse.getStatus().equals(EOrderStatus.DELIVERING))
			throw new CommonRuntimeException(dontchange);
		ShopOrder shopOrder = repository.getReferenceById(id);
		shopOrder.setStatus(status);
		shopOrder.setComplete(new Date());
		repository.save(shopOrder);
		createNotification(shopOrderManageResponse, "Order was delivered", movestatus + status.toString());
		return new BaseResponse(true, changestatus);
	}

	@Transactional
	private BaseResponse updateShopOrderToCanceled(Long idShop, Long id, EOrderStatus status) {
		ShopOrderManageResponse shopOrderManageResponse = getOrderByShopAndId(idShop, id).getData();
		EOrderStatus orderStatus = shopOrderManageResponse.getStatus();
		if (orderStatus.equals(EOrderStatus.DELIVERED) || orderStatus.equals(EOrderStatus.CANCELLED))
			throw new CommonRuntimeException(dontchange);
		if (orderStatus.equals(EOrderStatus.DELIVERING) || orderStatus.equals(EOrderStatus.WAIT_FOR_SENDING)) {
			// Rollback quantity of ProductClassification
			List<ProductOfShopOrderResponse> prs = shopOrderManageResponse.getProductResponse();
			for (ProductOfShopOrderResponse pr : prs) {
				ProductClassification pc = productClassificationRepository.getReferenceById(pr.getId());
				Long afterQuantity = pc.getQuantity() + pr.getQuantityBuy();
				repository.updateProductQuantity(pr.getId(), afterQuantity);
				pr.setQuantityInStock(afterQuantity);
			}
			// return the discount code of shop for user
			if (shopOrderManageResponse.getShopVoucher() != null && !shopOrderManageResponse.getShopVoucher().isBlank()) {
				CouponCode shopCoupon = couponRepository.findByCode(shopOrderManageResponse.getShopVoucher());

				BuyerCoupon buyerCoupon = buyerCouponRepository.findByIdCouponWithouBuyer(shopCoupon.getId());
				buyerCouponRepository.updateCouponStatus(buyerCoupon.getCouponCode().getId());
			}
		}
		ShopOrder shopOrder = repository.getReferenceById(id);
		shopOrder.setStatus(status);
		repository.save(shopOrder);
		createNotification(shopOrderManageResponse, "Order was canceled", movestatus + status.toString());
		return new BaseResponse(true, changestatus);
	}

	private void checkShopExist(Long id) {
		Shop shop = shopRepository.getReferenceById(id);
		if (!shop.getIsActive()) {
			throw new EntityNotFoundException(shopnotfound);
		}
	}

	private List<ShopOrderManageResponse> getResponses(Long idShop, List<ShopOrder> shopOrders) {

		if (!shopOrders.isEmpty()) {
			// Create return list
			List<ShopOrderManageResponse> listShopOrderManageResponses = new ArrayList<>();
			for (ShopOrder shopOrder : shopOrders) {

				ShopOrderManageResponse shopOrderManageResponse = getOrderByShopAndId(idShop, shopOrder.getId())
						.getData();
				listShopOrderManageResponses.add(shopOrderManageResponse);
			}
			return listShopOrderManageResponses;
		} else {
			throw new CommonRuntimeException(notfound);
		}
	}

	@Override
	public BaseResponse cancel(Long id, Long idBuyer) {
		if (repository.cancel(id, idBuyer))
			return new BaseResponse(true, "Cancelled");
		else
			return new BaseResponse(false, "Something went wrong please try again");
	}
}
