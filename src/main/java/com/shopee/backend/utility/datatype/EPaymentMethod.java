package com.shopee.backend.utility.datatype;

public enum EPaymentMethod {
	SHIP_COD,
	ONLINE_PAYMENT_MOMO
}
