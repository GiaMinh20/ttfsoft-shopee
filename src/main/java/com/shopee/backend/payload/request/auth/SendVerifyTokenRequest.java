package com.shopee.backend.payload.request.auth;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class SendVerifyTokenRequest {
	@Schema(type = "string", format = "email")
	@NotEmpty
	@JsonProperty("email")
	private String email;

	public SendVerifyTokenRequest(String email) {
		this.email = email;
	}

	public SendVerifyTokenRequest() {
	}
}
