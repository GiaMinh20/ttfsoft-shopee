package com.shopee.backend.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.dto.AdminDTO;
import com.shopee.backend.payload.request.auth.AdminProfileCreateRequest;
import com.shopee.backend.payload.request.auth.AdminProfileUpdateRequest;
import com.shopee.backend.payload.request.auth.PasswordUpdateRequest;
import com.shopee.backend.payload.response.DataResponse;

public interface IAdminService {

	DataResponse<AdminDTO> getById(Long id);

	DataResponse<AdminDTO> create(AdminProfileCreateRequest request, MultipartFile avatar) throws IOException;

	DataResponse<AdminDTO> update(Long id, AdminProfileUpdateRequest request, MultipartFile avatar) throws IOException;

	DataResponse<AdminDTO> updatePassword(Long id, PasswordUpdateRequest request);

}