package com.shopee.backend.repository.custom;

import java.util.Date;

import com.shopee.backend.payload.response.statistic.StatisticResponse;

public interface AdminRepositoryCustom {
	boolean setPassword(String email, String hashedPassword);
	Long getIdByEmail(String email);
	StatisticResponse customStatistic(Date dStart, Date dEnd, Long dLast);
}
