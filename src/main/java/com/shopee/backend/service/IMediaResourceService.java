package com.shopee.backend.service;

import com.shopee.backend.model.MediaResource;

public interface IMediaResourceService {

	MediaResource save(byte[] data);

	boolean delete(Long id);

}