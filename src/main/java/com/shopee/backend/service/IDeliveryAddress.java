package com.shopee.backend.service;

import com.shopee.backend.dto.DeliveryAddressDTO;
import com.shopee.backend.payload.response.ResponseData;

public interface IDeliveryAddress {

	void delete(Long[] ids, Long idBuyer);

	ResponseData<DeliveryAddressDTO> update(Long id, DeliveryAddressDTO input);

	ResponseData<DeliveryAddressDTO> save(DeliveryAddressDTO input);

	ResponseData<DeliveryAddressDTO> getBuyerDeliveryAddress(Long id, Long idBuyer);

	ResponseData<DeliveryAddressDTO> getBuyerDeliveryAddress(Long idBuyer);

}
