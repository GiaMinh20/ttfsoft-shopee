package com.shopee.backend.repository.custom;

import java.util.Date;
import java.util.List;

import com.shopee.backend.dto.holder.DataHolder;
import com.shopee.backend.model.Shop;
import com.shopee.backend.payload.response.WorkPerformance;
import com.shopee.backend.utility.datatype.EOrderStatus;

public interface ShopProductManagementRepoCustom {

	Long countOrderByStatus(Shop shop, EOrderStatus status);

	Long countOutOfStock(Long idShop);

	Long countAccessTimesShop(Shop shop);

	Long countOrderConfirmed(Shop shop);

	List<DataHolder> countOrderByStatus(Shop shop);

	Long sales(Shop shop, Date dStart, Date dEnd, Long dLast);

	WorkPerformance getWorkPerformance(Shop shop);

	Long salesValue(Shop shop, Date dStart, Date dEnd, Long dLast);
	
	

}
