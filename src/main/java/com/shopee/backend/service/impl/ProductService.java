package com.shopee.backend.service.impl;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.dto.ProductDTO;
import com.shopee.backend.dto.ProductGeneralDTO;
import com.shopee.backend.model.MediaResource;
import com.shopee.backend.model.Product;
import com.shopee.backend.model.ProductCategory;
import com.shopee.backend.model.ProductClassification;
import com.shopee.backend.model.ProductImage;
import com.shopee.backend.model.ProductType;
import com.shopee.backend.model.SellPlace;
import com.shopee.backend.model.Shop;
import com.shopee.backend.model.ShopCategory;
import com.shopee.backend.payload.request.product.CreateProductProductVariationRequest;
import com.shopee.backend.payload.request.product.CreateProductRequest;
import com.shopee.backend.payload.request.product.EditProductRequest;
import com.shopee.backend.payload.response.DataResponse;
import com.shopee.backend.payload.response.ListWithPagingResponse;
import com.shopee.backend.repository.OrderDetailRepository;
import com.shopee.backend.repository.ProductCategoryRepository;
import com.shopee.backend.repository.ProductClassificationRepository;
import com.shopee.backend.repository.ProductImageRepository;
import com.shopee.backend.repository.ProductRepository;
import com.shopee.backend.repository.ProductTypeRepository;
import com.shopee.backend.repository.SellPlaceRepository;
import com.shopee.backend.repository.ShopCategoryRepository;
import com.shopee.backend.repository.ShopRepository;
import com.shopee.backend.service.IMediaResourceService;
import com.shopee.backend.service.IProductService;
import com.shopee.backend.utility.Page;
import com.shopee.backend.utility.PlatformPolicy;
import com.shopee.backend.utility.datatype.EProductClassificationStatus;
import com.shopee.backend.utility.datatype.EProductStatus;
import com.shopee.backend.utility.datatype.EUserRole;

@Service
public class ProductService implements IProductService {
	@Autowired
	ProductRepository productRepository;

	@Autowired
	ProductClassificationRepository productClassificationRepository;

	@Autowired
	ProductTypeRepository productTypeRepository;

	@Autowired
	ProductCategoryRepository productCategoryRepository;

	@Autowired
	ShopRepository shopRepository;

	@Autowired
	ShopCategoryRepository shopCategoryRepository;

	@Autowired
	SellPlaceRepository sellPlaceRepository;

	@Autowired
	ProductImageRepository productImageRepository;

	@Autowired
	OrderDetailRepository orderDetailRepository;

	@Autowired
	IMediaResourceService mediaResourceService;

	@Autowired
	ProductClassificationService productClassificationService;

	@Autowired
	ModelMapper modelMapper;

	@Value("${product.notfoundwithid}")
	private String notfoundwithid;

	@Value("${product.typeisunavailable}")
	private String typeisunavailable;

	@Value("${product.categoryisoutofdate}")
	private String categoryisoutofdate;

	@Value("${product.deleteaccessdeny}")
	private String deleteaccessdeny;

	@Value("${product.updateaccessdeny}")
	private String updateaccessdenyYou;

	@Value("${product.sellplaceinvalid}")
	private String sellplaceinvalid;

	@Value("${product.updatebannedproduct}")
	private String updatebannedproduct;

	@Value("${product.deleteavatarfail}")
	private String deleteavatarfail;

	@Value("${product.variationrequire}")
	private String variationrequire;

	@Value("${product.notfound}")
	private String notfound;

	@Value("${product.shopcategorynotfound}")
	private String shopcategorynotfound;

	@Value("${producttype.notfoundwithid}")
	private String producttypenotfound;

	@Value("${productcategory.notfoundwithid}")
	private String productcategorynotfound;

	@Override
	public DataResponse<ProductDTO> create(Long idShop, CreateProductRequest request, MultipartFile inputAvatar,
			List<MultipartFile> media) throws IOException {
		validateCreateProductRequest(request);

		ShopCategory shopCategory = null;
		if (request.getIdShopCategory() != null) {
			shopCategory = shopCategoryRepository.getReferenceById(request.getIdShopCategory());
		}

		ProductType productType = productTypeRepository.getReferenceById(request.getIdType());
		if (!productType.getStatus())
			throw new CommonRuntimeException(typeisunavailable);
		ProductCategory productCategory = productCategoryRepository.getReferenceById(request.getIdCategory());
		if (productCategory.getDod().before(new Date()))
			throw new CommonRuntimeException(categoryisoutofdate);
		Optional<SellPlace> sellPlace = sellPlaceRepository.findByCode(request.getCodeSellPlace());
		if (sellPlace.isEmpty())
			throw new EntityNotFoundException(String.format(sellplaceinvalid, request.getCodeSellPlace()));
		Shop shop = shopRepository.getReferenceById(idShop);

		MediaResource avatar = mediaResourceService.save(inputAvatar.getBytes());

		Product p = new Product(productType, productCategory, shop, shopCategory, sellPlace.get(), request.getName(),
				request.getDescription(), request.getDiscountStart(), request.getDiscountEnd(), request.getIsNew(),
				avatar);

		p = productRepository.save(p);

		for (MultipartFile f : media) {
			productImageRepository.save(new ProductImage(p, mediaResourceService.save(f.getBytes())));
		}

		for (CreateProductProductVariationRequest v : request.getVariation()) {
			productRepository.refresh(p);
			productClassificationRepository.save(new ProductClassification(p, v.getVariationName(), v.getPrice(),
					v.getAvailableQuantity(), v.getDiscount()));
		}

		productRepository.refresh(productRepository.save(p));
		return dataResponseMakeup(p);
	}

	/**
	 * Get product by id
	 */
	@Override
	public DataResponse<ProductDTO> getById(Long id, EUserRole role, Long idShop) {
		Product p = productRepository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException(notfoundwithid + id.toString()));

		if (role.equals(EUserRole.BUYER)) {
			if (p.getStatus() != EProductStatus.ACTIVE)
				throw new EntityNotFoundException(notfoundwithid + id.toString());
			// increase view
			productRepository.setViewProduct(id);
			productRepository.refresh(p);

			// if a shop try to get hidden product of other shop then throw exception
		} else if (role.equals(EUserRole.SHOP) && p.getStatus() != EProductStatus.ACTIVE
				&& !p.getShop().getId().equals(idShop))
			throw new EntityNotFoundException(notfoundwithid + id.toString());
		
		return dataResponseMakeup(p);
	}

	@Override
	public int countProductImage(Long id) {
		return productRepository.getReferenceById(id).getImages().size();
	}

	@Override
	public ListWithPagingResponse<ProductGeneralDTO> getWithFilter(Long idShop, Long idCategory, Long idType,
			Long idShopCategory, Long idSellPlace, String searchName, EProductStatus status, Boolean isNew,
			Integer page, Integer size, Sort sort) {
		int count = productRepository
				.count(idShop, idCategory, idType, idShopCategory, idSellPlace, searchName, status, isNew).intValue();
		Page paging = new Page(page, size, count, sort);

		List<Product> products = productRepository.get(idShop, idCategory, idType, idShopCategory, idSellPlace,
				searchName, status, isNew, paging);
		return listWithPagingResponseMakeup(products, paging);
	}

	@Override
	public Long countWithFilter(Long idShop, Long idCategory, Long idType, Long idShopCategory, Long idSellPlace,
			String searchName, EProductStatus status, Boolean isNew) {
		return productRepository.count(idShop, idCategory, idType, idShopCategory, idSellPlace, searchName, status,
				isNew);
	}

	/**
	 * Delete product by hide it
	 */
	@Override
	public void delete(Long id, Long idShop) {
		if (!productRepository.existsById(id)) {
			throw new EntityNotFoundException(notfoundwithid + id.toString());
		}

		Product p = productRepository.getReferenceById(id);
		if (!p.getShop().getId().equals(idShop)) {
			throw new CommonRuntimeException(deleteaccessdeny);
		}

		p.setStatus(EProductStatus.HIDDEN);
		productRepository.save(p);
	}

	@Override
	public DataResponse<ProductDTO> update(Long idShop, EditProductRequest request, MultipartFile avatar)
			throws IOException {
		Product p = productRepository.getReferenceById(request.getId());
		if (!p.getShop().getId().equals(idShop)) {
			throw new CommonRuntimeException(updateaccessdenyYou);
		}

		if (StringUtils.isNotBlank(request.getName()) && !request.getName().equals(p.getName())) {
			p.setName(request.getName());
		}

		if (StringUtils.isNotBlank(request.getDescription()) && !request.getDescription().equals(p.getDescription())) {
			p.setDescription(request.getDescription());
		}

		if (request.getIdCategory() != null && !request.getIdCategory().equals(p.getProductCategory().getId())) {
			p.setProductCategory(productCategoryRepository.getReferenceById(request.getIdCategory()));
		}

		if (request.getIdShopCategory() != null && !request.getIdShopCategory().equals(p.getShopCategory().getId())) {
			p.setShopCategory(shopCategoryRepository.getReferenceById(request.getIdShopCategory()));
		}

		if (request.getIdType() != null && !request.getIdType().equals(p.getProductType().getId())) {
			p.setProductType(productTypeRepository.getReferenceById(request.getIdType()));
		}

		if (request.getIsNew() != null && !request.getIsNew().equals(p.getIsNew())) {
			p.setIsNew(request.getIsNew());
		}

		if (request.getCodeSellPlace() != null && !request.getCodeSellPlace().equals(p.getSellPlace().getCode())) {
			Optional<SellPlace> sellPlace = sellPlaceRepository.findByCode(request.getCodeSellPlace());
			if (sellPlace.isEmpty()) {
				throw new CommonRuntimeException(String.format(sellplaceinvalid, request.getCodeSellPlace()));
			}
			p.setSellPlace(sellPlace.get());
		}

		if (request.getStatus() != null && !request.getStatus().equals(p.getStatus())
				&& request.getStatus() != EProductStatus.BANNED) {
			p.setStatus(request.getStatus());
		} else if (request.getStatus() != null && request.getStatus() == EProductStatus.BANNED) {
			throw new CommonRuntimeException(updatebannedproduct);
		}

		if (request.getDiscountStart() != null && !request.getDiscountStart().equals(p.getDiscountStart())) {
			p.setDiscountStart(request.getDiscountStart());
		}

		if (request.getDiscountEnd() != null && !request.getDiscountEnd().equals(p.getDiscountEnd())) {
			p.setDiscountEnd(request.getDiscountEnd());
		}

		if (avatar != null) {
			if (p.getAvatar() != null) {
				MediaResource oldAvatar = p.getAvatar();
				p.setAvatar(null);
				if (!mediaResourceService.delete(oldAvatar.getId())) {
					throw new CommonRuntimeException(deleteavatarfail);
				}
			}
			MediaResource img = mediaResourceService.save(avatar.getBytes());
			p.setAvatar(img);
		}

		return dataResponseMakeup(productRepository.save(p));
	}

	@Override
	public ListWithPagingResponse<ProductGeneralDTO> getTopBuy() {
		return listWithPagingResponseMakeup(productRepository.getTopBuy(), new Page(1, 10, 0));
	}

	@Override
	public void updatePriceWhenVariationCreated(ProductClassification pc) {
		Product p = pc.getProduct();
		List<ProductClassification> pcs = p.getClassifications();
		pcs.add(pc);
		updatePrice(p, pcs);
	}

	@Override
	public void updatePriceWhenVariationChange(ProductClassification pc) {
		Product p = pc.getProduct();
		List<ProductClassification> pcs = p.getClassifications();
		updatePrice(p, pcs);
	}

	private void updatePrice(Product p, List<ProductClassification> pcs) {
		Supplier<Stream<ProductClassification>> streamSupplier = () -> pcs.stream()
				.filter(x -> x.getQuantity() > 0 && x.getStatus() == EProductClassificationStatus.ACTIVE);

		Optional<ProductClassification> minPc = streamSupplier.get()
				.min((first, second) -> Long.compare(first.getPrice(), second.getPrice()));
		Optional<ProductClassification> maxPc = streamSupplier.get()
				.max((first, second) -> Long.compare(first.getPrice(), second.getPrice()));

		if (minPc.isPresent()) {
			if (minPc.get().getDiscount() != null) {
				p.setPrice(Math.round(minPc.get().getPrice() * (1 - minPc.get().getDiscount())));
			} else {
				p.setPrice(minPc.get().getPrice());
			}
		}
		if (maxPc.isPresent()) {
			if (maxPc.get().getDiscount() != null) {
				p.setMaxPrice(Math.round(maxPc.get().getPrice() * (1 - maxPc.get().getDiscount())));
			} else {
				p.setMaxPrice(maxPc.get().getPrice());
			}
		}
		productRepository.save(p);
	}

	private void validateCreateProductRequest(CreateProductRequest request) {
		if (request.getVariation() == null
				|| request.getVariation().size() < PlatformPolicy.MIN_ALLOWED_PRODUCT_VARIATION) {
			throw new CommonRuntimeException(
					String.format(variationrequire, PlatformPolicy.MIN_ALLOWED_PRODUCT_VARIATION));
		}
		if (!productTypeRepository.existsById(request.getIdType())) {
			throw new EntityNotFoundException(producttypenotfound + request.getIdType());
		}
		if (!productCategoryRepository.existsById(request.getIdCategory())) {
			throw new EntityNotFoundException(productcategorynotfound + request.getIdCategory());
		}
		if (request.getIdShopCategory() != null && !shopCategoryRepository.existsById(request.getIdShopCategory())) {
			throw new EntityNotFoundException(shopcategorynotfound + request.getIdShopCategory());
		}
	}

	private ListWithPagingResponse<ProductGeneralDTO> listWithPagingResponseMakeup(List<Product> products, Page page) {
		if (products.isEmpty())
			throw new EntityNotFoundException(notfound);
		return new ListWithPagingResponse<>(page.getPageNumber() + 1, page.getTotalPage() + 1,
				products.stream().map(p -> modelMapper.map(p, ProductGeneralDTO.class)).toList());
	}

	private DataResponse<ProductDTO> dataResponseMakeup(Product product) {
		return new DataResponse<>(modelMapper.map(product, ProductDTO.class));
	}
}
