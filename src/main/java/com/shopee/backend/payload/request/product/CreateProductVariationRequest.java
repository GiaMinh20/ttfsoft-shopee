package com.shopee.backend.payload.request.product;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CreateProductVariationRequest {
	@NotNull
	private Long idProduct;
	
	@NotBlank
	private String variationName;
	
	@NotNull
	@Range(min = 1)
	private Long price;
	
	@NotNull
	private Long availableQuantity;
	
	private Double discount;
}