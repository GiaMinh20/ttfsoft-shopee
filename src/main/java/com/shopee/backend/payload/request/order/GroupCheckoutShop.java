package com.shopee.backend.payload.request.order;

import java.util.ArrayList;
import java.util.List;

import com.shopee.backend.dto.CartDetailDTO;

import lombok.Data;

@Data
public class GroupCheckoutShop {
    private Long idShop;
    private List<CartDetailDTO> cartDetails = new ArrayList<>();
    private String couponCode;
}
