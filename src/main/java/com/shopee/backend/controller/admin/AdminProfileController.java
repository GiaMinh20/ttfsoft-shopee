package com.shopee.backend.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.service.IAdminService;
import com.shopee.backend.service.auth.AdminDetails;

@RestController
@RequestMapping("/api/admin/profile")
public class AdminProfileController {
	@Autowired
	IAdminService adminService;

	@GetMapping
	public ResponseEntity<?> get(@AuthenticationPrincipal AdminDetails admin) {
		return ResponseEntity.ok(adminService.getById(admin.getId()));
	}
}
