package com.shopee.backend.dto;

import com.shopee.backend.dto.base.UserDTO;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AdminDTO extends UserDTO {
	private String address;
}
