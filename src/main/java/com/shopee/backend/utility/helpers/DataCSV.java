package com.shopee.backend.utility.helpers;

import java.util.Date;

import com.shopee.backend.utility.datatype.EOrderStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataCSV {
	private Long idOrder;
	private EOrderStatus status;
	private Long idProduct;
	private String productType;
	private Long productPrice;
	private Long quantityBuy;
	private Long totalPrice;
	private Date complete;
	private Date createdDate;

	private Long idUser;
	private String paymentMethod;

	public DataCSV(Long idOrder, EOrderStatus status, Long idProduct, String productType, Long productPrice,
			Long quantityBuy, Long totalPrice, Date complete, Date createdDate, Long idUser, String paymentMethod) {
		this.idOrder = idOrder;
		this.status = status;
		this.idProduct = idProduct;
		this.productType = productType;
		this.productPrice = productPrice;
		this.quantityBuy = quantityBuy;
		this.totalPrice = totalPrice;
		this.complete = complete;
		this.createdDate = createdDate;
		this.idUser = idUser;
		this.paymentMethod = paymentMethod;
	}

}
