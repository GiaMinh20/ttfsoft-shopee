package com.shopee.backend.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.dto.ProductCategoryDTO;
import com.shopee.backend.payload.request.productcategory.CreateProductCategoryRequest;
import com.shopee.backend.payload.request.productcategory.EditProductCategoryRequest;
import com.shopee.backend.payload.response.DataResponse;
import com.shopee.backend.payload.response.ListResponse;

public interface IProductCategoryService {

	DataResponse<ProductCategoryDTO> create(CreateProductCategoryRequest request, MultipartFile image)throws IOException;

	ListResponse<ProductCategoryDTO> getAll();

	DataResponse<ProductCategoryDTO> getById(Long id);

	ListResponse<ProductCategoryDTO> findByName(String searchName);

	DataResponse<ProductCategoryDTO> update(Long id, EditProductCategoryRequest request, MultipartFile image) throws IOException;

	boolean delete(Long id);

}