package com.shopee.backend.service;

import java.util.List;

import com.shopee.backend.payload.request.order.CreateOrderRequest;
import com.shopee.backend.payload.response.CreateOrderResponse;
import com.shopee.backend.payload.response.OrderManageResponse;

public interface IOrderService {
	List<OrderManageResponse> getOrdersByBuyer(Long id);
	
	List<OrderManageResponse> searchOrders(Long id, String keyword);

	OrderManageResponse orderDetail(Long id);
	
	List<OrderManageResponse> getOrderByStatus(Long buyerId, Long statusId);

	CreateOrderResponse createOrder(CreateOrderRequest body, Long idBuyer);

}
