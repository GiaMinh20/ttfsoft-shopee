package com.shopee.backend.repository.custom;

import com.shopee.backend.model.ProductClassification;

public interface ProductClassificationRepositoryCustom {
    ProductClassification descreaseQuantity(Long idProductClassification, Long quantity);

    ProductClassification checkProductClassification(ProductClassification entity, Long quantity);
}
