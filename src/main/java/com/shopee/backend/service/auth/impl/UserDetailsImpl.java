package com.shopee.backend.service.auth.impl;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public abstract class UserDetailsImpl implements UserDetails {
	protected static final long serialVersionUID = 905992472963249600L;
	protected Long id;
	protected String username;
	protected String password;
	protected Collection<? extends GrantedAuthority> authorities;
	
	public UserDetailsImpl() {
		super();
	}
	public UserDetailsImpl(Long id, String username, String password,
			Collection<? extends GrantedAuthority> authorities) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.authorities = authorities;
	}
}
