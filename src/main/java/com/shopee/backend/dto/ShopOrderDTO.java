package com.shopee.backend.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.shopee.backend.utility.datatype.EOrderStatus;
import lombok.Data;
@Data

public class ShopOrderDTO {
	private Long id;
	private Long idOrder;
	private Long idShop;
	private String shopVoucher;
	private Long originalPrice;
	private EOrderStatus status;
	private Long shipPrice;
	private Long priceOff;
	private Long totalPrice;
	private Long totalPayPrice;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date complete;
	private String buyerNote;
	private Date createdDate;
	private Long prepareDate;
}
