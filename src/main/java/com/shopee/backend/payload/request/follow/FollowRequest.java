package com.shopee.backend.payload.request.follow;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class FollowRequest {
	@NotNull
	private Long idShop;

	public FollowRequest(@NotNull Long idShop) {
		this.idShop = idShop;
	}
}
