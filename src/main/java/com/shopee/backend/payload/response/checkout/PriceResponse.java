package com.shopee.backend.payload.response.checkout;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class PriceResponse {
    List<PriceByShop> priceByShops = new ArrayList<>();
    private Long totalItemPrice = 0L;
    private Long totalShipPrice = 0L;
    private Long platformPriceOff = 0L;
    private Long shipPriceOff = 0L;
    private Long finalPrice = 0L;

    public void setFinalPrice(){
        finalPrice = totalItemPrice + totalShipPrice - shipPriceOff - platformPriceOff;
    }
}
