package com.shopee.backend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.shopee.backend.model.Notification;

public interface NotificationRepository extends JpaRepository<Notification, Long> {

	@Query(value = "Select * from notification where id_buyer=:id_buyer", nativeQuery = true)
	public List<Notification> getNotificationsByUserId(@Param("id_buyer") Long idBuyer);
}
