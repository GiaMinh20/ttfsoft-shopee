package com.shopee.backend.utility.helpers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.shopee.backend.payload.response.ShopOrderManageResponse;

public class ExportDataToCSV {
	private ExportDataToCSV() {
		throw new IllegalStateException("ExportDataToCSV class");
	}

	public static final String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	static String[] headers = { "Mã đơn hàng", "Trạng thái đơn hàng", "Mã sản phẩm", "Loại sản phẩm", "Gía sản phẩm",
			"Số lượng mua", "Tổng số tiền của hóa đơn", "Ngày đặt hàng", "Ngày hoàn thành", "Phương thức thanh toán",
			"Id người mua" };
	static String rootSheet = "ShopOrderData";

	public static ByteArrayInputStream shopOrderToExcel(List<ShopOrderManageResponse> datas) {
		try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			Sheet sheet = workbook.createSheet(rootSheet);
			Row headerRow = sheet.createRow(0);
			for (int col = 0; col < headers.length; col++) {
				Cell cell = headerRow.createCell(col);
				cell.setCellValue(headers[col]);
			}
			DataCSV[] dataArr = convertListDataToCSVDataArray(datas);
			createRow(dataArr, sheet);
			workbook.write(out);
			return new ByteArrayInputStream(out.toByteArray());
		} catch (IOException e) {
			throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
		}
	}

	private static void createRow(DataCSV[] dataArr, Sheet sheet) {
		int rowIdx = 1;
		for (DataCSV data : dataArr) {
			Row row = sheet.createRow(rowIdx++);
			if (data.getIdProduct() == null && data.getProductType() == null && data.getProductPrice() == null
					&& data.getQuantityBuy() == null) {
				row.createCell(0).setCellValue(data.getIdOrder());
				row.createCell(1).setCellValue(data.getStatus().toString());
				row.createCell(2).setCellValue("");
				row.createCell(3).setCellValue("");
				row.createCell(4).setCellValue("");
				row.createCell(5).setCellValue("");
				row.createCell(6).setCellValue(data.getTotalPrice());
				row.createCell(7).setCellValue(data.getCreatedDate());
				row.createCell(8).setCellValue(data.getComplete());
				row.createCell(9).setCellValue(data.getPaymentMethod());
				row.createCell(10).setCellValue(data.getIdUser());
			} else {
				row.createCell(0).setCellValue("");
				row.createCell(1).setCellValue("");
				row.createCell(2).setCellValue(data.getIdProduct());
				row.createCell(3).setCellValue(data.getProductType());
				row.createCell(4).setCellValue(data.getProductPrice());
				row.createCell(5).setCellValue(data.getQuantityBuy());
				row.createCell(6).setCellValue("");
				row.createCell(7).setCellValue("");
				row.createCell(8).setCellValue("");
				row.createCell(9).setCellValue("");
				row.createCell(10).setCellValue("");
			}
		}
	}

	private static DataCSV[] convertListDataToCSVDataArray(List<ShopOrderManageResponse> datas) {
		int rowCount = 0;
		List<Integer> titleRow = new ArrayList<>();
		for (ShopOrderManageResponse data : datas) {
			rowCount += data.getProductResponse().size() + 1;
			titleRow.add(rowCount - data.getProductResponse().size() - 1);
		}
		DataCSV[] dataArr = new DataCSV[rowCount];
		int dataTitle = 0;
		for (int i : titleRow) {
			dataArr[i] = new DataCSV(datas.get(dataTitle).getIdOrder(), datas.get(dataTitle).getStatus(), null, null,
					null, null, datas.get(dataTitle).getTotalPrice(), datas.get(dataTitle).getComplete(),
					datas.get(dataTitle).getCreatedDate(), datas.get(dataTitle).getIdUser(),
					datas.get(dataTitle).getPaymentMethod().toString());
			dataTitle++;
		}
		List<Integer> memberRow = new ArrayList<>();
		for (int i = 0; i < rowCount; i++) {
			if (!titleRow.contains(i)) {
				memberRow.add(i);
			}
		}
		dataTitle = 0;
		int productData = 0;
		for (int i : memberRow) {
			dataArr[i] = new DataCSV(null, null, datas.get(dataTitle).getProductResponse().get(productData).getId(),
					datas.get(dataTitle).getProductResponse().get(productData).getProductType(),
					datas.get(dataTitle).getProductResponse().get(productData).getPrice(),
					datas.get(dataTitle).getProductResponse().get(productData).getQuantityBuy(), null, null, null, null,
					null);
			productData++;
			if (productData == datas.get(dataTitle).getProductResponse().size()) {
				productData = 0;
				dataTitle++;
			}
		}
		return dataArr;
	}
}
