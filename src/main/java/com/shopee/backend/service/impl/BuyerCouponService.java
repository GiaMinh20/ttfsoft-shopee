package com.shopee.backend.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.model.BuyerCoupon;
import com.shopee.backend.repository.BuyerCouponRepository;
import com.shopee.backend.repository.custom.BuyerCouponRepositoryCustom;
import com.shopee.backend.service.IBuyerCouponService;

@Service
public class BuyerCouponService implements IBuyerCouponService{
    @Autowired
    BuyerCouponRepositoryCustom buyerCouponRepositoryCustom;

    @Autowired
    BuyerCouponRepository buyerCouponRepository;

    @Value("${coupon.errorPickup}")
    private String errorPickup;

    public BuyerCoupon findByCode(Long idBuyer, String coupon) {
        return buyerCouponRepository.findByCode(idBuyer,coupon);
    }

    @Override
    public BuyerCoupon pickCouponByCode(Long idBuyer, String code) {
        BuyerCoupon bc = buyerCouponRepositoryCustom.pickCouponByCode(idBuyer, code);
        if(bc != null) return bc;
        else throw new CommonRuntimeException(errorPickup + code); 
    }
}
