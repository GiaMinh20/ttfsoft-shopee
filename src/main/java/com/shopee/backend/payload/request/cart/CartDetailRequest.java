package com.shopee.backend.payload.request.cart;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class CartDetailRequest {
    @NotNull(message = "id product classification must not be null")
    private Long idProductClassification;
    @NotNull(message = "quantity must not be null")
    private Long quantity;
}
