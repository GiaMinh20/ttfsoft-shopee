package com.shopee.backend.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.service.IAdminProductManagement;
import com.shopee.backend.service.IProductService;
import com.shopee.backend.utility.datatype.EProductStatus;
import com.shopee.backend.utility.datatype.EUserRole;
import com.shopee.backend.utility.sort.ModelSorting;

@RestController
@RequestMapping("/api/admin")
public class ProductManageController {

	@Autowired
	IAdminProductManagement adminProductManagement;

	@Autowired
	IProductService productService;

	@GetMapping("/manage/product")
	public ResponseEntity<?> getProduct(
			@RequestParam(required = false) Long idShop,
			@RequestParam(required = false) Long idCategory, 
			@RequestParam(required = false) Long idType,
			@RequestParam(required = false) Long idShopCategory, 
			@RequestParam(required = false) Long idSellPlace,
			@RequestParam(required = false) String searchName, 
			@RequestParam(required = false) Boolean isNew,
			@RequestParam(required = false) EProductStatus status, 
			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer size, 
			@RequestParam(required = false) Integer sortBy,
			@RequestParam(required = false) Boolean sortDescending)  {
		
		if(isNew == null) isNew = true;
		return ResponseEntity.ok(
				productService.getWithFilter(
						idShop, 
						idCategory, 
						idType, 
						idShopCategory, 
						idSellPlace,
						searchName, 
						status, 
						isNew, 
						page, 
						size, 
						ModelSorting.getProductSort(sortBy, sortDescending)));
	}

	@GetMapping("/manage/product/{id}")
	public ResponseEntity<?> getProductById(@PathVariable("id") Long id) {
		return ResponseEntity.ok(productService.getById(id, EUserRole.ADMIN, null));
	}

	@PutMapping("/manage/product/ban")
	public ResponseEntity<?> banProduct(@RequestBody Long[] ids) {
		return ResponseEntity.ok(adminProductManagement.banProduct(ids));
	}
	
	@PutMapping("/manage/product/show")
	public ResponseEntity<?> showProduct(@RequestBody Long[] ids) {
		return ResponseEntity.ok(adminProductManagement.show_hide_Product(ids));
	}
	
	@DeleteMapping("/manage/product")
	public ResponseEntity<?> deleteProduct(@RequestBody Long[] ids) {
		return ResponseEntity.ok(adminProductManagement.deleteProduct(ids));
	}

}
