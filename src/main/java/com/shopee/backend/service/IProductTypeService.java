package com.shopee.backend.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.dto.ProductTypeDTO;
import com.shopee.backend.payload.request.producttype.CreateProductTypeRequest;
import com.shopee.backend.payload.request.producttype.EditProductTypeRequest;
import com.shopee.backend.payload.response.BaseResponse;
import com.shopee.backend.payload.response.DataResponse;
import com.shopee.backend.payload.response.ListResponse;
import com.shopee.backend.payload.response.ListWithPagingResponse;
import com.shopee.backend.utility.Page;

public interface IProductTypeService {

	boolean delete(Long id);

	DataResponse<ProductTypeDTO> update(Long id, EditProductTypeRequest request, MultipartFile image)
			throws IOException;

	ListWithPagingResponse<ProductTypeDTO> findByName(String searchName, Page page);

	DataResponse<ProductTypeDTO> getById(Long id);

	ListResponse<ProductTypeDTO> getAllActive();

	ListResponse<ProductTypeDTO> getAll();

	DataResponse<ProductTypeDTO> create(CreateProductTypeRequest request, MultipartFile image) throws IOException;

	ListResponse<ProductTypeDTO> getByParent(String parent);

	BaseResponse updateParent(Long id, Long parenId);

	DataResponse<ProductTypeDTO> getParent(Long id);

	Long getCountProductTypeByName(String searchName);


}
