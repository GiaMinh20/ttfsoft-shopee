package com.shopee.backend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.shopee.backend.model.Buyer;
import com.shopee.backend.model.DeliveryAddress;

@Repository
public interface DeliveryAddressRepository extends JpaRepository<DeliveryAddress, Long> {
	List<DeliveryAddress> findByBuyer(Buyer buyer);

	@Query(value = "Select count(id) from delivery_address where id_buyer = :idBuyer", nativeQuery = true)
	Long countDeliveryAddress(@Param("idBuyer") Long idBuyer);

}
