package com.shopee.backend.model.listener;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.shopee.backend.service.IProductReviewService;

@Component
public class ProductReviewListener {

	IProductReviewService productReviewService;

	public ProductReviewListener(@Lazy IProductReviewService productReviewService) {
		this.productReviewService = productReviewService;
	}
}
