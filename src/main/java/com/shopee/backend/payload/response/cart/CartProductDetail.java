package com.shopee.backend.payload.response.cart;

import java.util.ArrayList;
import java.util.List;

import com.shopee.backend.dto.ProductClassificationDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CartProductDetail {
	private String nameProduct;
	private String avatar;
	private List<ProductClassificationDTO> lstVariation = new ArrayList<>();
	private String currentNameVariation;
	private Long price;
	private Double discount;
	private Long quantity;
	private Boolean isAvailable;
}