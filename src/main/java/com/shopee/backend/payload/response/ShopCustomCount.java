package com.shopee.backend.payload.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ShopCustomCount {
	private Boolean success;
	private String message;
	private Long pOutOfStock = 0L;
	private Long countAccessTimesShop = 0L;
	private Long countOrderConfirmed = 0L;
	private Long pWAITFORPAYMENT = 0L;
	private Long pWAITFORCONFIRMATION = 0L;
	private Long pWAITFORSENDING = 0L;
	private Long pDELIVERING = 0L;
	private Long pDELIVERED = 0L;
	private Long pCANCELLED = 0L;
	private WorkPerformance workPerformance;
}
