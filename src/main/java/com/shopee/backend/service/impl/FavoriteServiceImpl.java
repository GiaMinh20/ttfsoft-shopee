package com.shopee.backend.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopee.backend.model.Buyer;
import com.shopee.backend.model.FavoriteBuyer;
import com.shopee.backend.model.pk.FavoriteBuyerPK;
import com.shopee.backend.repository.BuyerRepository;
import com.shopee.backend.repository.FavoriteRepository;
import com.shopee.backend.repository.ProductRepository;
import com.shopee.backend.service.IFavoriteService;

@Service
public class FavoriteServiceImpl implements IFavoriteService{

    @Autowired
    FavoriteRepository repository;

    @Autowired
    BuyerRepository buyerRepository;

    @Autowired
    ProductRepository productRepository;

    @Override
    public List<FavoriteBuyer> getAllByBuyer(Long idBuyer){
        return repository.getAllByBuyer(buyerRepository.getReferenceById(idBuyer));
    }

    @Override
    public List<FavoriteBuyer> getAllByProduct(Long idP){
        return repository.getAllByProduct(productRepository.getReferenceById(idP));
    }

    @Override
    public void addFavorites(Long idBuyer, Collection<Long> idProducts) {
        Buyer buyer = buyerRepository.getReferenceById(idBuyer);

        List<FavoriteBuyer> entities = idProducts.stream().map(idP -> {
            FavoriteBuyer entity = new FavoriteBuyer();
            entity.setProduct(productRepository.getReferenceById(idP));
            entity.setBuyer(buyer);
            entity.setId(new FavoriteBuyerPK(idBuyer, idP));
            return entity;
        }).collect(Collectors.toList());

        repository.saveAll(entities);
    }

    @Override
    public void unFavorites(Long idBuyer, Collection<Long> idProducts) {
        repository.deleteAllById(idBuyer, idProducts);
    }

    @Override
    public Boolean isFavorite(Long idBuyer, Long idP) {
        return repository.existsById(new FavoriteBuyerPK(idBuyer, idP));
    }
}
