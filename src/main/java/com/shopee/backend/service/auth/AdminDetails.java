package com.shopee.backend.service.auth;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.shopee.backend.model.Admin;
import com.shopee.backend.model.MediaResource;
import com.shopee.backend.service.auth.impl.UserDetailsImpl;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AdminDetails extends UserDetailsImpl {
	private static final long serialVersionUID = 788715377934319508L;
	private String phone;
	private String email;
	private String name;

	@JsonIgnore
	private String address;
	private MediaResource avatar;
	
	public AdminDetails() {
		super();
	}
	
	public AdminDetails(Long id, String username, String password, String phone, String email, String name,
			String address, MediaResource avatar, Collection<? extends GrantedAuthority> authorities) {
		super(id, username, password, authorities);
		this.phone = phone;
		this.email = email;
		this.name = name;
		this.address = address;
		this.avatar = avatar;
	}
	
	public static AdminDetails build(Admin user) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		
		return new AdminDetails(
				user.getId(), 
				user.getUsername(), 
				user.getPassword(), 
				user.getPhone(), 
				user.getEmail(), 
				user.getName(), 
				user.getAddress(), 
				user.getAvatar(),
				authorities);
	}
	
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}
	
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}
	
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}
	
	@Override
	public boolean isEnabled() {
		return true;
	}
}
