package com.shopee.backend.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductReviewDTO {
	private Long idProductClassification;
	private String variationName;
	private String content;
	private Integer rating;

	private List<MediaResourceDTO> images;

	private BuyerDTO buyer;
	
	
}
