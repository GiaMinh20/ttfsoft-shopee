package com.shopee.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;

import com.shopee.backend.config.websocket.InboundChatMessage;
import com.shopee.backend.config.websocket.OutboundChatMessage;
import com.shopee.backend.service.auth.websocket.WebSocketBuyerDetails;
import com.shopee.backend.service.auth.websocket.WebSocketShopDetails;
import com.shopee.backend.service.impl.ChatService;

@Controller
public class WebSocketChatController {
	@Autowired
	SimpMessagingTemplate simpMessagingTemplate;
	
	@Autowired
	ChatService chatService;
	
	@MessageMapping("/chat")
	public void send(SimpMessageHeaderAccessor sha, @Payload InboundChatMessage inboundMessage) {
		Object ouser = ((UsernamePasswordAuthenticationToken)sha.getUser()).getPrincipal();
		
		if (ouser instanceof WebSocketBuyerDetails) {
			WebSocketBuyerDetails user = (WebSocketBuyerDetails)ouser;
			chatService.saveMessage(inboundMessage, user.getId(), true);
			
			simpMessagingTemplate.convertAndSendToUser(
					chatService.getReceiverUsername(inboundMessage.getIdReceiver(), false),
					"/queue/messages", 
					new OutboundChatMessage(
							inboundMessage.getContentType(), 
							inboundMessage.getData()
						)
				);
		}
		else if (ouser instanceof WebSocketShopDetails) {
			WebSocketShopDetails user = (WebSocketShopDetails)ouser;
			chatService.saveMessage(inboundMessage, user.getId(), false);

			simpMessagingTemplate.convertAndSendToUser(
					chatService.getReceiverUsername(inboundMessage.getIdReceiver(), true),
					"/queue/messages", 
					new OutboundChatMessage(
							inboundMessage.getContentType(), 
							inboundMessage.getData()
						)
				);
		}
	}
}
