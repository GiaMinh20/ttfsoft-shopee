package com.shopee.backend.service;

import java.io.ByteArrayInputStream;

import com.shopee.backend.payload.response.BaseResponse;
import com.shopee.backend.payload.response.DataResponse;
import com.shopee.backend.payload.response.ListResponse;
import com.shopee.backend.payload.response.ShopOrderManageResponse;
import com.shopee.backend.utility.datatype.EOrderStatus;

public interface IShopOrderService {

	ByteArrayInputStream exportDataWithStatus(Long idShop, EOrderStatus status, String from, String to);

	ByteArrayInputStream exportData(Long idShop, String from, String to);

	DataResponse<ShopOrderManageResponse> getOrderByShopAndId(Long idShop, Long id);

	ListResponse<ShopOrderManageResponse> getOrderByShopAndStatus(Long idShop, EOrderStatus status, String from,
			String to);

	BaseResponse cancel(Long id, Long idBuyer);

	ListResponse<ShopOrderManageResponse> getOrderByShop(Long idShop, String from, String to);


	BaseResponse update(Long idShop, Long id, EOrderStatus status);
}
