package com.shopee.backend.payload.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductOfShopOrderResponse {
	private Long id;
	private String variationName;
	private Long mediaId;
	private Long price;
	private Long quantityInStock;
	private Long quantityBuy;
	private String description;
	private String shopCategory;
	private String productCategory;
	private String productType;
}
