package com.shopee.backend.payload.response;

import com.shopee.backend.dto.CouponCodeDTO;
import com.shopee.backend.dto.ShopDTO;

import lombok.Data;

@Data
public class CouponCodeResponse {
	private CouponCodeDTO coupon;
	private ShopDTO shop;

}
