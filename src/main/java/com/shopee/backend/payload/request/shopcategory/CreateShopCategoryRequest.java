package com.shopee.backend.payload.request.shopcategory;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


import lombok.Getter;
import lombok.Setter;

@Getter @Setter

public class CreateShopCategoryRequest {
	@NotBlank
	@Size(max = 30, min = 4)
	private String name;
}
