package com.shopee.backend.controller;

import java.sql.SQLIntegrityConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.dto.SellPlaceDTO;
import com.shopee.backend.payload.response.BaseResponse;
import com.shopee.backend.service.ISellPlace;

@RestController
@RequestMapping("/api")
public class SellPlaceController {

	@Autowired
	ISellPlace sellPlaceService;

	@Value("${sellplace.deletesuccess}")
	private String deletesuccess;
	
	@GetMapping("/sell-place")
	public ResponseEntity<?> getAll() {
		return ResponseEntity.ok(sellPlaceService.getAllPlaces());
	}

	@GetMapping("/sell-place/{code}")
	public ResponseEntity<?> getAll(@PathVariable("code") String code) {
		return ResponseEntity.ok(sellPlaceService.getPlaceByCode(code));
	}

	@PutMapping("/admin/sell-place/{id}")
	public ResponseEntity<?> updatePlace(@PathVariable("id") Long id, @RequestBody SellPlaceDTO input) throws SQLIntegrityConstraintViolationException {
		return ResponseEntity.ok(sellPlaceService.update(id, input));
	}

	@PostMapping("/admin/sell-place")
	public ResponseEntity<?> updatePlace(@RequestBody SellPlaceDTO input) throws SQLIntegrityConstraintViolationException {
		return ResponseEntity.ok(sellPlaceService.create(input));

	}
	@DeleteMapping("/admin/sell-place")
	public ResponseEntity<?> updatePlace(@RequestBody Long[] ids) {
		sellPlaceService.delete(ids);
		return ResponseEntity.ok(new BaseResponse(true, deletesuccess));

	}
}
