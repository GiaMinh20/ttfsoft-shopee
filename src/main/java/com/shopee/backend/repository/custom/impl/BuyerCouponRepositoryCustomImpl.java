package com.shopee.backend.repository.custom.impl;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.model.BuyerCoupon;
import com.shopee.backend.model.BuyerCoupon_;
import com.shopee.backend.model.CouponCode;
import com.shopee.backend.model.pk.BuyerCouponPK;
import com.shopee.backend.repository.custom.BuyerCouponRepositoryCustom;
import com.shopee.backend.service.impl.CouponCodeService;

@Repository
public class BuyerCouponRepositoryCustomImpl implements BuyerCouponRepositoryCustom {
	@PersistenceContext
	EntityManager em;

	@Autowired
	CouponCodeService couponCodeService;

	@Transactional
	public BuyerCoupon pickCouponByCode(Long idBUyer, String code) {
		CouponCode coupon = couponCodeService.findByCode(code);
		if (coupon == null)
			throw new CommonRuntimeException(String.format("COUPON_CODE_NOT_FOUND %s", code));

		BuyerCoupon bc = checkCoupon(idBUyer, coupon);
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaUpdate<BuyerCoupon> cu = cb.createCriteriaUpdate(BuyerCoupon.class);
		Root<BuyerCoupon> root = cu.from(BuyerCoupon.class);

		Predicate p1 = cb.equal(root.get(BuyerCoupon_.id), bc.getId());
		Predicate p2 = cb.equal(root.get(BuyerCoupon_.isUsed), 0);
		Predicate p = cb.and(p1, p2);

		cu.set(root.get(BuyerCoupon_.isUsed), 1);
		cu.where(p);

		if (em.createQuery(cu).executeUpdate() != 0)
			return bc;
		else
			return null;
	}

	private BuyerCoupon checkCoupon(Long idBUyer, CouponCode coupon) {
		BuyerCoupon entity = em.find(BuyerCoupon.class, new BuyerCouponPK(idBUyer, coupon.getId()));
		if (entity == null)
			throw new CommonRuntimeException(
					String.format("%s COUPON_CODE_NOT_FOUND in your collection", coupon.getCode()));

		if (entity.getIsUsed() == 1)
			throw new CommonRuntimeException(
					String.format("Coupon %s in your collection is already used", entity.getCouponCode().getCode()));

		if (entity.getTime().getTime() < new Date().getTime())
			throw new CommonRuntimeException(String.format("Coupon code %s is out of date %s",
					entity.getCouponCode().getCode(), entity.getTime()));

		return entity;
	}
}
