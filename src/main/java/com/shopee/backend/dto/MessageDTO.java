package com.shopee.backend.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.shopee.backend.dto.base.AbstractDTO;

import lombok.Data;

@Data
public class MessageDTO extends AbstractDTO {
	private Long idUser;
	private String nameUser;
	private Long idShop;
	private String nameShop;
	private String content;
	private Long sender;
	private Boolean isView;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date time;

}
