package com.shopee.backend.utility.mapper;

import java.util.List;

import org.modelmapper.ModelMapper;

import com.shopee.backend.payload.response.ListWithPagingResponse;
import com.shopee.backend.utility.Page;

public class ResponseMapper {
	public static <T, R> ListWithPagingResponse<T> listWithPagingResponseMakeup(List<R> data, Page page,
			Class<T> dtoType, ModelMapper modelMapper) {
		return new ListWithPagingResponse<>(page.getPageNumber() + 1, page.getTotalPage() + 1,
				data.stream().map(p -> modelMapper.map(p, dtoType)).toList());
	}
}
