package com.shopee.backend.repository.custom;

import java.util.List;

import com.shopee.backend.dto.ProductReviewDTO;
import com.shopee.backend.utility.Page;

public interface ProductReviewRepoCustom {

	List<ProductReviewDTO> getProductReview(List<Long> ids, Long rate, Page page);

	Long countProductReview(List<Long> ids);

}
