package com.shopee.backend.utility.datatype;

public enum EMediaType {
	IMAGE,
	VIDEO
}
