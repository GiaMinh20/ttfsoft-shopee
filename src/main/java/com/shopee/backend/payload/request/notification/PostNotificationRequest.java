package com.shopee.backend.payload.request.notification;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PostNotificationRequest {
	private Long imageId;
	private String title;
	private String content;
	public PostNotificationRequest(Long imageId, String title, String content) {
		super();
		this.imageId = imageId;
		this.title = title;
		this.content = content;
	}
	
}
