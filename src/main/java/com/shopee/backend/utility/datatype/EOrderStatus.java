package com.shopee.backend.utility.datatype;

public enum EOrderStatus {
	WAIT_FOR_PAYMENT,
	WAIT_FOR_CONFIRMATION,
	WAIT_FOR_SENDING,
	DELIVERING,
	DELIVERED,
	CANCELLED
}
