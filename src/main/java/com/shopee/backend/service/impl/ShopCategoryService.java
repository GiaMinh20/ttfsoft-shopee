package com.shopee.backend.service.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.dto.ShopCategoryDTO;
import com.shopee.backend.model.Shop;
import com.shopee.backend.model.ShopCategory;
import com.shopee.backend.payload.request.shopcategory.CreateShopCategoryRequest;
import com.shopee.backend.payload.request.shopcategory.EditShopCategoryRequest;
import com.shopee.backend.payload.response.DataResponse;
import com.shopee.backend.payload.response.ListResponse;
import com.shopee.backend.repository.ShopCategoryRepository;
import com.shopee.backend.repository.ShopRepository;
import com.shopee.backend.service.IShopCategoryService;

@Service
public class ShopCategoryService implements IShopCategoryService {

	@Autowired
	private ShopCategoryRepository repository;
	@Autowired
	ShopRepository shopRepository;
	@Autowired
	private ModelMapper modelMapper;

	@Value("${shopcategory.notfoundwithid}")
	private String notfoundwithid;
	
	@Value("${shopcategory.notfound}")
	private String notfound;
	
	@Value("${shopcategory.updatedeny}")
	private String updatedeny;

	@Value("${shopcategory.deletedeny}")
	private String deletedeny;

	@Value("${shop.shopnotfound}")
	private String shopnotfound;

	@Override
	public DataResponse<ShopCategoryDTO> create(Long idShop, CreateShopCategoryRequest request) {
		ShopCategory shopCategory = new ShopCategory(shopRepository.getReferenceById(idShop), request.getName());
		return new DataResponse<>(modelMapper.map(repository.save(shopCategory), ShopCategoryDTO.class));
	}

	@Override
	public ListResponse<ShopCategoryDTO> getAllByShop(Long idShop) {
		Shop shop = shopRepository.getReferenceById(idShop);

		if (!shop.getIsActive()) {
			throw new CommonRuntimeException(shopnotfound);
		}

		List<ShopCategory> shopCategories = repository.findAllByShop(shop.getId());
		if (shopCategories == null || shopCategories.isEmpty()) {
			throw new CommonRuntimeException(notfound);
		}

		return new ListResponse<>(shopCategories.stream().map(sc -> modelMapper.map(sc, ShopCategoryDTO.class))
				.toList());
	}

	@Override
	public ListResponse<ShopCategoryDTO> findByShopAndName(Long idShop, String name) {
		Shop shop = shopRepository.getReferenceById(idShop);

		if (!shop.getIsActive()) {
			throw new CommonRuntimeException(shopnotfound);
		}

		List<ShopCategory> shopCategories = repository.findByShopAndName(shop.getId(), name);
		if (shopCategories == null || shopCategories.isEmpty()) {
			throw new CommonRuntimeException(notfound);
		}

		return new ListResponse<>(shopCategories.stream().map(sc -> modelMapper.map(sc, ShopCategoryDTO.class))
				.toList());
	}

	@Override
	public DataResponse<ShopCategoryDTO> getById(Long id) {
		Optional<ShopCategory> shopCategory = repository.findById(id);
		if (shopCategory.isEmpty()) {
			throw new EntityNotFoundException(notfoundwithid + id);
		}
		return new DataResponse<>(modelMapper.map(shopCategory, ShopCategoryDTO.class));
	}

	@Override
	public ListResponse<ShopCategoryDTO> findByName(String name) {
		List<ShopCategory> shopCategories = repository.findByName(name);
		if (shopCategories == null || shopCategories.isEmpty()) {
			throw new CommonRuntimeException(notfound);
		}
		return new ListResponse<>(shopCategories.stream().map(sc -> modelMapper.map(sc, ShopCategoryDTO.class))
				.toList());
	}

	@Override
	public DataResponse<ShopCategoryDTO> update(Long idShop, Long id, EditShopCategoryRequest request) {
		Shop shop = shopRepository.getReferenceById(idShop);
		if (!shop.getIsActive()) {
			throw new CommonRuntimeException(shopnotfound);
		}
		ShopCategory shopCategory = repository.getReferenceById(id);

		if (!shopCategory.getShop().getId().equals(idShop)) {
			throw new CommonRuntimeException(updatedeny);
		}

		if (!StringUtils.isBlank(request.getName())) {
			shopCategory.setName(request.getName());
		}
		return new DataResponse<>(modelMapper.map(repository.save(shopCategory), ShopCategoryDTO.class));
	}

	@Override
	public boolean delete(Long idShop, Long id) {
		Shop shop = shopRepository.getReferenceById(idShop);

		if (!shop.getIsActive()) {
			throw new CommonRuntimeException(shopnotfound);
		}

		Optional<ShopCategory> shopCategory = repository.findById(id);
		if (shopCategory.isEmpty()) {
			throw new EntityNotFoundException(notfoundwithid + id);
		}
		if (!shopCategory.get().getShop().getId().equals(idShop)) {
			throw new CommonRuntimeException(deletedeny);
		}

		repository.deleteShopCategoryFromProduct(id);
		repository.delete(shopCategory.get());
		return true;
	}

}
