package com.shopee.backend.model.pk;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class BuyerCouponPK implements Serializable {
	private static final long serialVersionUID = -3614078496983013648L;
	
	@Column(name="id_buyer")
	private Long idBuyer;
	
	@Column(name="id_coupon")
	private Long idCoupon;
}
