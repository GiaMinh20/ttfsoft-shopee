package com.shopee.backend.utility.sort;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;

import com.shopee.backend.model.Follow_;
import com.shopee.backend.model.Product_;

public class ModelSorting {
	/**
	 * Sort by combination of selected attribute <br>
	 * <i>Example: sortBy = (1+2+4) = 7 => sort by price, view and averageRating
	 * attribute
	 * 
	 * @param sortBy         null for unsorted and possible value is 1, 2, 4, 8
	 * @param sortDescending true if sort in descending order, otherwise ascending
	 * @return Sort object <br>
	 *         sortBy possible value <br>
	 *         &nbsp;&nbsp;&nbsp;&nbsp; 1: by price <br>
	 *         &nbsp;&nbsp;&nbsp;&nbsp; 2: by view <br>
	 *         &nbsp;&nbsp;&nbsp;&nbsp; 4: by averageRating <br>
	 *         &nbsp;&nbsp;&nbsp;&nbsp; 8: by reviewCount <br>
	 *         &nbsp;&nbsp;&nbsp;&nbsp; 16: by modifiedDate <br>
	 *         &nbsp;&nbsp;&nbsp;&nbsp; 32: by sales
	 */
	public static Sort getProductSort(Integer sortBy, Boolean sortDescending) {
		Sort sort = Sort.unsorted();

		if (sortBy != null) {
			if (sortDescending == null || !sortDescending.booleanValue()) { // ASC
				if (sortBy >= 32) {
					sort = sort.and(JpaSort.of(Product_.sales).ascending());
					sortBy -= 32;
				}
				if (sortBy >= 16) {
					sort = sort.and(JpaSort.of(Product_.modifiedDate).ascending());
					sortBy -= 16;
				}
				if (sortBy >= 8) {
					sort = sort.and(JpaSort.of(Product_.reviewCount).ascending());
					sortBy -= 8;
				}
				if (sortBy >= 4) {
					sort = sort.and(JpaSort.of(Product_.averageRating).ascending());
					sortBy -= 4;
				}
				if (sortBy >= 2) {
					sort = sort.and(JpaSort.of(Product_.view).ascending());
					sortBy -= 2;
				}
				if (sortBy >= 1) {
					sort = sort.and(JpaSort.of(Product_.price).ascending());
					sortBy -= 1;
				}
			} else { // DESC
				if (sortBy >= 32) {
					sort = sort.and(JpaSort.of(Product_.sales).descending());
					sortBy -= 32;
				}
				if (sortBy >= 16) {
					sort = sort.and(JpaSort.of(Product_.modifiedDate).descending());
					sortBy -= 16;
				}
				if (sortBy >= 8) {
					sort = sort.and(JpaSort.of(Product_.reviewCount).descending());
					sortBy -= 8;
				}
				if (sortBy >= 4) {
					sort = sort.and(JpaSort.of(Product_.averageRating).descending());
					sortBy -= 4;
				}
				if (sortBy >= 2) {
					sort = sort.and(JpaSort.of(Product_.view).descending());
					sortBy -= 2;
				}
				if (sortBy >= 1) {
					sort = sort.and(JpaSort.of(Product_.price).descending());
					sortBy -= 1;
				}
			}
		}
		return sort;
	}

	/**
	 * Sort by time attribute
	 * 
	 * @param sortBy
	 * @return Sort object sortBy possible value <br>
	 *         &nbsp;&nbsp;&nbsp;&nbsp; 1: sort by time ascending <br>
	 *         &nbsp;&nbsp;&nbsp;&nbsp; 2: sort by time descending
	 */
	public static Sort getFollowSort(Integer sortBy) {
		Sort sort = Sort.unsorted();
		if (sortBy != null) {
			switch (sortBy) {
			case 1:
				sort = JpaSort.of(Follow_.time).ascending();
				break;
			case 2:
				sort = JpaSort.of(Follow_.time).descending();
				break;
			}
		}

		return sort;
	}
}
