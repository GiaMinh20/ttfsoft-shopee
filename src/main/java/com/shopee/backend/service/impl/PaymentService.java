package com.shopee.backend.service.impl;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.model.Order;
import com.shopee.backend.model.ShopOrder;
import com.shopee.backend.model.base.BaseModel;
import com.shopee.backend.payload.request.payment.PaymentResponse;
import com.shopee.backend.payload.request.payment.ShopOrderPaymentRequest;
import com.shopee.backend.repository.OrderRepository;
import com.shopee.backend.repository.ShopOrderRepository;
import com.shopee.backend.service.impl.onlinepayment.MomoService;
import com.shopee.backend.utility.datatype.EOrderStatus;
import com.shopee.backend.utility.datatype.EPaymentMethod;

@Service
public class PaymentService {
	@Autowired
	OrderRepository orderRepository;
	
	@Autowired
	ShopOrderRepository shopOrderRepository;
	
	@Autowired
	MomoService momoService;
	
	@Value("${momo.url.notify}")
	String momoNotifyUrl;
	
	public String orderConfirm(Long idBuyer, Long idOrder, String returnUrl, HttpServletRequest req) {
		Order o = orderRepository.getReferenceById(idOrder);
		
		if (!o.getBuyer().getId().equals(idBuyer)) {
			throw new CommonRuntimeException("You can only confirm payment of your order");
		}
		
		for (ShopOrder so : o.getShopOrders()) {
			if (so.getStatus() != EOrderStatus.WAIT_FOR_PAYMENT) {
				throw new CommonRuntimeException("Order status is not in wait for payment state");
			}
		}
		
		if (o.getPaymentMethod() == EPaymentMethod.SHIP_COD) {
			confirmOrderStatus(o);
			return "";
		}
		else if (o.getPaymentMethod() == EPaymentMethod.ONLINE_PAYMENT_MOMO) {
			if (StringUtils.isBlank(returnUrl)) {
				throw new CommonRuntimeException("Online payment required return url in order to redirect from 3rd party payment back to website");
			}
			
			return momoService.getOrderPayUrl(
						idOrder,
						String.format("%s://%s%s", req.getScheme(), req.getServerName(), momoNotifyUrl),
						returnUrl,
						o.getPriceFinal().toString()
					);
		}
		throw new CommonRuntimeException("Invalid payment method");
	}
	
	public PaymentResponse shopOrderConfirm(Long idBuyer, ShopOrderPaymentRequest request, HttpServletRequest req) {
		ShopOrder so = shopOrderRepository.getReferenceById(request.getIdShopOrder());
		
		if (!so.getOrder().getBuyer().getId().equals(idBuyer)) {
			throw new CommonRuntimeException("You can only confirm payment of your order");
		}
		else if (so.getStatus() != EOrderStatus.WAIT_FOR_PAYMENT) {
			throw new CommonRuntimeException("Order status is not in wait for payment state");
		}
		
		if (so.getOrder().getPaymentMethod() == EPaymentMethod.SHIP_COD) {
			confirmShopOrderStatus(so.getId());
			return new PaymentResponse();
		}
		else if (so.getOrder().getPaymentMethod() == EPaymentMethod.ONLINE_PAYMENT_MOMO) {
			if (StringUtils.isBlank(request.getReturnUrl())) {
				throw new CommonRuntimeException("Online payment required return url in order to redirect from 3rd party payment back to website");
			}
			
			String payUrl = momoService.getShopOrderPayUrl(
							request.getIdShopOrder(),
							String.format("%s://%s:%s%s", req.getScheme(), req.getServerName(), req.getServerPort(), momoNotifyUrl),
							request.getReturnUrl(),
							so.getTotalPayPrice().toString());
			
			if (StringUtils.isNotBlank(payUrl)) {
				return new PaymentResponse(payUrl);
			}
			else {
				return new PaymentResponse(false, "Failed to create payment request. Please try again", payUrl);
			}
		}
		throw new CommonRuntimeException("Invalid payment method");
	}
	
	public boolean confirmOrderStatus(Long idOrder) {
		return confirmOrderStatus(orderRepository.getReferenceById(idOrder));
	}
	
	public boolean confirmOrderStatus(Order o) {
		return confirmStatus(
				o
					.getShopOrders()
					.stream()
					.map(BaseModel::getId)
					.toList()
			);
	}
	
	public boolean confirmShopOrderStatus(Long idShopOrder) {
		return confirmStatus(Arrays.asList(idShopOrder));
	}
	
	private boolean confirmStatus(List<Long> idShopOrders) {
		try {
			shopOrderRepository.confirmPayment(idShopOrders);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
