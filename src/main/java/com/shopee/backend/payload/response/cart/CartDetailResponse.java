package com.shopee.backend.payload.response.cart;

import com.shopee.backend.utility.datatype.EProductStatus;

import lombok.Data;

@Data
public class CartDetailResponse{
    private Long quantity;

    // thuộc tính bảng sản phẩm
    private Long idProduct;
    private String nameProduct;
    private String imageCover;
    private EProductStatus pStatus;
    private Long idShop;

    // thuộc tính bảng phân loại
    private Long idProductClassification;
    private String nameClassification;
    private Double discount;
    private Long priceOriginal;
    private Long priceFinal;
    private Long availableQuantity;
    private EProductStatus pcStatus;
}
