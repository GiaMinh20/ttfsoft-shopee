package com.shopee.backend.controller.shop;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.model.Shop;
import com.shopee.backend.service.IShopService;
import com.shopee.backend.service.auth.ShopDetails;

@RestController
@RequestMapping("/api/shop")
public class ShopHomeController {

	@Autowired
	IShopService shopService;
	@Autowired
	ModelMapper modelMapper;

	@GetMapping
	public ResponseEntity<?> getCustomCount(@AuthenticationPrincipal ShopDetails shopdetails) {
		// Ưu tiên dLast. Nếu k có dlast thì mới dùng tới dStart, dEnd
		Shop shop = modelMapper.map(shopdetails, Shop.class);
		return ResponseEntity.ok(shopService.getCustomCountProduct(shop));
	}

	@GetMapping("/sales")
	public ResponseEntity<?> getSalesIndicator(
			@AuthenticationPrincipal ShopDetails shopdetails,
			@RequestParam(name = "dStart", required = false, defaultValue = "2001-01-01") String dStart,
			@RequestParam(name = "dEnd", required = false, defaultValue = "2091-01-01") String dEnd,
			@RequestParam(name = "dLast", required = false) Long dLast) throws ParseException {
		
		// Ưu tiên dLast. Nếu k có dlast thì mới dùng tới dStart, dEnd
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Shop shop = modelMapper.map(shopdetails, Shop.class);
		return ResponseEntity.ok(shopService.getSales(shop, formatter.parse(dStart), formatter.parse(dEnd), dLast));
	}
}
