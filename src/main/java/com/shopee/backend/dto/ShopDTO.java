package com.shopee.backend.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.shopee.backend.dto.base.UserDTO;
import com.shopee.backend.model.MediaResource;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ShopDTO extends UserDTO {
	private String address;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date joinDate;
	private MediaResource coverImage;
	private String defaultReplyMessage;
	private Long totalRating;
	private Float averageRating;
	private Boolean phoneConfirmed;
	private Boolean emailConfirmed;
	private Boolean isActive;
}
