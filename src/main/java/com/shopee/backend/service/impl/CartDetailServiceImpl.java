package com.shopee.backend.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.model.CartDetail;
import com.shopee.backend.model.ProductClassification;
import com.shopee.backend.model.pk.CartDetailPK;
import com.shopee.backend.payload.request.cart.CartDetailRequest;
import com.shopee.backend.payload.response.BaseResponse;
import com.shopee.backend.payload.response.cart.CartDetailResponse;
import com.shopee.backend.payload.response.cart.GroupCartDetailResponse;
import com.shopee.backend.repository.BuyerRepository;
import com.shopee.backend.repository.CartDetailRepository;
import com.shopee.backend.repository.CouponRepository;
import com.shopee.backend.repository.ProductRepository;
import com.shopee.backend.service.ICartDetailService;
import com.shopee.backend.service.IProductClassificationService;
import com.shopee.backend.utility.datatype.EProductClassificationStatus;
import com.shopee.backend.utility.datatype.EProductStatus;

@Service
public class CartDetailServiceImpl implements ICartDetailService {

	@Autowired
	CartDetailRepository cartDetailRepository;

	@Autowired
	BuyerRepository buyerRepository;

	@Autowired
	ProductRepository productRepository;

	@Autowired
	IProductClassificationService productClassificationService;

	@Autowired
	CouponRepository couponCodeRepository;

	@Autowired
	ModelMapper modelMapper;

	@Value("${cartdetail.notfound}")
	private String cartNotFound;
	@Value("${app.success}")
	private String success;
	@Value("${product.notAvailable}")
	private String productNotAvailable;
	@Value("${productclassification.notAvailable}")
	private String pcNotAvailable;
	@Value("${data.notEnoughQuantity}")
	private String notEnoughQuantity;
	@Value("${data.quantityGt0}")
	private String quantityMustGreaterThan0;

	@Override
	public CartDetail findById(CartDetailPK id) {
		return cartDetailRepository.findById(id).orElseThrow(() -> new CommonRuntimeException(String
				.format("Cartdetail not found with product classifications: %s", id.getIdProductClassification())));
	}

	@Override
	public List<CartDetail> findAllById(Collection<Long> idPs, Long idBuyer){
		return cartDetailRepository.findAllById(idPs, idBuyer);
	} 

	@Override
	public CartDetailResponse addToCart(CartDetailRequest requestBody, Long idBuyer) {
		Optional<CartDetail> optionalEntity = cartDetailRepository
				.findById(new CartDetailPK(idBuyer, requestBody.getIdProductClassification()));

		CartDetail entity;

		if (optionalEntity.isPresent()) { // already exists
			entity = optionalEntity.get();
			checkProductClassification(entity.getProductClassification());
		} else {
			entity = modelMapper.map(requestBody, CartDetail.class);
			entity.setProductClassification(
					productClassificationService.findById(requestBody.getIdProductClassification()));
			checkProductClassification(entity.getProductClassification());

			entity.setId(new CartDetailPK(idBuyer, requestBody.getIdProductClassification()));
			entity.setBuyer(buyerRepository.getReferenceById(idBuyer));
		}
		entity.setQuantity(getNewQuantity(entity, requestBody.getQuantity()));
		entity = cartDetailRepository.save(entity);
		return modelMapper.map(entity, CartDetailResponse.class);
	}

	private Long getNewQuantity(CartDetail entity, Long modifyQt) {
		Long newQuantity = entity.getQuantity() + modifyQt;
		checkAvailableQuantity(newQuantity, entity.getProductClassification().getQuantity());
		return newQuantity;
	}

	private void checkAvailableQuantity(Long needQuantity, Long avaiQuantity) {
		if (avaiQuantity < needQuantity)
			throw new CommonRuntimeException(String.format(notEnoughQuantity, needQuantity, avaiQuantity));
		if (needQuantity < 1)
			throw new CommonRuntimeException(quantityMustGreaterThan0);
	}

	private void checkProductClassification(ProductClassification entity) {
		if (entity.getProduct().getStatus() != EProductStatus.ACTIVE)
			throw new CommonRuntimeException(String.format(productNotAvailable, entity.getProduct().getName()));

		if (entity.getStatus() == EProductClassificationStatus.HIDDEN)
			throw new CommonRuntimeException(String.format(pcNotAvailable, entity.getVariationName()));
	}

	@Override
	public void delete(CartDetail entity) {
		cartDetailRepository.delete(entity);
	}

	@Override
	public void deleteAllByIds(Iterable<Long> idProductClassifications, Long idBuyer) {
		List<CartDetailPK> ids = new ArrayList<>();
		idProductClassifications.forEach(i -> ids.add(new CartDetailPK(idBuyer, i)));
		cartDetailRepository.deleteAllByIdInBatch(ids);
	}

	@Override
	public void deleteAllByBuyer(Long idBuyer) {
		cartDetailRepository.deleteAllByBuyerId(idBuyer);
	}

	@Override
	public List<GroupCartDetailResponse> findByBuyer(Long idBuyer) {
		List<CartDetail> entities = cartDetailRepository.findAllByBuyerOrderByShop(idBuyer);
		List<GroupCartDetailResponse> listGroup = new ArrayList<>();
		entities.stream().forEach(i -> {
			CartDetailResponse cartDetail = modelMapper.map(i, CartDetailResponse.class);
			// group by id_shop
			if (listGroup.isEmpty() // different idShop -> create new group
					|| !Objects.equals(cartDetail.getIdShop(), listGroup.get(listGroup.size() - 1).getIdShop())) {

				GroupCartDetailResponse group = modelMapper.map(cartDetail, GroupCartDetailResponse.class);
				group.setNameShop(i.getProductClassification().getProduct().getShop().getName());
				group.getItems().add(cartDetail);

				listGroup.add(group);
			} else { // have same idshop -> add to group
				listGroup.get(listGroup.size() - 1).getItems().add(cartDetail);
			}
		});
		return listGroup;
	}

	@Override
	public Long countByBuyer(Long idBuyer) {
		return cartDetailRepository.countQuantityByBuyer(idBuyer);
	}

	@Override
	public BaseResponse updateCart(CartDetailPK pk, Long quantity) {
		Optional<CartDetail> optCart = cartDetailRepository.findById(pk);
		if (optCart.isEmpty())
			throw new EntityNotFoundException(cartNotFound);
		CartDetail cart = optCart.get();

		checkProductClassification(cart.getProductClassification());
		checkAvailableQuantity(quantity, cart.getProductClassification().getQuantity());

		cart.setQuantity(quantity);

		cartDetailRepository.save(cart);
		return new BaseResponse(true, success);
	}
}