package com.shopee.backend.payload.response.checkout;

import lombok.Data;

@Data
public class PriceByShop {
    private Long idShop;
    private Long totalItemPrice = 0L;
    private Long shipPrice = 0L;
    private Long shopPriceOff = 0L;
    private Long shipPriceOff = 0L;

    public void addTotalItemPrice(Long itemPrice) {
        totalItemPrice += itemPrice;
    }

    
}
