package com.shopee.backend.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.shopee.backend.model.base.BaseModel;
import com.shopee.backend.utility.datatype.ECouponCodeType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "coupon_code")
public class CouponCode extends BaseModel {

	@ManyToOne
	@JoinColumn(name = "id_shop")
	private Shop shop;

	@Column(unique = true)
	@NotBlank
	private String code;

	@NotBlank
	private String name;

	@NotNull
	private int status;

	@Lob
	@Column(length = 65535)
	@NotBlank
	private String description;

	@NotNull
	@Min(value = 0)
	private Integer quantity;

	private Date dod;

	@Enumerated(EnumType.ORDINAL)
	@NonNull
	private ECouponCodeType type;

	private Long discount;

	private Long minPrice;

	private Long discountLimit;

}
