package com.shopee.backend.dto.base;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public abstract class AbstractDTO {
	protected Long id;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	protected Date createdDate;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	protected Date modifiedDate;
	protected String createdBy;
	protected String modifiedBy;
}
