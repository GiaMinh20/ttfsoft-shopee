package com.shopee.backend.dto;

import com.shopee.backend.dto.base.AbstractDTO;

import lombok.Data;

@Data
public class RatingMediaDTO extends AbstractDTO{
	private Long idRating;
	private String mediaLink;
	private int mediaType;
	
}
