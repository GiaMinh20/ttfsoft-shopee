package com.shopee.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.shopee.backend.model.ProductReview;
import com.shopee.backend.model.pk.ProductReviewPK;
import com.shopee.backend.repository.custom.ProductReviewRepoCustom;

@Repository
public interface ReviewRepository extends JpaRepository<ProductReview, ProductReviewPK>, ProductReviewRepoCustom {

	@Query(value = "select * from product_review where id_product_classification = :id1 and id_order = :id2 and id_shop_order = :id3", nativeQuery = true)
	ProductReview getOneByPK(@Param("id1") Long idClassify, @Param("id2") Long idOrder,
			@Param("id3") Long idShopOrder);

}
