package com.shopee.backend.payload.request.order;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class GroupCheckoutRequest {
    List<GroupCheckoutShop> groupCheckoutShops = new ArrayList<>();
    String freeshipCoupon;
    String platformCoupon;
}
