package com.shopee.backend.config;

import java.sql.Date;
import java.util.List;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.shopee.backend.dto.BuyerDTO;
import com.shopee.backend.dto.DeliveryAddressDTO;
import com.shopee.backend.dto.FavoriteDTO;
import com.shopee.backend.dto.FollowDTO;
import com.shopee.backend.dto.NotificationsDTO;
import com.shopee.backend.dto.OrderDetailDTO;
import com.shopee.backend.dto.ProductCategoryDTO;
import com.shopee.backend.dto.ProductClassificationDTO;
import com.shopee.backend.dto.ProductDTO;
import com.shopee.backend.dto.ProductGeneralDTO;
import com.shopee.backend.dto.ProductImageDTO;
import com.shopee.backend.dto.ProductTypeDTO;
import com.shopee.backend.dto.ShopCategoryDTO;
import com.shopee.backend.dto.ShopOrderDTO;
import com.shopee.backend.model.Buyer;
import com.shopee.backend.model.CartDetail;
import com.shopee.backend.model.DeliveryAddress;
import com.shopee.backend.model.FavoriteBuyer;
import com.shopee.backend.model.Follow;
import com.shopee.backend.model.MediaResource;
import com.shopee.backend.model.Notification;
import com.shopee.backend.model.Order;
import com.shopee.backend.model.OrderDetail;
import com.shopee.backend.model.Product;
import com.shopee.backend.model.ProductCategory;
import com.shopee.backend.model.ProductClassification;
import com.shopee.backend.model.ProductImage;
import com.shopee.backend.model.ProductType;
import com.shopee.backend.model.ShopCategory;
import com.shopee.backend.model.ShopOrder;
import com.shopee.backend.payload.request.order.CreateOrderRequest;
import com.shopee.backend.payload.request.order.ShopOrderItem;
import com.shopee.backend.payload.response.ProductOfShopOrderResponse;
import com.shopee.backend.payload.response.ShopOrderManageResponse;
import com.shopee.backend.payload.response.cart.CartDetailResponse;
import com.shopee.backend.payload.response.cart.GroupCartDetailResponse;
import com.shopee.backend.utility.datatype.EOrderStatus;
import com.shopee.backend.utility.datatype.EPaymentMethod;

@Configuration
public class ModelMapperConfig {

	@Bean
	public ModelMapper modelMapper() {
		Converter<MediaResource, String> mediaResourceCvt = c -> {
			if (c.getSource() == null)
				return "";
			else
				return c.getSource().getUrl();
		};

		Converter<CartDetail, Long> finalPriceCvt = c -> {
			ProductClassification p = c.getSource().getProductClassification();

			return (p.getDiscount() == null) ? p.getPrice() * c.getSource().getQuantity()
					: Math.round(p.getPrice() - (1 - p.getDiscount()));
		};

		Converter<List<ProductClassification>, List<ProductClassificationDTO>> productClassificationsCvt = c -> {
			ModelMapper mapper = new ModelMapper();
			if (c.getSource() == null)
				return null;
			else
				return c.getSource().stream().map(m -> mapper.map(m, ProductClassificationDTO.class)).toList();
		};

		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		// ProductType to ProductTypeDTO
		modelMapper.createTypeMap(ProductType.class, ProductTypeDTO.class).addMappings(mapper -> {
			mapper.map(src -> src.getParent().getId(), (dst, value) -> dst.setIdParent((Long) value));
			mapper.map(src -> src.getParent().getName(), (dst, value) -> dst.setNameParent((String) value));
		});
		// ProductCategory to ProductCategoryDTO
		modelMapper.createTypeMap(ProductCategory.class, ProductCategoryDTO.class).addMappings(
				mapper -> mapper.using(mediaResourceCvt).map(ProductCategory::getImage, ProductCategoryDTO::setImage));

		// Product to ProductDTO
		modelMapper.createTypeMap(Product.class, ProductDTO.class).addMappings(mapper -> {
			mapper.using(mediaResourceCvt).map(Product::getAvatar, ProductDTO::setAvatar);
			mapper.using(productClassificationsCvt).map(Product::getClassifications, ProductDTO::setClassifications);
			mapper.map(src -> src.getProductType().getId(), (dst, value) -> dst.setIdType((Long) value));
			mapper.map(src -> src.getProductCategory().getId(), (dst, value) -> dst.setIdCategory((Long) value));
			mapper.map(src -> src.getShop().getId(), (dst, value) -> dst.setIdShop((Long) value));
			mapper.map(src -> src.getSellPlace().getId(), (dst, value) -> dst.setIdSellPlace((Long) value));

			mapper.map(src -> src.getSellPlace().getName(), (dst, value) -> dst.setSellPlace((String) value));
			mapper.map(src -> src.getShopCategory() == null ? null : src.getShopCategory().getId(),
					(dst, value) -> dst.setIdShopCategory((Long) value));
		});
		// Product to ProductGeneralDTO
		modelMapper.createTypeMap(Product.class, ProductGeneralDTO.class).addMappings(mapper -> {
			mapper.using(mediaResourceCvt).map(Product::getAvatar, ProductGeneralDTO::setAvatar);
			mapper.map(src -> src.getSellPlace().getId(), (dst, value) -> dst.setIdSellPlace((Long) value));
			mapper.map(src -> src.getSellPlace().getName(), (dst, value) -> dst.setSellPlace((String) value));
		});
		// ProductImage to ProductImageDTO
		modelMapper.createTypeMap(ProductImage.class, ProductImageDTO.class).addMappings(mapper -> {
			mapper.map(src -> src.getProduct().getId(), (dst, value) -> dst.setIdProduct((Long) value));
			mapper.map(src -> src.getId(), (dst, value) -> dst.setId((Long) value));
			mapper.map(src -> src.getMedia().getUrl(), (dst, value) -> dst.setUrl((String) value));
			mapper.map(src -> src.getMedia().getResourceType(), (dst, value) -> dst.setResourceType((String) value));
		});

		// Delivery address to delivery addressDTO
		modelMapper.createTypeMap(DeliveryAddress.class, DeliveryAddressDTO.class).addMappings(
				mapper -> mapper.map(src -> src.getBuyer().getId(), (dst, value) -> dst.setIdBuyer(((Long) value))));
		// ShopCategory to ShopCategoryDTO
		modelMapper.createTypeMap(ShopCategory.class, ShopCategoryDTO.class).addMappings(
				mapper -> mapper.map(src -> src.getShop().getId(), (dst, value) -> dst.setIdShop((Long) value)));

		// ShopOrder to ShopOrderDTO
		modelMapper.createTypeMap(ShopOrder.class, ShopOrderDTO.class).addMappings(mapper -> {
			mapper.map(src -> src.getShop().getId(), (dst, value) -> dst.setIdShop((Long) value));
			mapper.map(src -> src.getOrder().getId(), (dst, value) -> dst.setIdOrder((Long) value));
		});

		// OrderDetail to OrderDetailDTO
		modelMapper.createTypeMap(OrderDetail.class, OrderDetailDTO.class).addMappings(mapper -> {
			mapper.map(src -> src.getShopOrder().getId(), (dst, value) -> dst.setIdOrderShop((Long) value));
			mapper.map(src -> src.getProductClassification().getId(),
					(dst, value) -> dst.setIdProductClassification((Long) value));
		});

		// ProductClassification to ProductClassificationDTO
		modelMapper.createTypeMap(ProductClassification.class, ProductClassificationDTO.class).addMappings(
				mapper -> mapper.map(src -> src.getProduct().getId(), (dst, value) -> dst.setIdProduct((Long) value)));

		// Follow to FollowDTO
		modelMapper.createTypeMap(Follow.class, FollowDTO.class).addMappings(mapper -> {
			mapper.map(src -> src.getShop().getId(), (dst, value) -> dst.setIdShop((Long) value));
			mapper.map(src -> src.getShop().getName(), (dst, value) -> dst.setShopName((String) value));
			mapper.map(src -> src.getTime(), (dst, value) -> dst.setFollowSince((Date) value));
		});

		modelMapper.createTypeMap(CreateOrderRequest.class, Order.class).addMappings(mapper -> {
			mapper.map(src -> src.getFreeshipCoupon(), (dst, value) -> dst.setFreeshipCoupon((String) value));
			mapper.map(src -> src.getPlatformCoupon(), (dst, value) -> dst.setPlatformCoupon((String) value));
			mapper.map(src -> src.getPaymentMethod(), (dst, value) -> dst.setPaymentMethod((EPaymentMethod) value));
		});

		modelMapper.createTypeMap(ShopOrderItem.class, ShopOrder.class).addMappings(mapper -> {
			mapper.map(src -> src.getBuyerNote(), (dst, value) -> dst.setBuyerNote((String) value));
			mapper.map(src -> src.getCouponCode(), (dst, value) -> dst.setShopVoucher((String) value));
		});

		modelMapper.createTypeMap(CartDetail.class, OrderDetail.class).addMappings(mapper -> {
			mapper.map(CartDetail::getProductClassification, OrderDetail::setProductClassification);
			mapper.map(CartDetail::getQuantity, OrderDetail::setQuantity);
		});

		modelMapper.createTypeMap(CartDetail.class, CartDetailResponse.class).addMappings(mapper -> {
			mapper.map(CartDetail::getQuantity, CartDetailResponse::setQuantity);

			mapper.map(src -> src.getProductClassification().getQuantity(), CartDetailResponse::setAvailableQuantity);
			mapper.map(src -> src.getProductClassification().getId(), CartDetailResponse::setIdProductClassification);
			mapper.map(src -> src.getProductClassification().getDiscount(), CartDetailResponse::setDiscount);
			mapper.map(src -> src.getProductClassification().getStatus(), CartDetailResponse::setPcStatus);
			mapper.map(src -> src.getProductClassification().getVariationName(),
					CartDetailResponse::setNameClassification);
			mapper.map(src -> src.getProductClassification().getPrice(), CartDetailResponse::setPriceOriginal);

			mapper.map(src -> src.getProductClassification().getProduct().getName(),
					CartDetailResponse::setNameProduct);
			mapper.map(src -> src.getProductClassification().getProduct().getId(), CartDetailResponse::setIdProduct);
			mapper.map(src -> src.getProductClassification().getProduct().getStatus(), CartDetailResponse::setPStatus);
			mapper.map(src -> src.getProductClassification().getProduct().getShop().getId(),
					CartDetailResponse::setIdShop);

			mapper.using(finalPriceCvt).map(src -> src, CartDetailResponse::setPriceFinal);
			mapper.using(mediaResourceCvt).map(src -> src.getProductClassification().getProduct().getAvatar(),
					CartDetailResponse::setImageCover);
		});

		modelMapper.createTypeMap(CartDetailResponse.class, GroupCartDetailResponse.class)
				.addMappings(mapper -> mapper.map(CartDetailResponse::getIdShop, GroupCartDetailResponse::setIdShop));

		// ShopOrder to ShopOrderManageResponse
		modelMapper.createTypeMap(ShopOrder.class, ShopOrderManageResponse.class).addMappings(mapper -> {
			mapper.map(src -> src.getId(), (dst, value) -> dst.setIdOrder((Long) value));
			mapper.map(src -> src.getShop().getId(), (dst, value) -> dst.setIdShop((Long) value));
			mapper.map(src -> src.getStatus(), (dst, value) -> dst.setStatus((EOrderStatus) value));
			mapper.map(src -> src.getShipPrice(), (dst, value) -> dst.setShipPrice((Long) value));
			mapper.map(src -> src.getTotalPrice(), (dst, value) -> dst.setTotalPrice((Long) value));
			mapper.map(src -> src.getCreatedDate(), (dst, value) -> dst.setCreatedDate((Date) value));
			mapper.map(src -> src.getPrepareDate(), (dst, value) -> dst.setPrepareDate((Long) value));
			mapper.map(src -> src.getShopVoucher(), (dst, value) -> dst.setShopVoucher((String) value));
			mapper.skip(ShopOrderManageResponse::setProductResponse);
			mapper.skip(ShopOrderManageResponse::setIdUser);
			mapper.skip(ShopOrderManageResponse::setIdAddress);
			mapper.skip(ShopOrderManageResponse::setPaymentMethod);
		});

		// ShopOrder to ShopOrderManageResponse
		modelMapper.createTypeMap(Order.class, ShopOrderManageResponse.class).addMappings(mapper -> {
			mapper.map(src -> src.getBuyer().getId(), (dst, value) -> dst.setIdUser((Long) value));
			mapper.map(src -> src.getAddress().getId(), (dst, value) -> dst.setIdAddress((Long) value));
			mapper.map(src -> src.getPaymentMethod(), (dst, value) -> dst.setPaymentMethod((EPaymentMethod) value));
			mapper.skip(ShopOrderManageResponse::setIdOrder);
			mapper.skip(ShopOrderManageResponse::setIdShop);
			mapper.skip(ShopOrderManageResponse::setStatus);
			mapper.skip(ShopOrderManageResponse::setShipPrice);
			mapper.skip(ShopOrderManageResponse::setTotalPrice);
			mapper.skip(ShopOrderManageResponse::setCreatedDate);
			mapper.skip(ShopOrderManageResponse::setPrepareDate);
			mapper.skip(ShopOrderManageResponse::setShopVoucher);
			mapper.skip(ShopOrderManageResponse::setProductResponse);
		});

		// ProductClassificationDTO to ProductOfShopOrderResponse
		modelMapper.createTypeMap(ProductClassificationDTO.class, ProductOfShopOrderResponse.class)
				.addMappings(mapper -> {
					mapper.map(src -> src.getId(), (dst, value) -> dst.setId((Long) value));
					mapper.map(src -> src.getVariationName(), (dst, value) -> dst.setVariationName((String) value));
					mapper.map(src -> src.getPrice(), (dst, value) -> dst.setPrice((Long) value));
					mapper.map(src -> src.getQuantity(), (dst, value) -> dst.setQuantityInStock((Long) value));
					mapper.skip(ProductOfShopOrderResponse::setDescription);
					mapper.skip(ProductOfShopOrderResponse::setShopCategory);
					mapper.skip(ProductOfShopOrderResponse::setProductCategory);
					mapper.skip(ProductOfShopOrderResponse::setProductType);
					mapper.skip(ProductOfShopOrderResponse::setQuantityBuy);
					mapper.skip(ProductOfShopOrderResponse::setMediaId);
				});

		// Product to ProductOfShopOrderResponse
		modelMapper.createTypeMap(Product.class, ProductOfShopOrderResponse.class).addMappings(mapper -> {
			mapper.map(src -> src.getDescription(), (dst, value) -> dst.setDescription((String) value));
			mapper.map(src -> src.getShopCategory().getName(), (dst, value) -> dst.setShopCategory((String) value));
			mapper.map(src -> src.getProductCategory().getName(),
					(dst, value) -> dst.setProductCategory((String) value));
			mapper.map(src -> src.getProductType().getName(), (dst, value) -> dst.setProductType((String) value));
			mapper.map(src -> src.getAvatar().getId(), (dst, value) -> dst.setMediaId((Long) value));
			mapper.skip(ProductOfShopOrderResponse::setId);
			mapper.skip(ProductOfShopOrderResponse::setVariationName);
			mapper.skip(ProductOfShopOrderResponse::setPrice);
			mapper.skip(ProductOfShopOrderResponse::setQuantityInStock);
			mapper.skip(ProductOfShopOrderResponse::setQuantityBuy);
		});

		// Notifications to NotificationDTO
		modelMapper.createTypeMap(Notification.class, NotificationsDTO.class).addMappings(mapper -> {
			mapper.map(src -> src.getBuyer().getId(), (dst, value) -> dst.setUserId((Long) value));
			mapper.map(src -> src.getImage().getUrl(), (dst, value) -> dst.setImage((String) value));

		});

		modelMapper.createTypeMap(FavoriteBuyer.class, FavoriteDTO.class).addMappings(mapper -> {
			mapper.map(FavoriteBuyer::getProduct, FavoriteDTO::setProduct);
		});
		
		modelMapper.createTypeMap(Buyer.class, BuyerDTO.class).addMappings(mapper -> {
			mapper.map(src -> src.getDeliveryAddresses(), (dst, value) -> dst.setNumDeliveryAddress((Long)value));
		});

		return modelMapper;
	}
}
