package com.shopee.backend.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.shopee.backend.model.ShopOrder;
import com.shopee.backend.repository.custom.ShopOrderRepositoryCustom;

public interface ShopOrderRepository extends JpaRepository<ShopOrder, Long>, ShopOrderRepositoryCustom {
	@Query(value = "SELECT * FROM shop_orders WHERE id_shop = :idShop AND createdate BETWEEN :from AND :to ORDER BY id DESC; ", nativeQuery = true)
	public List<ShopOrder> findByIdShop(@Param("idShop") Long idShop, @Param("from") String from, @Param("to") String to);

	@Query(value = "SELECT * FROM shop_orders WHERE id_shop = :idShop AND status = :status AND createdate BETWEEN :from AND :to ORDER BY id DESC ", nativeQuery = true)
	public List<ShopOrder> findByIdShopAndStatus(@Param("idShop") Long idShop, @Param("status") int status,
			@Param("from") String from, @Param("to") String to);

	@Query(value = "SELECT * FROM shop_orders WHERE id_shop = :idShop AND id = :id ", nativeQuery = true)
	public ShopOrder findByShopAndId(@Param("idShop") Long idShop, @Param("id") Long id);

	@Query(value = "SELECT * FROM shop_orders where id_order = :id_order", nativeQuery = true)
	public List<ShopOrder> getShopOrderByIdOrder(@Param("id_order") Long idOrder);

	@Query(value = "select shop_orders.id,shop_orders.createdby, shop_orders.createdate, shop_orders.modifiedby, shop_orders.modifieddate,shop_orders.buyer_note, "
			+ "shop_orders.complete,shop_orders.original_price,shop_orders.platform_price_off, shop_orders.prepare_date, shop_orders.price_off, "
			+ "shop_orders.ship_price,shop_orders.ship_price_off,shop_orders.shop_price_off,shop_orders.shop_voucher,shop_orders.status,shop_orders.total_pay_price, "
			+ "shop_orders.total_price,shop_orders.id_order, shop_orders.id_shop "
			+ "from (orders join buyer on orders.id_buyer = buyer.id) "
			+ "join shop_orders on orders.id = shop_orders.id_order where buyer.id = :id_buyer and shop_orders.status = :id_status", nativeQuery = true)
	public List<ShopOrder> getShopOrderByStatus(@Param("id_buyer") Long idOrder, @Param("id_status") Long idStatus);

	@Modifying
	@Transactional
	@Query(value = "UPDATE product_classification SET quantity = :quantity WHERE id = :id", nativeQuery = true)
	public void updateProductQuantity(@Param("id") Long id, @Param("quantity") Long quantity);

	@Modifying
	@Transactional
	@Query(value = "UPDATE product SET sales = :sale WHERE id = :id", nativeQuery = true)
	public void updateProductSale(@Param("id") Long id, @Param("sale") Long sale);
	
	@Modifying
	@Transactional
	@Query(value = "select shop_orders.id,shop_orders.createdby, shop_orders.createdate, shop_orders.modifiedby, shop_orders.modifieddate,shop_orders.buyer_note, "
			+ "shop_orders.complete,shop_orders.original_price,shop_orders.platform_price_off, shop_orders.prepare_date, shop_orders.price_off, "
			+ "shop_orders.ship_price,shop_orders.ship_price_off,shop_orders.shop_price_off,shop_orders.shop_voucher,shop_orders.status,shop_orders.total_pay_price, "
			+ "shop_orders.total_price,shop_orders.id_order, shop_orders.id_shop "
			+ "from (((((orders join buyer on orders.id_buyer = buyer.id)"
			+ "join shop_orders on orders.id = shop_orders.id_order)"
			+ "join shop on shop.id = shop_orders.id_shop) "
			+ "join order_detail on order_detail.id_shop_order = shop_orders.id) "
			+ "join product_classification on product_classification.id = order_detail.id_product_classification) "
			+ "join product on product.id = product_classification.id_product "
			+ "where buyer.id = :id_buyer and (product.name like %:keyword% or shop.shop_name like %:keyword% or shop_orders.id like %:keyword%) "
			+ "group by shop_orders.id", nativeQuery = true)
	public List<ShopOrder> search(@Param("id_buyer") Long idBuyer, @Param("keyword") String keyword);
	
	@Modifying
	@Transactional
	@Query(value="select shop_orders.id,shop_orders.createdby, shop_orders.createdate, shop_orders.modifiedby, shop_orders.modifieddate,shop_orders.buyer_note, "
			+ "shop_orders.complete,shop_orders.original_price,shop_orders.platform_price_off, shop_orders.prepare_date, shop_orders.price_off, "
			+ "shop_orders.ship_price,shop_orders.ship_price_off,shop_orders.shop_price_off,shop_orders.shop_voucher,shop_orders.status,shop_orders.total_pay_price, "
			+ "shop_orders.total_price,shop_orders.id_order, shop_orders.id_shop "
			+ "from (orders join buyer on orders.id_buyer = buyer.id) "
			+ "join shop_orders on orders.id = shop_orders.id_order where buyer.id = :id_buyer", nativeQuery = true)
	public List<ShopOrder> getShopOrdersByBuyer(@Param("id_buyer") Long idBuyer);

}
