package com.shopee.backend.controller.payment.momo;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shopee.backend.service.impl.PaymentService;
import com.shopee.backend.service.impl.onlinepayment.MomoService;

import lombok.Getter;
import lombok.Setter;

@RestController
@RequestMapping("${momo.url.notify}")
public class MomoNotifyController {
	@Autowired
	PaymentService paymentService;
	
	@Autowired
	MomoService momoService;
	
	@PostMapping
	public void notifyReciever(HttpServletRequest req, HttpServletResponse resp) {
		Map<String, Object> m = null;
		try {
			String body = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
			m = new ObjectMapper().readValue(body, HashMap.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		int resultCode = -1;
		
		try {
			resultCode = (int)m.get("resultCode");
		} catch (Exception e) {
		}
		
		if (resultCode == 9000) {
			String momoOrderId = m.get("orderId").toString();
			String amount = m.get("amount").toString();
			
			if (StringUtils.isNotBlank(momoOrderId)) {
				DecodedId id = decode(momoOrderId);
				
				boolean success = false;
				if (id.type.equals("order")) {
					success = paymentService.confirmOrderStatus(id.id);
				}
				else if (id.type.equals("shoporder")) {
					success = paymentService.confirmShopOrderStatus(id.id);
				}
				
				momoService.confirmPayment(momoOrderId, amount, success);
			}
		}
		resp.setStatus(204);
	}
	
	private DecodedId decode(String encodedId) {
		int i1 = encodedId.indexOf("_");
		int i2 = encodedId.lastIndexOf("_");
		return new DecodedId(
				Long.valueOf(encodedId.substring(0, i1)), 
				encodedId.substring(i1 + 1, i2)
			);
	}
	
	@Getter @Setter
	protected class DecodedId {
		Long id;
		String type;
		public DecodedId(Long id, String type) {
			this.id = id;
			this.type = type;
		}
	}
}
