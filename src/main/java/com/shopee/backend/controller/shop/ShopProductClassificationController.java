package com.shopee.backend.controller.shop;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.payload.request.product.CreateProductVariationRequest;
import com.shopee.backend.payload.request.product.EditProductVariationRequest;
import com.shopee.backend.payload.response.BaseResponse;
import com.shopee.backend.service.IProductClassificationService;
import com.shopee.backend.service.auth.ShopDetails;

@RestController
@RequestMapping("/api/shop/product/classification")
public class ShopProductClassificationController {
	@Autowired
	IProductClassificationService productClassificationService;
	
	@GetMapping("/p")
	public ResponseEntity<?> getByProduct(@AuthenticationPrincipal ShopDetails shop,
			@RequestParam("idProduct")Long idProduct)
	{
		return ResponseEntity.ok(productClassificationService.getByProductId(idProduct, shop.getId()));
	}
	@GetMapping
	public ResponseEntity<?> get(
			@AuthenticationPrincipal ShopDetails shop,
			@RequestParam Long id) {
		return ResponseEntity.ok(productClassificationService.getById(id, shop.getId()));
	}
	
	@PostMapping
	public ResponseEntity<?> create(
			@AuthenticationPrincipal ShopDetails shop,
			@RequestBody @Valid CreateProductVariationRequest request) {
		return ResponseEntity.ok(productClassificationService.create(shop.getId(), request));
	}
	
	@PutMapping
	public ResponseEntity<?> update(
			@AuthenticationPrincipal ShopDetails shop,
			@RequestBody @Valid EditProductVariationRequest request) {
		return ResponseEntity.ok(productClassificationService.update(shop.getId(), request));
	}
	
	@DeleteMapping
	public ResponseEntity<?> remove(
			@AuthenticationPrincipal ShopDetails shop,
			@RequestParam Long id) {
		productClassificationService.delete(id, shop.getId());
		return ResponseEntity.ok(new BaseResponse());
	}
}
