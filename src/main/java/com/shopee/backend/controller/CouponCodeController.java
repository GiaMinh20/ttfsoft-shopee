package com.shopee.backend.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.dto.CouponCodeDTO;
import com.shopee.backend.model.Shop;
import com.shopee.backend.service.ICouponCodeService;

@RestController
@RequestMapping("/api")
public class CouponCodeController {
	@Autowired
	private ICouponCodeService couponCodeService;
	@Autowired
	private ModelMapper modelMapper;
	
	@GetMapping("/coupon_code")
	public ResponseEntity<?> getAllCouponCodes(){
		return ResponseEntity.ok(couponCodeService.getAllCouponCodes());
	}
	
	@GetMapping("/coupon_code/{id}")
	public ResponseEntity<?> getCouponCodeById(@PathVariable("id") long id){
		return ResponseEntity.ok(couponCodeService.getCouponCodeById(id));
	}
	
	@PostMapping({"/admin/coupon_code/insert", "/shop/coupon_code/insert"})
	public ResponseEntity<?> createCouponCode(@RequestBody CouponCodeDTO couponCode) {
		return ResponseEntity.ok(couponCodeService.createCouponCode(couponCode));
	}
	
	@GetMapping("/coupon_code/shop/{id}")
	public ResponseEntity<?> getAllCouponCodesByIdShop(@PathVariable("id") Long id){
		return ResponseEntity.ok(couponCodeService.getAllCouponCodesByIdShop(id));
	}
	
	@GetMapping("/coupon_code/shopee")
	public ResponseEntity<?> getAllCouponCodesShopee(){
		return ResponseEntity.ok(couponCodeService.getAllCouponCodesShopee());
	}
	
	@PutMapping({"/admin/coupon_code/{id}", "/shop/coupon_code/edit/{id}"})
	public ResponseEntity<?> editCouponCode(@RequestBody CouponCodeDTO couponCodeDTO, @PathVariable Long id){
		return ResponseEntity.ok(couponCodeService.editCouponCode(couponCodeDTO, id));
	}
	
//	@DeleteMapping({"/admin/coupon_code/{id}", "/shop/coupon_code/{id}"})
//	public ResponseEntity<?> detleteCouponCode(@PathVariable Long id){
//		return ResponseEntity.ok(couponCodeService.detleteCouponCode(id));
//	}
	
	@PostMapping("/buyer/coupon_code/add/{id}")
	public ResponseEntity<?> addToList(@PathVariable Long id){
		return ResponseEntity.ok(couponCodeService.addToList(id));
	}
	
	@GetMapping("/buyer/coupon_code")
	public ResponseEntity<?> getAllCouponByIdBuyer(){
		return ResponseEntity.ok(couponCodeService.getAllCouponsByIdBuyer());
	}
	
	@GetMapping("/shop/coupon_code")
	public ResponseEntity<?> listCouponCodesOfShop(){
		Shop shop = modelMapper.map(SecurityContextHolder.getContext().getAuthentication().getPrincipal(), Shop.class);
		return ResponseEntity.ok(couponCodeService.getAllCouponCodesByIdShop(shop.getId()));
	}
	
	@GetMapping("/admin/coupon_code")
	public ResponseEntity<?> listCouponCodesShopee(){
		return ResponseEntity.ok(couponCodeService.getAllCouponCodesShopee());
	}
	
}
