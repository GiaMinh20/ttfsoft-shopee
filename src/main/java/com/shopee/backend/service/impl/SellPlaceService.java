package com.shopee.backend.service.impl;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.dto.SellPlaceDTO;
import com.shopee.backend.model.SellPlace;
import com.shopee.backend.payload.response.ResponseData;
import com.shopee.backend.repository.SellPlaceRepository;
import com.shopee.backend.service.ISellPlace;

@Service
public class SellPlaceService implements ISellPlace {

	@Autowired
	SellPlaceRepository sellPlaceRepository;
	@Autowired
	ModelMapper modelMapper;

	@PersistenceContext
	private EntityManager entityManager;

	@Value("${sellplace.notfound}")
	private String notfound;

	@Value("${sellplace.inuse}")
	private String inuse;

	@Value("${sellplace.invalidinput}")
	private String invalidinput;

	@Value("${sellplace.updatesuccess}")
	private String updatesuccess;

	@Value("${product.sellplaceinvalid}")
	private String sellplaceinvalid;

	@Value("${app.success}")
	private String success;

	@Value("${sellplace.isusedcode}")
	private String codeused;

	@Override
	public ResponseData<SellPlaceDTO> getAllPlaces() {
		ResponseData<SellPlaceDTO> res = new ResponseData<>();
		List<SellPlaceDTO> lst = new ArrayList<>();
		for (SellPlace s : sellPlaceRepository.findAll())
			lst.add(modelMapper.map(s, SellPlaceDTO.class));
		res.setDatas(lst);
		res.setSuccess(true);
		return res;
	}

	@Override
	public ResponseData<SellPlaceDTO> getPlaceByCode(String code) {
		ResponseData<SellPlaceDTO> res = new ResponseData<>();
		Optional<SellPlace> sellplace = sellPlaceRepository.findByCode(code);
		if (sellplace.isEmpty()) {
			res.setSuccess(false);
			res.setMessage(String.format(sellplaceinvalid, code));
		} else {
			res.setData(modelMapper.map(sellplace.get(), SellPlaceDTO.class));
			res.setSuccess(true);
		}

		return res;
	}

	@Override
	public ResponseData<SellPlaceDTO> create(SellPlaceDTO input) throws SQLIntegrityConstraintViolationException {
		ResponseData<SellPlaceDTO> res = new ResponseData<>();

		Optional<SellPlace> find = sellPlaceRepository.findByCode(input.getCode());
		if (find.isPresent())
			throw new CommonRuntimeException(codeused + input.getCode());
		SellPlace sp = modelMapper.map(input, SellPlace.class);
		SellPlaceDTO ret;
		ret = modelMapper.map(sellPlaceRepository.save(sp), SellPlaceDTO.class);
		res.setSuccess(true);
		res.setMessage(success);
		res.setData(ret);
		return res;

	}

	@Override
	public ResponseData<SellPlaceDTO> update(Long id, SellPlaceDTO input)
			throws SQLIntegrityConstraintViolationException {
		ResponseData<SellPlaceDTO> res = new ResponseData<>();
		if (input.getName() == null || input.getCode() == null || input.getName().equals("")
				|| input.getCode().equals("")) {
			res.setMessage(invalidinput);
			res.setSuccess(false);
			return res;
		}

		Optional<SellPlace> sellplace = sellPlaceRepository.findById(id);
		if (sellplace.isEmpty()) {
			res.setMessage(notfound);
			res.setSuccess(false);
			return res;
		}
		SellPlace sp = sellplace.get();
		sellplace = sellPlaceRepository.findByCode(input.getCode());
		if (sellplace.isPresent() && !sellplace.get().getId().equals(sp.getId()))
			throw new CommonRuntimeException(codeused + input.getCode());

		sp.setCode(input.getCode());
		sp.setName(input.getName());
		SellPlaceDTO ret = modelMapper.map(sellPlaceRepository.save(sp), SellPlaceDTO.class);
		res.setSuccess(true);
		res.setMessage(updatesuccess);
		res.setData(ret);
		return res;
	}

	@Override
	public void delete(Long[] ids) {
		try {
			deleteRunner(ids);
		} catch (DataIntegrityViolationException ex) {
			throw new CommonRuntimeException(inuse);

		} catch (EmptyResultDataAccessException ex) {
			throw new CommonRuntimeException(notfound);
		}
	}

	@Transactional
	public void deleteRunner(Long[] ids) {
		for (Long id : ids) {
			sellPlaceRepository.deleteById(id);
		}

	}
}
