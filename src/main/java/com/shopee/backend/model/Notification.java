package com.shopee.backend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "notification")
public class Notification {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_buyer")
	private Buyer buyer;

	@OneToOne
	@JoinColumn(name = "id_image")
	private MediaResource image;

	@NotBlank
	private String title;

	@Lob
	@Column(length = 65535)
	@NotBlank
	private String content;

	@NotNull
	private Boolean seen;

		public Notification(Buyer buyer, MediaResource image, @NotBlank String title, @NotBlank String content,
			@NotNull Boolean seen) {
		super();
		this.buyer = buyer;
		this.image = image;
		this.title = title;
		this.content = content;
		this.seen = seen;
	}
}
