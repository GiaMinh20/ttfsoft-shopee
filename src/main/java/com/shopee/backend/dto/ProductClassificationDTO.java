package com.shopee.backend.dto;

import com.shopee.backend.utility.datatype.EProductClassificationStatus;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class ProductClassificationDTO {
	private Long id;
	private Long idProduct;
	private String variationName;
	private Long price;
	private Long quantity;
	private Double discount;
	private EProductClassificationStatus status;
}
