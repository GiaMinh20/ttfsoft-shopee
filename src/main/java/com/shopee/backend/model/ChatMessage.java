package com.shopee.backend.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@Entity
@Table(name = "chat_message")
public class ChatMessage {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_buyer")
	private Buyer buyer;

	@ManyToOne
	@JoinColumn(name = "id_shop")
	private Shop shop;
	
	@OneToOne
	@JoinColumn(name = "id_image")
	private MediaResource image;

	@Lob
	@Column(length = 65535)
	@NotBlank
	private String content;

	@NotNull
	private Boolean isBuyerSent;

	@NotNull
	private Boolean seen;

	private Date time;

	public ChatMessage(Buyer buyer, Shop shop, MediaResource image, @NotBlank String content,
			@NotNull Boolean isBuyerSent, @NotNull Boolean seen, Date time) {
		super();
		this.buyer = buyer;
		this.shop = shop;
		this.image = image;
		this.content = content;
		this.isBuyerSent = isBuyerSent;
		this.seen = seen;
		this.time = time;
	}

}
