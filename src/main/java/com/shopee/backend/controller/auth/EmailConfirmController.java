package com.shopee.backend.controller.auth;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.payload.request.auth.EmailConfirmRequest;
import com.shopee.backend.service.IEmailConfirmService;
import com.shopee.backend.service.auth.BuyerDetails;
import com.shopee.backend.service.auth.ShopDetails;
import com.shopee.backend.utility.RolePrefix;

@RestController
@RequestMapping("/api")
public class EmailConfirmController {
	@Autowired
	IEmailConfirmService emailConfirmService;
	
	@PostMapping("/shop/sendemailconfirmcode")
	public ResponseEntity<?> shopSendEmailConfirmToken(@AuthenticationPrincipal ShopDetails shopdetails) {
		return ResponseEntity.ok(emailConfirmService.sendVerifyCode(shopdetails.getId(), RolePrefix.SHOP));
	}
	
	@PostMapping("/buyer/sendemailconfirmcode")
	public ResponseEntity<?> buyerSendEmailConfirmToken(@AuthenticationPrincipal BuyerDetails buyerDetails) {
		return ResponseEntity.ok(emailConfirmService.sendVerifyCode(buyerDetails.getId(), RolePrefix.BUYER));
	}
	
	@PostMapping("/shop/emailconfirm")
	public ResponseEntity<?> shopEmailConfirm(
			@AuthenticationPrincipal ShopDetails shopdetails,
			@RequestBody @Valid EmailConfirmRequest request) {
		return ResponseEntity.ok(emailConfirmService.confirmEmailConfirmToken(shopdetails.getId(), request, RolePrefix.SHOP));
	}
	
	@PostMapping("/buyer/emailconfirm")
	public ResponseEntity<?> buyerEmailConfirm(
			@AuthenticationPrincipal BuyerDetails buyerDetails,
			@RequestBody @Valid EmailConfirmRequest request) {
		return ResponseEntity.ok(emailConfirmService.confirmEmailConfirmToken(buyerDetails.getId(), request, RolePrefix.BUYER));
	}
}
