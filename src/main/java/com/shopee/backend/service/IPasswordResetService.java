package com.shopee.backend.service;

import com.shopee.backend.payload.request.auth.PasswordResetRequest;
import com.shopee.backend.payload.request.auth.SendVerifyTokenRequest;
import com.shopee.backend.payload.response.BaseResponse;

public interface IPasswordResetService {

	BaseResponse sendVerifyCode(String role, SendVerifyTokenRequest request);

	BaseResponse reset(String role, PasswordResetRequest request);

}