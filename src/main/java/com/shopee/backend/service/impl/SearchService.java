package com.shopee.backend.service.impl;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.shopee.backend.dto.ProductGeneralDTO;
import com.shopee.backend.model.Product;
import com.shopee.backend.payload.response.ListWithPagingResponse;
import com.shopee.backend.repository.SearchRepository;
import com.shopee.backend.service.ISearchService;
import com.shopee.backend.utility.Page;

@Service
public class SearchService implements ISearchService {

	@Autowired
	SearchRepository searchRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Value("${product.notfound}")
	private String notfound;

	@Override
	public ListWithPagingResponse<ProductGeneralDTO> searchProduct(String keyword, Page page, String locations,
			Long idCategory, Long idType, Long maxPrice, Long minPrice, Double rate, Boolean isNew) {
		List<Product> entities = searchRepository.getSearchProduct(keyword, page, locations, idCategory, idType,
				maxPrice, minPrice, rate, isNew);
		return listWithPagingResponseMakeup(entities, page);
	}

	@Override
	public Long countWithFilter(String keyword, Page page, String locations, Long idCategory, Long idType,
			Long maxPrice, Long minPrice, Double rate, Boolean isNew) {
		return searchRepository.getCountSearchProduct(keyword, page, locations, idCategory, idType, maxPrice, minPrice,
				rate, isNew);
	}

	private ListWithPagingResponse<ProductGeneralDTO> listWithPagingResponseMakeup(List<Product> products, Page page) {
		if (products.isEmpty())
			throw new EntityNotFoundException(notfound);
		return new ListWithPagingResponse<>(page.getPageNumber() + 1, page.getTotalPage(),
				products.stream().map(p -> modelMapper.map(p, ProductGeneralDTO.class)).toList());
	}
}
