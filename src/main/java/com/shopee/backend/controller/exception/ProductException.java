package com.shopee.backend.controller.exception;

public class ProductException extends RuntimeException{

    public ProductException(String message) {
        super(message);
    }
    
}
