package com.shopee.backend.controller;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.service.IProductTypeService;
import com.shopee.backend.utility.Page;

@RestController
@RequestMapping("/api")
public class ProductTypeController {
	@Autowired
	private IProductTypeService productTypeService;

	@GetMapping("/producttype")
	public ResponseEntity<?> getProductTypes(@RequestParam(value = "searchName", required = false) String searchName,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "limit", required = false) Integer limit) {
		if (StringUtils.isNotBlank(searchName)) {
			int count = productTypeService.getCountProductTypeByName(searchName).intValue();
			return ResponseEntity.ok(productTypeService.findByName(searchName, new Page(page, limit, count)));
		}
		return ResponseEntity.ok(productTypeService.getAllActive());

	}

	@GetMapping("/producttype/group/{parent}")
	public ResponseEntity<?> getProductTypeByParent(@PathVariable("parent") @Valid String parent) {
		return ResponseEntity.ok(productTypeService.getByParent(parent));
	}

	@GetMapping("/producttype/{id}")
	public ResponseEntity<?> getProductTypeById(@PathVariable("id") long productTypeId) {
		return ResponseEntity.ok(productTypeService.getById(productTypeId));
	}

	@GetMapping("/producttype/parent")
	public ResponseEntity<?> getParent(@RequestParam("id") Long id) {
		return ResponseEntity.ok(productTypeService.getParent(id));
	}
}
