package com.shopee.backend.controller.shop;

import java.io.IOException;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.model.Shop;
import com.shopee.backend.payload.request.product.CreateProductRequest;
import com.shopee.backend.payload.request.product.EditProductRequest;
import com.shopee.backend.payload.response.BaseResponse;
import com.shopee.backend.service.IProductService;
import com.shopee.backend.service.auth.ShopDetails;
import com.shopee.backend.utility.datatype.EProductStatus;
import com.shopee.backend.utility.datatype.EUserRole;
import com.shopee.backend.utility.sort.ModelSorting;

@RestController
@RequestMapping("/api/shop/product")
public class ShopProductController {
	@Autowired
	IProductService productService;
	
	@GetMapping
	public ResponseEntity<?> get(
			@AuthenticationPrincipal ShopDetails shop,
			@RequestParam(required = false) Long idCategory,
			@RequestParam(required = false) Long idType,
			@RequestParam(required = false) Long idShopCategory,
			@RequestParam(required = false) Long idSellPlace,
			@RequestParam(required = false) String searchName,
			@RequestParam(required = false) EProductStatus status,
			@RequestParam(required = false) Boolean isNew,
			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer size,
			@RequestParam(required = false) Integer sortBy,
			@RequestParam(required = false) Boolean sortDescending) {
		
		return ResponseEntity.ok(
				productService.getWithFilter(
					shop.getId(), 
					idCategory, 
					idType, 
					idShopCategory, 
					idSellPlace, 
					searchName, 
					status, 
					isNew, 
					page, 
					size, 
					ModelSorting.getProductSort(sortBy, sortDescending)
				)
			);
	}

	@GetMapping({"{id}"})
	public ResponseEntity<?> getProductById(@AuthenticationPrincipal ShopDetails shop, @PathVariable("id") long id) {
		return ResponseEntity.ok(productService.getById(id, EUserRole.SHOP, shop.getId()));
	}

	@PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> create(
			@AuthenticationPrincipal ShopDetails shop,
			@RequestPart("info") @Valid CreateProductRequest request,
			@RequestPart("avatar") @Valid @NotEmpty MultipartFile avatar,
			@RequestPart("images") @Valid @NotEmpty List<MultipartFile> images) throws IOException {
		
		 if (!shop.getEmailConfirmed())
	            throw new CommonRuntimeException("You need confirm email to access this feature!");
		
		return ResponseEntity.ok(
				productService.create(
					shop.getId(),
					request,
					avatar,
					images)
				);
	}
	
	@PutMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> update(
			@AuthenticationPrincipal ShopDetails shop,
			@RequestPart("info") @Valid EditProductRequest request,
			@RequestPart(name = "avatar", required = false) @Valid @NotEmpty MultipartFile avatar) throws IOException {
		
		return ResponseEntity.ok(productService.update(shop.getId(), request, avatar));
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(
			@AuthenticationPrincipal ShopDetails shop, 
			@PathVariable("id") Long id) {
		productService.delete(id, shop.getId());
		return ResponseEntity.ok(new BaseResponse());
	}
}