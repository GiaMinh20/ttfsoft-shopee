package com.shopee.backend.service.impl;

import java.util.Base64;
import java.util.Date;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopee.backend.config.websocket.InboundChatMessage;
import com.shopee.backend.dto.ChatMessageDTO;
import com.shopee.backend.model.Buyer;
import com.shopee.backend.model.ChatMessage;
import com.shopee.backend.model.MediaResource;
import com.shopee.backend.payload.response.ListWithPagingResponse;
import com.shopee.backend.repository.BuyerRepository;
import com.shopee.backend.repository.ChatMessageRepository;
import com.shopee.backend.repository.ShopRepository;
import com.shopee.backend.utility.Page;
import com.shopee.backend.utility.RolePrefix;
import com.shopee.backend.utility.datatype.EMessageContentType;
import com.shopee.backend.utility.mapper.ResponseMapper;

@Service
public class ChatService {
	@Autowired
	ChatMessageRepository chatMessageRepository;
	
	@Autowired
	BuyerRepository buyerRepository;
	
	@Autowired
	ShopRepository shopRepository;
	
	@Autowired
	MediaResourceService mediaResourceService;
	
	@Autowired
	ModelMapper modelMapper;
	
	public ListWithPagingResponse<ChatMessageDTO> fetchMessage(Long idBuyer, Long idShop, Integer ipage) {
		if (ipage == null) {
			ipage = 1;
		}

		Page page = new Page(
				ipage, 
				10, 
				chatMessageRepository.count(idBuyer, idShop).intValue());
		
		return ResponseMapper.listWithPagingResponseMakeup(
				chatMessageRepository.get(idBuyer, idShop, page), 
				page, 
				ChatMessageDTO.class, 
				modelMapper
			);
	}
	
	public void saveMessage(InboundChatMessage chatMessage, Long idUserSent, boolean isBuyerSent) {
		MediaResource img = null;
		String messageContent = null;
		if (chatMessage.getContentType() == EMessageContentType.IMAGE) {
			byte[] data = Base64.getDecoder().decode(chatMessage.getData());
			img = mediaResourceService.save(data);
		}
		else {
			messageContent = chatMessage.getData();
		}
		
		chatMessageRepository.save(
				new ChatMessage(
					isBuyerSent ? buyerRepository.getReferenceById(idUserSent) : buyerRepository.getReferenceById(chatMessage.getIdReceiver()), 
					isBuyerSent ? shopRepository.getReferenceById(chatMessage.getIdReceiver()) : shopRepository.getReferenceById(idUserSent), 
					img,
					messageContent, 
					isBuyerSent, 
					false, 
					new Date()
				)
			);
	}
	
	public String getReceiverUsername(Long idUser, boolean isBuyer) {
		if (isBuyer) {
			Buyer buyer = buyerRepository.findById(idUser).get();
			return RolePrefix.BUYER + buyer.getUsername();
		}
		else {
			return RolePrefix.SHOP + shopRepository.findById(idUser).get().getUsername();
		}
	}
}
