package com.shopee.backend.service.auth.websocket;

import javax.transaction.Transactional;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.shopee.backend.model.Buyer;
import com.shopee.backend.model.Shop;
import com.shopee.backend.repository.BuyerRepository;
import com.shopee.backend.repository.ShopRepository;

public class WebSocketUserDetailsServiceImpl implements UserDetailsService {
	BuyerRepository buyerRepository;
	ShopRepository shopRepository;

	public WebSocketUserDetailsServiceImpl(BuyerRepository buyerRepository, ShopRepository shopRepository) {
		super();
		this.buyerRepository = buyerRepository;
		this.shopRepository = shopRepository;
	}

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String usernameWithRolePrefix) throws UsernameNotFoundException {
		String role = usernameWithRolePrefix.substring(0, usernameWithRolePrefix.indexOf('@'));
		String username = usernameWithRolePrefix.substring(usernameWithRolePrefix.indexOf('@') + 1, usernameWithRolePrefix.length());
		
		switch (role) {
		case "buyer":
			Buyer buyer = buyerRepository.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException(String.format("%s user not found with username: %s", role, username)));
			return WebSocketBuyerDetails.build(buyer);
		case "shop":
			Shop shop = shopRepository.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException(String.format("%s user not found with username: %s", role, username)));
			return WebSocketShopDetails.build(shop);
		default:
			throw new UsernameNotFoundException(String.format("Invalid role prefix: %s", role));
		}
	}
}
