package com.shopee.backend.utility.onlinepayment;

import org.springframework.web.client.RestTemplate;

public class ExecuteRequest {
	public static String execute(String url, Object payload) {
		return new RestTemplate().postForEntity(url, payload, String.class).getBody();
	}
}
