package com.shopee.backend.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import com.shopee.backend.model.pk.OrderDetailPK;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "order_detail")
public class OrderDetail {
	@EmbeddedId
	private OrderDetailPK id;

	@ManyToOne
	@MapsId("idShopOrder")
	@JoinColumn(name = "id_shop_order", insertable = false, updatable = false)
	private ShopOrder shopOrder;

	@ManyToOne
	@MapsId("idProductClassification")
	@JoinColumn(name = "id_product_classification", insertable = false, updatable = false)
	private ProductClassification productClassification;

	private Long quantity;

	@Column(name = "unit_price")
	private Long unitPrice;
}