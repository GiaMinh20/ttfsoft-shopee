package com.shopee.backend.service.impl;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.shopee.backend.dto.BuyerDTO;
import com.shopee.backend.dto.ShopDTO;
import com.shopee.backend.model.Buyer;
import com.shopee.backend.model.Shop;
import com.shopee.backend.payload.response.BaseResponse;
import com.shopee.backend.repository.BuyerRepository;
import com.shopee.backend.repository.ShopRepository;
import com.shopee.backend.service.IAdminAccountManagement;

@Service
public class AdminAccountManagement implements IAdminAccountManagement {

	@Autowired
	BuyerRepository buyerRepo;

	@Autowired
	ShopRepository shopRepo;
	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	ModelMapper modelMapper;

	@Value("${user.notfoundwithemail}")
	private String notfoundwithemail;

	@Value("${user.notfoundwithid}")
	private String notfoundwithid;

	@Value("${user.updatepasswordsuccess}")
	private String updatepasswordsuccess;

	@Value("${user.lock}")
	private String lockmessage;
	@Value("${user.unlock}")
	private String unlockmessage;

	@Override
	public BuyerDTO getBuyerByEmail(String email) {
		Optional<Buyer> opt = buyerRepo.findByEmail(email);
		if (opt.isEmpty())
			throw new EntityNotFoundException(notfoundwithemail + email);
		return modelMapper.map(opt.get(), BuyerDTO.class);
	}

	@Override
	public ShopDTO getShopByEmail(String email) {
		Optional<Shop> opt = shopRepo.findByEmail(email);
		if (opt.isEmpty())
			throw new EntityNotFoundException(notfoundwithemail + email);
		return modelMapper.map(opt.get(), ShopDTO.class);
	}

	@Override
	public BaseResponse updateBuyerPassword(Long idBuyer, String newPassword) {
		Buyer buyer = buyerRepo.getReferenceById(idBuyer);
		buyer.setPassword(passwordEncoder.encode(newPassword));
		buyerRepo.save(buyer);
		return new BaseResponse(true, updatepasswordsuccess);
	}

	@Override
	public BaseResponse updateShopPassword(Long idShop, String newPassword) {
		Shop shop = shopRepo.getReferenceById(idShop);
		shop.setPassword(passwordEncoder.encode(newPassword));
		shopRepo.save(shop);
		return new BaseResponse(true, updatepasswordsuccess);
	}

	@Override
	public BaseResponse unlockBuyerAccount(Long idBuyer, Boolean unlock) {
		Buyer buyer = buyerRepo.getReferenceById(idBuyer);
		buyer.setIsActive(unlock);
		buyerRepo.save(buyer);
		String message = unlock ? unlockmessage : lockmessage;
		return new BaseResponse(true, message);
	}

	@Override
	public BaseResponse unlockShopAccount(Long idShop, Boolean unlock) {
		Shop shop = shopRepo.getReferenceById(idShop);
		shop.setIsActive(unlock);
		shopRepo.save(shop);
		String message = unlock ? unlockmessage : lockmessage;
		return new BaseResponse(true, message);
	}
}
