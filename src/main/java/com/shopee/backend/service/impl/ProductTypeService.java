package com.shopee.backend.service.impl;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.dto.ProductTypeDTO;
import com.shopee.backend.model.MediaResource;
import com.shopee.backend.model.ProductType;
import com.shopee.backend.payload.request.producttype.CreateProductTypeRequest;
import com.shopee.backend.payload.request.producttype.EditProductTypeRequest;
import com.shopee.backend.payload.response.BaseResponse;
import com.shopee.backend.payload.response.DataResponse;
import com.shopee.backend.payload.response.ListResponse;
import com.shopee.backend.payload.response.ListWithPagingResponse;
import com.shopee.backend.repository.ProductTypeRepository;
import com.shopee.backend.service.IMediaResourceService;
import com.shopee.backend.service.IProductTypeService;
import com.shopee.backend.utility.Page;

@Service
public class ProductTypeService implements IProductTypeService {

	@Autowired
	private ProductTypeRepository repository;
	@Autowired
	IMediaResourceService mediaResourceService;
	@Autowired
	private ModelMapper modelMapper;

	@Value("${producttype.notfound}")
	private String notfound;
	@Value("${producttype.notfoundwithid}")
	private String notfoundwithid;
	@Value("${producttype.notfoundwithname}")
	private String notfoundwithname;
	@Value("${producttype.uploadimagefail}")
	private String uploadimagefail;
	@Value("${producttype.updateparentfail}")
	private String updateparentfail;
	@Value("${producttype.levellimit}")
	private String levellimit;
	@Value("${producttype.nochildbranch}")
	private String nochildbranch;
	@Value("${app.success}")
	private String successmesssage;

	@Override
	public DataResponse<ProductTypeDTO> create(CreateProductTypeRequest request, MultipartFile image)
			throws IOException {
		MediaResource mediaResource = null;
		if (image != null)
			mediaResource = mediaResourceService.save(image.getBytes());
		ProductType productType = new ProductType(mediaResource, request.getName(), request.getStatus());
		ProductType parent = null;
		if (request.getIdParent() != null)
			parent = request.getIdParent() == null ? null : repository.getReferenceById(request.getIdParent());
		productType.setParent(parent);
		if (parent == null)
			productType.setLevel(1L);
		else
			productType.setLevel(parent.getLevel() + 1L);
		return new DataResponse<>(modelMapper.map(repository.save(productType), ProductTypeDTO.class));
	}

	@Override
	public ListResponse<ProductTypeDTO> getAll() {
		List<ProductType> productTypes = repository.findAll();
		if (productTypes.isEmpty()) {
			throw new CommonRuntimeException(notfound);
		}
		return new ListResponse<>(productTypes.stream().map(c -> modelMapper.map(c, ProductTypeDTO.class)).toList());
	}

	@Override
	public ListResponse<ProductTypeDTO> getAllActive() {
		List<ProductType> productTypes = repository.findAllActive();
		if (productTypes == null || productTypes.isEmpty()) {
			throw new CommonRuntimeException(notfound);
		}
		return new ListResponse<>(productTypes.stream().map(c -> modelMapper.map(c, ProductTypeDTO.class)).toList());
	}

	@Override
	public ListResponse<ProductTypeDTO> getByParent(String parent) {
		List<ProductType> productTypes;
		if (!parent.equals("null"))
			productTypes = repository.getByParentId(Long.valueOf(parent));
		else
			productTypes = repository.getByParentIdNull();
		if (productTypes == null || productTypes.isEmpty()) {
			throw new EntityNotFoundException(notfound);
		}
		return new ListResponse<>(productTypes.stream().map(c -> modelMapper.map(c, ProductTypeDTO.class)).toList());
	}

	@Override
	public DataResponse<ProductTypeDTO> getById(Long id) {
		Optional<ProductType> productType = repository.findById(id);
		if (productType.isEmpty() || !productType.get().getStatus()) {
			throw new CommonRuntimeException(notfoundwithid);
		}

		return new DataResponse<>(modelMapper.map(repository.findById(id), ProductTypeDTO.class));
	}

	@Override
	public ListWithPagingResponse<ProductTypeDTO> findByName(String searchName, Page page) {
		List<ProductType> productTypes = repository.searchProductTypeByName(searchName, page.getOffset(), page.getPageSize());
		if (productTypes == null || productTypes.isEmpty()) {
			throw new CommonRuntimeException(notfoundwithname + searchName);
		}
		return new ListWithPagingResponse<ProductTypeDTO>(page.getPageNumber() + 1, page.getTotalPage(), productTypes.stream().map(c -> modelMapper.map(c, ProductTypeDTO.class)).toList());

	}
	@Override
	public Long getCountProductTypeByName(String searchName) {
		return repository.countSearchProductTypeByName(searchName);
	}
	@Transactional
	@Override
	public DataResponse<ProductTypeDTO> update(Long id, EditProductTypeRequest request, MultipartFile image)
			throws IOException {
		ProductType productType = repository.getReferenceById(id);
		productType.setName(request.getName());

		if (request.getStatus() != null) {
			productType.setStatus(request.getStatus());
		}
		if (image != null) {
			MediaResource oldImage = productType.getImage();
			productType.setImage(null);
			if (oldImage != null && !mediaResourceService.delete(oldImage.getId())) {
				productType.setImage(oldImage);
				throw new CommonRuntimeException(uploadimagefail + id);
			}
			MediaResource mediaResource = mediaResourceService.save(image.getBytes());
			productType.setImage(mediaResource);
		}
		// Allow change parent that have the same level with old parent
		ProductType parent = null;
		if (request.getIdParent() != null)
			parent = repository.getReferenceById(request.getIdParent());
		if (parent == null) {
			if (productType.getLevel() > 1L)
				throw new CommonRuntimeException(updateparentfail);
		} else {
			if (parent.getLevel() != productType.getLevel() - 1L)
				throw new CommonRuntimeException(updateparentfail);
			productType.setParent(parent);
		}
		return new DataResponse<>(modelMapper.map(repository.save(productType), ProductTypeDTO.class));
	}

	@Override
	@Transactional
	public BaseResponse updateParent(Long id, Long parentId) {
		ProductType productType = repository.getReferenceById(id);
		ProductType parent = parentId == null ? null : repository.getReferenceById(parentId);
		if (productType.getLevel() == 3L && parent == null || productType.getChildren().isEmpty() && parent == null)
			throw new CommonRuntimeException(nochildbranch);
		else if (parent != null && parent.getLevel() == 3L)
			throw new CommonRuntimeException(levellimit);
		if (parent == null) {
			if (productType.getLevel() == 1L)
				return new BaseResponse(true, successmesssage);
			// parent == null -> set to lv1
			productType.setParent(parent);
			productType.setLevel(1L);
			List<ProductType> lst = productType.getChildren();
			lst.stream().forEach(p -> p.setLevel(productType.getLevel() + 1));
			repository.saveAll(lst);
		} else if (parent.getLevel() < productType.getLevel()) {
			// parent lvl is 2 and 1. And productType is 2 and 3
			// because parent lvl < child lvl
			productType.setParent(parent);
			productType.setLevel(parent.getLevel() + 1L);

		} else if (parent.getLevel().equals(productType.getLevel())) {
			// Because parent level can not be 3 -> only 1 = 1 and 2 = 2
			switch (productType.getLevel().intValue()) {
			case 2:
				if (productType.getChildren().isEmpty()) {
					productType.setParent(parent);
					productType.setLevel(parent.getLevel() + 1L);
				} else
					throw new CommonRuntimeException(levellimit);
				break;
			case 1:
				Long maxLevel = repository.getMaxLevelOfProductTypeLevel1(id);
				if (maxLevel == null && productType.getChildren().isEmpty())
					maxLevel = 2L;
				else
					maxLevel = 1L;
				if (maxLevel == 3)
					throw new CommonRuntimeException(levellimit);
				productType.setParent(parent);
				productType.setLevel(parent.getLevel() + 1L);
				if (maxLevel == 2) {
					// max level = 2, when update parent we need to update level of children
					List<ProductType> lst = productType.getChildren();
					lst.stream().forEach(p -> p.setLevel(productType.getLevel() + 1));
					repository.saveAll(lst);
				}
				break;
			default:
				break;
			}

		} else {
			// paren > productType
			// -> producttype < 3. (1)
			// -> parent > 1. (2)
			Long maxLevel = repository.getMaxLevelOfProductTypeLevel1(id);
			if (maxLevel == null && productType.getChildren().isEmpty())
				maxLevel = 2L;
			else
				maxLevel = 1L;
			if (maxLevel == 3 || maxLevel == 2)
				throw new CommonRuntimeException(levellimit);
			// productType do not have child
			productType.setParent(parent);
			productType.setLevel(parent.getLevel() + 1L);
		}
		repository.save(productType);
		return new BaseResponse(true, successmesssage);
	}

	@Override
	public DataResponse<ProductTypeDTO> getParent(Long id) {
		ProductType pt = repository.getReferenceById(id);
		int loop = 0;
		while (pt.getParent() != null && loop < 3) {
			pt = pt.getParent();
			loop++;
		}
		if (loop == 3)
			throw new EntityNotFoundException(notfound);
		DataResponse<ProductTypeDTO> result = new DataResponse<>();
		result.setSuccess(true);
		result.setData(modelMapper.map(pt, ProductTypeDTO.class));
		return result;
	}

	@Override
	@Transactional
	public boolean delete(Long id) {
		ProductType productType = repository.getReferenceById(id);
		productType.setStatus(false);
		repository.save(productType);

		return true;
	}
}
