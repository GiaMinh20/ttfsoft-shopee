package com.shopee.backend.controller.admin;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.service.impl.AdminStatistic;

@RestController
@RequestMapping("/api/admin")
public class AdminStatisticController {

	@Autowired
	AdminStatistic adminStatistic;

	@GetMapping
	public ResponseEntity<?> getStatistic(
			@RequestParam(name = "dStart", required = false, defaultValue = "2001-01-01") String dStart,
			@RequestParam(name = "dEnd", required = false, defaultValue = "2091-01-01") String dEnd,
			@RequestParam(name = "dLast", required = false) Long dLast) throws ParseException {
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		return ResponseEntity.ok(adminStatistic.getStatistic(formatter.parse(dStart), formatter.parse(dEnd), dLast));
	}
}
