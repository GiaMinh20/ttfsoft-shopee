package com.shopee.backend.repository.custom.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.shopee.backend.dto.holder.DataHolder;
import com.shopee.backend.model.OrderDetail;
import com.shopee.backend.model.OrderDetail_;
import com.shopee.backend.model.Product;
import com.shopee.backend.model.ProductClassification;
import com.shopee.backend.model.ProductClassification_;
import com.shopee.backend.model.ProductType_;
import com.shopee.backend.model.Product_;
import com.shopee.backend.model.Shop;
import com.shopee.backend.model.ShopOrder;
import com.shopee.backend.model.ShopOrder_;
import com.shopee.backend.model.Shop_;
import com.shopee.backend.payload.response.WorkPerformance;
import com.shopee.backend.repository.custom.ShopProductManagementRepoCustom;
import com.shopee.backend.utility.datatype.EOrderStatus;
import com.shopee.backend.utility.datatype.EProductStatus;

public class ShopProductManagementRepoCustomImpl implements ShopProductManagementRepoCustom {
	@PersistenceContext
	private EntityManager em;

	@Override
	public Long countOrderByStatus(Shop shop, EOrderStatus status) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<ShopOrder> root = cq.from(ShopOrder.class);

		Predicate predicate1 = cb.equal(root.get(ShopOrder_.shop).get(Shop_.id), shop.getId());
		Predicate predicate2 = cb.equal(root.get(ShopOrder_.status), status);

		cq.select(cb.count(root)).where(predicate1, predicate2);

		return em.createQuery(cq).getSingleResult();
	}

	@Override
	public Long countOutOfStock(Long idShop) {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<ProductClassification> root = cq.from(ProductClassification.class);

		Predicate predicate1 = cb.equal(root.get(ProductClassification_.product).get(Product_.shop).get(Shop_.id),
				idShop);
		Predicate predicate2 = cb.equal(root.get(ProductClassification_.quantity), 0);

		cq.select(cb.count(root)).where(predicate1, predicate2);
		return em.createQuery(cq).getSingleResult();
	}

	@Override
	public Long countAccessTimesShop(Shop shop) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<Product> root = cq.from(Product.class);

		Predicate predicate = cb.equal(root.get(Product_.shop).get(Shop_.id), shop.getId());

		cq.select(cb.sum(root.get(Product_.view))).where(predicate);
		Long result = em.createQuery(cq).getSingleResult();
		return result == null ? 0L : result;
	}

	@Override
	public Long countOrderConfirmed(Shop shop) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<ShopOrder> root = cq.from(ShopOrder.class);
		List<Predicate> filters = new ArrayList<>();
		filters.add(cb.equal(root.get(ShopOrder_.shop).get(Shop_.id), shop.getId()));
		filters.add(cb.notEqual(root.get(ShopOrder_.status), EOrderStatus.CANCELLED));
		filters.add(cb.notEqual(root.get(ShopOrder_.status), EOrderStatus.WAIT_FOR_CONFIRMATION));
		filters.add(cb.notEqual(root.get(ShopOrder_.status), EOrderStatus.WAIT_FOR_PAYMENT));

		Predicate predicate = cb.and(filters.toArray(new Predicate[0]));
		cq.select(cb.count(root)).where(predicate);
		return em.createQuery(cq).getSingleResult();
	}

	@Override
	public List<DataHolder> countOrderByStatus(Shop shop) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DataHolder> cq = cb.createQuery(DataHolder.class);
		Root<ShopOrder> root = cq.from(ShopOrder.class);

		Predicate predicate1 = cb.equal(root.get(ShopOrder_.shop).get(Shop_.id), shop.getId());

		cq.multiselect(cb.count(root), root.get(ShopOrder_.status)).where(predicate1)
				.groupBy(root.get(ShopOrder_.status));

		return em.createQuery(cq).getResultList();
	}

	@Override
	public Long sales(Shop shop, Date dStart, Date dEnd, Long dLast) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<ShopOrder> root = cq.from(ShopOrder.class);
		
		Join<ShopOrder, OrderDetail> join = root.join(ShopOrder_.orderDetails, JoinType.LEFT);
		List<Predicate> filters = new ArrayList<>();
		filters.add(cb.equal(root.get(ShopOrder_.shop).get(Shop_.id), shop.getId()));
		Expression<EOrderStatus> lstStatus = root.get(ShopOrder_.status);
		filters.add(lstStatus.in(Arrays.asList(EOrderStatus.DELIVERING, EOrderStatus.DELIVERED, EOrderStatus.WAIT_FOR_SENDING)));

		if (dLast != null) {
			dEnd = new Date(System.currentTimeMillis());
			dStart = new Date(dEnd.getTime() - dLast * 24 * 3600 * 1000L);
		}

		if (dStart != null && dEnd != null) {
			filters.add(cb.between(root.get(ShopOrder_.createdDate), dStart, dEnd));
		}

		Predicate predicate = cb.and(filters.toArray(new Predicate[0]));
		cq.select(cb.sum(join.get(OrderDetail_.quantity))).where(predicate);
		Long res = em.createQuery(cq).getSingleResult();
		if (res == null)
			res = 0L;
		return res;
	}

	@Override
	public WorkPerformance getWorkPerformance(Shop shop) {
		WorkPerformance res = new WorkPerformance();
		Long bannedProduct = 0L;
		Double rating = 0.0;
		Double timePrepare = 0.0;
		Double canceledOrderRate = 0.0;
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<Product> root = cq.from(Product.class);
		
		List<Predicate> filters = new ArrayList<>();
		filters.add(cb.equal(root.get(Product_.shop).get(Shop_.id), shop.getId()));
		filters.add(cb.equal(root.get(Product_.status), EProductStatus.BANNED));
		Predicate predicate = cb.and(filters.toArray(new Predicate[0]));
		
		cq.select(cb.count(root)).where(predicate);
		bannedProduct = em.createQuery(cq).getSingleResult();

		cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq1 = cb.createQuery(Long.class);
		Root<ShopOrder> root1 = cq1.from(ShopOrder.class);
		
		filters = new ArrayList<>();
		filters.add(cb.equal(root1.get(ShopOrder_.shop).get(Shop_.id), shop.getId()));
		filters.add(cb.equal(root1.get(ShopOrder_.status), EOrderStatus.CANCELLED));
		predicate = cb.and(filters.toArray(new Predicate[0]));
		cq1.select(cb.count(root1));
		
		Long countCanceledOrder = em.createQuery(cq1.where(predicate)).getSingleResult();
		predicate = cb.equal(root1.get(ShopOrder_.shop).get(Shop_.id), shop.getId());
		Long totalOrder = em.createQuery(cq1.where(predicate)).getSingleResult();
		if(totalOrder == 0)
			canceledOrderRate = 0.0;
		else
			canceledOrderRate = (double)countCanceledOrder / totalOrder;
		
		rating = shop.getAverageRating();

		cb = em.getCriteriaBuilder();
		CriteriaQuery<Double> cq2 = cb.createQuery(Double.class);
		Root<ShopOrder> root2 = cq2.from(ShopOrder.class);
		
		filters = new ArrayList<>();
		filters.add(cb.equal(root2.get(ShopOrder_.shop).get(Shop_.id), shop.getId()));
		filters.add(cb.notEqual(root2.get(ShopOrder_.status), EOrderStatus.CANCELLED));
		
		predicate = cb.and(filters.toArray(new Predicate[0]));
		cq2.select(cb.avg(root2.get(ShopOrder_.prepareDate))).where(predicate);
		Double b = em.createQuery(cq2).getSingleResult();
		
		timePrepare = Math.floor(b == null ? 0.0 : b / (24 * 60 * 60 * 1000L) * 100) / 100;
	
		res.setBannedProduct(bannedProduct);
		res.setCanceledOrderRate(canceledOrderRate);
		res.setRating(rating);
		res.setAvgTimePrepare(timePrepare);
		return res;
	}

	@Override
	public Long salesValue(Shop shop, Date dStart, Date dEnd, Long dLast) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<ShopOrder> root = cq.from(ShopOrder.class);
		
		List<Predicate> filters = new ArrayList<>();
		filters.add(cb.equal(root.get(ShopOrder_.shop).get(Shop_.id), shop.getId()));
		Expression<EOrderStatus> lstStatus = root.get(ShopOrder_.status);
		filters.add(lstStatus.in(Arrays.asList(EOrderStatus.DELIVERING, EOrderStatus.DELIVERED, EOrderStatus.WAIT_FOR_SENDING)));

		
		if (dLast != null) {
			dEnd = new Date(System.currentTimeMillis());
			dStart = new Date(dEnd.getTime() - dLast * 24 * 3600 * 1000L);
		}

		if (dStart != null && dEnd != null) {
			filters.add(cb.between(root.get(ShopOrder_.createdDate), dStart, dEnd));
		}

		Predicate predicate = cb.and(filters.toArray(new Predicate[0]));
		cq.select(cb.sum(root.get(ShopOrder_.totalPayPrice))).where(predicate);
		Long res = em.createQuery(cq).getSingleResult();
		if (res == null)
			res = 0L;
		return res;
	}
}
