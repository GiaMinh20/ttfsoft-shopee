package com.shopee.backend.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
@Entity
@Table(name = "shop_category")
public class ShopCategory {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "id_shop")
	private Shop shop;
	
	@OneToMany(mappedBy = "shopCategory")
	private List<Product> products;

	@NotBlank
	@Column(length = 100)
	private String name;

	public ShopCategory(Shop shop, @NotBlank String name) {
		super();
		this.shop = shop;
		this.name = name;
	}
	
}
