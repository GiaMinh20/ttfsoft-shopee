package com.shopee.backend.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.shopee.backend.model.pk.CartDetailPK;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "cart_detail")
public class CartDetail {
	@EmbeddedId
	private CartDetailPK id;

	@MapsId("idBuyer")
	@ManyToOne
	@JoinColumn(name = "id_buyer", insertable = false, updatable = false)
	private Buyer buyer;

	@MapsId("idProductClassification")
	@ManyToOne
	@JoinColumn(name = "id_product_classification", insertable = false, updatable = false)
	private ProductClassification productClassification;

	@NotNull
	private Long quantity = 0L;
}
