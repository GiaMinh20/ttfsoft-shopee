package com.shopee.backend.controller.shop;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.payload.request.auth.PasswordUpdateRequest;
import com.shopee.backend.payload.request.auth.ShopProfileUpdateRequest;
import com.shopee.backend.service.IShopService;
import com.shopee.backend.service.auth.ShopDetails;

@RestController
@RequestMapping("/api/shop/profile")
public class ShopProfileController {	
	@Autowired
	IShopService shopService; 
	
	@GetMapping
    public ResponseEntity<?> get(@AuthenticationPrincipal ShopDetails shop) {
		return ResponseEntity.ok(shopService.getById(shop.getId()));
    }
	
	@PutMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> update(
			@AuthenticationPrincipal ShopDetails shop,
			@RequestPart(name = "info", required = false) @Valid ShopProfileUpdateRequest request,
			@RequestPart(name = "avatar", required = false) MultipartFile avatar,
			@RequestPart(name = "coverImage", required = false) MultipartFile coverImage) throws IOException {
		
		return ResponseEntity.ok(shopService.update(shop.getId(), request, avatar, coverImage));
	}
	
	@PutMapping("/password")
	public ResponseEntity<?> updatePassword(
			@AuthenticationPrincipal ShopDetails shop,
			@RequestBody @Valid PasswordUpdateRequest request) {
		
		return ResponseEntity.ok(shopService.updatePassword(shop.getId(), request));
	}
}
