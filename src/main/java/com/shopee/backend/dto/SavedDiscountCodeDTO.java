package com.shopee.backend.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.shopee.backend.dto.base.AbstractDTO;

import lombok.Data;

@Data
public class SavedDiscountCodeDTO extends AbstractDTO {
	private Long idUser;
	private String code;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date time;
	private Boolean isUsed;

}
