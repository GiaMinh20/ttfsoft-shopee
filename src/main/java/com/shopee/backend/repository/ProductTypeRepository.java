package com.shopee.backend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.shopee.backend.model.ProductType;

@Repository
public interface ProductTypeRepository extends JpaRepository<ProductType, Long> {
	@Query(value = "SELECT * FROM product_type WHERE status = '1' AND level = 1 ORDER BY modifiedDate DESC", nativeQuery = true)
	public List<ProductType> findAllActive();

	@Query(value = "SELECT * FROM product_type WHERE name LIKE %:keyword% AND status='1' limit :index,:limit", nativeQuery = true)
	public List<ProductType> searchProductTypeByName(@Param("keyword") String keyword, @Param("index") Long index,
			@Param("limit") int limit);

	@Query(value = "SELECT count(*) FROM product_type WHERE name LIKE %:keyword% AND status='1'", nativeQuery = true)
	public Long countSearchProductTypeByName(@Param("keyword") String keyword);

	@Query(value = "SELECT * FROM product_type WHERE status = '1' and parent_id = :parent_id ORDER BY modifieddate DESC", nativeQuery = true)
	public List<ProductType> getByParentId(@Param("parent_id") Long idParent);

	@Query(value = "SELECT * FROM product_type WHERE status = '1' and parent_id is null ORDER BY modifieddate DESC", nativeQuery = true)
	public List<ProductType> getByParentIdNull();

	@Query(value = "select max(p2.level) from product_type p1, product_type p2 where p1.parent_id = :id and p2.parent_id = p1.id", nativeQuery = true)
	public Long getMaxLevelOfProductTypeLevel1(@Param("id") Long id);
}
