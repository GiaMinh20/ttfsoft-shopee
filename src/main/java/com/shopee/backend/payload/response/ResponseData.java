package com.shopee.backend.payload.response;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseData<T> {
	private Boolean success;
	private String message;
	private int page;
	private int totalPage;
	private List<T> datas = new ArrayList<>();
	private T data ;
	public ResponseData(Boolean success, T data) {
		this.success = success;
		this.data = data;
	}
	public ResponseData(Boolean success, String message, T data) {
		super();
		this.success = success;
		this.message = message;
		this.data = data;
	}
	
	
}
