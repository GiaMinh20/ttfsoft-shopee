package com.shopee.backend.repository.custom.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.data.jpa.repository.query.QueryUtils;

import com.shopee.backend.dto.BuyerDTO;
import com.shopee.backend.dto.MediaResourceDTO;
import com.shopee.backend.dto.ProductReviewDTO;
import com.shopee.backend.model.ProductClassification_;
import com.shopee.backend.model.ProductReview;
import com.shopee.backend.model.ProductReview_;
import com.shopee.backend.repository.custom.ProductReviewRepoCustom;
import com.shopee.backend.utility.Page;

public class ProductReviewRepoCustomImpl implements ProductReviewRepoCustom {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public List<ProductReviewDTO> getProductReview(List<Long> ids, Long rate, Page page) {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ProductReview> cq = cb.createQuery(ProductReview.class);
		Root<ProductReview> root = cq.from(ProductReview.class);

		Expression<Long> expression = root.get(ProductReview_.productClassification).get(ProductClassification_.id);
		List<Predicate> lstPredicate = new ArrayList<>();
		lstPredicate.add(expression.in(ids));
		if (rate != null)
			lstPredicate.add(cb.equal(root.get(ProductReview_.rating), rate));

		Sort sort = Sort.unsorted();
		sort = sort.and(JpaSort.of(ProductReview_.id).descending());

		Predicate predicate = cb.and(lstPredicate.toArray(new Predicate[0]));

		cq.select(root).where(predicate).orderBy(QueryUtils.toOrders(sort, root, cb));
		TypedQuery<ProductReview> query = em.createQuery(cq);
		if (page != null) {
			query.setFirstResult(page.getPageNumber() * page.getPageSize());
			query.setMaxResults(page.getPageSize());
		}
		List<ProductReview> lstProductReview = query.getResultList();
		List<ProductReviewDTO> result = new ArrayList<>();

		for (ProductReview pr : lstProductReview) {
			ProductReviewDTO prD = modelMapper.map(pr, ProductReviewDTO.class);
			prD.setIdProductClassification(pr.getProductClassification().getId());
			prD.setVariationName(pr.getProductClassification().getVariationName());
			prD.setBuyer(modelMapper.map(pr.getOrder().getBuyer(), BuyerDTO.class));
			List<MediaResourceDTO> mrD = pr.getMedias().stream().map(m -> modelMapper.map(m, MediaResourceDTO.class))
					.toList();
			prD.setImages(mrD);
			result.add(prD);
		}

		return result;
	}

	@Override
	public Long countProductReview(List<Long> ids) {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<ProductReview> root = cq.from(ProductReview.class);

		Expression<Long> expression = root.get(ProductReview_.productClassification).get(ProductClassification_.id);
		Predicate predicate = expression.in(ids);
		cq.select(cb.count(root)).where(predicate);

		return em.createQuery(cq).getSingleResult();
	}
}
