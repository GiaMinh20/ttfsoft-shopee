package com.shopee.backend.config;

import java.io.IOException;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

@Configuration
public class ContentTypeInterceptor implements ClientHttpRequestInterceptor {
	@Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        ClientHttpResponse http = execution.execute(request, body);

        HttpHeaders headers = http.getHeaders();
        if (headers.getContentType() == MediaType.APPLICATION_OCTET_STREAM) {
            headers.setContentType(MediaType.APPLICATION_JSON);
        }

        return http;
    }
}
