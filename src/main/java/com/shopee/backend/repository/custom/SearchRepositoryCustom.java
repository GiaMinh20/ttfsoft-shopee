package com.shopee.backend.repository.custom;

import java.util.List;

import com.shopee.backend.model.Product;
import com.shopee.backend.utility.Page;

public interface SearchRepositoryCustom {

	List<Product> getSearchProduct(String keyword, Page page, String locations, Long idCategory, Long idType, Long maxPrice, Long minPrice,
			Double rate, Boolean isNew);

	Long getCountSearchProduct(String keyword, Page page, String locations, Long idCategory, Long idType, Long maxPrice,
			Long minPrice, Double rate, Boolean isNew);

	void refresh(Product product);

}
