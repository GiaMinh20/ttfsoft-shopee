package com.shopee.backend.payload.response;

import com.shopee.backend.dto.MediaResourceDTO;

import lombok.Data;
@Data
public class ProductOrderRespone {
	private String name;
	private String classification;
	private MediaResourceDTO avatar;
	private Long quantity;
	private Long priceOrigin;

}
