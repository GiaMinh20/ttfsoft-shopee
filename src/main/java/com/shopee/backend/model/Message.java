package com.shopee.backend.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "message")
public class Message {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_buyer")
	private Buyer buyer;

	@ManyToOne
	@JoinColumn(name = "id_shop")
	private Shop shop;

	@Lob
	@Column(length = 65535)
	@NotBlank
	private String content;
	
	@Column(name = "id_sender")
	@NotNull
	private Long idSender;

	@NotNull
	private Boolean seen;

	private Date time;
}
