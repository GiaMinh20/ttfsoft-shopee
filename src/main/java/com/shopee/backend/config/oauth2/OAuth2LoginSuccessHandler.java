package com.shopee.backend.config.oauth2;

import java.io.IOException;
import java.util.Date;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.shopee.backend.model.Buyer;
import com.shopee.backend.repository.BuyerRepository;
import com.shopee.backend.service.auth.impl.CustomOAuth2User;
import com.shopee.backend.utility.RolePrefix;
import com.shopee.backend.utility.jwt.JwtUtils;


@Component
public class OAuth2LoginSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

	@Autowired
	BuyerRepository buyerRepository;

	@Autowired
	JwtUtils jwtUtils;
	@Value("${fontend.app.googleloginurl}")
	String redirectUrl;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		// Data google send back
		CustomOAuth2User oAuth2User = (CustomOAuth2User) authentication.getPrincipal();
		String email = oAuth2User.getEmail();
		String name = oAuth2User.getFullName();
		// only allow buyer login with google
		String jwt;
		Optional<Buyer> optionalUser = buyerRepository.findByEmail(email);
		if (optionalUser.isEmpty()) {
			String username = email.split("@")[0];
			if (buyerRepository.existsByUsername(username)) {
				username = username + Long.toString(new Date().getTime());
			}

			Buyer buyer = new Buyer();
			buyer.setUsername(username);
			buyer.setName(name);
			buyer.setEmail(email);
			buyer.setEmailConfirmed(true);
			buyer.setIsActive(true);
			buyer.setPhoneConfirmed(false);
			buyerRepository.save(buyer);
			jwt = jwtUtils.generateJwtToken(username, RolePrefix.BUYER);
		}
		else {
			if(optionalUser.get().getIsActive())
				jwt = jwtUtils.generateJwtToken(optionalUser.get().getUsername(), RolePrefix.BUYER);
			else jwt = "";
		}
		
		request.setAttribute("token", jwt);
		getRedirectStrategy().sendRedirect(request, response, redirectUrl + jwt);
		super.onAuthenticationSuccess(request, response, authentication);
	}
}
