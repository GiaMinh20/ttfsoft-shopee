package com.shopee.backend.utility.datatype;

public enum EProductStatus {
	ACTIVE, HIDDEN, BANNED
}
