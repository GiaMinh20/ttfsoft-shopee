package com.shopee.backend.payload.request.productcategory;


import java.sql.Date;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EditProductCategoryRequest {
	@Size(max = 30, min = 4)
	private String name;
	
	@Schema(type = "string", pattern = "^\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}$", example = "2023-01-01 00:00:00")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date dod;
}
