package com.shopee.backend.controller.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.payload.request.admin.PasswordUpdate;
import com.shopee.backend.payload.request.admin.UserUnlock;
import com.shopee.backend.service.IAdminAccountManagement;

@RestController
@RequestMapping("/api/admin/manage/account")
public class AdminAccountManagementController {

	@Autowired
	IAdminAccountManagement accountManagement;

	@GetMapping("/buyer")
	public ResponseEntity<?> getBuyerByEmail(@RequestParam String email) {
		return ResponseEntity.ok(accountManagement.getBuyerByEmail(email));
	}

	@GetMapping("/shop")
	public ResponseEntity<?> getShopByEmail(@RequestParam String email) {
		return ResponseEntity.ok(accountManagement.getShopByEmail(email));
	}

	@PutMapping("/buyer")
	public ResponseEntity<?> updateBuyerPassword(@RequestBody @Valid PasswordUpdate body) {
		return ResponseEntity.ok(accountManagement.updateBuyerPassword(body.getId(), body.getNewpassword()));
	}

	@PutMapping("/shop")
	public ResponseEntity<?> updateShopPassword(@RequestBody @Valid PasswordUpdate body) {
		return ResponseEntity.ok(accountManagement.updateShopPassword(body.getId(), body.getNewpassword()));
	}

	@PutMapping("/buyer/active")
	public ResponseEntity<?> unlockBuyerAccount(@RequestBody UserUnlock body) {
		return ResponseEntity.ok(accountManagement.unlockBuyerAccount(body.getId(), body.getUnlock()));
	}

	@PutMapping("/shop/active")
	public ResponseEntity<?> unlockShopAccount(@RequestBody UserUnlock body) {
		return ResponseEntity.ok(accountManagement.unlockShopAccount(body.getId(), body.getUnlock()));
	}
}
