package com.shopee.backend.utility.datatype;

public enum EUserRole {
    BUYER,
    SHOP,
    ADMIN
}
