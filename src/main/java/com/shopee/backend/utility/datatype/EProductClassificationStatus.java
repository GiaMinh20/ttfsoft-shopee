package com.shopee.backend.utility.datatype;

public enum EProductClassificationStatus {
	ACTIVE,
	HIDDEN
}
