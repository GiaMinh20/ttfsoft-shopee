package com.shopee.backend.repository.custom.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.repository.query.QueryUtils;

import com.shopee.backend.model.Buyer_;
import com.shopee.backend.model.Follow;
import com.shopee.backend.model.Follow_;
import com.shopee.backend.repository.custom.FollowRepositoryCustom;
import com.shopee.backend.utility.Page;

public class FollowRepositoryCustomImpl implements FollowRepositoryCustom {
	@PersistenceContext
	private EntityManager em;
	
	@Override
	public List<Follow> getBuyerFollowList(Long idBuyer, Page page) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Follow> cq = cb.createQuery(Follow.class);
		Root<Follow> root = cq.from(Follow.class);
		
		cq.select(root).where(cb.equal(root.get(Follow_.buyer).get(Buyer_.id), idBuyer));
		if (page != null) {
			cq.orderBy(QueryUtils.toOrders(page.getSort(), root, cb));
		}
		
		TypedQuery<Follow> query = em.createQuery(cq);

		if (page != null) {
			query.setFirstResult(page.getPageNumber() * page.getPageSize());
			query.setMaxResults(page.getPageSize());
		}
		
		return query.getResultList();
	}
	
	@Override
	public Long countBuyerFollowList(Long idBuyer) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<Follow> root = cq.from(Follow.class);
		cq.select(cb.count(root)).where(cb.equal(root.get(Follow_.buyer).get(Buyer_.id), idBuyer));
		return em.createQuery(cq).getSingleResult();
	}
}
