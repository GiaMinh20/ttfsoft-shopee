package com.shopee.backend.payload.response;

import com.shopee.backend.utility.datatype.EPaymentMethod;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data @AllArgsConstructor
public class CreateOrderResponse {
    private Long idOrder;
    private EPaymentMethod paymentMethod;
    private Long price;
    private String payUrl;

    public CreateOrderResponse(Long idOrder, EPaymentMethod paymentMethod, Long price) {
        this.idOrder = idOrder;
        this.paymentMethod = paymentMethod;
        this.price = price;
    }
}
