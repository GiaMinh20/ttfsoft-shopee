package com.shopee.backend.repository.custom.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.shopee.backend.model.Shop;
import com.shopee.backend.model.Shop_;
import com.shopee.backend.repository.custom.ShopRepositoryCustom;

@Repository
public class ShopRepositoryCustomImpl implements ShopRepositoryCustom {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	@Override
	public boolean setPassword(String email, String hashedPassword) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaUpdate<Shop> cu = cb.createCriteriaUpdate(Shop.class);
		Root<Shop> root = cu.from(Shop.class);
		
		cu.set(root.get(Shop_.password), hashedPassword);
		cu.where(cb.equal(root.get(Shop_.email), email));
		
		return em.createQuery(cu).executeUpdate() != 0;
	}
	
	@Override
	public Long getIdByEmail(String email) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<Shop> root = cq.from(Shop.class);
		
		cq.select(root.get(Shop_.id)).where(cb.equal(root.get(Shop_.email), email));
		try {
			return em.createQuery(cq).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
	@Transactional
	@Override
	public boolean setEmailConfirmed(Long id) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaUpdate<Shop> cu = cb.createCriteriaUpdate(Shop.class);
		Root<Shop> root = cu.from(Shop.class);
		
		cu.set(root.get(Shop_.emailConfirmed), Boolean.TRUE);
		cu.where(cb.equal(root.get(Shop_.id), id));
		
		return em.createQuery(cu).executeUpdate() != 0;
	}
}
