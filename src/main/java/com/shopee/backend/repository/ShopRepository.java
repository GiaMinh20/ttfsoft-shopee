package com.shopee.backend.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shopee.backend.model.Shop;
import com.shopee.backend.repository.custom.ShopProductManagementRepoCustom;
import com.shopee.backend.repository.custom.ShopRepositoryCustom;

@Repository
public interface ShopRepository extends JpaRepository<Shop, Long>, ShopProductManagementRepoCustom, ShopRepositoryCustom {
	Optional<Shop> findByUsername(String username);
	
	Optional<Shop> findByEmail(String email);

	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);
	
	Boolean existsByPhone(String phone);
}
