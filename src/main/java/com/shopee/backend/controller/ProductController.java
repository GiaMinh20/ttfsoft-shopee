package com.shopee.backend.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.service.IProductService;
import com.shopee.backend.utility.datatype.EProductStatus;
import com.shopee.backend.utility.datatype.EUserRole;
import com.shopee.backend.utility.sort.ModelSorting;

@RestController
@RequestMapping("/api")
public class ProductController {
	@Autowired
	IProductService productService;
	@Autowired
	ModelMapper modelMapper;

	@GetMapping(value = "/product")
	public ResponseEntity<?> get(@RequestParam(required = false) Long idShop,
			@RequestParam(required = false) Long idCategory, @RequestParam(required = false) Long idType,
			@RequestParam(required = false) Long idShopCategory, @RequestParam(required = false) Long idSellPlace,
			@RequestParam(required = false) String searchName, @RequestParam(required = false) Boolean isNew,
			@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size,
			@RequestParam(required = false) Integer sortBy, @RequestParam(required = false) Boolean sortDescending) {
		if (isNew == null)
			isNew = true;
		return ResponseEntity
				.ok(productService.getWithFilter(idShop, idCategory, idType, idShopCategory, idSellPlace, searchName,
						EProductStatus.ACTIVE, isNew, page, size, ModelSorting.getProductSort(sortBy, sortDescending)));
	}

	@GetMapping("/product/{id}")
	public ResponseEntity<?> getProductById(@PathVariable("id") long id) {
		return ResponseEntity.ok(productService.getById(id, EUserRole.BUYER, null));
	}

	@GetMapping("/product/top-buy-product")
	public ResponseEntity<?> getTopBytProduct() {
		return ResponseEntity.ok(productService.getTopBuy());

	}

}