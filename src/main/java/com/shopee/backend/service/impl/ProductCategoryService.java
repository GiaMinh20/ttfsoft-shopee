package com.shopee.backend.service.impl;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.dto.ProductCategoryDTO;
import com.shopee.backend.model.MediaResource;
import com.shopee.backend.model.ProductCategory;
import com.shopee.backend.payload.request.productcategory.CreateProductCategoryRequest;
import com.shopee.backend.payload.request.productcategory.EditProductCategoryRequest;
import com.shopee.backend.payload.response.DataResponse;
import com.shopee.backend.payload.response.ListResponse;
import com.shopee.backend.repository.ProductCategoryRepository;
import com.shopee.backend.service.IMediaResourceService;
import com.shopee.backend.service.IProductCategoryService;

@Service
public class ProductCategoryService implements IProductCategoryService {

	@Autowired
	private ProductCategoryRepository repository;

	@Autowired
	IMediaResourceService mediaResourceService;

	@Autowired
	private ModelMapper modelMapper;

	@Value("${productcategory.notfound}")
	private String notfound;
	@Value("${productcategory.notfoundwithid}")
	private String notfoundwithid;
	@Value("${productcategory.notfoundwithname}")
	private String notfoundwithname;
	@Value("${productcategory.updatefailed}")
	private String updatefailed;

	public DataResponse<ProductCategoryDTO> create(CreateProductCategoryRequest request, MultipartFile image)
			throws IOException {
		ProductCategory productCategory;
		productCategory = new ProductCategory(mediaResourceService.save(image.getBytes()), request.getName(),
				request.getDod());
		return new DataResponse<>(modelMapper.map(repository.save(productCategory), ProductCategoryDTO.class));

	}

	@Override
	public ListResponse<ProductCategoryDTO> getAll() {
		List<ProductCategory> productCategories = repository.findAll();
		if (productCategories.isEmpty()) {
			throw new CommonRuntimeException(notfound);
		}

		return new ListResponse<>(productCategories.stream().map(c -> modelMapper.map(c, ProductCategoryDTO.class))
				.toList());
	}

	@Override
	public DataResponse<ProductCategoryDTO> getById(Long id) {
		Optional<ProductCategory> c = repository.findById(id);

		if (c.isEmpty()) {
			throw new CommonRuntimeException(notfoundwithid + id);
		}

		return new DataResponse<>(modelMapper.map(repository.findById(id), ProductCategoryDTO.class));
	}

	@Override
	public ListResponse<ProductCategoryDTO> findByName(String searchName) {
		List<ProductCategory> productCategories = repository.search(searchName);

		if (productCategories == null) {
			throw new CommonRuntimeException(notfoundwithname + searchName);
		}

		return new ListResponse<>(productCategories.stream().map(c -> modelMapper.map(c, ProductCategoryDTO.class))
				.toList());
	}

	@Override
	public DataResponse<ProductCategoryDTO> update(Long id, EditProductCategoryRequest request, MultipartFile image)
			throws IOException {
		ProductCategory c = repository.getReferenceById(id);

		if (!StringUtils.isBlank(request.getName())) {
			c.setName(request.getName());
		}

		if (request.getDod() != null) {
			c.setDod(request.getDod());
		}

		if (image != null) {
			MediaResource oldImage = c.getImage();
			c.setImage(null);
			if (!mediaResourceService.delete(oldImage.getId())) {
				c.setImage(oldImage);
				throw new CommonRuntimeException(String.format(updatefailed, id));
			}
			c.setImage(mediaResourceService.save(image.getBytes()));
		}

		return new DataResponse<>(modelMapper.map(repository.save(c), ProductCategoryDTO.class));
	}

	@Override
	@Transactional
	public boolean delete(Long id) {
		ProductCategory c = repository.getReferenceById(id);
		try {
			repository.delete(c);
		} catch (DataIntegrityViolationException ex) {
			return false;
		} catch (EmptyResultDataAccessException ex) {
			throw new CommonRuntimeException(notfound);
		}
		MediaResource image = c.getImage();
		c.setImage(null);
		if (!mediaResourceService.delete(image.getId())) {
			c.setImage(image);
			return false;
		}
		return true;
	}
}