package com.shopee.backend.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.shopee.backend.model.MediaResource;
import com.shopee.backend.utility.datatype.EProductStatus;

import lombok.Data;

@Data
public class ProductDetailsDTO {
	private String name;
	private Long price;
	private String description;
	private Long availableQuantity;
	private Long favorite;
	private Long sale;
	private Long view;
	private Long idCategory;
	private Long idType;
	private Long idShopCategory;
	private Long idSeller;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date discountStart;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date discountEnd;
	private Double averageRating;
	private EProductStatus status;
	private Long reviewCount;
	private Boolean isNew;
	private Long sellPlaceId;
	private String avatar;
	private String sellPlace;
	private List<MediaResource> images = new ArrayList<>();
	private String video;
}
