package com.shopee.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.service.ISearchService;
import com.shopee.backend.utility.Page;
import com.shopee.backend.utility.sort.ModelSorting;

@RestController
@RequestMapping("/api")
public class SearchController {
	@Autowired
	ISearchService searchSerivce;

	@GetMapping("/search")
	public ResponseEntity<?> search(@RequestParam(value = "keyword", required = true) String keyword,
			@RequestParam(value = "locations", required = false) String locations,
			@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "limit", required = false, defaultValue = "10") int limit,
			@RequestParam(value = "category", required = false) Long idCategory,
			@RequestParam(value = "type", required = false) Long idType,
			@RequestParam(value = "maxPrice", required = false) Long maxPrice,
			@RequestParam(value = "minPrice", required = false) Long minPrice,
			@RequestParam(value = "ratingFilter", required = false) Double rate,
			@RequestParam(value = "newProduct", required = false) Boolean isNew,
			@RequestParam(required = false) Integer sortBy, @RequestParam(required = false) Boolean sortDescending) {
		if (isNew == null)
			isNew = true;
		int productCount = searchSerivce
				.countWithFilter(keyword, null, locations, idCategory, idType, maxPrice, minPrice, rate, isNew)
				.intValue();

		return ResponseEntity.ok(searchSerivce.searchProduct(keyword,
				new Page(page, limit, productCount, ModelSorting.getProductSort(sortBy, sortDescending)), locations,
				idCategory, idType, maxPrice, minPrice, rate, isNew));
	}
}
