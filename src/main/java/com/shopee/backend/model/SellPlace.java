package com.shopee.backend.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.shopee.backend.model.base.BaseModel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "sell_place")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SellPlace extends BaseModel {
	@NotBlank
	@Column(length = 20, unique = true)
	private String code;
	@NotBlank
	@Column(length = 20)
	private String name;

	@OneToMany(mappedBy = "sellPlace")
	List<Product> products = new ArrayList<>();
}
