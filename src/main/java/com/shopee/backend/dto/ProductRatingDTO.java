package com.shopee.backend.dto;

import com.shopee.backend.dto.base.AbstractDTO;

import lombok.Data;

@Data
public class ProductRatingDTO extends AbstractDTO{
	private Long idOrder;
	private Long idProduct;
	private String content;
	private int rating;
}
