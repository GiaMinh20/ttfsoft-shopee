package com.shopee.backend.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.dto.ProductReviewDTO;
import com.shopee.backend.model.MediaResource;
import com.shopee.backend.model.Order;
import com.shopee.backend.model.Product;
import com.shopee.backend.model.ProductClassification;
import com.shopee.backend.model.ProductReview;
import com.shopee.backend.model.Shop;
import com.shopee.backend.model.ShopOrder;
import com.shopee.backend.model.pk.ProductReviewPK;
import com.shopee.backend.payload.response.BaseResponse;
import com.shopee.backend.payload.response.ListWithPagingResponse;
import com.shopee.backend.repository.OrderRepository;
import com.shopee.backend.repository.ProductClassificationRepository;
import com.shopee.backend.repository.ProductRepository;
import com.shopee.backend.repository.ReviewRepository;
import com.shopee.backend.repository.ShopOrderRepository;
import com.shopee.backend.repository.ShopRepository;
import com.shopee.backend.service.IMediaResourceService;
import com.shopee.backend.service.IProductReviewService;
import com.shopee.backend.service.auth.BuyerDetails;
import com.shopee.backend.utility.Page;
import com.shopee.backend.utility.datatype.EOrderStatus;

@Service
public class ProductReviewService implements IProductReviewService {

	@Autowired
	ReviewRepository reviewRepository;

	@Autowired
	IMediaResourceService mediaResourceService;

	@Autowired
	ShopOrderRepository shopOrderRepository;

	@Autowired
	OrderRepository orderRepository;

	@Autowired
	ProductClassificationRepository classificationRepository;

	@Autowired
	ProductRepository productRepository;

	@Autowired
	ShopRepository shopRepository;

	@PersistenceContext
	EntityManager em;

	@Override
	public ListWithPagingResponse<ProductReviewDTO> getProductReview(Long idProduct, Long rate, Page page) {
		// find the list id of classification of the product
		List<Long> idsClassification = classificationRepository.findidByProduct(idProduct);
		// get review by list id found
		List<ProductReviewDTO> result = reviewRepository.getProductReview(idsClassification, rate, page);

		return new ListWithPagingResponse<>(page.getPageNumber() + 1, page.getTotalPage(), result);
	}

	@Override
	public Long countProductReview(Long idProduct) {
		List<Long> idsClassification = classificationRepository.findidByProduct(idProduct);
		// get review by list id found
		return reviewRepository.countProductReview(idsClassification);
	}

	@Override
	@Transactional
	public BaseResponse createReview(Long idOrder, Long idShopOrder, Long idProductClassification, String content,
			Integer rating, List<MultipartFile> images) throws IOException {
		ProductReview review = reviewRepository.getOneByPK(idProductClassification, idOrder, idShopOrder);
		if (review != null)
			throw new CommonRuntimeException("You have reviewed this product !");

		review = new ProductReview();

		ShopOrder shopOrder = shopOrderRepository.getReferenceById(idShopOrder);
		checkLogic(shopOrder);
		ProductClassification pc = classificationRepository.getReferenceById(idProductClassification);
		Order order = orderRepository.getReferenceById(idOrder);
		review.setId(new ProductReviewPK(idOrder, idShopOrder, idProductClassification));
		review.setOrder(order);
		review.setShopOrder(shopOrder);
		review.setProductClassification(pc);
		review.setContent(content);
		review.setRating(rating);

		List<MediaResource> mediaResources = new ArrayList<>();
		if (images != null)
			for (MultipartFile image : images)
				mediaResources.add(mediaResourceService.save(image.getBytes()));

		review.setMedias(mediaResources);

		em.persist(review);

		recaculate(pc);

		return new BaseResponse(true, "Review successfully !.");
	}

	@Override
	@Transactional
	public BaseResponse updateContent(Long idOrder, Long idShopOrder, Long idProductClassification, String content,
			Integer rating) throws IOException {
		ProductReview review = reviewRepository
				.getReferenceById(new ProductReviewPK(idOrder, idShopOrder, idProductClassification));

		ShopOrder shopOrder = review.getShopOrder();

		checkLogic(shopOrder);

		review.setContent(content);
		review.setRating(rating);
		reviewRepository.save(review);
		recaculate(review.getProductClassification());
		return new BaseResponse(true, "Update review successfully !.");
	}

	@Override
	@Transactional
	public BaseResponse deleteReview(Long idOrder, Long idShopOrder, Long idProductClassification) {
		ProductReview review = reviewRepository
				.getReferenceById(new ProductReviewPK(idOrder, idShopOrder, idProductClassification));

		ShopOrder shopOrder = shopOrderRepository.getReferenceById(idShopOrder);

		checkLogic(shopOrder);

		for (MediaResource media : review.getMedias())
			if (!mediaResourceService.delete(media.getId())) {
				throw new CommonRuntimeException("Can not delete media resource !");
			}

		reviewRepository.delete(review);

		recaculate(review.getProductClassification());
		return new BaseResponse(true, "Delete review successfully !.");

	}

	@Override
	public Map<String, Long> getProductRating(ProductReview pr) {
		return productRepository.getProductRating(pr.getProductClassification().getProduct().getId());
	}

	@Override
	@Transactional
	public void recaculate(ProductClassification pc) {
		// Lock product p
		Product p = em.find(Product.class, pc.getProduct().getId(), LockModeType.OPTIMISTIC);

		Map<String, Long> result = productRepository.getProductRating(p.getId());

		// Average rating for product
		Double avg = (double) Math.round(((double) result.get("sum") / result.get("amount")) * 100) / 100;

		// --> Update average rating for product and review count
		p.setAverageRating(avg);
		p.setReviewCount(result.get("amount"));
		em.persist(p);

		// Average rating for shop
		Shop shop = em.find(Shop.class, p.getShop().getId(), LockModeType.OPTIMISTIC);
		Map<String, Double> values = productRepository.getShopRating(shop.getId());

		// --> Update rate for shop
		shop.setAverageRating(values.get("average_rating"));
		shop.setTotalRating(values.get("amount_rating").longValue());

		em.persist(shop);
	}

	private void checkLogic(ShopOrder shopOrder) {
		// Check if it is user's shop order
		Long idBuyer = ((BuyerDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
		if (!idBuyer.equals(shopOrder.getOrder().getBuyer().getId()))
			throw new CommonRuntimeException("You do not have right to review .!");
		// check the status of shop order.
		if (shopOrder.getStatus() != EOrderStatus.DELIVERED)
			throw new CommonRuntimeException("You must complete the order before you review it. !");
	}
}
