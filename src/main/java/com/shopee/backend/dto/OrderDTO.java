package com.shopee.backend.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.shopee.backend.dto.base.AbstractDTO;

import lombok.Data;

@Data
public class OrderDTO extends AbstractDTO {
	private Long idUser;
	private Long idAddress;
	private Long priceOff;
	private Long priceOrginal;
	private String couponCode;
	private Long priceFinal;
	private Integer paymentMethod;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date complete;

}
