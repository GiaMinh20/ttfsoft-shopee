package com.shopee.backend.service.impl;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.model.Product;
import com.shopee.backend.payload.response.BaseResponse;
import com.shopee.backend.repository.ProductRepository;
import com.shopee.backend.service.IAdminProductManagement;
import com.shopee.backend.utility.datatype.EProductStatus;

@Service
public class AdminProductManagement implements IAdminProductManagement {
	@Autowired
	ProductRepository productRepository;

	@Value("${product.notfound}")
	private String notfound;

	@Value("${app.success}")
	private String success;

	@Value("${product.showbanned}")
	private String showbanned;

	@Override
	public BaseResponse banProduct(Long[] ids) {
		List<Product> products = productRepository.findAllById(Arrays.asList(ids));
		if (products.isEmpty())
			throw new EntityNotFoundException(notfound);

		products.stream().forEach(p -> {
			if (p.getStatus() == EProductStatus.BANNED)
				p.setStatus(EProductStatus.HIDDEN);
			else
				p.setStatus(EProductStatus.BANNED);
		});
		productRepository.saveAll(products);
		BaseResponse res = new BaseResponse();
		res.setSuccess(true);
		res.setMessage(success);
		return res;
	}

	@Override
	public BaseResponse show_hide_Product(Long[] ids) {
		List<Product> products = productRepository.findAllById(Arrays.asList(ids));
		if (products.isEmpty())
			throw new EntityNotFoundException(notfound);
		products.stream().forEach(p -> {
			if (p.getStatus() == EProductStatus.HIDDEN)
				p.setStatus(EProductStatus.ACTIVE);
			else if (p.getStatus() != EProductStatus.BANNED)
				p.setStatus(EProductStatus.HIDDEN);
			else
				throw new CommonRuntimeException(showbanned);
		});
		productRepository.saveAll(products);
		BaseResponse res = new BaseResponse();
		res.setSuccess(true);
		res.setMessage(success);
		return res;

	}

	@Override
	public BaseResponse deleteProduct(Long[] ids) {
		BaseResponse res = new BaseResponse();
		List<Product> products = productRepository.findAllById(Arrays.asList(ids));
		if (products.isEmpty())
			throw new EntityNotFoundException(notfound);
		products.stream().forEach(p -> p.setStatus(EProductStatus.BANNED));
		productRepository.saveAll(products);
		res.setMessage(success);
		res.setSuccess(true);
		return res;

	}

}
