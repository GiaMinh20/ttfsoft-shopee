package com.shopee.backend.service;

import java.io.IOException;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.dto.ShopDTO;
import com.shopee.backend.model.Shop;
import com.shopee.backend.payload.request.auth.PasswordUpdateRequest;
import com.shopee.backend.payload.request.auth.ShopProfileCreateRequest;
import com.shopee.backend.payload.request.auth.ShopProfileUpdateRequest;
import com.shopee.backend.payload.response.DataResponse;
import com.shopee.backend.payload.response.ShopCustomCount;
import com.shopee.backend.service.impl.ShopService.SalesIndicator;

public interface IShopService {

	DataResponse<ShopDTO> getById(Long id);

	DataResponse<ShopDTO> create(ShopProfileCreateRequest request, MultipartFile avatar, MultipartFile coverImage)
			throws IOException;

	DataResponse<ShopDTO> update(Long id, ShopProfileUpdateRequest request, MultipartFile avatar,
			MultipartFile coverImage) throws IOException;

	DataResponse<ShopDTO> updatePassword(Long id, PasswordUpdateRequest request);

	ShopCustomCount getCustomCountProduct(Shop shop);

	DataResponse<SalesIndicator> getSales(Shop shop, Date dStart, Date dEnd, Long dLast);

}