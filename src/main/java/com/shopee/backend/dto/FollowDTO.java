package com.shopee.backend.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class FollowDTO {
	private Long idShop;
	private Date followSince;
	private String shopName;
}
