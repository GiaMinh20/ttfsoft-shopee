package com.shopee.backend.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shopee.backend.service.IShopCategoryService;

@RestController
@RequestMapping("/api/shopcategory")
public class CategoryController {
	@Autowired
	private IShopCategoryService shopCategoryService;
	
	@GetMapping
	public ResponseEntity<?> getShopCategories(
			@RequestParam Long idShop,
			@RequestParam(required = false) String searchName) {
		
		if (StringUtils.isNotBlank(searchName)) {
			return ResponseEntity.ok(shopCategoryService.findByShopAndName(idShop, searchName));
		}
		else {
			return ResponseEntity.ok(shopCategoryService.getAllByShop(idShop));
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getShopCategoryById(@PathVariable("id") long shopCategoryId) {
		return ResponseEntity.ok(shopCategoryService.getById(shopCategoryId));
	}
}
