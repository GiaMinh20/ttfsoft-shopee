package com.shopee.backend.model;

import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import com.shopee.backend.model.pk.FollowPK;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@Entity
@Table(name = "follow")
public class Follow {
	@EmbeddedId
	private FollowPK id;

	@MapsId("idBuyer")
	@ManyToOne
	@JoinColumn(name = "id_buyer", insertable = false, updatable = false)
	private Buyer buyer;

	@MapsId("idShop")
	@ManyToOne
	@JoinColumn(name = "id_shop", insertable = false, updatable = false)
	private Shop shop;

	private Date time;
	
	public Follow(FollowPK id, Buyer buyer, Shop shop, Date time) {
		super();
		this.id = id;
		this.buyer = buyer;
		this.shop = shop;
		this.time = time;
	}
}
