package com.shopee.backend.repository.custom.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.model.ProductClassification;
import com.shopee.backend.repository.custom.ProductClassificationRepositoryCustom;
import com.shopee.backend.utility.datatype.EProductClassificationStatus;
import com.shopee.backend.utility.datatype.EProductStatus;

@Repository
public class ProductClassificationRepositoryCustomImpl implements ProductClassificationRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Value("${productclassification.notEnoughQuantity}")
    private String notEnoughQuantity;

    @Value("${product.notAvailable}")
    private String pNotAvailable;

    @Value("${productclassification.notAvailable}")
    private String pcNotAvailable;

    @Transactional
    public ProductClassification descreaseQuantity(Long idProductClassification, Long quantity) {
        ProductClassification entity = em.find(ProductClassification.class, idProductClassification);
        Assert.notNull(entity, "Entitie must not be null!");
        entity = checkProductClassification(entity, quantity);
        
        entity.setQuantity(entity.getQuantity() - quantity);

        return em.merge(entity);
    }

    public ProductClassification checkProductClassification(ProductClassification entity, Long quantity) {
        if (entity.getQuantity() < quantity)
            throw new CommonRuntimeException(
                    String.format(notEnoughQuantity,
                            entity.getProduct().getName()));

        if (entity.getProduct().getStatus() != EProductStatus.ACTIVE)
            throw new CommonRuntimeException(
                    String.format(pNotAvailable,
                            entity.getProduct().getName()));

        if (entity.getStatus() != EProductClassificationStatus.ACTIVE)
            throw new CommonRuntimeException(
                    String.format(pcNotAvailable,
                            entity.getVariationName()));
        
        return entity;
    }
}
