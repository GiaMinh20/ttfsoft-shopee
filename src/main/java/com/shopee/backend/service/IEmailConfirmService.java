package com.shopee.backend.service;

import com.shopee.backend.payload.request.auth.EmailConfirmRequest;
import com.shopee.backend.payload.response.BaseResponse;

public interface IEmailConfirmService {

	BaseResponse sendVerifyCode(Long id, String role);

	BaseResponse confirmEmailConfirmToken(Long id, EmailConfirmRequest request, String role);

}