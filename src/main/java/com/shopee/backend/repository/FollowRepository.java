package com.shopee.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shopee.backend.model.Follow;
import com.shopee.backend.model.pk.FollowPK;
import com.shopee.backend.repository.custom.FollowRepositoryCustom;

@Repository
public interface FollowRepository extends JpaRepository<Follow, FollowPK>, FollowRepositoryCustom {
	
}
