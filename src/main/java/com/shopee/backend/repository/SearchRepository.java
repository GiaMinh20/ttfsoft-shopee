package com.shopee.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shopee.backend.model.Product;
import com.shopee.backend.repository.custom.SearchRepositoryCustom;

@Repository
public interface SearchRepository extends JpaRepository<Product, Long>, SearchRepositoryCustom {

}
