package com.shopee.backend.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
@Data
public class BuyerCouponDTO {
	private Long idBuyer;
	private Long idCoupon;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date time;
	private int isUsed;

}
