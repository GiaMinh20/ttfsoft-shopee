package com.shopee.backend.service;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.dto.ProductImageDTO;
import com.shopee.backend.payload.response.ListResponse;

public interface IProductImageService {
	
	ListResponse<ProductImageDTO> create(Long shopId, Long idProduct, List<MultipartFile> images) throws IOException;

	boolean delete(Long shopId, Long idProduct, List<Long> idProductImage);

}