package com.shopee.backend.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.shopee.backend.model.base.BaseModel;
import com.shopee.backend.utility.datatype.EProductStatus;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "product")
public class Product extends BaseModel {
	@OneToMany(mappedBy = "product")
	private List<FavoriteBuyer> favoriteBuyers;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_type", nullable = true)
	private ProductType productType;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_category", nullable = true)
	private ProductCategory productCategory;

	@OneToMany(mappedBy = "product")
	@OrderBy("price ASC")
	private List<ProductClassification> classifications;

	@OneToMany(mappedBy = "product")
	private List<ProductImage> images;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_shop")
	private Shop shop;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_shop_category")
	private ShopCategory shopCategory;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sell_place_id")
	private SellPlace sellPlace;

	@Lob
	@Column(length = 200)
	@NotBlank
	private String name;

	@NotNull
	private Long price;

	@NotNull
	@Column(name = "max_price")
	private Long maxPrice;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_avatar")
	private MediaResource avatar;

	@Lob
	@Column(length = 65535)
	@NotBlank
	private String description;

	@Min(value = 0)
	private Long sales;

	@Min(value = 0)
	private Long view;

	@Column(name = "discount_start")
	private Date discountStart;

	@Column(name = "discount_end")
	private Date discountEnd;

	@Column(name = "average_rating")
	@Min(value = 0)
	@Max(value = (long) 5.0)
	private Double averageRating;

	@Enumerated(EnumType.ORDINAL)
	@NotNull
	private EProductStatus status;

	@Column(name = "review_count")
	@Min(value = 0)
	private Long reviewCount;

	@Column(name = "is_new")
	private Boolean isNew;

	@Version
	@Column(columnDefinition = "bigint DEFAULT 0", nullable = false)
	private Long version = 0L;

	public Product(ProductType productType, ProductCategory productCategory, Shop shop, ShopCategory shopCategory,
			SellPlace sellPlace, @NotBlank String name, @NotBlank String description, Date discountStart,
			Date discountEnd, Boolean isNew, MediaResource avatar) {
		this.productType = productType;
		this.productCategory = productCategory;
		this.shop = shop;
		this.shopCategory = shopCategory;
		this.sellPlace = sellPlace;
		this.name = name;
		this.description = description;
		this.discountStart = discountStart;
		this.discountEnd = discountEnd;
		this.isNew = isNew;
		this.avatar = avatar;

		this.view = 0L;
		this.averageRating = 0.0;
		this.status = EProductStatus.HIDDEN;
		this.reviewCount = 0L;
		this.sales = 0L;
		this.price = 0L;
		this.maxPrice = 0L;
	}
}
