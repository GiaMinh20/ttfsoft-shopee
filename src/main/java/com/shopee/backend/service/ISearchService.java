package com.shopee.backend.service;

import com.shopee.backend.dto.ProductGeneralDTO;
import com.shopee.backend.payload.response.ListWithPagingResponse;
import com.shopee.backend.utility.Page;

public interface ISearchService {

	ListWithPagingResponse<ProductGeneralDTO> searchProduct(String keyword, Page page, String locations,
			Long idCategory, Long idType, Long maxMrice, Long minPrice, Double rate, Boolean isNew);

	Long countWithFilter(String keyword, Page page, String locations, Long idCategory, Long idType, Long maxMrice,
			Long minPrice, Double rate, Boolean isNew);

}
