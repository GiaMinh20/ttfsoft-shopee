package com.shopee.backend.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.dto.ProductImageDTO;
import com.shopee.backend.model.MediaResource;
import com.shopee.backend.model.Product;
import com.shopee.backend.model.ProductImage;
import com.shopee.backend.payload.response.ListResponse;
import com.shopee.backend.repository.ProductImageRepository;
import com.shopee.backend.repository.ProductRepository;
import com.shopee.backend.service.IMediaResourceService;
import com.shopee.backend.service.IProductImageService;
import com.shopee.backend.service.IProductService;
import com.shopee.backend.utility.PlatformPolicy;

@Service
public class ProductImageService implements IProductImageService {
	@Autowired
	ModelMapper modelMapper;
	
	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	ProductImageRepository productGalleryRepository;
	
	@Autowired
	IMediaResourceService mediaResourceService;
	
	@Autowired
	IProductService productService;
	
	@Transactional
	@Override
	public ListResponse<ProductImageDTO> create(Long idShop, Long idProduct, List<MultipartFile> images) throws IOException {
		Product p = productRepository.getReferenceById(idProduct);
		
		if(p.getShop().getId() != idShop) {
			throw new CommonRuntimeException("You can not edit product of other shop");
		}			
		
		if (productService.countProductImage(idProduct) + images.size() > PlatformPolicy.MAX_ALLOWED_PRODUCT_IMAGE) {
			throw new CommonRuntimeException(String.format("Maximum image(s) for a product is %d", PlatformPolicy.MAX_ALLOWED_PRODUCT_IMAGE));
		}
		
		List<ProductImage> list = new ArrayList<>();
		for (MultipartFile  image : images) {
			ProductImage pg = new ProductImage(
						productRepository.getReferenceById(idProduct), 
						mediaResourceService.save(image.getBytes())
					);
			list.add(productGalleryRepository.save(pg));
		}
		
		return listResponseMakeup(list);
	}
	
	@Override
	public boolean delete(Long idShop, Long idProduct, List<Long> idProductImages) {
		if (idProductImages.isEmpty()) {
			throw new CommonRuntimeException("At least one image is specified to be deleted");
		}
		
		List<ProductImage> l = new ArrayList<>();
		for (Long idProductImage : idProductImages) {
			ProductImage img = productGalleryRepository.getReferenceById(idProductImage);
			Product p = img.getProduct();
			
			if(p.getShop().getId() != idShop) {
				throw new CommonRuntimeException("You can not delete product of other shop");
			}
			
			if (!p.getId().equals(idProduct) ) {
				throw new CommonRuntimeException("Product doesn't have any image with id " + idProductImage.toString());
			}
			
			l.add(img);
		}
		return deleteWithEntity(l);
	}
	
	@Transactional
	private boolean deleteWithEntity(List<ProductImage> productImages) {
		if (productService.countProductImage(productImages.get(0).getProduct().getId()) - productImages.size() < PlatformPolicy.MIN_ALLOWED_PRODUCT_IMAGE) {
			throw new CommonRuntimeException(String.format("Product should have at least %d image(s)", PlatformPolicy.MIN_ALLOWED_PRODUCT_IMAGE));
		}
		
		for (ProductImage productImage : productImages) {
			MediaResource media = productImage.getMedia();
			productImage.setMedia(null);
			if (!mediaResourceService.delete(media.getId())) {
				productImage.setMedia(media);
				throw new CommonRuntimeException("Delete product image failed");
			}
			productGalleryRepository.delete(productImage);
		}
		return true;
	}
	
	private ListResponse<ProductImageDTO> listResponseMakeup(List<ProductImage> images) {
		return new ListResponse<>(
				images.stream().map(p -> modelMapper.map(p, ProductImageDTO.class)).toList()
			);
	}
}
