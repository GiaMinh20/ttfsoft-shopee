package com.shopee.backend.service;

import java.sql.SQLIntegrityConstraintViolationException;

import com.shopee.backend.dto.SellPlaceDTO;
import com.shopee.backend.payload.response.ResponseData;

public interface ISellPlace {

	void delete(Long[] ids);

	ResponseData<SellPlaceDTO> update(Long id, SellPlaceDTO input) throws SQLIntegrityConstraintViolationException;

	ResponseData<SellPlaceDTO> create(SellPlaceDTO input) throws SQLIntegrityConstraintViolationException;

	ResponseData<SellPlaceDTO> getPlaceByCode(String code);

	ResponseData<SellPlaceDTO> getAllPlaces();

}
