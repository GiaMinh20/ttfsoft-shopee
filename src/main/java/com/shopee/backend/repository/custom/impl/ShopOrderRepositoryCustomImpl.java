package com.shopee.backend.repository.custom.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.model.ShopOrder;
import com.shopee.backend.model.ShopOrder_;
import com.shopee.backend.repository.custom.ShopOrderRepositoryCustom;
import com.shopee.backend.utility.datatype.EOrderStatus;

@Repository
public class ShopOrderRepositoryCustomImpl implements ShopOrderRepositoryCustom {
    @PersistenceContext
    private EntityManager em;

    @Transactional
    @Override
    public Boolean cancel(Long id, Long idBuyer) {
        ShopOrder entity = em.find(ShopOrder.class, id, LockModeType.PESSIMISTIC_READ);
       
        if (entity == null || entity.getOrder().getBuyer().getId() != idBuyer)
            throw new CommonRuntimeException("Could not find shop order " + id);
            
        if (entity.getStatus() == EOrderStatus.CANCELLED)
            throw new CommonRuntimeException("Order is already canceled");

        if (entity.getStatus() == EOrderStatus.DELIVERED || entity.getStatus() == EOrderStatus.DELIVERING)
            throw new CommonRuntimeException("Action doesn't allowed, order is already delivered");

        if (entity.getStatus() == EOrderStatus.WAIT_FOR_SENDING
                || entity.getStatus() == EOrderStatus.WAIT_FOR_CONFIRMATION
                || entity.getStatus() == EOrderStatus.WAIT_FOR_PAYMENT) {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaUpdate<ShopOrder> cu = cb.createCriteriaUpdate(ShopOrder.class);
            Root<ShopOrder> root = cu.from(ShopOrder.class);

            cu.set(root.get(ShopOrder_.status), EOrderStatus.CANCELLED);
            cu.where(cb.equal(root.get(ShopOrder_.id), id));

            return em.createQuery(cu).executeUpdate() != 0;
        }
        
        return false;
    }
    
    @Override
	@Transactional(rollbackFor = Exception.class)
    public boolean confirmPayment(List<Long> idShopOrders) throws Exception {    	
    	List<ShopOrder> sos = getShopOrders(idShopOrders);    	
    	for (ShopOrder so : sos) {
    		if (so.getStatus() != EOrderStatus.WAIT_FOR_PAYMENT) {
    			return false;
    		}
    	}
    	
    	CriteriaBuilder cb = em.getCriteriaBuilder();
    	CriteriaUpdate<ShopOrder> cu = cb.createCriteriaUpdate(ShopOrder.class);
    	Root<ShopOrder> root2 = cu.from(ShopOrder.class);
    	cu.set(ShopOrder_.status, EOrderStatus.WAIT_FOR_CONFIRMATION).where(root2.get(ShopOrder_.id).in(idShopOrders));
    	
    	if (em.createQuery(cu).executeUpdate() != idShopOrders.size()) 
    		throw new Exception("Number of record update in database is not same as requested confirm payment");
    	else
    		return true;
    }
    
    private List<ShopOrder> getShopOrders(List<Long> idShopOrders) {
    	CriteriaBuilder cb = em.getCriteriaBuilder();
    	CriteriaQuery<ShopOrder> cq = cb.createQuery(ShopOrder.class);
    	Root<ShopOrder> root = cq.from(ShopOrder.class);
    	cq.select(root).where(root.get(ShopOrder_.id).in(idShopOrders));
    	return em.createQuery(cq).setLockMode(LockModeType.PESSIMISTIC_WRITE).getResultList(); //.setHint("javax.persistence.query.timeout", 2000)
    }

}
