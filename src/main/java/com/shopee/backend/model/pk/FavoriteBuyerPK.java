package com.shopee.backend.model.pk;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class FavoriteBuyerPK implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5467421588915178002L;

	@Column(name="id_buyer")
	private Long idBuyer;

	@Column(name="id_product")
	private Long idProduct;
}
