package com.shopee.backend.service.impl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.shopee.backend.controller.exception.CommonRuntimeException;
import com.shopee.backend.model.Buyer;
import com.shopee.backend.model.Shop;
import com.shopee.backend.payload.request.auth.PasswordResetRequest;
import com.shopee.backend.payload.request.auth.SendVerifyTokenRequest;
import com.shopee.backend.payload.response.BaseResponse;
import com.shopee.backend.repository.BuyerRepository;
import com.shopee.backend.repository.ShopRepository;
import com.shopee.backend.service.IPasswordResetService;
import com.shopee.backend.utility.RandomString;
import com.shopee.backend.utility.RolePrefix;
import com.shopee.backend.utility.jwt.JwtUtils;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Service
public class PasswordResetService implements IPasswordResetService {
	@Autowired
	BuyerRepository buyerRepository;

	@Autowired
	ShopRepository shopRepository;

	@Autowired
	JavaMailSender javaMailSender;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	private JavaMailSender sender;

	@Autowired
	private Configuration config;

	@Value("${spring.mail.username}")
	private String shopeeEmail;
	
	@Value("${email.content.resetpassword}")
	private String content;
	
	private static final String TOKEN_FORMAT = "%s@password";

	@Override
	public BaseResponse sendVerifyCode(String role, SendVerifyTokenRequest request) {
		String code = RandomString.get(10);
		String name = "";

		if (role.equals(RolePrefix.SHOP)) {
			Optional<Shop> oshop = shopRepository.findByEmail(request.getEmail());
			if (oshop.isEmpty()) {
				throw new CommonRuntimeException("No shop user is associated with this email");
			}
			Shop shop = oshop.get();
			name = shop.getName();
			shop.setVerificationToken(String.format(TOKEN_FORMAT, code));
			shopRepository.save(shop);
		} else if (role.equals(RolePrefix.BUYER)) {
			Optional<Buyer> obuyer = buyerRepository.findByEmail(request.getEmail());
			if (obuyer.isEmpty()) {
				throw new CommonRuntimeException("No buyer user is associated with this email");
			}
			Buyer buyer = obuyer.get();
			name = buyer.getName();
			buyer.setVerificationToken(String.format(TOKEN_FORMAT, code));
			buyerRepository.save(buyer);
		}
		MimeMessage message = sender.createMimeMessage();
		try {
			// set mediaType
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
					StandardCharsets.UTF_8.name());
			// add attachment

			Template t = config.getTemplate("email-template.ftl");
			Map<String, Object> model = new HashMap<>();
			model.put("content", content);
			model.put("name", name);
			model.put("email", request.getEmail());
			model.put("code", code);
			String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
			//                    

			helper.setTo(request.getEmail());
			helper.setText(html, true);
			helper.setSubject("[TTFSOFT-SHOPEE] Reset password code");
			helper.setFrom(shopeeEmail);
			sender.send(message);

		} catch (MessagingException | IOException | TemplateException e) {
			throw new CommonRuntimeException(String.format("Send verify code failed (%s)", e.getMessage()));
		}

		return new BaseResponse();
	}

	@Override
	public BaseResponse reset(String role, PasswordResetRequest request) {
		String code = String.format(TOKEN_FORMAT, request.getResetCode());

		if (role.equals(RolePrefix.SHOP)) {
			Shop shop = shopRepository.findByEmail(request.getEmail()).get();
			if (shop.getVerificationToken().equals(code)) {
				shop.setVerificationToken(null);
				shop.setPassword(passwordEncoder.encode(request.getNewPassword()));
				shopRepository.save(shop);
			} else
				return new BaseResponse(false, "Invalid password reset code");
		} else if (role.equals(RolePrefix.BUYER)) {
			Buyer buyer = buyerRepository.findByEmail(request.getEmail()).get();
			if (buyer.getVerificationToken().equals(code)) {
				buyer.setVerificationToken(null);
				buyer.setPassword(passwordEncoder.encode(request.getNewPassword()));
				buyerRepository.save(buyer);
			} else
				return new BaseResponse(false, "Invalid password reset code");
		}

		return new BaseResponse();
	}
}
