package com.shopee.backend.service;

import com.shopee.backend.dto.BuyerDTO;
import com.shopee.backend.dto.ShopDTO;
import com.shopee.backend.payload.response.BaseResponse;

public interface IAdminAccountManagement {

	BaseResponse unlockShopAccount(Long idShop, Boolean unlock);

	BaseResponse unlockBuyerAccount(Long idBuyer, Boolean unlock);

	BaseResponse updateShopPassword(Long idShop, String newPassword);

	BaseResponse updateBuyerPassword(Long idBuyer, String newPassword);

	ShopDTO getShopByEmail(String email);

	BuyerDTO getBuyerByEmail(String email);

}
