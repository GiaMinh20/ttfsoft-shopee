package com.shopee.backend.controller.admin;

import java.io.IOException;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.shopee.backend.payload.request.productcategory.CreateProductCategoryRequest;
import com.shopee.backend.payload.request.productcategory.EditProductCategoryRequest;
import com.shopee.backend.payload.response.BaseResponse;
import com.shopee.backend.service.IProductCategoryService;

@RestController
@RequestMapping("/api/admin/productcategory")
public class AdminProductCategoryController {
	@Autowired
	private IProductCategoryService productCategoryService;

	@PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> createProductCategory(
			@RequestPart(name = "info") CreateProductCategoryRequest request,
			@RequestPart(name = "image") @Valid @NotEmpty MultipartFile image) throws IOException {
		
		return ResponseEntity.ok(productCategoryService.create(request, image));
	}

	@PutMapping(value = "/{id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> editProductCategory(
			@PathVariable("id") long id,
			@RequestPart(name = "info") EditProductCategoryRequest request,
			@RequestPart(required = false) MultipartFile image) throws IOException {
		
		return ResponseEntity.ok(productCategoryService.update(id, request, image));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteProductCategory(@PathVariable("id") long id) {
		if (!productCategoryService.delete(id))
			return ResponseEntity.ok(new BaseResponse(false, "Delete product category failed"));
		else
			return ResponseEntity.ok(new BaseResponse());
	}
}
