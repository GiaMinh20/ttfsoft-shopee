package com.shopee.backend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.shopee.backend.model.Order;

public interface OrderRepository extends JpaRepository<Order, Long>{
	@Query(value = "SELECT * FROM orders where id_buyer = :id_buyer", nativeQuery = true)
	public List<Order> getOrdersByBuyer(@Param("id_buyer") Long id);

}
