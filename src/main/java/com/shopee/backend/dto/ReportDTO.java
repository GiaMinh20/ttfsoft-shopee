package com.shopee.backend.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.shopee.backend.dto.base.AbstractDTO;

import lombok.Data;
@Data
public class ReportDTO extends AbstractDTO{
	private Long idOrder;
	private Long idProduct;
	private String content;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date time;
}
