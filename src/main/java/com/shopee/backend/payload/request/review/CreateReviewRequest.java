package com.shopee.backend.payload.request.review;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateReviewRequest {
	@NotNull
	private Long idOrder;
	@NotNull
	private Long idShopOrder;
	@NotNull
	private Long idProductClassification;
	@NotBlank
	@Size(min = 2)
	private String content;
	@NotNull
	@Min(value = 1)
	@Max(value = 5)
	private Integer rating;
}
