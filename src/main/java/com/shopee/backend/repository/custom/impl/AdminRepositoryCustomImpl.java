package com.shopee.backend.repository.custom.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.shopee.backend.dto.holder.AdminDataHolder;
import com.shopee.backend.dto.holder.DataHolder;
import com.shopee.backend.model.Admin;
import com.shopee.backend.model.Admin_;
import com.shopee.backend.model.CouponCode;
import com.shopee.backend.model.CouponCode_;
import com.shopee.backend.model.OrderDetail;
import com.shopee.backend.model.OrderDetail_;
import com.shopee.backend.model.ShopOrder;
import com.shopee.backend.model.ShopOrder_;
import com.shopee.backend.payload.response.statistic.StatisticResponse;
import com.shopee.backend.repository.custom.AdminRepositoryCustom;
import com.shopee.backend.utility.datatype.EOrderStatus;

@Repository
public class AdminRepositoryCustomImpl implements AdminRepositoryCustom {
	@PersistenceContext
	private EntityManager em;

	private static final String SALETOTALSHOP = "sale_total_shop";
	private static final String SALETOTALSHOPEE = "sale_total_shopee";
	private static final String TOTALSHIPAMOUNT = "total_ship_amount";
	private static final String TOTALSHIPPRICEOFF = "ship_price_off";
	private static final String TOTALPLATFORMPRICEOFF = "total_platform_price_off";

	@Transactional
	@Override
	public boolean setPassword(String email, String hashedPassword) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaUpdate<Admin> cu = cb.createCriteriaUpdate(Admin.class);
		Root<Admin> root = cu.from(Admin.class);

		cu.set(root.get(Admin_.password), hashedPassword);
		cu.where(cb.equal(root.get(Admin_.email), email));
		return em.createQuery(cu).executeUpdate() != 0;
	}

	@Override
	public Long getIdByEmail(String email) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<Admin> root = cq.from(Admin.class);

		cq.select(root.get(Admin_.id)).where(cb.equal(root.get(Admin_.email), email));
		try {
			return em.createQuery(cq).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public StatisticResponse customStatistic(Date dStart, Date dEnd, Long dLast) {
		StatisticResponse result = new StatisticResponse();
		CriteriaBuilder cb = em.getCriteriaBuilder();

		// Working voucher
		CriteriaQuery<Long> cqWorkingVoucher = cb.createQuery(Long.class);
		Root<CouponCode> rootCoupon = cqWorkingVoucher.from(CouponCode.class);

		List<Predicate> filters = new ArrayList<>();
		filters.add(cb.greaterThanOrEqualTo(rootCoupon.get(CouponCode_.dod), new Date()));
		Predicate predicate = cb.and(filters.toArray(new Predicate[0]));
		cqWorkingVoucher.select(cb.count(rootCoupon)).where(predicate);
		Long workingVoucher = em.createQuery(cqWorkingVoucher).getSingleResult();
		result.setWorkingVoucher(workingVoucher);
		// Count order by Status
		CriteriaQuery<DataHolder> cq = cb.createQuery(DataHolder.class);
		Root<ShopOrder> root = cq.from(ShopOrder.class);
		filters = new ArrayList<>();

		if (dLast != null) {
			dEnd = new Date();
			dStart = new Date(dEnd.getTime() - dLast * 24 * 60 * 60 * 1000L);
		}
		if (dStart != null && dEnd != null) {
			filters.add(cb.between(root.get(ShopOrder_.createdDate), dStart, dEnd));
		}
		filters.add(cb.notEqual(root.get(ShopOrder_.status), EOrderStatus.CANCELLED));
		predicate = cb.and(filters.toArray(new Predicate[0]));
		cq.multiselect(root.get(ShopOrder_.status), cb.count(root)).where(predicate)
				.groupBy(root.get(ShopOrder_.status));
		List<DataHolder> countByStatus = em.createQuery(cq).getResultList();
		result.setDataHolder(countByStatus);
		// Sale Amount, total. ship
		CriteriaQuery<Tuple> cqAdminHolder = cb.createQuery(Tuple.class);
		Root<ShopOrder> rootAdminHolder = cqAdminHolder.from(ShopOrder.class);
		filters = new ArrayList<>();

		if (dLast != null) {
			dEnd = new Date();
			dStart = new Date(dEnd.getTime() - dLast * 24 * 60 * 60 * 1000L);
		}
		if (dStart != null && dEnd != null) {
			filters.add(cb.between(rootAdminHolder.get(ShopOrder_.createdDate), dStart, dEnd));
		}
		filters.add(cb.notEqual(rootAdminHolder.get(ShopOrder_.status), EOrderStatus.CANCELLED));

		predicate = cb.and(filters.toArray(new Predicate[0]));
		cqAdminHolder
				.multiselect(cb.sum(rootAdminHolder.get(ShopOrder_.totalPrice)).alias(SALETOTALSHOP),
						cb.sum(rootAdminHolder.get(ShopOrder_.totalPayPrice)).alias(SALETOTALSHOPEE),
						cb.sum(rootAdminHolder.get(ShopOrder_.shipPrice)).alias(TOTALSHIPAMOUNT),
						cb.sum(rootAdminHolder.get(ShopOrder_.shipPriceOff)).alias(TOTALSHIPPRICEOFF),
						cb.sum(rootAdminHolder.get(ShopOrder_.platformPriceOff)).alias(TOTALPLATFORMPRICEOFF))
				.where(predicate);
		Tuple resultList = em.createQuery(cqAdminHolder).getSingleResult();

		AdminDataHolder adminDataHolder = new AdminDataHolder();
		adminDataHolder.setSaleTotalShop(
				resultList.get(SALETOTALSHOP, Long.class) == null ? 0L : resultList.get(SALETOTALSHOP, Long.class));
		adminDataHolder.setSaleTotalShopee(
				resultList.get(SALETOTALSHOPEE, Long.class) == null ? 0L : resultList.get(SALETOTALSHOPEE, Long.class));
		adminDataHolder.setTotalShipAmount(
				resultList.get(TOTALSHIPAMOUNT, Long.class) == null ? 0L : resultList.get(TOTALSHIPAMOUNT, Long.class));
		adminDataHolder.setShipPriceOff(resultList.get(TOTALSHIPPRICEOFF, Long.class) == null ? 0L
				: resultList.get(TOTALSHIPPRICEOFF, Long.class));
		adminDataHolder.setTotalPlatFormPriceOff(resultList.get(TOTALPLATFORMPRICEOFF, Long.class) == null ? 0L
				: resultList.get(TOTALPLATFORMPRICEOFF, Long.class));

		adminDataHolder.setRevenue();

		result.setAdminDataHolder(adminDataHolder);
		result.setSaleAmount(sales(dStart, dEnd, dLast));
		result.setSuccess(true);
		return result;
	}

	public Long sales(Date dStart, Date dEnd, Long dLast) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<ShopOrder> root = cq.from(ShopOrder.class);
		Join<ShopOrder, OrderDetail> join = root.join(ShopOrder_.orderDetails, JoinType.LEFT);
		List<Predicate> filters = new ArrayList<>();
		filters.add(cb.notEqual(root.get(ShopOrder_.status), EOrderStatus.CANCELLED));
		if (dLast != null) {
			dEnd = new Date(System.currentTimeMillis());
			dStart = new Date(dEnd.getTime() - dLast * 24 * 3600 * 1000L);
		}

		if (dStart != null && dEnd != null) {
			filters.add(cb.between(root.get(ShopOrder_.createdDate), dStart, dEnd));
		}

		Predicate predicate = cb.and(filters.toArray(new Predicate[0]));
		cq.select(cb.sum(join.get(OrderDetail_.quantity))).where(predicate);
		Long res = em.createQuery(cq).getSingleResult();
		if (res == null)
			res = 0L;
		return res;
	}
}
